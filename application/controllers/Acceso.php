<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Acceso extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','email','security'));
		$this->load->model(array('Thememodel','Emailmodel'));
	    $this->load->library('form_validation');
	    $this->load->helper('cookie');
	   	}

	public function index(){
		if( !empty( $this->session->userdata('type'))){
			redirect(base_url().'cuenta');
		}
		$data = $this->load_data();
		cargar_template('template/acceso', $data);
	}

/*
	public function registro(){
		$data = $this->load_data();
		cargar_template('template/acceso-registro', $data);
	}
*/
	function registrado(){
		$data = $this->load_data();
		$this->load->library('form_validation');
		$pass = $this->input->post('pass');
		$this->form_validation->set_rules('name','nombre','trim|required');
		$this->form_validation->set_rules('email','email','trim|required|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('pass','contraseña','trim|required');
		if($this->form_validation->run() != FALSE){
			$datos['email'] = $this->input->post('email');
			$datos['pass'] = md5( $this->input->post('pass') );
			$datos['name'] = $this->input->post('name');
			$datos['type'] = 'user';
			$datos['status'] = 'activo';
			$datos['dateregistered'] = date('Y-m-d H:i:s');
			$email_repetido = $this->Thememodel->get_all_values_where('users',array('email' => $datos['email']));
			if(	! empty( $email_repetido ) ){
				$data['error'] = 'Este email pertenece a un usuario ya registrado. Si es suyo pruebe a recuperar su contraseña, si tiene problemas para acceder contacte con nosotros';
				cargar_template('template/acceso-registro', $data);
				return;
			}
			// añadimos usuario
			if( $this->Thememodel->insert_value('users',$datos) ){
				//$this->Emailmodel->send_email_bienvenida( $this->Thememodel->get_value('user',array('email' => $datos['email'] ) ));
				$this->login( 'Bienvenido, no olvide rellenar sus datos en su perfil' );
			}
		}else{
			$data['error'] = validation_errors();
		}
		cargar_template('template/acceso-registro', $data);
	}


	
	function entrar(){
		$this->view_alerts();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email','email','trim|required|strip_tags');
		$this->form_validation->set_rules('pass','contraseña','trim|required|strip_tags|md5');
		if($this->form_validation->run() == FALSE){
			$this->session->set_userdata('error','Por favor introduzca un usuario y contraseña correctos');
			redirect(base_url().'acceso');
		}else{
			$email = $this->input->post('email');
			$password = $this->input->post('pass');
			$usuario = $this->Thememodel->get_value('users',array('email' => $email));
		   

			if(empty($usuario)){
				$usuario = $this->Thememodel->get_value('users',array('n_identificacion' => $email));
			}

			if( !empty($usuario) && ($usuario->pass == $password )){
                 if($usuario->logged_in == 1){
               	    $status = $this->verificarSession($usuario->id)['status'];
					$id_session = $this->verificarSession($usuario->id)['id_session'];
					if($status){ 
                    
					  $this->cerrarSessionUser($usuario->id, $id_session);
                      $this->session->set_userdata('infoSession', "Mantiene una sesión activa, por su seguridad procederemos a cerrarla");
				    }

               }
				if($usuario->status != 'activo'){
				  
					$this->session->set_userdata('error','Su usuario no está activado contacte con el adminitrador');

					redirect(base_url().'acceso');

				}
				$roles = $this->Thememodel->get_select('id_rol','users',array('id'=>$usuario->id));
				$rol = $roles[0];
				if($rol){
					$permisos = $this->Thememodel->get_select('permisos','roles',array('id'=>$rol->id_rol));
					$permisos = json_decode($permisos[0]->permisos);
					
					$roles_actuales = array();
					foreach($permisos as $key){
						$roles_actuales[] = $key->id;
					}
				}
				$this->session->set_userdata('roles', $roles_actuales );
				$this->session->set_userdata('user_id', $usuario->id);
				$this->session->set_userdata('email', $usuario->email);
				$this->session->set_userdata('nicename', $usuario->name );
				$this->session->set_userdata('image', $usuario->image );
				$this->Thememodel->save_value('users',array('datelastlogin' => date('Y-m-d H:i:s' ), 'logged_in' => 1),array('id' =>$usuario->id));

				$cookie_name =  'cookie_session';
                $cookie_value = 'cookie_session_empty';
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), '/'); // 86400 = 1 day
                
				$id = $this->session->userdata('user_id');
		        if($id != null){
                   $join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
                       $where = array('prestamos_estados.id' => 9,'users.id' => $id);

		             $data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('id_prestamo','prestamos',$join,$where,'id_prestamo','ASC',10,0);

		             if(!empty($data['prestamo'])){
			             $dataPrestamo = array('prestamo_estado_id' => 8);
                     $this->Thememodel->update_value('prestamos',$dataPrestamo,array('id_prestamo' => $data['prestamo'][0]->id_prestamo));
		         }
		     }
		    
		     if($usuario->nicename != ''){
					$this->session->set_userdata('info', "Hola $usuario->nicename. Bienvenido !!");
					
				}else{
                   

					$this->session->set_userdata('info', "Hola $usuario->name. Bienvenido !!" );
					
				}
				if(check_rol_view($this->session->userdata('roles'), array(0))) {
					/*if(!empty($this->session->userdata('info1'))){
					   $this->session->set_userdata('info', $this->session->userdata('info1'));	
					}*/
					redirect(base_url().'cuenta');
					
				
				}else {
                   redirect(base_url().'dashboard');
				}
			}else{
				$this->session->set_userdata('error','Por favor introduzca un usuario y contraseña válidos');
				redirect(base_url().'acceso');
			}
		}
	}

	function entrar_clicredit_ajax(){
		/*
		Se permite loguear con el n_identificación, pero éste debe ser único
		*/
		$this->view_alerts();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email','email','trim|required|strip_tags');
		$this->form_validation->set_rules('pass','contraseña','trim|required|strip_tags|md5');
		if($this->form_validation->run() == FALSE){
			echo 'Por favor introduzca un usuario y contraseña correctos';
		}else{
			$email = $this->input->post('email');
			$password = $this->input->post('pass');
			$usuario = $this->Thememodel->get_value('users',array('email' => $email));

			if(empty($usuario)){
				$usuario = $this->Thememodel->get_value('users',array('n_identificacion' => $email));
			}

			if( !empty($usuario) && ($usuario->pass == $password )){
               if($usuario->logged_in == 1){
               	$status = $this->verificarSession($usuario->id)['status'];
			    $id_session = $this->verificarSession($usuario->id)['id_session'];
					if($status){ 
					  $this->cerrarSessionUser($usuario->id, $id_session);
					   $this->session->set_userdata('infoSession', "Mantiene una sesión activa, por su seguridad procederemos a cerrarla");
				    }
               }
				if($usuario->status != 'activo'){
					  $id_session = $this->session->session_id;
				    if(!empty($id_session)){
				    	 $this->cerrarSessionUser($usuario->id, $id_session);
				    }

					echo 'Su usuario no está activado contacte con el adminitrador';
					 return;
				}
				$roles = $this->Thememodel->get_select('id_rol','users',array('id'=>$usuario->id));
				$rol = $roles[0];
				if($rol){
					$permisos = $this->Thememodel->get_select('permisos','roles',array('id'=>$rol->id_rol));
					$permisos = json_decode($permisos[0]->permisos);
					
					$roles_actuales = array();
					foreach($permisos as $key){
						$roles_actuales[] = $key->id;
					}
				}
				$this->session->set_userdata('roles', $roles_actuales );
				$this->session->set_userdata('user_id', $usuario->id);
				$this->session->set_userdata('email', $usuario->email);
				$this->session->set_userdata('nicename', $usuario->name );
				$this->session->set_userdata('image', $usuario->image );

				$cookie_name =  'cookie_session';
                $cookie_value = 'cookie_session_empty';
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), '/'); 
				$this->Thememodel->save_value('users',array('datelastlogin' => date('Y-m-d H:i:s' ), 'logged_in' => 1),array('id' =>$usuario->id));
			     $id = $this->session->userdata('user_id');
		         if($id != null){
		           //verifica si el usuario tiene algún prestamo incompleto para pasarlo al estado en proceso luego del inicio de sesión y redireccionarlo al paso donde quedó
                   $join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
                       $where = array('prestamos_estados.id' => 9,'users.id' => $id);

		             $data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('id_prestamo','prestamos',$join,$where,'id_prestamo','ASC',10,0);
                    
                    $prestamo_actual = $this->Thememodel->get_query("SELECT id_prestamo from `prestamos` WHERE id_cliente =   $id AND prestamo_actual = 1");

		             if(!empty($data['prestamo'])){
			             $dataPrestamo = array('prestamo_estado_id' => 8);
                     $this->Thememodel->update_value('prestamos',$dataPrestamo,array('id_prestamo' => $data['prestamo'][0]->id_prestamo));
                      echo 1;
		         }else if(empty($prestamo_actual)){
		         	echo 1;
		         }else{
		           echo 2;
		     }
		         }
		    
		    
		 }


		 else{
				echo 'Por favor introduzca un usuario y contraseña correctos';
			}
		}
	}
//Verifica por el email si el usuario ya está registrado o no
function exist_user_by_email() {
	$email = $this->input->post('email');
	$user = $this->Thememodel->get_value('users',array('email' => $email));
	echo json_encode(array("status" => !!$user));
}

//muestra el mensaje de la sesión vieja del navegador y elimina la cookie de la sesión
public function removeSessionBrowser(){

	$id_session = $this->session->session_id;
	$data_session = $this->Thememodel->get_query("SELECT id,data FROM sessions WHERE id = '$id_session'");
	if(empty($data_session[0]->data) && isset($_COOKIE['cookie_session'])){
		unset($_COOKIE['cookie_session']);
		setcookie('cookie_session', '', time() + (86400 * 30), '/'); // 86400 = 1 day
		echo json_encode(array("status" => true));
     }else{
		echo json_encode(array("status" => false)); 
	}
}
//muestra el mensaje en caso de tener otra sesión abierta en el navegador
public function showMessageSession(){
	if(!empty($this->session->userdata('infoSession'))){
		$msg = $this->session->userdata('infoSession');
		echo json_encode(array("status" => true, 'msg'=>$msg)); 
	}else{
		echo json_encode(array("status" => false)); 
	}
}

// elimina el mensaje que indica si tiene una sesión abierta en el navegador 
public function quitMessageSession(){
	 $this->session->unset_userdata('infoSession');
	 echo json_encode(array("status" => true));
}


//verifica en la tabla sesions a través de la columna data si el usuario tiene una sesion activa
public function verificarSession($id_user){
   
 $users = $this->Thememodel->get_query("SELECT id,data from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))<1800 order by data DESC");
       $return_data = array();
       $listUsers = array();
     foreach ($users as $key => $value){
         $session_data = $value->data;
        
         if(!empty($session_data)){
        
         $offset = 0;
      while ($offset < strlen($session_data)) {
        if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
  
      $return_data['id'] = $value->id;
      $listUsers[$key] = $return_data;
       
    }
    
}
   
     $users_online =  $listUsers;
   
     foreach ($users_online as $key => $user) {
     
      if($user["user_id"]==$id_user){
        return [
        "id_session" => $user['id'],
        "status" => true
      ];
     }
    }

 }



	public function token()
	{
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return  $token;
	}

	public function salir(){
		$id = $this->session->userdata('user_id');
		if($id != null){
        $join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
         $where = array('prestamos_estados.id' => 8,'users.id' => $id);

		$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('id_prestamo','prestamos',$join,$where,'id_prestamo','ASC',10,0);

		if(!empty($data['prestamo'])){
			$dataPrestamo = array('prestamo_estado_id' => 9);

			$this->Thememodel->update_value('prestamos',$dataPrestamo,array('id_prestamo' => $data['prestamo'][0]->id_prestamo));
		}
		
		
		}
		$this->Thememodel->save_value('users',array('logged_in' => 0),array('id' =>$id));
		unset($_COOKIE['cookie_session']);
		setcookie('cookie_session', '', time() + (86400 * 30), '/'); // 86400 = 1 day
		$this->session->sess_destroy();
		redirect(base_url());

	}


  






   //función encargada de cerrar la sesión del usuario en caso de tenerla activa en otro dispositivo o navegador
	public function cerrarSessionUser($user_id, $id_session){
		$id = $user_id;
		if($id != null){
        $join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
         $where = array('prestamos_estados.id' => 8,'users.id' => $id);

		$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('id_prestamo','prestamos',$join,$where,'id_prestamo','ASC',10,0);

		if(!empty($data['prestamo'])){
			$dataPrestamo = array('prestamo_estado_id' => 9);

			$this->Thememodel->update_value('prestamos',$dataPrestamo,array('id_prestamo' => $data['prestamo'][0]->id_prestamo));
		}
		
		
		}
		$this->Thememodel->save_value('users',array('logged_in' => 0),array('id' =>$id));
		$this->Thememodel->del_value('sessions',array('id' => $id_session));

		
	}

	public function recuperar_cuenta(){
		$data = $this->load_data();
		cargar_template('template/acceso-recuperar-pass', $data);
	}

	public function enviarEmailRecuperacion(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email','email','trim|required|valid_email|strip_tags');
		$this->form_validation->set_rules('digito','campo digito','trim|required|strip_tags');
		if($this->form_validation->run() == FALSE){
			$this->session->set_userdata('error',validation_errors());
			redirect(base_url().'acceso');
		}else{

			$digito = $this->input->post('digito');
			$email = $this->input->post('email');

			if( $digito == date('d')+date('m') ){
				$datosUsuario = $this->Thememodel->get_value('users', array('email' => $email ));

				if( empty( $datosUsuario )){
					$this->session->set_userdata( 'info' , 'Este email no existe en nuestra base de datos. Por favor regístrese');
					redirect(base_url().'acceso');
				}
				//Generamos el token, asignamos el token y el tiempo de expiración al usuario que tiene asociado el email
				$token = $this->token();
				$this->Thememodel->save_value('users',array('token' => $token, ' token_expiration_TS' => date('Y-m-d H:i:s', strtotime("+60 min"))),array('email' => $email));
                
                //enviamos el correo
                $this->Emailmodel->email_recovery_password($datosUsuario,$token);


               $this->session->set_userdata('info', 'Se le ha enviado un email para restablecer su contraseña, revise su bandeja de entrada');
				redirect(base_url().'acceso');
			}else{
				$this->session->set_userdata('error','El digito de control no es correcto');
				redirect(base_url().'acceso/recuperar-cuenta');
			}
		}
	}


 private function checkIsLiveToken($token)
 {  

    $current_stamp = date('Y-m-d H:i:s');
    $datosUsuario = $this->Thememodel->get_value('users', array('token' => $token ));
    if(!empty($datosUsuario)){
    if($datosUsuario->token_expiration_TS > $current_stamp){
    	return $datosUsuario;
    }
    else{
    	return FALSE;
    }
   }else{
   	 return FALSE;
   }
 }




public function recovery_password($token = "")
 {
       //si el password ha caducado
       if($this->checkIsLiveToken($token) === FALSE)
       {
         $this->session->set_userdata(
         "info", "Si necesita recuperar su contraseña rellene el 
          formulario con su email y le haremos llegar un correo con las instrucciones"
        );
         redirect(base_url("acceso/recuperar-cuenta"),"refresh");
       }
       else{
           $data["token"] = $token;
       	   cargar_template('template/acceso-cambiar-pass', $data);
       	}
 }


 public function update_password(){
		
		$data = $this->load_data();
		$token = $this->input->post('token');

		$this->form_validation->set_rules('password','contraseña','trim|required');
		$this->form_validation->set_rules('password2','repetir contraseña','trim|required|matches[password]');

		if($this->form_validation->run() == FALSE){
			$data['error'] = validation_errors();
			$data["token"] = $token;
       	    cargar_template('template/acceso-cambiar-pass', $data);
		}else{
			$this->Thememodel->save_value('users',array('pass' => md5($this->input->post('password')), 'token' => $this->token()),array('token' => $token ) );
			$this->session->set_userdata('info', 'Contraseña guardada correctametne');
			redirect(base_url());
		}
	}





	private function view_alerts(){
		$data = '';
		if($this->session->userdata('info')){
			$data['info'] = $this->session->userdata('info');
			$this->session->unset_userdata('info');
		}
		if($this->session->userdata('error')){
			$data['error'] = $this->session->userdata('error');
			$this->session->unset_userdata('error');
		}
		$data['messages'] = $this->Thememodel->get_all_values('messages');
		return $data;
	}
// aux de todas las páginas
	private function load_data(){
		include 'Themeoptions.php';
		return $data;
	}


}
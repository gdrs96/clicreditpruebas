<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bienvenida extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','security'));
		$this->load->model(array('Thememodel'));
		$this->load->library('form_validation');
	}

	public function index(){
		//$this->output->cache(36);
		$data = $this->load_data();
		$id = $this->session->userdata('user_id');
		if($id != null){
			$user = $this->Thememodel->get_value('users',array('id' => $id));
			$calificacion = $user->id_calificacion;
      $join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
      $where = array('prestamos_estados.id' => 8,'users.id' => $id);
			$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('cantidad, tiempo, ajustar_quincena','prestamos',$join,$where,'id_prestamo','ASC',10,0);  
			if(!empty($data['prestamo'])){
				$data['cantidad'] = (int)str_replace(' ', '',$data['prestamo'][0]->cantidad );
				$data['tiempo'] = (int)$data['prestamo'][0]->tiempo;
				$data['ajustar_quincena'] = $data['prestamo'][0]->ajustar_quincena;
			}
		} else {
			$calificacion = '1';
		}
		$data['calificacion'] = $this->Thememodel->get_value('calificacion',array('id' => $calificacion));
		$banner_image = $this->Thememodel->get_value('options',array('option_name' => 'banner_image'));
		$banner_image_float = $this->Thememodel->get_value('options',array('option_name' => 'banner_image_float'));
		$show_image_float = $this->Thememodel->get_value('options',array('option_name' => 'show_image_float'));
		$data['banner_image'] = $banner_image->option_value;
		$data['banner_image_float'] = $banner_image_float->option_value;
		$data['show_image_float'] = ($show_image_float->option_value) === 'true' ? true : false;
		cargar_template('template/home', $data);
	}

	private function load_data(){
		//$web = $this->Thememodel->get_all_values('themeoption');
		//$content = $this->Thememodel->get_all_values('post');
		$postmetas = $this->Thememodel->get_all_values('posts_metas');
		//$Themeoptions = $this->Thememodel->get_all_values('themeoption');

/*		$content = $this->Thememodel->get_all_values_where('post',array('type'=>'Título','type'=>'Quote','type'=>'Página','type'=>'Servicio','type'=>'Footer','type'=>'Portfolio'));
		$content = $this->Thememodel->get_all_values_where('post',array('type'=>'Título','type'=>'Quote','type'=>'Página','type'=>'Servicio','type'=>'Footer','type'=>'Portfolio'));
		$content = $this->Thememodel->get_all_values_where('post',array('type'=>'Título','type'=>'Quote','type'=>'Página','type'=>'Servicio','type'=>'Footer','type'=>'Portfolio'));
		$content = $this->Thememodel->get_all_values_where('post',array('type'=>'Título','type'=>'Quote','type'=>'Página','type'=>'Servicio','type'=>'Footer','type'=>'Portfolio'));
*/
		//$data['titulo'] = array();
		$where = array('posts.type' => 'Post','posts.status'=>'publicado');
		$join = array(array('table'=>'users','join'=>'users.id = posts.idauthor'));
		$data['posts'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('posts.slug,title,posts.type,posts.content,posts.keywords,posts.description,posts.image,datepublish,users.name','posts',$join,$where,'datepublish','DESC',3,0);

		$data['quote'] = $this->Thememodel->get_all_values_where('posts',array('type'=>'Quote'));
		$data['servicio'] = $this->Thememodel->get_all_values_where('posts',array('type'=>'Servicio'));
		$data['portfolio'] = $this->Thememodel->get_all_values_where('posts',array('type'=>'Portfolio'));
		$data['pagina'] = $this->Thememodel->get_all_values_where('posts',array('type'=>'Página'));
		include 'Themeoptions.php';
		//POSTMETA
		$data['PM_portfolio'] = array();
		if(sizeof($data['portfolio']) > 0 ){
			foreach($postmetas as $key){
				foreach($data['portfolio'] as $des){
					if($key->idpost == $des->id){
					$data['PM_portfolio'][$des->id] = $key;
					}
				}
			}
		}
		$data['PM_titulo'] = array();
		foreach($postmetas as $key){
			foreach($data['titulo'] as $des){
				if($key->idpost == $des->id){
				$data['PM_titulo'][$des->id] = $key;
				}
			}
		}
		return $data;
	}
	private function view_alerts(){
		$data = '';
		if($this->session->userdata('info')){
			$data['info'] = $this->session->userdata('info');
			$this->session->unset_userdata('info');
		}
		if($this->session->userdata('error')){
			$data['error'] = $this->session->userdata('error');
			$this->session->unset_userdata('error');
		}
		$data['messages'] = $this->Thememodel->get_all_values('message');
		return $data;
	}
	function _404(){

		if(!empty($this->session->userdata('url'))){
			$ruta = $this->session->userdata('url');
			$this->session->unset_userdata('url');
		}elseif(!empty($this->uri->uri_string()) ){
			$ruta = $this->uri->uri_string();
		}
		if(!empty($ruta)){
			$data['enrutado'] = $this->Thememodel->get_value('redirects',array('url_origin'=>$ruta));
			if(!empty($data['enrutado'])){
				redirect($data['enrutado']->url_destiny);
			}
		}

		$data = $this->load_data();
		$this->output->set_status_header('404');
		$data['title'] = 'Error 404';
		$data['subtitule'] = 'Página no encontrada';
		$data['description'] = 'LA PÁGINA SOLICITADA YA NO EXISTE O NO HA EXISTIDO NUNCA';
		$data['keywords'] = 'error 404,error,404';
		cargar_template('template/404', $data);
	}

}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Como_funciona extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','security'));
		$this->load->model(array('Thememodel'));
	}
	public function index(){
		$data = $this->load_data();
		cargar_template('template/como-funciona',$data);

	}
	private function load_data(){
		$data = array();
		include 'Themeoptions.php';
		return $data;
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','file','security'));
		$this->load->model(array('Thememodel','Emailmodel'));
	}

	public function index(){
		//$this->output->cache(3600);
		$data = $this->load_data();

		// $data['content'] = $this->Thememodel->get_all_values_where('post',array('slug'=>'portfolio'));

		$data['title'] = '';
		$data['description'] = '';
		$data['keywords'] = '';

		cargar_template('template/contacto', $data);
	}

// Recoger datos del formulario de contacto
	function formcontacto(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name','Nombre','trim|required|strip_tags|prep_for_form');
		$this->form_validation->set_rules('email','email','trim|valid_email|strip_tags|prep_for_form');
		$this->form_validation->set_rules('phone','phone','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('message','Mensaje','trim|strip_tags|prep_for_form');
		if($this->form_validation->run() == FALSE){
			$this->session->set_userdata('error',validation_errors());
			redirect(base_url());
		}else{
			$datos = array();
			$datos['name'] = $this->input->post('name');
			$datos['email'] = $this->input->post('email');
			$datos['message'] = $this->input->post('message').' - '.$this->input->post('phone');
			if(!empty($datos)){
				$datos['date'] = date('Y-m-d H:i:s');
				if( $datos['email'] != '' && $this->Thememodel->insert_value('messages',$datos) ){

					$this->Emailmodel->send_email('info@clicredit.com', $datos['name'], 'miguelangelgomezsa@gmail.com', '', '','Mensaje de '.$datos['name'].' - '.$datos['email'], $datos['message'].' - '.$datos['email'].' - '.$this->input->post('phone').' - '.date('d-m-Y H:i:s') );
					echo 'Formulario enviado con éxito';
				}else{
					echo 'No se ha enviado el formulario por favor compruebe los datos';
				}
			}
		}
	}
	function sendEmailOnly($from, $namefrom, $to, $cc, $ccoo, $asunto, $data){
//	var dataString='name='+$('#name').val()+'&email='+$('#email').val()+'&sudject='+ $('#subject').val() + '&message=' + $('#message').val();
		//$emails = $this->Thememodel->get_select('email','user', array('type' => 'administrador' ) );
		$this->Emailmodel->send_email($from, $namefrom, $to, $cc, $ccoo, $asunto, $data);
	}

	private function load_data(){
		include 'Themeoptions.php';
		return $data;
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cron extends CI_Controller {

	function __construct(){
		parent::__construct();
    $this->load->helper(array('url','util','date','email','security','file','form'));
		$this->load->model(array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');

	}



    //Función para cerrar la sesion de todos los usuarios que tienen un tiempo de inactividad mayor a 25 minutos
	public function index(){
       
      $now = new DateTime();
      $now = $now->format('Y-m-d H:i:s'); 
	 // $this->Thememodel->execute_sql("UPDATE prestamos as p LEFT JOIN users as u ON p.id_cliente = u.id SET p.prestamo_estado_id = 9 WHERE prestamo_estado_id = 8 AND TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'), u.datelastlogin)) >1500");

    $listUsers = $this->listaUsuariosInactivos();
    foreach ($listUsers as $key => $value) {
      $session_id = $value['id'];
      if(!empty($value['user_id'])){
      $user_id = $value['user_id'];
    
      $this->Thememodel->execute_sql("UPDATE prestamos SET `prestamo_estado_id`=9  where  `id_cliente`= $user_id AND `prestamo_estado_id`=8");

       $this->Thememodel->execute_sql("UPDATE users SET logged_in = 0 WHERE  id=$user_id");

       $this->Thememodel->execute_sql("DELETE FROM sessions WHERE id='$session_id'");
     }
    }

   

   /* $session =  $this->Thememodel->execute_sql("DELETE FROM sessions WHERE TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))<1500");
    if($session){
      $this->session->sess_destroy();
    }*/
	}

    //Función para eliminar los prestamos incompletos con 48 horas de inactividad
	public function delete_prestamos(){


    $documentos_asociados = $this->Thememodel->get_query("SELECT doc.fichero FROM documentacion_asociada as doc LEFT JOIN prestamos as p ON p.id_prestamo = doc.prestamo_id WHERE p.prestamo_estado_id = 9 and p.ultima_actualizacion < DATE_SUB(NOW(), INTERVAL 48 HOUR) GROUP by (p.id_prestamo)");

	   $emails = $this->Thememodel->get_query("SELECT name, email FROM users as us LEFT JOIN prestamos as p ON p.id_cliente = us.id WHERE p.prestamo_estado_id = 9 and p.ultima_actualizacion < DATE_SUB(NOW(), INTERVAL 48 HOUR)");

	   $delete_prestamos = $this->Thememodel->execute_sql("DELETE FROM prestamos WHERE ultima_actualizacion < DATE_SUB(NOW(), INTERVAL 48 HOUR) AND prestamo_estado_id = 9");
            
        if($delete_prestamos){
         
         foreach ($emails as $key => $value) { 
          $data['user'] = $value;
          $data['email_template'] = $this->Thememodel->get_all_values('email_template');
          $web = 'CliCREDIT Panamá';
          $from = "info@pa.clicredit.com";
          $data['email'] = $data['email_template'][7];
          $asunto = $data['email']->asunto;
          $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$value->email);
             
         }

         foreach ($documentos_asociados as $key => $value) {
                 $directory = dirname($value->fichero);
                 remove_directory($directory);
         }
      }

	}

  // funcion que retorna la lista de usuarios que tienen una sesion de 25 minutos de inactividad 
  private function listaUsuariosInactivos(){
   $users = $this->Thememodel->get_query("SELECT * from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))>1500 and data<>''");
       $return_data = array();
       $listUsers = array();
     foreach ($users as $key => $value){
         $session_data = $value->data;
        
         if(!empty($session_data)){
        
         $offset = 0;
      while ($offset < strlen($session_data)) {
        if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
  
      $return_data['id'] = $value->id;
      $listUsers[$key] = $return_data;
       
    }
    
}
   
    return $listUsers;

 }


 public function eliminarSessionesInactivas(){
         $this->Thememodel->get_query("DELETE from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))>3000");
 }



 public function enviarRecordatorioPagoQuincenas(){
    $datos_quincenas = $this->Thememodel->get_query("SELECT dp.fecha_vencimiento, us.email,us.name,dp.cuota, dp.numero_quincena, dp.quincena_estado FROM detalle_prestamo as dp LEFT JOIN prestamos as p ON p.id_prestamo = dp.id_prestamo LEFT JOIN users as us ON us.id = p.id_cliente WHERE dp.quincena_estado = 3 AND p.prestamo_estado_id=1 AND DATEDIFF(dp.fecha_vencimiento,NOW())=2");

      if(!empty($datos_quincenas)){
        foreach ($datos_quincenas as $key => $value) {
          $data['data'] = $value;
          $email = $value->email;
          $web = 'CliCREDIT Panamá';
          $from = "info@pa.clicredit.com";
          $asunto = 'Dos (2) días para el próximo pago de su préstamo CliCREDIT Panamá.';
          $template =  $this->load->view("emails/email_recordatorio", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$email);
         
        }
      } 
       
    }

 public function enviar_alerta_prestamoProceso(){
   $users =   $this->Thememodel->get_query("SELECT us.name, us.email, p.id_prestamo FROM users as us LEFT JOIN prestamos as p ON p.id_cliente = us.id WHERE p.prestamo_estado_id = 9 and p.status_alerta = 1 and p.ultima_actualizacion <= DATE_SUB(NOW(), INTERVAL 12 HOUR)");

   if(!empty($users)){
        foreach ($users as $key => $value) { 
          $data['user'] = $value;
          $data['email_template'] = $this->Thememodel->get_all_values('email_template');
          $web = 'CliCREDIT Panamá';
          $from = "info@pa.clicredit.com";
          $data['email'] = $data['email_template'][6];
          $asunto = $data['email']->asunto;
          $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$value->email);
          $id_prestamo = $value->id_prestamo;
          $this->Thememodel->update_value('prestamos',array('status_alerta'=>0),array('id_prestamo' => $id_prestamo));
        
             
         }
   }


 }




  public function actualizar_prestamo_actual(){

     $this->Thememodel->execute_sql("UPDATE  prestamos SET prestamo_actual=0 WHERE ultima_actualizacion < DATE_SUB(NOW(), INTERVAL 48 HOUR) AND prestamo_estado_id = 10");

  }


  public function eliminarLogs(){
   
     $this->Thememodel->execute_sql(" DELETE logs_sms FROM logs_sms LEFT JOIN prestamos ON prestamos.id_prestamo = logs_sms.id_prestamo WHERE prestamos.prestamo_estado_id=9");
  }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cuenta extends CI_Controller {

	function __construct(){
		parent::__construct();
		//date_default_timezone_set('Europe/Madrid');
		$this->load->helper(array('url','util','date','email','file','security'));
		$this->load->model(array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');
		check_rol($this->session->userdata('roles'), array(0));
	}


	public function index(){
		check_rol($this->session->userdata('roles'),array(0));
		$data = $this->load_data();
		$id = $this->session->userdata('user_id');
		$data['user'] = $this->Thememodel->get_value('users', array('id' => $id ));
		

		$data['prestamos'] = $this->Thememodel->get_query("SELECT pe.nombre, pe.color, p.* FROM prestamos as p LEFT JOIN prestamos_estados as pe ON pe.id = p.prestamo_estado_id where id_cliente = $id AND p.prestamo_actual=1");


         $data['calificacion'] = $this->Thememodel->get_query("SELECT c.id, c.imagen, c.tasa_interes FROM `calificacion` as c INNER JOIN users as u WHERE u.id_calificacion = c.id and u.id = $id");

         $where = array('prestamos.id_cliente' => $id);
				$where_in = 'prestamos.prestamo_estado_id';
				$where_ids = array(1,2,3,4,5,6,7);		 
		 $data['prestamos_aprobados'] = $this->Thememodel->total_where_where_in('prestamos',$where,$where_in,$where_ids);

         $data['prestamos_historicos'] = $this->Thememodel->get_query("SELECT p.cantidad, p.tiempo, p.ultima_actualizacion, pe.nombre , pe.color FROM `prestamos` as p LEFT JOIN prestamos_estados AS pe ON pe.id = p.prestamo_estado_id WHERE p.id_cliente = $id and p.prestamo_actual=0 ORDER BY p.id_prestamo DESC LIMIT 5");

         $where_ids = array(2,3,5,6,8,9,10,11,12);		 
		 $data['prestamos_proceso'] = $this->Thememodel->total_where_where_in('prestamos',$where,$where_in,$where_ids);

         if(count ($data['prestamos'])!=0){
           $id_estado_prestamo = $data['prestamos'][0]->prestamo_estado_id;
           $id_prestamo = $data['prestamos'][0]->id_prestamo;
           $where = array('mensajes_estado_prestamo.id_prestamos_estados' => $id_estado_prestamo);
           $data['mensaje_estado_prestamo'] = $this->Thememodel->get_value('mensajes_estado_prestamo',$where);
           
           $total_importes_cancelados = $this->Thememodel->get_query("SELECT SUM(importe) as total_importes FROM `reporte_pagos` WHERE reporte_pagos_estado_id=2 AND id_prestamo = $id_prestamo");
   
           if(!empty($total_importes_cancelados)){
           	  $total_importes_cancelados = (float)$total_importes_cancelados[0]->total_importes;
           }else{
           	   $total_importes_cancelados = 0;
           }
           
           $data['fecha_finalizacion_prestamo'] = $this->Thememodel->get_query("SELECT fecha_vencimiento FROM `detalle_prestamo` where id_prestamo=$id_prestamo order by id desc LIMIT 1");
         if(!empty($data['fecha_finalizacion_prestamo'])){
           $fecha_actual =  new DateTime(date("Y-m-d"));
           $fecha_efecto =  new DateTime(date("Y-m-d", strtotime($data['prestamos'][0]->fecha_efectivo_prestamo)));
           $fecha_finalizacion_prestamo = new DateTime($data['fecha_finalizacion_prestamo'][0]->fecha_vencimiento);
           $total_dias_devolucion =  $fecha_actual->diff($fecha_efecto);
           $total_dias = $fecha_finalizacion_prestamo->diff($fecha_efecto);
           $total_dias_restante_prestamo = $fecha_actual->diff($fecha_finalizacion_prestamo);
           $dias_restante_prestamo = $total_dias_restante_prestamo->days;
           $dias_prestamo_devolucion = $total_dias_devolucion->days;
           $dias_prestamo = $total_dias->days;
           $cantidad = (float)$data['prestamos'][0]->cantidad;
           $tasa_interes = (float)$data['calificacion'][0]->tasa_interes;
           $coste = ($tasa_interes*$dias_prestamo)*$cantidad;

         
        // $data['total_monto_prestamo'] = ((($dias_prestamo*$tasa_interes)*$cantidad)+$cantidad);
         $data['total_monto_devolucion'] = ((($dias_prestamo_devolucion*$tasa_interes)*$cantidad)+$cantidad)-$total_importes_cancelados;
         $data['total_ahorro'] = ($coste/$dias_prestamo)*$dias_restante_prestamo;

        }

        }

         //var_dump($dias_restante_prestamo);
          cargar_template('template/cuenta/cuenta', $data);
	   
	}

  public function cambiar_clave(){
		check_rol($this->session->userdata('roles'),array(0));
		$data = $this->load_data();
		$id = $this->session->userdata('user_id');
		$data['user'] = $this->Thememodel->get_value('users', array('id' => $id ));

		cargar_template('template/cuenta/cambiar-clave', $data);
	}
	public function cambiar_clave_guardar(){
		check_rol($this->session->userdata('roles'),array(0));
		$data = $this->load_data();
		$id = $this->session->userdata('user_id');

		$this->form_validation->set_rules('password','contraseña','trim|required');
		$this->form_validation->set_rules('password2','repetir contraseña','trim|required|matches[password]');

		if($this->form_validation->run() == FALSE){
			$data['error'] = validation_errors();
			cargar_dashboard_nomenu('template/cuenta/cambiar-clave',$data);
		}else{
			$this->Thememodel->save_value('users',array('pass' => md5($this->input->post('password') ) ),array('id' => $id ) );
			$this->session->set_userdata('info', 'Contraseña guardada correctametne');
			redirect(base_url().'cuenta');
		}
	}

     

     public function devolver_prestamo(){
         
         $id = $this->session->userdata('user_id');
         $data['user'] = $this->Thememodel->get_value('users', array('id' => $id ));
         $data['prestamo'] = $this->Thememodel->get_query("SELECT * FROM `prestamos` WHERE id_cliente = $id AND prestamo_actual = 1");
         $id_prestamo = $data['prestamo'][0]->id_prestamo;
         $data['fecha_finalizacion_prestamo'] = $this->Thememodel->get_query("SELECT fecha_vencimiento FROM `detalle_prestamo` where id_prestamo=$id_prestamo order by id desc LIMIT 1");

         $fecha_actual = new DateTime();
         $fecha_efecto  = new DateTime($data['prestamo'][0]->fecha_efectivo_prestamo);
         $fecha_finalizacion_prestamo = new DateTime($data['fecha_finalizacion_prestamo'][0]->fecha_vencimiento);
         $total_dias_devolucion =  $fecha_actual->diff($fecha_efecto);
         $total_dias = $fecha_finalizacion_prestamo->diff($fecha_efecto);
         $dias_prestamo_devolucion = $total_dias_devolucion->days;
         $dias_prestamo = $total_dias->days;


        






        var_dump($data['user']);


     }



	public function cambiarDatos() {
		// $datos['email'] = $this->input->post('nuevo_email');
		// $this->session->set_userdata('email', $datos['email']);
		// $this->Thememodel->update_value('users', $datos, array('id' => $this->session->userdata('user_id')));
		// redirect(base_url().'solicitud/finalizado');
		if ($this->input->post('nuevo_email')) {
			$datos['email'] = $this->input->post('nuevo_email');
			$this->session->set_userdata('email', $datos['email']);
			$this->Thememodel->update_value('users', $datos, array('id' => $this->session->userdata('user_id')));
			redirect(base_url().'cuenta');
		} else if($this->input->post('nuevo_telefono')) {
			$datos['telefono'] = $this->input->post('nuevo_telefono');
			$this->session->set_userdata('telefono', $datos['telefono']);
			$this->Thememodel->update_value('users', $datos, array('id' => $this->session->userdata('user_id')));
			redirect(base_url().'cuenta');
		} else if($this->input->post('nuevo_pass')) {
			$datos['pass'] = md5($this->input->post('nuevo_pass'));
			$this->Thememodel->update_value('users', $datos, array('id' => $this->session->userdata('user_id')));
			redirect(base_url().'cuenta');
		}
		else{
			redirect(base_url());
		}
	}
	private function load_data(){
		//$data['ofertas_categorias'] = $this->Thememodel->get_all_values('ofertas_categorias');
		//$data['etiquetas'] = $this->Thememodel->get_all_values('tags');
		include 'Themeoptions.php';
		$data['cta'] = true;
		return $data;
	}


	





}
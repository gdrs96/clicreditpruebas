<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','file','security','email','date'));
		$this->load->model(array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');
		$this->load->library('pagination');		
		$this->lang->load('formulario','castellano');
		$this->load->model('Coeficientes');
		$this->load->model('VariablesPrimarias');
		$this->load->model('CoeficientesInferencia');
		$this->load->model('DataMaster');
		check_rol($this->session->userdata('roles'), array(1,2,3,4,5,6));

		// --- jjy
		$this->load->model('Algoritmo');
	}

	public function index(){
		$data = $this->view_alerts();
		$data['users'] = $this->Thememodel->get_all_values_order_pag('users','datelastlogin','DESC',10,0);
		$data['users_online_total'] = $this->Thememodel->total_where('sessions',array('timestamp >' => time()-1800 ));
		$today = date('Y-m-d');
		$data['users_today'] = $this->Thememodel->get_query('SELECT * FROM users WHERE DATE_FORMAT(dateregistered, "%Y-%m-%d") = "'.$today.'"' );
		$data['users_total'] = $this->Thememodel->get_all_values('users');
		cargar_dashboard('dashboard/index', $data);
	}

	public function users(){
		check_rol($this->session->userdata('roles'),array(1));
		$data = $this->view_alerts();
		$data['users_total'] = $this->Thememodel->total('users');
		$per_page = 20;
		/*$this->pagination($per_page, base_url().'dashboard/users/', 3, $data['users_total']);
		if($this->uri->segment(3) == FALSE){
			$segmento = 0;
		}else{
			$segmento = ($this->uri->segment(3) - 1) * $per_page;
		}*/
		//$data['users'] = $this->Thememodel->get_all_values_order_pag('users','datelastlogin','DESC',null,null);

		$users = $this->Thememodel->get_all_values_order_pag('users','datelastlogin','DESC',null,null);
		$data['users'] = array();
		foreach ($users as $key) {

			$data['users'][] = $this->Thememodel->get_query("SELECT DISTINCT us.*, ro.permisos, ro.name as name_rol from users as us LEFT JOIN roles as ro ON us.id_rol = ro.id WHERE us.id = $key->id");
            
		}
         


		if(!empty($data['users'])){
			$data['pagination'] = $this->pagination->create_links();
		}else{
			$data['info'] = 'No hay usuarios.';
		}
		cargar_dashboard('dashboard/users', $data);
		//var_dump($data['users'][1][0]->name_rol);
	}

	public function user($id=null){
		check_rol($this->session->userdata('roles'),array(1));
		$data = $this->view_alerts();
		$data['roles'] = $this->Thememodel->get_query("SELECT r.id, r.name FROM `roles` as r");
		$data['calificaciones'] = $this->Thememodel->get_all_values_pag('calificacion',10,0);	
		$data['error'] = '';
		if(!empty($this->input->post())){
			$this->form_validation->set_rules('name','Nombre de usuario','required|trim|html_escape');
			$this->form_validation->set_rules('email','Email','required|trim|valid_email|html_escape');
			$this->form_validation->set_rules('nicename','Nombre','trim|html_escape');
			$this->form_validation->set_rules('status','Estado','required|trim|html_escape');
			$this->form_validation->set_rules('rol','Rol','required|trim|html_escape');
			//Definir si esta creando o editando el usuario
			$id = $this->input->post('id');
			if(!empty($id)) {
				$this->form_validation->set_rules('pass','Contraseña','trim|html_escape|matches[pass1]');
				$this->form_validation->set_rules('pass1','Repetir contraseña','trim|html_escape');
			} else {	
				$this->form_validation->set_rules('pass','Contraseña','required|trim|html_escape|matches[pass1]');
				$this->form_validation->set_rules('pass1','Repetir contraseña','required|trim|html_escape');			
			}
			if($this->form_validation->run() == FALSE){
				$data['error'] .= validation_errors();
				cargar_dashboard('dashboard/user', $data);
			} else {
				$datos = array();
				$datos['name'] = $this->input->post('name');
				$datos['email'] = $this->input->post('email');
				$datos['nicename'] = $this->input->post('nicename');
				$datos['status'] = $this->input->post('status');
				$datos['id_calificacion'] = $this->input->post('calificacion');
				$datos['id_rol'] = $this->input->post('rol');
				$datos['TS_user'] = $this->session->user_id;
				$datos['image'] = "-/img-users/usuario-default.svg";
				if(!empty($this->input->post('pass'))){
					$datos['pass'] = md5($this->input->post('pass'));
				}
				if($id){
					$this->Thememodel->save_value('users',$datos,array('id' =>$id));
				}else{
					$datos['dateregistered'] = date('Y-m-d H:i:s');
					$id = $this->Thememodel->insert_value('users',$datos);
				}

				//guardar imagen
				$nombre_imagen = md5($datos['email']);
				$path = '-/img-users/';
				
				if( ! empty( $_FILES['userfile']['name'] ) ){
					$config['upload_path'] = $path;
					$config['allowed_types'] = 'gif|jpg|png';
					// EN KB
					$config['max_size'] = '3000';
					$config['max_width'] = '5000';
					$config['max_height'] = '5000';
					$config['file_name'] = $nombre_imagen;
					$this->load->library('upload', $config);
					if ( ! $this->upload->do_upload() ){
						$data['error'] .= $this->upload->display_errors();
						if( ! isset($data['error']) || $data ['error'] == '' ){
							$data['error'] .= 'No se ha subido correctamente la imagen. Debe ser de menos de 3 MB';
						}
					}else{
						$file_name = $this->upload->data();
						$url_imagen = $path.$file_name['file_name'];
						$user = $this->Thememodel->get_value('users',array('id' => $id));
					//$foto_anterior = $this->Usermodel->have_image( $username );
						if( $user->image != '' && !file_exists("-/img-users/usuario-default.svg")){
							unlink($user->image);
						}
						$imagen = array('image' => $path.$file_name['file_name']);
						$this->Thememodel->save_value('users',$imagen,array('id' =>$id) );
					}
				}
				$this->session->set_userdata('info', 'Usuario guardado con éxito' );
				redirect(base_url().'dashboard/user/'.$id);
			}
		} else {
			if($id){
				$data['calificacion'] = $this->Thememodel->get_query("SELECT c.id, c.imagen FROM `calificacion` as c INNER JOIN users as u WHERE u.id_calificacion = c.id and u.id = $id");
				$data['user'] = $this->Thememodel->get_value('users',array('id' => $id) );
				if(empty($data['user'])){
					$data['info'] = 'El usuario no existe';
				}
			}
			cargar_dashboard('dashboard/user', $data);
		}
	}

	function users_online(){
		check_rol($this->session->userdata('roles'),array(1));
		$data = $this->view_alerts();
		$data['users_online_total'] = $this->Thememodel->total_where('sessions',array('timestamp >' => time()-1800 ));
		$per_page = 20;
		$this->pagination($per_page, base_url().'dashboard/user/', 3, $data['users_online_total']);
		if($this->uri->segment(3) == FALSE){
			$segmento = 0;
		}else{
			$segmento = ($this->uri->segment(3) - 1) * $per_page;
		}
		$users = $this->Thememodel->get_all_values_where_order_pag('sessions',array('timestamp >' => time()-1800 ),'timestamp','DESC',$per_page,$segmento);
		if(!empty($users)){
			$data['pagination'] = $this->pagination->create_links();
		}else{
			$data['info'] = 'No hay usuarios.';
		}
		//$users = $this->db->where('timestamp >', time()-500)->order_by('timestamp', 'desc')->get('sessions')->result();
		$data['users_online'] = array();
		foreach ($users as $key){
			$session_data = $key->data;
			$return_data = array();
			$offset = 0;
			while ($offset < strlen($session_data)) {
				if (!strstr(substr($session_data, $offset), "|")) {
					throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
				}
				$pos = strpos($session_data, "|", $offset);
				$num = $pos - $offset;
				$varname = substr($session_data, $offset, $num);
				$offset += $num + 1;
				$datos = unserialize(substr($session_data, $offset));
				$return_data[$varname] = $datos;
				$offset += strlen(serialize($datos));
			}
			$return_data['last_activity'] = date('H:i:s',($key->timestamp));
			$return_data['ip'] = $key->ip_address;
			$return_data['id'] = $key->id;
			$data['users_online'][] = $return_data;
		}
	      // var_dump($data['users_online']);
	       cargar_dashboard('dashboard/users-online', $data);
	}

	function groups(){
		check_rol($this->session->userdata('roles'),array(2));
		$data = $this->view_alerts();
		$data['groups'] = $this->Thememodel->get_all_values('roles');
		if(empty($data['groups'])){
			$data['info'] = 'No hay grupos.';
		}
		cargar_dashboard('dashboard/groups', $data);
	}

	function group($id=null){
		check_rol($this->session->userdata('roles'),array(2));
		$data = $this->view_alerts();
		if(!empty($this->input->post())){
			$this->form_validation->set_rules('name','Nombre','trim|max_length[20]|required|html_escape');
			$this->form_validation->set_rules('description','Descripción','trim|html_escape');
			$this->form_validation->set_rules('id','Elemento','trim|numeric|html_escape');
			if($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}else{
				$datos = $this->input->post();
				if($this->input->post('id')){
					$this->Thememodel->save_value('roles',$datos,array('id' =>$this->input->post('id')));
					$data['info'] = 'Se ha guardado con éxito';
				}else{
					$id = $this->Thememodel->insert_value('roles',$datos);
					$data['info'] = 'Se ha guardado con éxito';
				}
			}
		}
		if($id){
			$data['group'] = $this->Thememodel->get_value('roles',array('id' => $id));
		}
		cargar_dashboard('dashboard/group', $data);
	}

	function media(){
		check_rol($this->session->userdata('roles'), array(6));
		$data = $this->view_alerts();
		$files = get_dir_file_info('./img/', $top_level_only = TRUE);
		if(!empty($files)){
			foreach($files as $key){
				$tipo = explode('.', $key['name']);
				if( !empty($tipo[1])){
					if($tipo[1] == 'png' || $tipo[1] == 'jpg' || $tipo[1] == 'jpeg'){
						$data['files'][] = $key;
					}
				}
			}
		}
		$banner_image = $this->Thememodel->get_value('options',array('option_name' => 'banner_image'));
		$banner_image_float = $this->Thememodel->get_value('options',array('option_name' => 'banner_image_float'));
		$show_image_float = $this->Thememodel->get_value('options',array('option_name' => 'show_image_float'));
		$data['banner_image'] = $banner_image->option_value;
		$data['banner_image_float'] = $banner_image_float->option_value;
		$data['show_image_float'] = ($show_image_float->option_value) === 'true' ? true : false;
		cargar_dashboard('dashboard/media', $data);
	}

	function editBannerImage() {
		check_rol($this->session->userdata('roles'), array(6));
		$this->form_validation->set_rules('banner_image','Nombre de portada','trim|required|html_escape');
		$this->form_validation->set_rules('banner_image_float','Nombre imagen flotante','trim|required|html_escape');
		$this->form_validation->set_rules('show_image_float','Mostrar imagen flotante','trim|required|html_escape');
		$name = $this->input->post('banner_image');
		$float = $this->input->post('banner_image_float');
		$show = $this->input->post('show_image_float');
		$this->Thememodel->update_value('options',array('option_value' => $name),array('option_name' => 'banner_image'));
		$this->Thememodel->update_value('options',array('option_value' => $float),array('option_name' => 'banner_image_float'));
		$this->Thememodel->update_value('options',array('option_value' => $show),array('option_name' => 'show_image_float'));
		redirect(base_url().'dashboard/media');
	}

	function uploadMedia(){
		//$ds = DIRECTORY_SEPARATOR;
		$storeFolder = 'img/';
		if(!empty($_FILES)){
			$imagen = $_FILES['file']['tmp_name'];
			$archivo = dirname( __FILE__ ).'/../../'.$storeFolder. $_FILES['file']['name'];
			move_uploaded_file($imagen,$archivo);
		}
	}

	function media_edit($name=null){
		$data = $this->view_alerts();
		if(!empty( $this->input->post())){
			$this->form_validation->set_rules('name','Nombre','trim|required|html_escape');
			$this->form_validation->set_rules('path','Ruta','trim|required|html_escape');
			$this->form_validation->set_rules('old_name','Nombre de archivo actual','trim|required|html_escape');
			if($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}else{
				$name = $this->input->post('name',TRUE);
				$path = $this->input->post('path',TRUE);
				$old_name = $this->input->post('old_name',TRUE);
				if(!file_exists($path.'/'.$name) && isset($name)){
					rename($old_name,$path.'/'.$name);
					$data['info'] = 'Se ha modificado el fichero correctamente';
				}
			}
		}
		if($name){
			$data['file'] = get_file_info('img/'.$name);
		}
		cargar_dashboard('dashboard/media-edit', $data);
	}

	function gallery(){
		$data = $this->view_alerts();
		$files = get_dir_file_info('./img/', $top_level_only = TRUE);
		if(!empty($files)){
			foreach($files as $key){
				$tipo = explode('.', $key['name']);
				if( !empty($tipo[1])){
					if($tipo[1] == 'png' || $tipo[1] == 'jpg' || $tipo[1] == 'jpeg'){
						$data['files'][] = $key;
					}
				}
			}
		}
		cargar_dashboard('dashboard/gallery', $data);
	}

	function calendar(){
		$data = $this->view_alerts();
		$data['email'] = $this->session->userdata('email');
		cargar_dashboard('dashboard/calendar', $data);
	}

	function saveMetaConfig(){
		check_rol($this->session->userdata('roles'), array(1));
		$datos = $this->input->post();
		$datos['justvaluesin'] = trim($datos['justvaluesin'], ',');
		foreach($datos as $key ){
			$ids = explode(',', $datos['justvaluesin'] );
		}
		foreach($ids as $key ){
			$data = array('metakey'=> $datos['justmeta_'.$key], 'metavalue' => $datos['justvalue_'.$key]);
		}
		$text_info = 'Se ha guardado con éxito el contenido';
		$metas_metidas= 0;
		for($i=0; $i <=$datos['newmetas']; $i++){
			if( ! empty($datos['meta_'.$i]) ){
				$data = array('metakey'=> $datos['meta_'.$i], 'metavalue' => $datos['value_'.$i]);
			}
		}
		$text_info .= " con $metas_metidas metas nuevas";
		$this->session->set_userdata('info', $text_info);
		redirect( base_url().'dashboard/config');
	}

	public function calificaciones(){
		check_rol($this->session->userdata('roles'),array(3));
		$data = $this->view_alerts();
		$data['calificaciones_total'] = $this->Thememodel->total('calificacion');
	  
		$per_page = 20;
		$this->pagination($per_page, base_url().'dashboard/calificaciones/', 3, $data['calificaciones_total']);
		if($this->uri->segment(3) == FALSE){
			$segmento = 0;
		}else{
			$segmento = ($this->uri->segment(3) - 1) * $per_page;
		}
		$data['calificaciones'] = $this->Thememodel->get_all_values_order_pag('calificacion','id','ASC',$per_page,$segmento);
		if(!empty($data['calificaciones'])){
			$data['pagination'] = $this->pagination->create_links();
		}else{
			$data['info'] = 'No hay calificaciones registradas.';
		}
		cargar_dashboard('dashboard/calificaciones', $data);
	}

	public function calificacion(){
        $data = $this->view_alerts();
        $data['nueva_calificacion'] =  $this->Thememodel->total('calificacion')+1;
		cargar_dashboard('dashboard/calificacion', $data);
	}

	public function clientes(){
		$data = $this->load_data();
		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$query = $this->db->select('*')->from('users')->where('id IN ('.$subquery.')');
		$sql = $query->get_compiled_select();
		$total = $data['users_total'] =	$this->Thememodel->total_get_query($sql);
		$per_page = 50;
		$this->pagination($per_page, base_url().'dashboard/clientes/', 3, $total);
		if($this->uri->segment(3) != NULL && is_numeric($this->uri->segment(3))){
			$segmento = ($this->uri->segment(3) - 1) * $per_page;
		}else{
			$segmento = $this->uri->segment(3);
		}
		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$query1 = $this->db->select('*')->from('users')->where('id IN ('.$subquery.')')->order_by('id','DESC')->limit($per_page,$segmento);
		$data['users'] = $query1->get()->result();
		if(!empty($data['users'])){
			$data['pagination'] = $this->pagination->create_links();
		}else{
			$data['info_search'] = '<h2>Usuarios encontrados: '.$total.'</h2> <a href="'.base_url().'dashboard/clientes">Puede ver el listado de clientes aquí</a>';
		}
		$data['estados'] = $this->Thememodel->get_all_values('prestamos_estados');

		cargar_template('template/gestion/clientes', $data);
	}
	
	public function buscar_clientes(){
		$data = $this->load_data();
		$or_like = array();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('cliente','Cliente','trim|strip_tags');
		$this->form_validation->run();
		if(!empty($this->input->post())){
			$this->session->set_userdata('buscar_por',$this->input->post());
		}
		$data['titulo'] = '';
		if(!empty($this->session->userdata('buscar_por')['cliente'])){
			$or_like['id'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['n_identificacion'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['email'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['primer_apellido'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['segundo_apellido'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['telefono'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['name'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['segundo_nombre'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['status'] = $this->session->userdata('buscar_por')['cliente'];
			$data['titulo'] .= ' para el cliente '.$this->session->userdata('buscar_por')['cliente'];
		}
		if(empty($or_like)){
			$this->session->set_userdata('info','Debe seleccionar al menos un filtro de búsqueda, puede ver todas las <a href="'.base_url().'gestion">gestion aquí</a>');
			redirect(base_url().'dashboard');
		}
		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$data['users_total'] = $total = $this->db->group_start()->or_like($or_like)->group_end()->where('id IN ('.$subquery.')')->get('users')->num_rows();

		$per_page = 50;
		$this->pagination($per_page, base_url().'dashboard/buscar-clientes/', 3, $total);
		if($this->uri->segment(3) == FALSE){
			$segmento = 0;
		}else{
			$segmento = $this->uri->segment(3);
		}

		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$query1 = $this->db->select('*')->from('users')->group_start()->or_like($or_like)->group_end()->where('id IN ('.$subquery.')')->order_by('datelastlogin','ASC')->limit($per_page,$segmento);

		$data['users'] = $query1->get()->result();
		if(!empty($data['users'])){
			$data['pagination'] = $this->pagination->create_links();
		}else{
			$data['info_search'] = '<h2>Usuarios encontrados '.$data['titulo'].': '.$total.'</h2> <a href="'.base_url().'dashboard/clientes">Puede ver el listado de clientes aquí</a>';
		}
		cargar_template('template/gestion/clientes', $data);
	}

	public function cliente($id=null){
		$data = $this->load_data();
		$data['error'] = '';
		if(!empty($this->input->post())){
			$this->form_validation->set_rules('name','Nombre de usuario','trim|html_escape');
			$this->form_validation->set_rules('email','Email','trim|valid_email|html_escape');
			$this->form_validation->set_rules('status','Estado','trim|html_escape');
			if($this->form_validation->run() == FALSE){
				$data['error'] .= validation_errors();
			}
			$datos = array();
			$datos['name'] = $this->input->post('name');
			$datos['email'] = $this->input->post('email');
			$datos['status'] = $this->input->post('status');
			$datos['id_calificacion'] = $this->input->post('id_calificacion');
			$datos['TS_user'] = $this->session->user_id;
			$id = $this->input->post('id');
			if($id){
				$this->Thememodel->save_value('users',$datos,array('id' =>$id));
			}else{
				$datos['dateregistered'] = date('Y-m-d H:i:s');
				$id = $this->Thememodel->insert_value('users',$datos);
			}
			//$data['info'] = 'Se ha guardado con éxito';
			//guardar imagen
			$hoy = new Datetime();
			$nombre_imagen = $id.'_'.$hoy->format('dmY_hms');
			$path = '-/img-users/';
			if( ! empty( $_FILES['userfile']['name'] ) ){
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|png';
				// EN KB
				$config['max_size'] = '3000';
				$config['max_width'] = '5000';
				$config['max_height'] = '5000';
				$config['file_name'] = $nombre_imagen;
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload() ){
					$data['error'] .= $this->upload->display_errors();
					if( ! isset($data['error']) || $data ['error'] == '' ){
						$data['error'] .= 'No se ha subido correctamente la imagen. Debe ser de menos de 3 MB';
					}
				}else{
					$file_name = $this->upload->data();
					$url_imagen = $path.$file_name['file_name'];
					$user = $this->Thememodel->get_value('users',array('id' => $id));
				//$foto_anterior = $this->Usermodel->have_image( $username );
					if( $user->image != '' && !file_exists("-/img-users/usuario-default.svg")){

						unlink($user->image);
					}
					$imagen = array('image' => $path.$file_name['file_name']);
					$this->Thememodel->save_value('users',$imagen,array('id' =>$id) );
				}
			}
		}
		if($id){
			$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
			$subquery = $this->db->get_compiled_select();
			$query1 = $this->db->select('*')->from('users')->where('id IN ('.$subquery.')')->where(array('id' => $id));
			$data['user'] = $query1->get()->row();
			$data['calificaciones'] = $this->Thememodel->get_all_values_pag('calificacion',10,0);
			$data['prestamo_actual'] = $this->Thememodel->get_query("SELECT pe.nombre, pe.color, p.* FROM prestamos as p LEFT JOIN prestamos_estados as pe ON pe.id=p.prestamo_estado_id WHERE id_cliente = $id AND p.prestamo_actual=1");
		}
		cargar_template('template/gestion/cliente', $data);
	}
	public function prestamo($id=null){
		if(is_numeric($id)){
			$data = $this->load_data();
			$join = array(array('table'=>'users','join'=>'users.id = prestamos.id_cliente'), array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'));
			// array('table' => 'prestamos_estados_historico','join'=>'prestamos_estados_historico.idprestamo = prestamos.id_prestamo'));
			$where = array('prestamos.id_prestamo' => $id);

			$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);
         

			//$this->Thememodel->get_all_values_leftjoin_where_order_pag('users.name, users.primer_apellido,users.n_identificacion','users',$join,$where,'id_prestamo','ASC',10,0);
           if(!empty($data['prestamo'])){


			$id_cliente = $data['prestamo'][0]->id_cliente;
			$data['cliente'] = $this->Thememodel->get_value('users', array('id' => $id_cliente));
			
			$data['cedula_cliente'] =  $this->Thememodel->get_query("SELECT fichero from documentacion_asociada where tipo_documento_id = 1 and prestamo_id=$id ");

            $data['calificacion'] = $this->Thememodel->get_query("SELECT c.tasa_interes FROM `calificacion` as c INNER JOIN users as u WHERE u.id_calificacion = c.id and u.id = $id_cliente");


			$join = array(array('table'=>'prestamos','join'=>'prestamos.id_prestamo = detalle_prestamo.id'));
			$where = array('detalle_prestamo.id_prestamo' => $id);
            

			//$data['cantidad_quincenas'] = $this->Thememodel->total_where_where_in('detalle_prestamo',$where,null,null);
			


            $data['total_quincenas'] = $this->Thememodel->get_query("SELECT SUM(cuota) as total, COUNT(id_prestamo) as cantidad FROM detalle_prestamo WHERE id_prestamo = $id");


		    $estados_prestamo = $data['prestamo'][0]->prestamo_estado_id;
		     $join = array(array('table'=>'clasificacion_prestamo_estado','join'=>'clasificacion_prestamo_estado.id_estado_prestamo = prestamos_estados.id'),array('table'=>'clasificacion_estados','join'=>'clasificacion_estados.id = clasificacion_prestamo_estado.id_clasificacion') );
			if( $estados_prestamo==1 || $estados_prestamo==2 || $estados_prestamo==3 || $estados_prestamo==5 || $estados_prestamo==6 || $estados_prestamo==7)
			{
			   
			    $where = array('clasificacion_prestamo_estado.id_clasificacion' => 1);
			    $data['estados'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('prestamos_estados.id,prestamos_estados.nombre','prestamos_estados',$join,$where,'prestamos_estados.id','ASC',10);

			}
			if($estados_prestamo==8 || $estados_prestamo==9 || $estados_prestamo==10 || $estados_prestamo==11 || $estados_prestamo==12 || $estados_prestamo==13)
			{
				
			    $where = array('clasificacion_prestamo_estado.id_clasificacion' => 2);
			    $data['estados'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('prestamos_estados.id,prestamos_estados.nombre','prestamos_estados',$join,$where,'prestamos_estados.id','ASC',10);
			}

			if($estados_prestamo==8 || $estados_prestamo==9)
			{

			    $where = array('clasificacion_prestamo_estado.id_clasificacion' => 3);
			    $data['estados'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('prestamos_estados.id,prestamos_estados.nombre','prestamos_estados',$join,$where,'prestamos_estados.id','ASC',10);
			}
			
			// --- jjy
			$data['quincenas_estados'] 	   = $this->Thememodel->get_all_values('quincenas_estados');
			$data['reporte_pagos_estados'] = $this->Thememodel->get_all_values('reporte_pagos_estados');
			$data['perfil_social'] 		   = $this->Coeficientes->ProcessCoeficientes("Perfil social");			
			$data['maestros_inferencia']   = $this->CoeficientesInferencia->GetMaestrosInferencia();
			$data['preguntas'] 			   = $this->DataMaster->GetData();
			$data['respuestas'] 		   = $this->DataMaster->GetDataResponses($data['prestamo']);
			
			// --- jjy --- algoritmo para el calculo de Grupos de Variables
			$idP = $id; // $id del prestamo recibido por parametros

			$data['variables_perfil_social']   = $this->Algoritmo->getCalculosVariables( $idP, "Perfil social" );
			$data['variables_capacidad_pago']  = $this->Algoritmo->getCalculosVariables( $idP, "Capacidad de Pago" );
			$data['variables_responsabilidad'] = $this->Algoritmo->getCalculosVariables( $idP, "Responsabilidad" );
			$data['variables_honestidad']      = $this->Algoritmo->getCalculosVariables( $idP, "Honestidad" );
			$data['variables_trabajo'] 		   = $this->Algoritmo->getCalculosVariables( $idP, "Trabajo" );
			$data['variables_macroeconomicas'] = $this->Algoritmo->getCalculosVariables( $idP, "Macroeconomicas" );
			
			// Arbol de Aprobacion - Algoritmo -- jjy
			// ----- EJEMPLO DE USO ----------
			$data['respuesta_algoritmo'] = $this->Algoritmo->getArbolDeAprobacion( $idP );


			/*$join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos_estados_historico.idestado'));
			$where = array('idprestamo' => $id);
			$data['estados_prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('prestamos_estados.nombre,prestamos_estados.color,prestamos_estados_historico.observaciones, prestamos_estados_historico.created_at as fecha_actualizacion','prestamos_estados_historico',$join,$where,'idestado','ASC',99);*/


            
			$data['estados_prestamo_actual'] = $this->Thememodel->get_query("SELECT prestamos_estados.nombre,prestamos_estados.color,prestamos_estados_historico.observaciones, prestamos_estados_historico.created_at as fecha_actualizacion from prestamos_estados_historico,prestamos_estados WHERE prestamos_estados.id = prestamos_estados_historico.idestado and prestamos_estados_historico.idprestamo = $id and prestamos_estados_historico.id = (SELECT MAX(id) FROM `prestamos_estados_historico` WHERE idprestamo = $id) order by prestamos_estados_historico.id desc");			
			$data['estados_prestamo'] = $this->Thememodel->get_query("SELECT prestamos_estados.nombre,prestamos_estados.color,prestamos_estados_historico.observaciones, prestamos_estados_historico.created_at as fecha_actualizacion from prestamos_estados_historico,prestamos_estados WHERE prestamos_estados.id = prestamos_estados_historico.idestado and prestamos_estados_historico.idprestamo = $id and prestamos_estados_historico.id <> (SELECT MAX(id) FROM `prestamos_estados_historico` WHERE idprestamo = $id) order by prestamos_estados_historico.id desc");


			$data['prestamos'] = $this->Thememodel->get_query("SELECT * FROM `prestamos` JOIN `prestamos_estados` ON prestamos.prestamo_estado_id=prestamos_estados.id WHERE id_cliente=$id_cliente");
         	$data['estados_busqueda'] = $this->Thememodel->get_all_values('prestamos_estados'); 

			$data['fecha_finalizacion_prestamo'] = $this->Thememodel->get_query("SELECT fecha_vencimiento FROM `detalle_prestamo` where id_prestamo=$id order by id desc LIMIT 1");

			// Obtener alternativas de las preguntas en solicitud
			$clases_sociales = $this->Thememodel->get_all_values_where('data_page','question="2_3"');
			$generos = $this->Thememodel->get_all_values_where('data_page','question="2_4"');
			$lugares_nac = $this->Thememodel->get_all_values_where('data_page','question="2_2"');
			$periodo_pago  = $this->Thememodel->get_all_values_where('data_page','question="3_7"');
			$has_credits_card = $this->Thememodel->get_all_values_where('data_page','question="3_14"');
			$how_credits = $this->Thememodel->get_all_values_where('data_page','question="3_14_1"');
			$provincias = $this->Thememodel->get_all_values_where('data_page', 'question="4_1"');
			$distritos = $this->Thememodel->get_all_values_where('data_page', "question='4_2'");
			$corregimientos = $this->Thememodel->get_all_values_where('data_page', "question='4_3'");
			$sectores = $this->Thememodel->get_all_values_where('data_page', "question='4_4'");
			$sector_vives = $this->Thememodel->get_all_values_where('data_page','question="2_17_1"');
            $bancos = $this->Thememodel->get_all_values_where_pag('data_page','question="5_2"',50,0);
     
			//obtener respuesta seleccionada
			$sel = json_decode($data['prestamo'][0]->datos, true);

			$select['2_2'] = isset($sel['2_2']) ? $lugares_nac[$sel['2_2']]->description : null; 
			$select['2_3'] = isset($sel['2_3']) ? $clases_sociales[$sel['2_3']]->description: null;
			$select['2_4'] = isset($sel['2_4']) ? $generos[$sel['2_4']]->description : null; 
			$select['5_2'] = isset($sel['5_2']) ? $bancos[intval($sel['5_2'])]->description : null;
			if(isset($sel['3_7'])) {
				if(strlen($sel['3_7']) > 0) {
					$select['3_7'] = $periodo_pago[$sel['3_7']]->description;
				}else {
					$select['3_7'] = '-';
				}
			}			
			$select['3_14'] = isset($sel['3_14']) ? $has_credits_card[$sel['3_14']]->description : null;
			if(isset($sel['3_14_1'])) {
				if(strlen($sel['3_14_1']) > 0) {
					$select['3_14_1'] = $how_credits[$sel['3_14_1']]->description;
				}else {
					$select['3_14_1'] = '-';
				}
			}
			$select['4_1'] = isset($sel['4_1']) ? $provincias[intval(substr($sel['4_1'], 2))-1]->description : null;
			$select['4_2'] = isset($sel['4_2']) ? $distritos[intval(substr($sel['4_2'], 2))-1]->description : null;
			if(isset($sel['4_3'])) {
				if(strlen($sel['4_3']) > 2) {
					$select['4_3'] = $corregimientos[intval(substr($sel['4_3'], 2))-1]->description;
				}else {
					$select['4_3'] = '-';
				}
			}
			if(isset($sel['4_4'])) {
				if(strlen($sel['4_4']) > 2) {
					$select['4_4'] = $sectores[intval(substr($sel['4_4'], 2))-1]->description;
				}else {
					$select['4_4'] = '-';
				}
			}
			
			if(isset($sel['2_17_1'])) {
				if(strlen($sel['2_17_1']) > 0) {
					$select['2_17_1'] = $sectores[intval($sel['2_17_1'])-1]->description;
				}else {
					$select['2_17_1'] = null;
				}
			}
			

			$data['select'] = $select;


			cargar_template('template/gestion/prestamo', $data);

			
           }
           else{
           	 redirect(base_url().'template/404');
           }
		}
	}

	public function prestamo_guardar($id=null){
		if(is_numeric($id)){

			$prestamo = $this->Thememodel->get_all_values_where('prestamos',array('id_prestamo' => $id ));

			//var_dump($prestamo[0]->datos);


			//echo sizeof($this->input->post());

			$total = sizeof($this->input->post()) / 6;
			$datos = array();
			for ($i=0; $i < $total; $i++) {
				$prestamo_datos = array(
					'campo' => $this->input->post($i.'_campo'),
					'valor' => $this->input->post($i.'_valor'),
					'puntuacion' => $this->input->post($i.'_puntuacion'),
					'vp' => $this->input->post($i.'_vp'),
					'vi' => $this->input->post($i.'_vi')
					);
				$datos[] = json_encode($prestamo_datos);
			}
		//	$prestamo[0]->datos = json_encode($this->input->post());
			$total = sizeof($datos);
			$json = '';
			for ($i=0; $i < $total; $i++) {
				$json .= $datos[$i].',';
			}
			$json = trim($json,',');

			/*
			var_dump('['.$json.']');
			var_dump(json_encode($datos));
			*/
			$prestamo[0]->datos = '['.$json.']';

			$this->Thememodel->save_value('prestamos',array('datos' => $prestamo[0]->datos ),array('id_prestamo' => $id));

		}
	}
	public function prestamos(){
		$data = $this->load_data();
		$total = $data['prestamos_total'] =	$this->Thememodel->total('prestamos');
		$per_page = 50;
		$this->pagination($per_page, base_url().'dashboard/prestamos/', 3, $total);
		if($this->uri->segment(3) != NULL && is_numeric($this->uri->segment(3))){
			$segmento = ($this->uri->segment(3) - 1) * $per_page;
		}else{
			$segmento = $this->uri->segment(3);
		}
		
		$query1 = $this->db->select('*')->from('prestamos')->join('users', 'users.id = prestamos.id_cliente', 'left')->join('prestamos_estados', 'prestamos_estados.id = prestamos.prestamo_estado_id', 'left')->order_by('created_at','DESC')->limit($per_page,$segmento);
		$data['prestamos'] = $query1->get()->result();
		if(!empty($data['prestamos'])){
			$data['pagination'] = $this->pagination->create_links();
		}else{
			$data['info'] = '<h2> No hay préstamos</h2>';
		}

		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$query = $this->db->select('*')->from('users')->where('id IN ('.$subquery.')');
		$data['usuarios'] = $query->get()->result();
		$data['estados'] = $this->Thememodel->get_all_values('prestamos_estados');
		cargar_template('template/gestion/prestamos', $data);
	}

	public function buscar_prestamos(){
		$data = $this->load_data();
		$or_like = array();
		$where = array();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('cliente','Cliente','trim|strip_tags');
		$this->form_validation->set_rules('estado','Estado','trim|strip_tags');
		$this->form_validation->run();
		if(!empty($this->input->post())){
			$this->session->set_userdata('buscar_por',$this->input->post());
		}
		$data['titulo'] = '';
		if(!empty($this->session->userdata('buscar_por')['cliente'])){
			$or_like['id_cliente'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['n_identificacion'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['email'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['primer_apellido'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['segundo_apellido'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['telefono'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['name'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['segundo_nombre'] = $this->session->userdata('buscar_por')['cliente'];
			$data['titulo'] .= ' para el cliente '.$this->session->userdata('buscar_por')['cliente'];
		}
		if(!empty($this->session->userdata('buscar_por')['estado'])){
			$where['prestamo_estado_id'] = $this->session->userdata('buscar_por')['estado'];
			$data['titulo'] .= ' '.$this->session->userdata('buscar_por')['estado'];
		}
		if(empty($where) && empty($or_like)){
			$this->session->set_userdata('info','Debe seleccionar al menos un filtro de búsqueda, puede ver todas las <a href="'.base_url().'gestion">gestion aquí</a>');
			redirect(base_url().'gestion');
		}

		if(!empty($where) && !empty($or_like)) {
			$subquery = $this->db
				->select('*')
				->from('prestamos')
				->join('users', 'users.id = prestamos.id_cliente', 'left')
				->join('prestamos_estados', 'prestamos_estados.id = prestamos.prestamo_estado_id', 'left')
				->where($where)
				->where("(
					id_cliente LIKE '%".$or_like['id_cliente']."%' 
					OR n_identificacion LIKE '%".$or_like['id_cliente']."%' 
					OR email LIKE '%".$or_like['id_cliente']."%' 
					OR primer_apellido LIKE '%".$or_like['id_cliente']."%' 
					OR segundo_apellido LIKE '%".$or_like['id_cliente']."%' 
					OR telefono LIKE '%".$or_like['id_cliente']."%'
					OR name LIKE '%".$or_like['id_cliente']."%'
					OR segundo_nombre LIKE '%".$or_like['id_cliente']."%'
				)");
		}else if(empty($where)) {
			$subquery = $this->db
				->select('*')
				->from('prestamos')
				->join('users', 'users.id = prestamos.id_cliente', 'left')
				->join('prestamos_estados', 'prestamos_estados.id = prestamos.prestamo_estado_id', 'left')
				->or_like($or_like);
		} else if(empty($or_like)) {
			$subquery = $this->db
				->select('*')
				->from('prestamos')
				->join('users', 'users.id = prestamos.id_cliente', 'left')
				->join('prestamos_estados', 'prestamos_estados.id = prestamos.prestamo_estado_id', 'left')
				->where($where);
		}
		$query1 = $subquery->get();
		$total = $data['prestamos_total']= $query1->num_rows();
		$data['prestamos'] = $query1->result();
		if(empty($data['prestamos'])){
			$data['info_search'] = '<h2>Préstamos encontrados '.$data['titulo'].': '.$total.'</h2> <a href="'.base_url().'dashboard/prestamos">Puede ver el listado de préstamos aquí</a>';
		}
		$data['estados'] = $this->Thememodel->get_all_values('prestamos_estados');
		cargar_template('template/gestion/prestamos', $data);
	}
	// INICIO funciones auxiliares
	private function view_alerts(){
		$data = array();
		if($this->session->userdata('info')){
			$data['info'] = $this->session->userdata('info');
			$this->session->unset_userdata('info');
		}
		if($this->session->userdata('error')){
			$data['error'] = $this->session->userdata('error');
			$this->session->unset_userdata('error');
		}
		$data['total_messages'] = $this->Thememodel->total_or_where('messages',array(),array('read'=>'SE','read'=>''));
		$data['messages'] = $this->Thememodel->get_all_values_orwhere_order_pag('messages',array('read'=>''),array('read'=>'SE'),'date','DESC',5,0);
		$versions = $this->Thememodel->get_all_values('versiones');
		$data['last_version'] = end($versions);
		return $data;
	}
	private	function pagination($per_page, $url, $uri_segment, $total){
		$this->load->library('pagination');
		$config['base_url'] = $url;
		$config['total_rows'] = $total;
		$config['per_page'] = $per_page;
		$config['use_page_numbers'] = TRUE;
		$config['num_links'] = 10;
		$config['uri_segment'] = $uri_segment;
		$config['first_link'] = '<i class="fa fa-step-backward"></i>';$config['last_link'] = '<i class="fa fa-step-forward"></i>';$config['next_link'] = 'Siguiente <i class="fa fa-chevron-circle-right"></i>';
		$config['prev_link'] = '<i class="fa fa-chevron-circle-left"></i> Anterior';$config['full_tag_open'] = '<ul class="pagination">';$config['full_tag_close'] = '</ul>';$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';$config['last_tag_open'] = '<li>';$config['last_tag_close'] = '</li>';$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';$config['num_tag_open'] = '<li>';$config['num_tag_close'] = '</li>';$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';$config['next_tag_open'] = '<li>';$config['next_tag_close'] = '</li>';
		return $this->pagination->initialize( $config );
	}
	//Función encargada de guardar la calificación
    public function saveCalificacion(){
		$data = $this->view_alerts();
		$data['error'] = '';
		$this->form_validation->set_rules('interes','Interes','trim|numeric|html_escape');
       
	    if($this->form_validation->run() == FALSE){
			$data['error'] .= validation_errors();
		}
		else {
			if( ! empty( $_FILES['userfile']['name'] ) ){
				$path = './files/calificaciones/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = '3000';
				$config['max_width'] = '5000';
				$config['max_height'] = '5000';

				$this->load->library('upload', $config);
            
				//SI LA IMAGEN FALLA AL SUBIR MOSTRAMOS EL ERROR EN LA VISTA UPLOAD_VIEW
				if ( ! $this->upload->do_upload() ){
					$data['error'] .= $this->upload->display_errors();
					if( ! isset($data['error']) || $data ['error'] == '' ){
						$data['error'] .= 'No se ha subido correctamente la imagen. Debe ser de menos de 3 MB';
					}
					cargar_dashboard('dashboard/calificacion', $data);
				} 
				else {
					
                    $datos = array();
			        $datos['tasa_interes'] = $this->input->post('interes');
			        $datos['TS_user'] = $this->session->user_id;
			        $file_name = $this->upload->data();
					$url_imagen = $path.$file_name['file_name'];
			        $datos['imagen'] =  $url_imagen;
                    $this->Thememodel->insert_value('calificacion',$datos);
			        redirect(base_url().'dashboard/calificaciones');
                }
            } else{
              	$data['error'] .= 'Debe subir una imagen';
              	 $data['nueva_calificacion'] =  $this->Thememodel->total('calificacion')+1;
              	cargar_dashboard('dashboard/calificacion', $data);
			}
              
		}
	}
	//Función para editar una calificación
	public function editCalificacion(){
		check_rol($this->session->userdata('roles'),array(3));
		$data = $this->view_alerts();
		$data['error'] = '';
		$id = $this->uri->segment(3);

		$this->form_validation->set_rules('interes','Interes','trim|numeric|html_escape');

		$data['calificacion_edit'] = $this->Thememodel->get_value('calificacion',array('id' => $id));
		if(empty($data['calificacion_edit'])){
			$data['info'] = 'El elemento no existe';
		}
	    if($this->form_validation->run() == FALSE){
			cargar_dashboard('dashboard/editCalificacion', $data);
		}
		else{
            if( ! empty( $_FILES['userfile']['name'] ) ){
				$path = 'files/calificaciones/';
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size'] = '3000';
				$config['max_width'] = '5000';
				$config['max_height'] = '5000';

				$this->load->library('upload', $config);
            
				//SI LA IMAGEN FALLA AL SUBIR MOSTRAMOS EL ERROR EN LA VISTA UPLOAD_VIEW
				if ( ! $this->upload->do_upload() ){
					$data['error'] .= $this->upload->display_errors();
					if( ! isset($data['error']) || $data ['error'] == '' ){
						$data['error'] .= 'No se ha subido correctamente la imagen. Debe ser de menos de 3 MB';
					}
					cargar_dashboard('dashboard/editCalificacion', $data);
				}
				else {
					$id = $this->input->post('id');
					$data['calificacion_edit'] = $this->Thememodel->get_value('calificacion',array('id' => $id));
					$ruta_directorio = $data['calificacion_edit']->imagen;
				
					if (file_exists($ruta_directorio)) {
						unlink($ruta_directorio);
					}

					$datos = array();
					$datos['tasa_interes'] = $this->input->post('interes');
					$datos['TS_user'] = $this->session->user_id;
					$file_name = $this->upload->data();
					$url_imagen = $path.$file_name['file_name'];
					$datos['imagen'] =  $url_imagen;
					$id = $this->input->post('id');
					$this->Thememodel->save_value('calificacion',$datos,array('id' =>$id));
					redirect(base_url().'dashboard/calificaciones');
				}
			} else {
				$datos = array();
				$datos['tasa_interes'] = $this->input->post('interes');
				$datos['TS_user'] = $this->session->user_id;
				$id = $this->input->post('id');
				
				$this->Thememodel->save_value('calificacion',$datos,array('id' =>$id));
				$data['info'] = 'Se ha guardado con éxito';
				//var_dump($id);
				redirect(base_url().'dashboard/calificaciones');

			}
		}
	}
	function estadisticas() {
		check_rol($this->session->userdata('roles'), array(9));
		$data = $this->view_alerts();
		$users = $this->Thememodel->get_all_values('users');
		$red_sociales = array(
			'facebook' => 0,
			'instagram' => 0,
			'twitter' => 0,
			'linkedin' => 0,
			'google' => 0,
			'otra' => 0,
			'ninguno' => 0
		);
		foreach ($users as $user) {
			if($user->red_social !== null) {
				$red_sociales[$user->red_social] += 1;
			}
		}
		$data['red_sociales'] = $red_sociales;
		cargar_dashboard('dashboard/estadisticas', $data);
	}
	function versiones() {
		check_rol($this->session->userdata('roles'), array(8));
		$data = $this->view_alerts();
		$data['versions'] = $this->Thememodel->get_all_values('versiones');
		if(empty($data['versions'])){
			$data['info'] = 'No hay versiones.';
		}
		cargar_dashboard('dashboard/versions', $data);
	}
	function version($id=null) {
		check_rol($this->session->userdata('roles'), array(8));
		$data = $this->view_alerts();
		if(!empty($this->input->post())){
			$this->form_validation->set_rules('number_version','Número','trim|required|html_escape');
			$this->form_validation->set_rules('name_version','Nombre','trim|required|html_escape');
			$this->form_validation->set_rules('version_db','Versión BD','trim|required|html_escape');
			$this->form_validation->set_rules('author_version','Autor','trim|required|html_escape');
			$this->form_validation->set_rules('code_version','Link a código','trim|required|html_escape');
			$this->form_validation->set_rules('log_version','Link a cambios','trim|required|html_escape');
			$this->form_validation->set_rules('id_version','Elemento','trim|numeric|html_escape');
			if($this->form_validation->run() == FALSE){
				$data['error'] = validation_errors();
			}else{
				$datos = $this->input->post();
				if($this->input->post('id_version')){
					$this->Thememodel->save_value('versiones',$datos,array('id_version' => $datos['id_version']));
					$data['info'] = 'Se ha guardado con éxito';
				}else{
					$id = $this->Thememodel->insert_value('versiones',$datos);
					$data['info'] = 'Se ha guardado con éxito';
				}
			}
		}
		if($id){
			$data['version'] = $this->Thememodel->get_value('versiones',array('id_version' => $id));
		}
		cargar_dashboard('dashboard/version', $data);
	}
	private function load_data(){
		$id = $this->session->userdata('user_id');
		include 'Themeoptions.php';
		return $data;
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_ajax extends CI_Controller {

	function __construct(){
		parent::__construct();
//		date_default_timezone_set('Europe/Madrid');
		$this->load->helper(array('url','security'));
		$this->load->model(array('Thememodel','Searchmodel'));
		check_rol($this->session->userdata('roles'),array(2,3));
	}

	public function look(){
/*		if( $this->uri->segment(3) ){
			$busqueda = explode(' ',$this->uri->segment(3));
			$columnas = array('message', 'name', 'email', 'subject');
			$data['messages'] = $this->Searchmodel->get_all_values_like_order_pag('message',$columnas,$busqueda,'date','DESC',25);
var_dump($data);
			$data['messages'] = ($data['messages']) ?: 'No hay resultados';
			$this->load->view('dashboard/ajax//messages-result',$data);
		}*/
	}
	function looking_for_content(){
		if( $this->input->post('s') ){
			if(is_numeric($this->input->post('s'))){
				$data['posts'] = $this->Thememodel->get_all_values_where('posts',array('id' =>$this->input->post('s')));
			}else{
				$busqueda = trim( $this->input->post('s') );
				$columnas = array('title', 'slug', 'description', 'content', 'keywords');
				$data['posts'] = $this->Searchmodel->get_all_values_like_order_pag('posts',$columnas,$busqueda,'datepublish','DESC',25);
			}
			$this->load->view('dashboard/ajax/content-result',$data);

		}
	}

	function looking_for_message(){
		if( $this->input->post('s') ){
			$busqueda = trim( $this->input->post('s') );
			$columnas = array('messages','name','email','subject');
			$data['messages'] = $this->Searchmodel->get_all_values_like_order_pag('messages',$columnas,$busqueda,'date','DESC',25);
			//$data['messages'] = ($data['messages']) ?: 'No hay resultados';
			$this->load->view('dashboard/ajax/messages-result',$data);

		}
	}

	// Borrados
	function user_image_del($id){
		if(!empty($id)){
			$result = $this->Thememodel->get_select('image','users', array('id' => $id));
			$this->Thememodel->save_value('users',array('image' => '-/img-users/usuario-default.svg'), array('id' => $id));
			if(file_exists($result[0]->image) && $result[0]->image!='-/img-users/usuario-default.svg'){
				
				unlink($result[0]->image);
				echo 'Se ha borrado la imagen';
			
			}else{
				echo 'No existe la imagen';
			}
		}
	}

	
	function del_image(){
		if(!empty($this->input->get_post('id'))){
			$result = $this->Thememodel->get_select('image','post', array('id' => $this->input->get_post('id')));
			$this->Thememodel->save_value('posts',array('image' => ''), array('id' => $this->input->get_post('id')));
			if(unlink($result[0]->image)){
				echo 'Se ha borrado la imagen';
			}
		}
	}
	function del_user(){
		$id = $this->input->post('id',TRUE);
		if($this->Thememodel->del_value('users',array('id'=>$id))){
			echo 'Se ha eliminado correctamente';
		}else{
			echo 'Error al eliminar prueba más tarde';
		}
	}
	function del_post(){
		$id = $this->input->post('id',TRUE);
		$this->Thememodel->del_value('posts_metas',array('idpost'=>$id));
		if( $this->Thememodel->del_value('posts',array('id'=>$id))){
			echo 'Se ha eliminado correctamente';
		}else{
			echo 'Error al eliminar prueba más tarde';
		}
	}
	function del_group(){
		$id = $this->input->post('id',TRUE);
		if($this->Thememodel->del_value('roles',array('id'=>$id))){
			echo 'Se ha eliminado correctamente';
		}else{
			echo 'Error al eliminar prueba más tarde';
		}
	}
	function del(){
		$id = $this->input->post('id',TRUE);
		$table = $this->input->post('type',TRUE);
		if( $this->Thememodel->del_value($table,array('id'=>$id))){
			echo 'Se ha eliminado correctamente';
		}else{
			echo 'Error al eliminar prueba más tarde';
		}
	}
	function delpostmeta(){
		$id = $this->input->post('id',TRUE);
		if( ! $this->Thememodel->del_value('posts_metas',array('id'=>$id))){
			echo 'Error al eliminar el post prueba más tarde';
		}else{
			echo 'Se ha eliminado correctamente';
		}
	}
	function deleteMedia(){
		$file = $this->input->post('name',TRUE);
		if( ! unlink($file)){
			echo 'Error al eliminar el archivo';
		}else{
			echo 'Se ha eliminado correctamente';
		}
	}

	// Mensajes
	function checkMsg(){
		$id = $this->input->post('id',TRUE);
		$data = array('read'=> 'SI');
		if( ! $this->Thememodel->save_value('messages',$data, array('id' => $id)) ){
			echo 'Error al marcar como leído';
		}else{
			echo 'Marcado de leído';
		}
	}
	function checkSe(){
		$id = $this->input->post('id',TRUE);
		$data = array('read'=> 'SE');
		if( ! $this->Thememodel->save_value('messages',$data, array('id' => $id)) ){
			echo 'Error al marcar para Seguimiento';
		}else{
			echo 'Marcado para Seguimiento';
		}
	}

	// Sesiones
	function del_duplicated_sessions(){

		$users = $this->Thememodel->get_all_values_where('sessions',array('timestamp >' => $this->config->item('sess_expiration')),'timestamp');

		$data['users_online'] = array();
		foreach ($users as $key){
			$session_data = $key->data;
			$return_data = array();
			$offset = 0;
			while ($offset < strlen($session_data)) {
				if (!strstr(substr($session_data, $offset), "|")) {
					throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
				}
				$pos = strpos($session_data, "|", $offset);
				$num = $pos - $offset;
				$varname = substr($session_data, $offset, $num);
				$offset += $num + 1;
				$datos = unserialize(substr($session_data, $offset));
				$return_data[$varname] = $datos;
				$offset += strlen(serialize($datos));
			}
			$return_data['last_activity'] = date('H:i:s',($key->timestamp));
			$return_data['ip'] = $key->ip_address;
			$return_data['id'] = $key->id;
			$data['users_online'][] = $return_data;
		}

		$otras_sesiones = array();
		foreach ($data['users_online'] as $key1) {
			var_dump($key1);
			if(isset($key1['user_id']) && $key1['user_id'] == $this->session->userdata('user_id') && $return_data['last_activity'] < date('H:i:s') ){ $otras_sesiones[] = $key1; }
		}
		if(!empty($otras_sesiones)){
			foreach ($otras_sesiones as $key2) {
				$this->Thememodel->del_value('sessions',array('id' => $key2['id'], 'id <>' => $this->session->session_id));
			}
			echo 'Sesiones cerradas';
		}else{
			echo 'No hay sesiones que cerrar';
		}
	}



   function cerrar_sesion(){
		$id = $this->input->post('user_id');
		$id_session = $this->input->post('id_session');
		if($id != null){
        $join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
         $where = array('prestamos_estados.id' => 8,'users.id' => $id);

		$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('id_prestamo','prestamos',$join,$where,'id_prestamo','ASC',10,0);

		if(!empty($data['prestamo'])){
			$dataPrestamo = array('prestamo_estado_id' => 9);

			$this->Thememodel->update_value('prestamos',$dataPrestamo,array('id_prestamo' => $data['prestamo'][0]->id_prestamo));
		}
		
		
		}
		$this->Thememodel->save_value('users',array('logged_in' => 0),array('id' =>$id));
		$this->Thememodel->del_value('sessions',array('id' => $id_session));
		echo 'Sesión cerrada';

		
	}
	function borrar_sesiones(){
		check_rol($this->session->userdata('roles'),array(2));
		if($this->db->truncate('sessions')){
			echo 'Sessiones borradas';
		}
	}

	// Caché
	function del_all_cache(){
		$cache = APPPATH.$this->config->item('cache_path');
		if($cache == APPPATH.$this->config->item('cache_path')){
			$cache = APPPATH.'cache/';
		}
		$i = 0;
		foreach(glob(APPPATH . 'cache/*') as $file){
			$i++;
			$file;
			unlink($file);
		}
		echo "Se han borrado $i archivos de cache";
		/*if(delete_files($cache)){
			echo "Se ha borrado toda la cache";
		}*/
	}
	function del_cache_blog(){
		if(!empty($this->input->post('slug'))){
			$cache = APPPATH.$this->config->item('cache_path');
			if($cache == APPPATH.$this->config->item('cache_path')){
				$cache = APPPATH.'cache/';
			}
			$url = $this->config->item('base_url').$this->config->item('index_page').'blog/'.$this->input->get_post('slug');
			if(file_exists($cache.md5($url))){
				unlink($cache.md5($url));
				echo 'Se ha borrado la cache del contenido';
			}else{
				echo 'No hay cache de este contenido';
			}
		}
	}


	public function block_user(){
        
        $id_user = $this->uri->segment(3);
		if(!empty($id_user)){
			$data =  array('status' => 'inactivo',  'TS_user'=> $this->session->user_id);
			$where = array('id'=> $id_user);
			$user_update = $this->Thememodel->update_value('users',$data,$where);
			if($user_update){
				 echo json_encode(array("status" => TRUE));
			}
		}

	}

	public function unblock_user(){

        $id_user = $this->uri->segment(3);
		if(!empty($id_user)){
			$data =  array('status' => 'activo','TS_user'=> $this->session->user_id);
			$where = array('id'=> $id_user);
			$user_update = $this->Thememodel->update_value('users',$data,$where);
			if($user_update){
				 echo json_encode(array("status" => TRUE));
			}
		}

	}




}
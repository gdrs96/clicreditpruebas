<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_export extends CI_Controller {

	function __construct(){
		parent::__construct();
//		date_default_timezone_set('Europe/Madrid');
		$this->load->helper(array('url','security','file','download','security'));
		$this->load->model(array('Thememodel'));
		check_rol($this->session->userdata('roles'),array(2));
	}

	public function messages(){

		$this->load->dbutil();
	//	$query = $this->db->query("SELECT * FROM message");
	//	$query = $this->Thememodel->get_all_values('message');
		$query = $this->db->get('messages');
		$data = $this->dbutil->csv_from_result($query);
		$name = 'mensajes.csv';
		force_download($name, $data);
/*
		$delimiter = ",";
		$newline = "\r\n";
		$enclosure = '"';
		echo $this->dbutil->csv_from_result($query, $delimiter, $newline, $enclosure);
*/
	}
	public function bbdd(){
		$this->load->dbutil();
		$fecha_hora = date('Y-m-d_His');
		$prefs = array(
			'tables'		=> array(),
			'ignore'		=> array(),
			'format'		=> 'zip',
			'filename'		=> 'backup'.$fecha_hora.'.sql',	// Nombre de archivo - NECESARIO SOLO CON ARCHIVOS ZIP
			'add_drop'		=> TRUE,		// Agregar o no la sentencia DROP TABLE al archivo de respaldo
			'add_insert'	=> TRUE,	// Agregar o no datos de INSERT al archivo de respaldo
			'newline'		=> "\n"		// Caracter de nueva línea usado en el archivo de respaldo
			);
		$copia_de_seguridad = $this->dbutil->backup($prefs);
		if ( ! write_file('./backup/backup_'.$fecha_hora.'.zip', $copia_de_seguridad)){
			$this->session->set_userdata('error','No se ha podido crear la copia.');
		}else{
			$this->session->set_userdata('success','Copia creada satisfactoriamente, se ha guardado en el directorio backup');
		}
		force_download('backup_'.$fecha_hora.'.zip', $copia_de_seguridad);
			//redirect(base_url().'dashboard');
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','cookie','email'));
		$this->load->model( array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');
	}

	public function index(){
		$data = $this->view_alerts();
		if( $this->session->userdata('roles') ){
			redirect( base_url().'dashboard');
		}else{
			cargar_dashboard_nomenu('dashboard/login/login', $data);
		}
	}

	public function login_in($info=null){
		$data = $this->view_alerts();
		$this->session->set_userdata('info', $info);
		$num_maximo_intentos = 3;
		$tiempo_espera = 300; //5 minutos en segundos
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('password','contraseña','trim|required');
		if( $this->session->userdata('intentos') >= $num_maximo_intentos){
			if( strtotime('now') < ($this->session->userdata('time_ultimo_intento')+$tiempo_espera) ){
				$data['error'] = 'Ha intenteado iniciar sesión demasiadas veces.<br> Pruebe dentro de 5 minutos';
				cargar_dashboard_nomenu('dashboard/login/login',$data);
				return;
			}else{
				$this->session->unset_userdata('intentos');
			}
		}
		if($this->form_validation->run() == FALSE){
			$data['error'] = validation_errors();
			cargar_dashboard_nomenu('dashboard/login/login',$data);
			return;
		}else{
			$usuario = $this->_is_registered($this->input->post('email'),$this->input->post('password'));
			if( $usuario ){
				if($usuario->status != 'activo'){
					$data['error'] = 'Su usuario no está activo contacte con el adminitrador';
					cargar_dashboard_nomenu('dashboard/login/login');
					return;
				}
				  if($usuario->logged_in == 1){
               	    $status = $this->verificarSession($usuario->id)['status'];
					$id_session = $this->verificarSession($usuario->id)['id_session'];
					if($status){ 
                    
					  $this->cerrarSessionUser($usuario->id, $id_session);
                      $this->session->set_userdata('infoSession', "Mantiene una sesión activa, por su seguridad procederemos a cerrarla");
				    }

               }
				$roles = $this->Thememodel->get_select('id_rol','users',array('id'=>$usuario->id));
				$rol = $roles[0];
				if($rol){
					$permisos = $this->Thememodel->get_select('permisos','roles',array('id'=>$rol->id_rol));
					$permisos = json_decode($permisos[0]->permisos);
					
					$roles_actuales = array();
					foreach($permisos as $key){
						$roles_actuales[] = $key->id;
					}
				}
				$this->session->set_userdata('roles', $roles_actuales );
				$this->session->set_userdata('user_id', $usuario->id);
				$this->session->set_userdata('email', $usuario->email);
				$this->session->set_userdata('nicename', $usuario->nicename );
				if($usuario->image){
					$this->session->set_userdata('image', $usuario->image );
				}
				$this->session->unset_userdata('intentos');
				$this->Thememodel->save_value('users',array('datelastlogin' => mdate('%Y-%m-%d %H:%i:%s', time() ), 'logged_in' => 1), array('id' => $usuario->id ));
                $cookie_name =  'cookie_session';
                $cookie_value = 'cookie_session_empty';
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), '/'); // 86400 = 1 day
				if($usuario->name != ''){
					$this->session->set_userdata('info', "Hola $usuario->name. Bienvenido !");
				}else{
					$this->session->set_userdata('info', "Hola $usuario->nicename. Bienvenido !");
				}
				redirect(base_url().'dashboard');
			}else{
				if(empty($this->session->userdata('intentos'))){
					$this->session->set_userdata('intentos',1);
					$this->session->set_userdata('time_ultimo_intento',strtotime('now'));
				}else{
					$intentos = $this->session->userdata('intentos')+1;
					$this->session->set_userdata('intentos',$intentos);
					$this->session->set_userdata('time_ultimo_intento',strtotime('now'));
				}
				$numero_intentos_restantes = $num_maximo_intentos-$this->session->userdata('intentos');
				$data['error'] = 'Por favor introduzca un usuario y contraseña válidos';
				cargar_dashboard_nomenu('dashboard/login/login',$data);
			}
		}
	}


	//verifica en la tabla sesions a través de la columna data si el usuario tiene una sesion activa
public function verificarSession($id_user){
   
 $users = $this->Thememodel->get_query("SELECT id,data from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))<1800 order by data DESC");
       $return_data = array();
       $listUsers = array();
     foreach ($users as $key => $value){
         $session_data = $value->data;
        
         if(!empty($session_data)){
        
         $offset = 0;
      while ($offset < strlen($session_data)) {
        if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
  
      $return_data['id'] = $value->id;
      $listUsers[$key] = $return_data;
       
    }
    
}
   
     $users_online =  $listUsers;
   
     foreach ($users_online as $key => $user) {
     
      if($user["user_id"]==$id_user){
        return [
        "id_session" => $user['id'],
        "status" => true
      ];
     }
    }

 }


 
   //función encargada de cerrar la sesión del usuario en caso de tenerla activa en otro dispositivo o navegador
	public function cerrarSessionUser($user_id, $id_session){
		$id = $user_id;
		if($id != null){
        $join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
         $where = array('prestamos_estados.id' => 8,'users.id' => $id);

		$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('id_prestamo','prestamos',$join,$where,'id_prestamo','ASC',10,0);

		if(!empty($data['prestamo'])){
			$dataPrestamo = array('prestamo_estado_id' => 9);

			$this->Thememodel->update_value('prestamos',$dataPrestamo,array('id_prestamo' => $data['prestamo'][0]->id_prestamo));
		}
		
		
		}
		$this->Thememodel->save_value('users',array('logged_in' => 0),array('id' =>$id));
		$this->Thememodel->del_value('sessions',array('id' => $id_session));

		
	}

	public function recuperar_cuenta(){
		$data = $this->view_alerts();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email','email','trim|required|valid_email|strip_tags');
		$this->form_validation->set_rules('digito','campo digito','trim|required|strip_tags');
		if($this->form_validation->run() == FALSE){
			$data['error'] = validation_errors();
			cargar_dashboard_nomenu('dashboard/login/login',$data);
		}else{
			$datos = $this->input->post();
			if( $datos['digito'] == date('d') + date('m') ){
				$datosUsuario = $this->Thememodel->get_value('users', array('email' => $datos['email'] ));

				if( empty( $datosUsuario )){
					$data['info'] = 'No hemos podido enviarle el email de recuperación';
					cargar_dashboard_nomenu('dashboard/login/login',$data);
				}
				$nuevaPass = date('dmYiim');
				$this->Thememodel->save_value('users',array('pass' => md5( $nuevaPass )),array('email' => $datos['email']) );
				$this->Emailmodel->send_email_for_recover_password( $datosUsuario, $nuevaPass );
				$data['info'] = 'Se le ha enviado un email para restablecer su contraseña, revise su bandeja de entrada o la carpeta de spam';
			}else{
				$data['info'] = 'Por favor debe rellenar el campo de control';
				cargar_dashboard_nomenu('dashboard/login/login',$data);
			}
		}
	}






	public function _is_registered($email,$password){
		$usuario = $this->Thememodel->get_value('users', array('email' => $email));
		if( !empty($usuario) && ($usuario->pass == md5( $password ))){
			/*$id = $usuario->id;
			$type = $usuario->type;
			return $id.'-'.$type;
			*/
			return $usuario;
		}
		else{
			return FALSE;
		}
	}



	public function enviarEmailRecuperacion(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('email','email','trim|required|valid_email|strip_tags');
		$this->form_validation->set_rules('digito','campo digito','trim|required|strip_tags');
		if($this->form_validation->run() == FALSE){
			$this->session->set_userdata('error',validation_errors());
			redirect(base_url().'acceso');
		}else{

			$digito = $this->input->post('digito');
			$email = $this->input->post('email');

			if( $digito == date('d')+date('m') ){
				$datosUsuario = $this->Thememodel->get_value('users', array('email' => $email ));

				if( empty( $datosUsuario )){
				$data['info'] = 'Este email no existe en nuestra base de datos. Por favor regístrese';
				cargar_dashboard_nomenu('dashboard/login/login',$data);
				}
				//Generamos el token, asignamos el token y el tiempo de expiración al usuario que tiene asociado el email
				$token = $this->token();
				$this->Thememodel->save_value('users',array('token' => $token, ' token_expiration_TS' => date('Y-m-d H:i:s', strtotime("+60 min"))),array('email' => $email));
                
                //enviamos el correo
                $this->Emailmodel->email_recovery_password($datosUsuario,$token);

                $data['info'] = 'Se le ha enviado un email para restablecer su contraseña, revise su bandeja de entrada';
				cargar_dashboard_nomenu('dashboard/login/login',$data);
              
			}else{
				  $data['error'] = 'El digito de control no es correcto';
				cargar_dashboard_nomenu('dashboard/login/login',$data);
			
			}
		}
	}
public function token()
	{
		$token = md5(uniqid(rand(),true));
		$this->session->set_userdata('token',$token);
		return  $token;
	}

 

	public function logout(){
		$id = $this->session->userdata('user_id');
		$this->Thememodel->save_value('users',array('logged_in' => 0),array('id' =>$id));
		unset($_COOKIE['cookie_session']);
		setcookie('cookie_session', '', time() + (86400 * 30), '/'); // 86400 = 1 day
		$this->session->sess_destroy();
		redirect( base_url().'login');
	}

	private function view_alerts(){
		$data = '';
		if($this->session->userdata('info')){
			$data['info'] = $this->session->userdata('info');
			$this->session->unset_userdata('info');
		}
		if($this->session->userdata('error')){
			$data['error'] = $this->session->userdata('error');
			$this->session->unset_userdata('error');
		}
		return $data;
	}

}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Detalle_prestamo extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email','security'));
		$this->load->model(array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');
		$this->lang->load('formulario','castellano');

	}



/******************************************************************************
    Función para obtener el json de las quincenas asociadas a un prestamo
******************************************************************************/
  public function obtenerDetallePrestamo(){

	$id_prestamo= $this->uri->segment(3); //id del prestamo
  if($id_prestamo!=null){
	   $data['data']= $this->Thememodel->get_query("SELECT  dp.id, dp.numero_quincena,FORMAT(dp.cuota, 2) as cuota,DATE_FORMAT(dp.fecha_vencimiento,'%d-%m-%Y') as fecha_vencimiento,dp.quincena_estado as quincena_estado,qe.nombre, qe.color, dp.id_prestamo FROM prestamos as p LEFT JOIN detalle_prestamo as dp ON dp.id_prestamo = p.id_prestamo LEFT JOIN quincenas_estados as qe ON dp.quincena_estado = qe.id WHERE dp.id_prestamo = $id_prestamo");
	     
	    echo json_encode($data);
    }
    else
    {
      $data['data']=[];
      echo json_encode($data);
      // redirect(base_url().'template/404');
    }

	}


/********************************************************************************************
    Función para agregar una quincena del lado del administrador
*********************************************************************************************/
     public function agregarQuincena(){
         check_rol($this->session->userdata('roles'),array(1));
         $id_prestamo = $this->uri->segment(3); //id del prestamo
         $total_prestamo = (float)$this->input->post('total_prestamo'); //monto total del prestamo
      	 $importe = $this->input->post('importe'); 
         $fecha_vencimiento = date("Y-m-d", strtotime($this->input->post('fecha_vencimiento')));
         $fecha_final_prestamo = date("Y-m-d", strtotime($this->input->post('fecha_final_prestamo')));
         $total_quincenas = $this->Thememodel->get_query("SELECT SUM(cuota) as total, COUNT(id_prestamo) as cantidad FROM detalle_prestamo WHERE id_prestamo = $id_prestamo");
         $datos_prestamo = $this->Thememodel->get_query("SELECT tiempo, fecha_efectivo_prestamo FROM `prestamos` WHERE id_prestamo=$id_prestamo");
         $total_prestamo_acumulado = (float)$total_quincenas[0]->total+(float)$importe;
         $cantidad_quincenas_acumuladas = (int)$total_quincenas[0]->cantidad;
         
         $cantidad_quincena = (int)$datos_prestamo[0]->tiempo;
        
         $fecha_efectivo_prestamo = date("Y-m-d", strtotime($datos_prestamo[0]->fecha_efectivo_prestamo));
         
         $fecha_ultima_quincena = $this->Thememodel->get_query("SELECT fecha_vencimiento FROM `detalle_prestamo` where id_prestamo=$id_prestamo order by id desc LIMIT 1");
         $numero_quincena = $this->input->post('numero_quincena');

         if(!empty($fecha_ultima_quincena)){ 
             $fecha_ultima_quincena = date("Y-m-d", strtotime($fecha_ultima_quincena[0]->fecha_vencimiento));
         }
         else{
            $fecha_ultima_quincena = $fecha_efectivo_prestamo;
         }


         if($fecha_vencimiento>=$fecha_efectivo_prestamo && $fecha_vencimiento<=$fecha_final_prestamo){

          if($fecha_vencimiento>$fecha_ultima_quincena){

          if($total_prestamo_acumulado<=$total_prestamo && $cantidad_quincenas_acumuladas<=$cantidad_quincena ){
              $data = array(
                'numero_quincena' => $numero_quincena,
                'fecha_vencimiento' => date("Y-m-d H:i:s", strtotime($this->input->post('fecha_vencimiento'))),
                'cuota' => round($this->input->post('importe'),2),
                'quincena_estado' => $this->input->post('estado_quincena'),
                'id_prestamo' => $id_prestamo,
                'TS_user' => $this->session->user_id,
            );

             if(date("Y-m-d") <= date("Y-m-d", strtotime($this->input->post('fecha_vencimiento'))) && $this->input->post('estado_quincena')==1 ){
                 echo json_encode(array("status" => "ERROR_OPERACION_ESTADO"));
             }else{
               $this->Thememodel->insert_value('detalle_prestamo',$data);
               echo json_encode(array("status" => TRUE, "numero_quincena"=> $numero_quincena));
              }
         }else{
           echo json_encode(array("status" => "ERROR_OPERACION_QUINCENA"));
         }
        
      }else{
         echo json_encode(array("status" => "ERROR_AJUSTE_QUINCENA"));
      }

      }

        else{
           echo json_encode(array("status" => "ERROR_AJUSTE_FECHA"));
        }

       }




/********************************************************************************************
    Función para actualizar una quincena del lado del administrador
*********************************************************************************************/
    public function actualizarQuincena(){
      check_rol($this->session->userdata('roles'),array(1));
      $id_prestamo = $this->input->post('id_prestamo');
      $id_quincena = $this->input->post('id_quincena');
      $total_prestamo = (float)$this->input->post('total_prestamo');
      $numero_quincena = (int)$this->input->post('numero_quincena');
      $importe = $this->input->post('importe'); 
      $total_quincenas = $this->Thememodel->get_query("SELECT SUM(cuota) as total, COUNT(id_prestamo) as cantidad FROM detalle_prestamo WHERE id_prestamo = $id_prestamo");
      $monto_quincena = $this->Thememodel->get_query("SELECT cuota FROM `detalle_prestamo` WHERE id=$id_quincena");


      $datos_prestamo = $this->Thememodel->get_query("SELECT tiempo, fecha_efectivo_prestamo FROM `prestamos` WHERE id_prestamo=$id_prestamo");
      
      $total_prestamo_anterior = (float)$total_quincenas[0]->total-(float)$monto_quincena[0]->cuota;
      $total_prestamo_acumulado = $total_prestamo_anterior+(float)$importe;
      $cantidad_quincenas_acumuladas = (int)$total_quincenas[0]->cantidad;
      $cantidad_quincena = (int)$datos_prestamo[0]->tiempo;
      $fecha_efectivo_prestamo = date("Y-m-d", strtotime($datos_prestamo[0]->fecha_efectivo_prestamo));
      $fecha_vencimiento = date("Y-m-d", strtotime($this->input->post('fecha_vencimiento')));
      $fecha_final_prestamo = date("Y-m-d", strtotime($this->input->post('fecha_final_prestamo')));
      $numero_quincena_anterior = (int) $numero_quincena-1;
      
      $fecha_quincena_anterior = $this->Thememodel->get_query("SELECT fecha_vencimiento FROM `detalle_prestamo` where id_prestamo=$id_prestamo and numero_quincena = $numero_quincena_anterior");
   
    if(!empty($fecha_quincena_anterior)){
          $fecha_quincena_anterior = date("Y-m-d", strtotime($fecha_quincena_anterior[0]->fecha_vencimiento));
      }else{
         $fecha_quincena_anterior =  $fecha_efectivo_prestamo;
      }

      if($fecha_vencimiento>=$fecha_efectivo_prestamo && $fecha_vencimiento<=$fecha_final_prestamo){

      if($fecha_vencimiento>$fecha_quincena_anterior){
         if($total_prestamo_acumulado<=$total_prestamo){

          $data = array(
                'numero_quincena' => $this->input->post('numero_quincena'),
                'fecha_vencimiento' => date("Y-m-d H:i:s", strtotime($this->input->post('fecha_vencimiento'))),
                'cuota' => round($this->input->post('importe'),2),
                'quincena_estado' => $this->input->post('estado_quincena'),
                
            );

        if(date("Y-m-d") <= date("Y-m-d", strtotime($this->input->post('fecha_vencimiento'))) && $this->input->post('estado_quincena')==1 ){
           echo json_encode(array("status" => "ERROR_OPERACION_ESTADO"));
        }
        else{
          $detalle_prestamo =  $this->Thememodel->save_value('detalle_prestamo',$data,array('id' => $id_quincena));
            if($detalle_prestamo){
              echo json_encode(array("status" => TRUE));
            }
            else{
              echo json_encode(array("status" => FALSE));
            }
         }
      }else{
           echo json_encode(array("status" => "ERROR_OPERACION_QUINCENA"));
         }
       }else{
         echo json_encode(array("status" => "ERROR_AJUSTE_QUINCENA"));
         }
       }
       else{
           echo json_encode(array("status" => "ERROR_AJUSTE_FECHA"));
       }

    }



       public function eliminarQuincena(){
         check_rol($this->session->userdata('roles'),array(1));
        $id = $this->input->post('id');
        $id_prestamo = $this->input->post('id_prestamo');
        $prestamo = $this->Thememodel->get_value('prestamos',array('id_prestamo'=>$id_prestamo));
       

        if($prestamo->prestamo_estado_id==1 || $prestamo->prestamo_estado_id==2 || $prestamo->prestamo_estado_id==3 || $prestamo->prestamo_estado_id==5 || $prestamo->prestamo_estado_id==6 || $prestamo->prestamo_estado_id==7){

            echo json_encode(array("status" => FALSE));

        }else{

          $quincena_eliminada =  $this->Thememodel->del_value('detalle_prestamo',array('id'=>$id));
          if($quincena_eliminada==TRUE){
          $quincenas = $this->Thememodel->get_all_values_where('detalle_prestamo',array('id_prestamo'=>$id_prestamo));
          if(count($quincenas)>0){
            for($i=0; $i<count($quincenas); $i++) { 
                 $data = array('numero_quincena' => $i+1);
                $this->Thememodel->save_value('detalle_prestamo',$data,array('id' => $quincenas[$i]->id));
            }
          }
             echo json_encode(array("status" => TRUE));
          }
        }

        
       }





}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Documentacion_asociada extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email','security','file'));
		$this->load->model(array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');
		$this->lang->load('formulario','castellano');
		

	}


	public function obtenerDocumentacion($id){

		$id_prestamo = $this->uri->segment(3); //id del prestamo
		if($id_prestamo!=null){
		 	
             $data['data'] =  $this->Thememodel->get_query("SELECT id, DATE_FORMAT(fecha_subida,'%d/%m/%Y') as fecha_subida, descripcion, observacion, fichero,tipo_documento_id, prestamo_id FROM `documentacion_asociada` WHERE prestamo_id = $id_prestamo order by id DESC");

               echo json_encode($data);
            }
          else{
          	  $data['data']=[];
              echo json_encode($data);
          }

	}


  public function actualizarSeleccionDocumento(){
    $id_prestamo = $this->input->post("id_prestamo");
    $id_documento = $this->input->post("id_documento");
    $documento_cedula_cliente =  $this->Thememodel->get_query("SELECT id from documentacion_asociada where tipo_documento_id = 1 and prestamo_id=$id_prestamo");
    if(!empty($documento_cedula_cliente)){
        $data['tipo_documento_id']=1;
        $data_documento_anterior['tipo_documento_id']=0;
        $id_documento_anterior = $documento_cedula_cliente[0]->id;
        $this->Thememodel->save_value('documentacion_asociada',$data,array('id' =>$id_documento));

        $this->Thememodel->save_value('documentacion_asociada',$data_documento_anterior,array('id' =>$id_documento_anterior));
        
        echo json_encode(array("status" => TRUE));
    }else{
         $data['tipo_documento_id']=1;
           $this->Thememodel->save_value('documentacion_asociada',$data,array('id' =>$id_documento));
           echo json_encode(array("status" => TRUE));
    }

  }

//Parámetros de configuración para el almacenamiento de la imagen o archivo
  private function set_upload_options($id_prestamo)
  {   
      $config = array();
      $path = 'files/prestamos/'.'prestamo_'.$id_prestamo;
      if (!is_dir($path)) {
           mkdir('./files/prestamos/'.'prestamo_'.$id_prestamo, 0777, TRUE);
      }
      $config['upload_path'] = $path;
      $config['allowed_types'] = 'gif|jpg|png|pdf';
      return $config;
  }

//Parámetros de configuración para el ajuste de imagenes
 private function resizeImage($filename){
        $config['image_library'] = 'gd2';
        $config['source_image'] = $filename;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['new_image']= $filename;
        $config['quality'] = 60;  
        $config['width'] = 400;  
        $config['height'] = 300;
        return $config;
      
    }

//Función encargada de guardar multiples imagenes o archivos de la documentacion asociada al prestamo  
 public function cargarDocumentacion()
  {
    $files = $_FILES;
    $this->load->library('image_lib');
    foreach($files as $key => $value)
    {
        for($s = 0; $s < count($files['imagen_documento']['size']); $s++)
        {
            $_FILES['imagen_documento']['name']     = $value['name'][$s];
            $_FILES['imagen_documento']['type']     = $value['type'][$s];
            $_FILES['imagen_documento']['tmp_name'] = $value['tmp_name'][$s];
            $_FILES['imagen_documento']['error']    = $value['error'][$s];
            $_FILES['imagen_documento']['size']     = $value['size'][$s]; 
            
            $this->load->library('upload'); 
            $this->upload->initialize($this->set_upload_options($this->input->post('id_prestamo')));
          
           
          
            if(!$this->upload->do_upload('imagen_documento'))  
            {  
               echo json_encode(array("status" => FALSE));
            }  
             else
             {  
                  $datos = array();
                  $path = $this->set_upload_options($this->input->post('id_prestamo'))['upload_path'];
                  $file_name = $this->upload->data();
                  $url_imagen = $path.'/'.$file_name['file_name'];
                  $extension = end((explode(".", $file_name['file_name'])));
                  
                  /*if($extension!='pdf'){
                  $this->image_lib->initialize($this->resizeImage($url_imagen)); 
                    if (!$this->image_lib->resize()){
                        echo $this->image_lib->display_errors();
                    }
                 }*/
                  
                  $datos['fichero'] =  $url_imagen;
                  $datos['fecha_subida'] = date("Y-m-d H:i:s");
                  $datos['descripcion'] = $file_name['file_name'];
                  $datos['prestamo_id'] = $this->input->post('id_prestamo');
                  $documentacion = $this->Thememodel->insert_value('documentacion_asociada',$datos);

             }  
         }
         if(!empty($documentacion)){
         if($documentacion!=FALSE){
              echo json_encode(array("status" => TRUE));
            }
        }


    }
}


   public function actualizarDocumento(){
        check_rol($this->session->userdata('roles'),array(1));
        $id= $this->input->post('idDocumento');
        $data = array('observacion' => $this->input->post('observacion'), 'descripcion' => $this->input->post('descripcion'));
        $documento_actualizado =  $this->Thememodel->save_value('documentacion_asociada',$data,array('id' =>$id));
        if($documento_actualizado==TRUE){
           echo json_encode(array("status" => TRUE));
         }else
         {
           echo json_encode(array("status" => FALSE));
         }



   }

   public function eliminarDocumento(){
        check_rol($this->session->userdata('roles'),array(1));
        $id = $this->input->post('id');
         $documento = $this->Thememodel->get_value('documentacion_asociada',array('id'=>$id));
        

         $documento_eliminado =  $this->Thememodel->del_value('documentacion_asociada',array('id'=>$id));
          if($documento_eliminado==TRUE){
             if(file_exists($documento->fichero)){
              unlink($documento->fichero);
               
           }
           
            echo json_encode(array("status" => TRUE));
          
         }else
         {
           echo json_encode(array("status" => FALSE));
         }
       }





}
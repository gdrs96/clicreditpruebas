<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Feed extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('xml','text','url'));
		$this->load->model(array('Thememodel'));
	}
	public function index(){
		$data['feed_name'] = 'miguelgomezsa.com';
		$data['feed_url'] = base_url().'feed';
		$data['page_description'] = 'Blog de Miguel Gómez, aquí encontrarás técnicas de programación, consejos para programar. Guías para SEO. Y otros artículos sobre desarrollo.';
		$data['page_language'] = 'es'; // the language
		$data['creator_email'] = 'miguelangelgomezsa@gmail.com'; // your email
		$where = array('post.type' => 'Post','post.status'=>'publicado');
		$join = array(array('table'=>'user','join'=>'user.id = post.idauthor'),array('table'=>'category','join'=>'category.id = post.idcategory'));
		$data['posts'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('post.slug,title,post.type,post.content,post.tags,post.image,post.keywords,post.description,datepublish,user.name,category.name as nombre_categoria','post',$join,$where,'datepublish','DESC',12,0);
		$blog = $this->Thememodel->get_value('post',array('slug'=>'blog','post.status'=>'publicado'));
		if(!empty($blog)){
			$data['title'] = $blog->title;
			$data['description'] = $blog->description;
			$data['keywords'] = $blog->keywords;
		}
		header("Content-Type: application/rss+xml");
		//$this->load->view('template/rss', $data);
		echo '<?xml version="1.0" encoding="UTF-8" ?>';
?>
		<rss version="2.0"
		xmlns:content="http://purl.org/rss/1.0/modules/content/"
		xmlns:wfw="http://wellformedweb.org/CommentAPI/"
		xmlns:dc="http://purl.org/dc/elements/1.1/"
		xmlns:atom="http://www.w3.org/2005/Atom"
		xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
		xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
			<channel>
				<title><?= $data['title'] ?></title>
				<atom:link href="<?= base_url() ?>feed" rel="self" type="application/rss+xml" />
				<link><?= base_url() ?>feed</link>
				<description><?= $data['description'] ?></description>
				<lastBuildDate><?= date('D, d M Y H:i:s O') ?></lastBuildDate>
				<language>es</language>
				<sy:updatePeriod>hourly</sy:updatePeriod>
				<sy:updateFrequency>1</sy:updateFrequency>
		<?php foreach($data['posts'] as $post): ?>
				<item>
					<title><?= $post->title ?></title>
					<link><?= base_url().'blog/'.$post->slug ?></link>
					<comments><?= base_url().'blog/'.$post->slug ?>#comentarios</comments>
					<pubDate><?= date('D, d M Y H:i:s O',strtotime($post->datepublish)) ?></pubDate>
					<dc:creator><![CDATA[<?= $post->name ?>]]></dc:creator>
		<?php if(!empty($post->nombre_categoria)){ ?>
					<category><![CDATA[<?= $post->nombre_categoria ?>]]></category>
		<?php } ?>
					<guid isPermaLink="false"><?= base_url().'blog/'.$post->slug ?></guid>
					<description><![CDATA[<?= $post->description ?>]]></description>
					<content:encoded><![CDATA[<a href="<?= base_url() ?>blog/<?= $post->slug ?>" title="<?= $post->title ?>"><img src="<?= base_url().$post->image ?>" alt="<?= $post->title ?>"></a><?= $post->content ?>]]></content:encoded>
				</item>
		<?php endforeach; ?>
			</channel>
		</rss>
<?php
	}

	public function atom(){

		$data['feed_name'] = 'miguelgomezsa.com';
		$data['feed_url'] = base_url().'feed';
		$data['page_description'] = 'Blog de Miguel Gómez, aquí encontrarás técnicas de programación, consejos para programar. Guías para SEO. Y otros artículos sobre desarrollo.';
		$data['page_language'] = 'es'; // the language
		$data['creator_email'] = 'miguelangelgomezsa@gmail.com'; // your email
		$where = array('post.type' => 'Post','post.status'=>'publicado');
		$join = array(array('table'=>'user','join'=>'user.id = post.idauthor'),array('table'=>'category','join'=>'category.id = post.idcategory'));
		$data['posts'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('post.slug,title,post.type,post.content,post.tags,post.keywords,post.image,post.description,datepublish,user.name,category.name as nombre_categoria','post',$join,$where,'datepublish','DESC',12,0);
		$blog = $this->Thememodel->get_value('post',array('slug'=>'blog','post.status'=>'publicado'));
		if(!empty($blog)){
			$data['title'] = $blog->title;
			$data['description'] = $blog->description;
			$data['keywords'] = $blog->keywords;
		}
		header("Content-Type: application/atom+xml");
		//$this->load->view('template/rss', $data);
	echo '<?xml version="1.0" encoding="UTF-8" ?>';
?>
		<feed
			xmlns="http://www.w3.org/2005/Atom"
			xmlns:thr="http://purl.org/syndication/thread/1.0"
			xml:lang="es-ES"
			xml:base="<?= base_url() ?>/wp-atom.php"
			>
			<title type="text"><?= $data['title'] ?></title>
			<subtitle type="text"><?= $data['description'] ?></subtitle>

			<updated><?= date('Y-m-d\TH:i:s.uP') ?></updated>

			<link rel="alternate" type="text/html" href="<?= base_url() ?>" />
			<id><?= base_url()?>feed/atom</id>
			<link rel="self" type="application/atom+xml" href="<?= base_url()?>feed/atom" />
			<generator uri="http://miguelgomezsa.com" version="1.1">MiguelGómezCMS</generator>
	<?php foreach($data['posts'] as $post): ?>
			<entry>
				<author>
					<name><?= $post->name ?></name>
					<uri><?= base_url() ?></uri>
				</author>
				<title type="html"><![CDATA[Tres maneras de obtener ideas de negocio (principalmente la tercera)]]></title>
				<link rel="alternate" type="text/html" href="<?= base_url().'blog/'.$post->slug ?>" />
				<id><?= base_url().'blog/'.$post->slug ?></id>
				<updated><?= date('Y-m-d\TH:i:s.uP',strtotime($post->datepublish)) ?></updated>
				<published><?= date('Y-m-d\TH:i:s.uP',strtotime($post->datepublish)) ?></published>
		<?php if(!empty($post->nombre_categoria)){ ?>
					<category scheme="<?= base_url() ?>" term="<?= $post->nombre_categoria ?>" />
		<?php } ?>
				<summary type="html"><![CDATA[<?= $post->description ?>]]></summary>
				<content type="html" xml:base="<?= base_url().'blog/'.$post->slug ?>"><![CDATA[<a href="<?= base_url() ?>blog/<?= $post->slug ?>" title="<?= $post->title ?>"><img src="<?= base_url().$post->image ?>" alt="<?= $post->title ?>"></a><?= $post->content ?>]]></content>
			</entry>
	<?php endforeach; ?>
		</feed>
<?php
	}

}
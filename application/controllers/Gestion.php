<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gestion extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email','security'));
		$this->load->model(array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');
		$this->lang->load('formulario','castellano');
		$this->load->model('Coeficientes');
		$this->load->model('VariablesPrimarias');
		$this->load->model('CoeficientesInferencia');
		$this->load->model('DataMaster');

		// --- jjy
		$this->load->model('Algoritmo');

		check_rol($this->session->userdata('roles'),array(2,5));
	}

	public function index(){
		$data = $this->load_data();
		cargar_template('template/gestion/gestion', $data);
	}

	public function clientes(){
		$data = $this->load_data();
		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$query = $this->db->select('*')->from('users')->where('id IN ('.$subquery.')');
		$sql = $query->get_compiled_select();
		$total = $data['users_total'] =	$this->Thememodel->total_get_query($sql);
		$per_page = 12;
		$this->pagination($per_page, base_url().'gestion/clientes/', 3, $total);
		if($this->uri->segment(3) == FALSE){
			$segmento = 0;
		}else{
			$segmento = $this->uri->segment(3);
		}
		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$query1 = $this->db->select('*')->from('users')->where('id IN ('.$subquery.')')->order_by('datelastlogin','ASC')->limit($per_page,$segmento);
		$data['users'] = $query1->get()->result();
		if(!empty($data['users'])){
			$data['pagination'] = $this->pagination->create_links();
		}else{
			$data['info_search'] = '<h2>Clientes encontrados: '.$total.'</h2> <a href="'.base_url().'gestion">Puede ver el listado de clientes aquí</a>';
		}
		$data['estados'] = $this->Thememodel->get_all_values('prestamos_estados');

		cargar_template('template/gestion/clientes', $data);
	}


	public function buscar_clientes(){
		$data = $this->load_data();
		$or_like = array();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('cliente','Cliente','trim|strip_tags');
		$this->form_validation->run();
		if(!empty($this->input->post())){
			$this->session->set_userdata('buscar_por',$this->input->post());
		}
		$data['titulo'] = '';
		if(!empty($this->session->userdata('buscar_por')['cliente'])){
			$or_like['id'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['n_identificacion'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['email'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['primer_apellido'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['segundo_apellido'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['telefono'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['name'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['segundo_nombre'] = $this->session->userdata('buscar_por')['cliente'];
			$or_like['status'] = $this->session->userdata('buscar_por')['cliente'];
			$data['titulo'] .= ' para el cliente '.$this->session->userdata('buscar_por')['cliente'];
		}
		if(empty($or_like)){
			$this->session->set_userdata('info','Debe seleccionar al menos un filtro de búsqueda, puede ver todas las <a href="'.base_url().'gestion">gestion aquí</a>');
			redirect(base_url().'gestion');
		}
		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$data['users_total'] = $total = $this->db->group_start()->or_like($or_like)->group_end()->where('id IN ('.$subquery.')')->get('users')->num_rows();

		$per_page = 12;
		$this->pagination($per_page, base_url().'gestion/buscador/', 3, $total);
		if($this->uri->segment(3) == FALSE){
			$segmento = 0;
		}else{
			$segmento = $this->uri->segment(3);
		}

		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$query1 = $this->db->select('*')->from('users')->group_start()->or_like($or_like)->group_end()->where('id IN ('.$subquery.')')->order_by('datelastlogin','ASC')->limit($per_page,$segmento);

		$data['users'] = $query1->get()->result();
		if(!empty($data['users'])){
			$data['pagination'] = $this->pagination->create_links();
		}else{
			$data['info'] = '<h2>Clientes encontrados '.$data['titulo'].': '.$total.'</h2> <a href="'.base_url().'gestion">Puede ver el listado de clientes aquí</a>';
		}
		cargar_template('template/gestion/clientes', $data);
	}

	public function cliente($id=null){
		$data = $this->load_data();
		$data['error'] = '';
		if(!empty($this->input->post())){
			$this->form_validation->set_rules('name','Nombre de usuario','trim|html_escape');
			$this->form_validation->set_rules('email','Email','trim|valid_email|html_escape');
			$this->form_validation->set_rules('status','Estado','trim|html_escape');
			if($this->form_validation->run() == FALSE){
				$data['error'] .= validation_errors();
			}
			$datos = array();
			$datos['name'] = $this->input->post('name');
			$datos['email'] = $this->input->post('email');
			$datos['status'] = $this->input->post('status');
			$datos['id_calificacion'] = $this->input->post('id_calificacion');
			$datos['TS_user'] = $this->session->user_id;
			$id = $this->input->post('id');
			if($id){
				$this->Thememodel->save_value('users',$datos,array('id' =>$id));
			}else{
				$datos['dateregistered'] = date('Y-m-d H:i:s');
				$id = $this->Thememodel->insert_value('users',$datos);
			}
			$data['info'] = 'Se ha guardado con éxito';
			//guardar imagen
			$nombre_imagen = md5($datos['email']);
			$path = '-/img-users/';
			if( ! empty( $_FILES['userfile']['name'] ) ){
				$config['upload_path'] = $path;
				$config['allowed_types'] = 'gif|jpg|png';
				// EN KB
				$config['max_size'] = '3000';
				$config['max_width'] = '5000';
				$config['max_height'] = '5000';
				$config['file_name'] = $nombre_imagen;
				$this->load->library('upload', $config);
				if ( ! $this->upload->do_upload() ){
					$data['error'] .= $this->upload->display_errors();
					if( ! isset($data['error']) || $data ['error'] == '' ){
						$data['error'] .= 'No se ha subido correctamente la imagen. Debe ser de menos de 3 MB';
					}
				}else{
					$file_name = $this->upload->data();
					$url_imagen = $path.$file_name['file_name'];
					$user = $this->Thememodel->get_value('users',array('id' => $id));
				//$foto_anterior = $this->Usermodel->have_image( $username );
					if( $user->image != '' ){
						unlink($user->image);
					}
					$imagen = array('image' => $path.$file_name['file_name']);
					$this->Thememodel->save_value('users',$imagen,array('id' =>$id) );
					$this->session->set_userdata('image', $path.$file_name['file_name']);
				}
			}
		}
		if($id){
			$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
			$subquery = $this->db->get_compiled_select();
			$query1 = $this->db->select('*')->from('users')->where('id IN ('.$subquery.')')->where(array('id' => $id));
			$data['user'] = $query1->get()->row();
            $data['calificaciones'] = $this->Thememodel->get_all_values_pag('calificacion',10,0);
			$data['prestamo_actual'] = $this->Thememodel->get_query("SELECT pe.nombre, pe.color, p.* FROM prestamos as p LEFT JOIN prestamos_estados as pe ON pe.id=p.prestamo_estado_id WHERE id_cliente = $id AND p.prestamo_actual=1");
		}
		cargar_template('template/gestion/cliente', $data);
	}

	public function prestamo($id=null){


   if(is_numeric($id)){

	$data = $this->load_data();
	
		


		if(is_numeric($id)){
			$data = $this->load_data();

			$join = array(array('table'=>'users','join'=>'users.id = prestamos.id_cliente'), array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'));
//				array('table' => 'prestamos_estados_historico','join'=>'prestamos_estados_historico.idprestamo = prestamos.id_prestamo'));
			$where = array('prestamos.id_prestamo' => $id);

			$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);
            
            if(!empty($data['prestamo'])){
			$id_cliente = $data['prestamo'][0]->id_cliente;
			$data['cliente'] = $this->Thememodel->get_value('users', array('id' => $id_cliente));
            $data['calificacion'] = $this->Thememodel->get_query("SELECT c.tasa_interes FROM `calificacion` as c INNER JOIN users as u WHERE u.id_calificacion = c.id and u.id = $id_cliente");



			//$this->Thememodel->get_all_values_leftjoin_where_order_pag('users.name, users.primer_apellido,users.n_identificacion','users',$join,$where,'id_prestamo','ASC',10,0);


		

			$join = array(array('table'=>'prestamos','join'=>'prestamos.id_prestamo = detalle_prestamo.id'));
			$where = array('detalle_prestamo.id_prestamo' => $id);
            

			//$data['cantidad_quincenas'] = $this->Thememodel->total_where_where_in('detalle_prestamo',$where,null,null);
			


            $data['total_quincenas'] = $this->Thememodel->get_query("SELECT SUM(cuota) as total, COUNT(id_prestamo) as cantidad FROM detalle_prestamo WHERE id_prestamo = $id");


		    $estados_prestamo = $data['prestamo'][0]->prestamo_estado_id;
		     $join = array(array('table'=>'clasificacion_prestamo_estado','join'=>'clasificacion_prestamo_estado.id_estado_prestamo = prestamos_estados.id'),array('table'=>'clasificacion_estados','join'=>'clasificacion_estados.id = clasificacion_prestamo_estado.id_clasificacion') );
			if( $estados_prestamo==1 || $estados_prestamo==2 || $estados_prestamo==3 || $estados_prestamo==5 || $estados_prestamo==6 || $estados_prestamo==7)
			{
			   
			    $where = array('clasificacion_prestamo_estado.id_clasificacion' => 1);
			    $data['estados'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('prestamos_estados.id,prestamos_estados.nombre','prestamos_estados',$join,$where,'prestamos_estados.id','ASC',10);

			}
			if($estados_prestamo==8 || $estados_prestamo==9 || $estados_prestamo==10 || $estados_prestamo==11 || $estados_prestamo==12 || $estados_prestamo==13)
			{
				
			    $where = array('clasificacion_prestamo_estado.id_clasificacion' => 2);
			    $data['estados'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('prestamos_estados.id,prestamos_estados.nombre','prestamos_estados',$join,$where,'prestamos_estados.id','ASC',10);
			}

			if($estados_prestamo==8 || $estados_prestamo==9)
			{

			    $where = array('clasificacion_prestamo_estado.id_clasificacion' => 3);
			    $data['estados'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('prestamos_estados.id,prestamos_estados.nombre','prestamos_estados',$join,$where,'prestamos_estados.id','ASC',10);
			}
			
			// --- jjy
			$data['quincenas_estados'] 	   = $this->Thememodel->get_all_values('quincenas_estados');
			$data['reporte_pagos_estados'] = $this->Thememodel->get_all_values('reporte_pagos_estados');
			$data['perfil_social'] 		   = $this->Coeficientes->ProcessCoeficientes("Perfil social");			
			$data['maestros_inferencia']   = $this->CoeficientesInferencia->GetMaestrosInferencia();
			$data['preguntas'] 			   = $this->DataMaster->GetData();
			$data['respuestas'] 		   = $this->DataMaster->GetDataResponses($data['prestamo']);
			
			// --- jjy --- algoritmo para el calculo de Grupos de Variables
			$idP = $id; // $id del prestamo recibido por parametros

			$data['variables_perfil_social']   = $this->Algoritmo->getCalculosVariables( $idP, "Perfil social" );
			$data['variables_capacidad_pago']  = $this->Algoritmo->getCalculosVariables( $idP, "Capacidad de Pago" );
			$data['variables_responsabilidad'] = $this->Algoritmo->getCalculosVariables( $idP, "Responsabilidad" );
			$data['variables_honestidad']      = $this->Algoritmo->getCalculosVariables( $idP, "Honestidad" );
			$data['variables_trabajo'] 		   = $this->Algoritmo->getCalculosVariables( $idP, "Trabajo" );
			$data['variables_macroeconomicas'] = $this->Algoritmo->getCalculosVariables( $idP, "Macroeconomicas" );
			
			// Arbol de Aprobacion - Algoritmo -- jjy
			// ----- EJEMPLO DE USO ----------
			$data['respuesta_algoritmo'] = $this->Algoritmo->getArbolDeAprobacion( $idP );


			/*$join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos_estados_historico.idestado'));
			$where = array('idprestamo' => $id);
			$data['estados_prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('prestamos_estados.nombre,prestamos_estados.color,prestamos_estados_historico.observaciones, prestamos_estados_historico.created_at as fecha_actualizacion','prestamos_estados_historico',$join,$where,'idestado','ASC',99);*/


            
			$data['estados_prestamo_actual'] = $this->Thememodel->get_query("SELECT prestamos_estados.nombre,prestamos_estados.color,prestamos_estados_historico.observaciones, prestamos_estados_historico.created_at as fecha_actualizacion from prestamos_estados_historico,prestamos_estados WHERE prestamos_estados.id = prestamos_estados_historico.idestado and prestamos_estados_historico.idprestamo = $id and prestamos_estados_historico.id = (SELECT MAX(id) FROM `prestamos_estados_historico` WHERE idprestamo = $id) order by prestamos_estados_historico.id desc");			
			$data['estados_prestamo'] = $this->Thememodel->get_query("SELECT prestamos_estados.nombre,prestamos_estados.color,prestamos_estados_historico.observaciones, prestamos_estados_historico.created_at as fecha_actualizacion from prestamos_estados_historico,prestamos_estados WHERE prestamos_estados.id = prestamos_estados_historico.idestado and prestamos_estados_historico.idprestamo = $id and prestamos_estados_historico.id <> (SELECT MAX(id) FROM `prestamos_estados_historico` WHERE idprestamo = $id) order by prestamos_estados_historico.id desc");


			$data['prestamos'] = $this->Thememodel->get_query("SELECT * FROM `prestamos` JOIN `prestamos_estados` ON prestamos.prestamo_estado_id=prestamos_estados.id WHERE id_cliente=$id_cliente");
         	$data['estados_busqueda'] = $this->Thememodel->get_all_values('prestamos_estados'); 

			$data['fecha_finalizacion_prestamo'] = $this->Thememodel->get_query("SELECT fecha_vencimiento FROM `detalle_prestamo` where id_prestamo=$id order by id desc LIMIT 1");

			
			cargar_template('template/gestion/prestamo', $data);

			
           }
           else{
           	 redirect(base_url().'template/404');
           }
		}
	}
}

	public function prestamo_guardar($id=null){
		if(is_numeric($id)){

			$prestamo = $this->Thememodel->get_all_values_where('prestamos',array('id_prestamo' => $id ));

			//var_dump($prestamo[0]->datos);


			//echo sizeof($this->input->post());

			$total = sizeof($this->input->post()) / 6;
			$datos = array();
			for ($i=0; $i < $total; $i++) {
				$prestamo_datos = array(
					'campo' => $this->input->post($i.'_campo'),
					'valor' => $this->input->post($i.'_valor'),
					'puntuacion' => $this->input->post($i.'_puntuacion'),
					'vp' => $this->input->post($i.'_vp'),
					'vi' => $this->input->post($i.'_vi')
					);
				$datos[] = json_encode($prestamo_datos);
			}
		//	$prestamo[0]->datos = json_encode($this->input->post());
			$total = sizeof($datos);
			$json = '';
			for ($i=0; $i < $total; $i++) {
				$json .= $datos[$i].',';
			}
			$json = trim($json,',');

			/*
			var_dump('['.$json.']');
			var_dump(json_encode($datos));
			*/
			$prestamo[0]->datos = '['.$json.']';

			$this->Thememodel->save_value('prestamos',array('datos' => $prestamo[0]->datos ),array('id_prestamo' => $id));

		}
	}

	public function prestamos_estados(){
		$data = $this->load_data();
		$data['prestamos_estados'] = $this->Thememodel->get_all_values_order('prestamos_estados','created_at','DESC');
		cargar_template('template/gestion/prestamos-estados', $data);
	}
	public function prestamo_estado($id=null){
		$data = $this->load_data();
		$data['prestamos'] = $this->Thememodel->get_all_values_order('prestamos','created_at','DESC');
		$subquery = $this->db->select('id')->from('users')->where('id_rol',2);
		$subquery = $this->db->get_compiled_select();
		$query = $this->db->select('*')->from('users')->where('id IN ('.$subquery.')');
		$data['usuarios'] = $query->get()->result();
		cargar_template('template/gestion/prestamo-estado', $data);
	}
	public function prestamo_estado_guardar(){
		$data = $this->load_data();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('nombre','Nombre','trim|required|strip_tags|html_escape');
		$this->form_validation->set_rules('color','color','trim|required|strip_tags|html_escape');
		if($this->form_validation->run() == FALSE){
			$this->session->set_userdata('error',validation_errors());
			if($this->input->post('id')){
				redirect(base_url().'gestion/prestamo-estado/'.$this->input->post('id'));
			}else{
				cargar_template('template/gestion/prestamo-estado', $data);
			}
		}else{
			$datos['nombre'] = $this->input->post('nombre');
			$datos['color'] = $this->input->post('color');
			if($this->input->post('id')){
				$data['prestamo'] = $this->Thememodel->get_value('prestamos_estados',array('id' => $id));
				if( ! empty($data['prestamo']) ){
					$this->Thememodel->save_value('prestamos_estados',$datos,array('id'=>$id));
					$this->session->set_userdata('info', 'La información se ha guardado');
				}else{
					$this->session->set_userdata('error', 'Ha ocurrido un error al guardar');
				}
				redirect(base_url().'gestion/prestamo-estado/'.$id);
			}else{
				$datos['created_at'] = date('Y-m-d H:i:s');
				$id = $this->Thememodel->insert_value('prestamos_estados',$datos);
				if($id){
					$this->session->set_userdata('info', 'La información se ha guardado');
				}else{
					$this->session->set_userdata('error', 'Ha ocurrido un error al crear');
				}
				redirect(base_url().'gestion/prestamo-estado/'.$id);
			}
		}
	}

	public function prestamo_estado_borrar($id=null){
		if( $id ){
			$prestamos_estado = $this->Thememodel->get_value('prestamos_estados', array('id' => $id));
			if($this->Thememodel->del_value('prestamos_estados', array('id' => $id ))){
				$this->session->set_userdata('info', 'Se ha borrado correctamente');
			}else{
				$this->session->set_userdata('error', 'Ha ocurrido algún problema');
			}
		}
		redirect(base_url().'gestion/prestamos_estados');
	}
	public function prestamo_pdf($id=null){
		if(is_numeric($id)){

			$data = $this->load_data();

			$join = array(array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
//				array('table' => 'prestamos_estados_historico','join'=>'prestamos_estados_historico.idprestamo = prestamos.id_prestamo'));
			$where = array('prestamos.id_prestamo' => $id);
			$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);

			$data['estados'] = $this->Thememodel->get_all_values('prestamos_estados');
			$join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos_estados_historico.idestado'));
			$where = array('idprestamo' => $id);
			$data['estados_prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos_estados_historico',$join,$where,'idestado','ASC',99);
			cargar_template('template/gestion/prestamo', $data);
		}
	}
	private function load_data(){
		$id = $this->session->userdata('user_id');
		include 'Themeoptions.php';
		return $data;
	}
	private	function pagination($per_page, $url, $uri_segment, $total){
		$this->load->library('pagination');
		$config['base_url'] = $url;
		$config['total_rows'] = $total;
		$config['per_page'] = $per_page;
		$config['use_page_numbers'] = TRUE;
		$config['num_links'] = 5;
		$config['uri_segment'] = $uri_segment;
		$config['first_link'] = '<i class="fa fa-step-backward"></i>';$config['last_link'] = '<i class="fa fa-step-forward"></i>';$config['next_link'] = 'Siguiente';
		$config['prev_link'] = 'Anterior';$config['full_tag_open'] = '<ul class="pagination">';$config['full_tag_close'] = '</ul>';$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';$config['last_tag_open'] = '<li>';$config['last_tag_close'] = '</li>';$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';$config['num_tag_open'] = '<li>';$config['num_tag_close'] = '</li>';$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';$config['next_tag_open'] = '<li>';$config['next_tag_close'] = '</li>';
		return $this->pagination->initialize( $config );
	}



   





}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gestion_ajax extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email','security'));
		$this->load->model(array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');
		//check_rol($this->session->userdata('roles'),array(2,5));
	}


	
	/**********************************************************************************************
	                      Función para actualizar el estado de un prestamo
	*********************************************************************************************/
	public function prestamos_estados_asignar(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('idestado','Estado','trim|required|strip_tags|html_escape');
		$this->form_validation->set_rules('idprestamo','Préstamo','trim|required|strip_tags|html_escape');
		$this->form_validation->set_rules('observaciones','observaciones','trim|strip_tags|html_escape');
	    if($this->form_validation->run() == FALSE){
			echo validation_errors();
		}else{
            
            $id_prestamo = $this->input->post('idprestamo');
            $fecha_final_prestamo = date("Y-m-d", strtotime($this->input->post('fecha_final_prestamo')));


			      $id = $this->session->userdata('user_id');
		       //Cantidad de historicos de estado de préstamo
            $numero_historicos = $this->Thememodel->get_query("SELECT COUNT(idprestamo) as cantidad FROM prestamos_estados_historico WHERE idprestamo = $id_prestamo");
            
            $datos_prestamo = $this->Thememodel->get_query("SELECT prestamo_estado_id,tiempo, fecha_efectivo_prestamo FROM `prestamos` WHERE id_prestamo=$id_prestamo");
             
            $total_quincenas = $this->Thememodel->get_query("SELECT ROUND(SUM(cuota),2) as total, COUNT(id_prestamo) as cantidad FROM detalle_prestamo WHERE id_prestamo = $id_prestamo");
               
            $fecha_efectivo_prestamo = date("Y-m-d", strtotime($datos_prestamo[0]->fecha_efectivo_prestamo));  
            $fecha_ultima_quincena = $this->Thememodel->get_query("SELECT fecha_vencimiento FROM `detalle_prestamo` where id_prestamo=$id_prestamo order by id desc LIMIT 1");

            if(!empty($fecha_ultima_quincena)){
              $fecha_ultima_quincena = date("Y-m-d", strtotime($fecha_ultima_quincena[0]->fecha_vencimiento));
            }else{
              $fecha_ultima_quincena = $fecha_efectivo_prestamo;
            }

            $total_prestamo_acumulado = (float)$total_quincenas[0]->total;
            $total_prestamo = (float)$this->input->post('total_prestamo');
            $cantidad_quincenas_acumuladas = (int)$total_quincenas[0]->cantidad;
            $cantidad_quincena = (int)$datos_prestamo[0]->tiempo;
            $id_estado = $this->input->post('idestado');
            $numero_historicos = (int)$numero_historicos[0]->cantidad;
            $estado_actual_prestamo = $datos_prestamo[0]->prestamo_estado_id;
           
           if($numero_historicos==0){
              //datos del estado inicial
              $datos['idestado'] = $estado_actual_prestamo;
              $datos['observaciones'] = "";
              $datos['idprestamo'] = $id_prestamo;
              $datos['TS_user'] = $this->session->user_id;
              $datos['created_at'] = date('Y-m-d H:i:s');
                
              //datos del segundo estado actualizado  
              $datos1['idestado'] =  $id_estado;
              $datos1['observaciones'] = $this->input->post('observaciones');
              $datos1['idprestamo'] = $id_prestamo;
              $datos1['TS_user'] = $this->session->user_id;
              $datos1['created_at'] = date('Y-m-d H:i:s');

           }
           else{
               $datos['idestado'] =  $id_estado;
               $datos['observaciones'] = $this->input->post('observaciones');
               $datos['idprestamo'] = $id_prestamo;
               $datos['TS_user'] = $this->session->user_id;
               $datos['created_at'] = date('Y-m-d H:i:s');
           }
			      
          
           
            $data = array('prestamo_estado_id' => $this->input->post('idestado'), 'TS_user'=>$id, 'monto_total_prestamo' => $total_prestamo);
             
             if($id_estado!=$estado_actual_prestamo){
            if($id_estado==1){
             
             if($total_prestamo_acumulado==$total_prestamo && $cantidad_quincenas_acumuladas==$cantidad_quincena){

               if($fecha_final_prestamo==$fecha_ultima_quincena){
                 if($numero_historicos==0){
               $save_prestamos = $this->Thememodel->save_value('prestamos',$data,array('id_prestamo' => $id_prestamo));
		
		             $save_prestamos_estados_historico = $this->Thememodel->insert_value('prestamos_estados_historico',$datos);
                  $this->Thememodel->insert_value('prestamos_estados_historico',$datos1);
              }else{
               $save_prestamos = $this->Thememodel->save_value('prestamos',$data,array('id_prestamo' => $id_prestamo));
    
             $save_prestamos_estados_historico = $this->Thememodel->insert_value('prestamos_estados_historico',$datos);
           }

               $idCliente = $this->input->post('idCliente');
		           $id_estado_prestamo_actual = $this->input->post('idestado');
		
           if($save_prestamos_estados_historico ==TRUE && $save_prestamos == TRUE){
           $this->enviarMailEstadoPrestamo($id_estado_prestamo_actual,$idCliente);
			   echo json_encode(array("status" => TRUE));
           }
           else{
           	  echo json_encode(array("status" => FALSE));
           }
        
           }else{
                  echo json_encode(array("status" => "ERROR_AJUSTE_FECHA"));
           }

         }
         else{
              echo json_encode(array("status" => 'ERROR_OPERACION_PRESTAMO'));
         }
		     


         }else{
             if($numero_historicos==0){
             $save_prestamos = $this->Thememodel->save_value('prestamos',$data,array('id_prestamo' => $id_prestamo));
    
             $save_prestamos_estados_historico = $this->Thememodel->insert_value('prestamos_estados_historico',$datos);
              $this->Thememodel->insert_value('prestamos_estados_historico',$datos1);
           }else{
               $save_prestamos = $this->Thememodel->save_value('prestamos',$data,array('id_prestamo' => $id_prestamo));
    
             $save_prestamos_estados_historico = $this->Thememodel->insert_value('prestamos_estados_historico',$datos);
           }

            $idCliente = $this->input->post('idCliente');
            $id_estado_prestamo_actual = $this->input->post('idestado');
    
           if( $save_prestamos_estados_historico ==TRUE && $save_prestamos == TRUE){
              $this->enviarMailEstadoPrestamo($id_estado_prestamo_actual,$idCliente);
         echo json_encode(array("status" => TRUE));
           }
           else{
              echo json_encode(array("status" => FALSE));
           }

         }

     }else{
          echo json_encode(array("status" => "ERROR_ESTADO_ACTUAL"));
     }


    }
	}



	public function enviarMailEstadoPrestamo($idestado,$idCliente){


          $data['user'] = $this->Thememodel->get_value('users', array('id' => $idCliente ));
          $destinatario = $data['user']->email;
          $data['email_template'] = $this->Thememodel->get_all_values('email_template');
          $web = 'CliCREDIT Panamá';
          $from = "info@pa.clicredit.com";
          switch ($idestado) {
          	case '1':
          	   $data['email'] = $data['email_template'][12];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
          	 break;
          	 case '2':
          	   $data['email'] = $data['email_template'][13];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;
             case '3':
          	   $data['email'] = $data['email_template'][14];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;
             case '5':
          	   $data['email'] = $data['email_template'][15];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;
             case '6':
          	   $data['email'] = $data['email_template'][16];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;
             case '8':
          	   $data['email'] = $data['email_template'][6];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;
             case '9':
          	   $data['email'] = $data['email_template'][7];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;
             case '10':
          	   $data['email'] = $data['email_template'][8];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;
             case '11':
          	   $data['email'] = $data['email_template'][9];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;
             case '12':
          	   $data['email'] = $data['email_template'][10];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;
             case '13':
          	   $data['email'] = $data['email_template'][11];
               $asunto = $data['email']->asunto;
               $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
               $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
             break;

          	
          }


	}


   public function enviarReportePago1(){
    $tiempo = $this->Thememodel->get_query("SELECT tiempo FROM `prestamos` WHERE id_prestamo=237");
             $total_quincenas = $this->Thememodel->get_query("SELECT SUM(cuota) as total, COUNT(id_prestamo) as cantidad FROM detalle_prestamo WHERE id_prestamo = 237");
           
            $total_prestamo_acumulado = (float)$total_quincenas[0]->total;
            $total_prestamo = (float)$this->input->post('total_prestamo');
            $cantidad_quincenas_acumuladas = (int)$total_quincenas[0]->cantidad;
            $cantidad_quincena = (int)$tiempo[0]->tiempo;

            var_dump($total_prestamo_acumulado);
   
  }




}
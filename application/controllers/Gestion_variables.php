<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Gestion_variables extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email','security'));
		$this->load->model(array('Thememodel','Algoritmo'));
		$this->load->library('form_validation');
		$this->lang->load('formulario','castellano');
        check_rol($this->session->userdata('roles'), array(1,2,3,4,5,6));
	}


	public function index(){

		$data['version_actual'] = $this->Thememodel->get_query("SELECT codigo from version_algoritmo where version_actual = 1");
		cargar_dashboard('dashboard/algoritmo/gestion_variables', $data);
	}


	public function coeficienteInferencia(){
         $data['cantidad_coeficientes'] = $this->Thememodel->get_query("SELECT COUNT(id) as cantidad FROM maestro_coeficiente_inferencia");
         cargar_dashboard('dashboard/algoritmo/coeficientes_inferencia', $data);
	}

	
	public function obtenerCoeficientesInferencia(){
       $data['data'] = $this->Thememodel->get_query("SELECT mc.id,mc.description,mc.coeficiente,mc.observacion, DATE_FORMAT(mc.TS_timestamp,'%d-%m-%Y %H:%i:%s') as ultima_actualizacion,mc.status, CONCAT(us.name,' ',us.primer_apellido) as name from maestro_coeficiente_inferencia as mc LEFT JOIN users as us ON mc.TS_user = us.id order by mc.id ASC");
		   if(!empty($data)){
          echo json_encode($data);
          }else{
              $data['data']=[];
              echo json_encode($data);
          }
		
	}


	public function crearCoeficienteInferencia(){
		
	 $this->form_validation->set_rules('etiqueta','Etiqueta','trim|strip_tags|html_escape|required');
	 $this->form_validation->set_rules('coeficiente','Coeficiente','trim|strip_tags|html_escape|required');
	 $this->form_validation->set_rules('observacion','Notas','trim|strip_tags|html_escape|required');
	 if($this->form_validation->run()){
	 	$data = array('description'=>$this->input->post('etiqueta'),
	 		          'coeficiente' => $this->input->post('coeficiente'),
	                  'observacion' => $this->input->post('observacion'),
	                  'TS_user' => $this->session->userdata('user_id'),
	                  'TS_timestamp' => date("Y-m-d H:i:s"),
	                  'status' => 1);
         $this->Thememodel->insert_value('maestro_coeficiente_inferencia',$data);
         echo json_encode(array("status" => TRUE));

	 }else{
	 	echo json_encode(array("status" => FALSE));
	 }

	}


	public function actualizarCoeficienteInferencia(){
		 $this->form_validation->set_rules('etiqueta','Etiqueta','trim|strip_tags|html_escape|required');
	 $this->form_validation->set_rules('coeficiente','Coeficiente','trim|strip_tags|html_escape|required');
	 $this->form_validation->set_rules('observacion','Notas','trim|strip_tags|html_escape|required');
	 if($this->form_validation->run()){
	 	$data = array('description'=>$this->input->post('etiqueta'),
	 		          'coeficiente' => $this->input->post('coeficiente'),
	                  'observacion' => $this->input->post('observacion'),
	                  'TS_user' => $this->session->userdata('user_id'),
	                  'TS_timestamp' => date("Y-m-d H:i:s"),
	                   'status' => 1);
        $dato['status'] = 0;
	 	$this->Thememodel->insert_value('maestro_coeficiente_inferencia',$data);
	 	$this->Thememodel->save_value('maestro_coeficiente_inferencia',$dato,array('id' => $this->input->post('id_coeficiente')));
         echo json_encode(array("status" => TRUE));

	 }else{
	 	echo json_encode(array("status" => FALSE));
	 }

	}

	public function obtenerVariablesMacroeconomicas(){
		 $data['data'] = $this->Thememodel->get_query("SELECT mc.id,mc.description,mc.coeficiente,mc.observacion, DATE_FORMAT(mc.TS_timestamp,'%d-%m-%Y %H:%i:%s') as ultima_actualizacion, CONCAT(us.name,' ',us.primer_apellido) as name from maestro_coeficiente_inferencia as mc LEFT JOIN users as us ON mc.TS_user = us.id order by mc.id");
		   if(!empty($data)){
          echo json_encode($data);
          }else{
              $data['data']=[];
              echo json_encode($data);
          }
	}


	public function obtenerVersionesAlgoritmo(){
		$data['data'] = $this->Thememodel->get_query("SELECT va.id,va.codigo,DATE_FORMAT(va.fecha_inicio,'%d-%m-%Y %H:%i:%s') as fecha_inicio, DATE_FORMAT(va.fecha_inactividad,'%d-%m-%Y %H:%i:%s') as fecha_inactividad,va.nombre_version,va.version_variables, DATE_FORMAT(va.TS_timestamp,'%d-%m-%Y %H:%i:%s') as ultima_actualizacion, CONCAT(us.name,' ',us.primer_apellido) as name from version_algoritmo as va LEFT JOIN users as us ON va.TS_user = us.id order by va.id asc");
		 if(!empty($data)){
          echo json_encode($data);
          }else{
              $data['data']=[];
              echo json_encode($data);
          }

	}



}
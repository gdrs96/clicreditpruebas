<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Image extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->library('image_lib');
	}

	public function img(){
		$url =	explode('/',$this->uri->uri_string());
		$file = '';
		$width = $url[1];
		for ($i=2; $i < sizeof($url); $i++){
			$file .= $url[$i].'/';
		}
		$file = trim($file,'/');
		if(file_exists($file)){
			$image_data = getimagesize($file);
			$img_width = $image_data[0];
			$img_height = $image_data[1];

			// calculamos los tamaños para no perder la proporción de la imagen
			$height = $width * $img_height / $img_width;
			// creamos la imágen dependiendo de el tipo
			switch ( $image_data[2] ) {
				case IMAGETYPE_GIF:  $img = @imagecreatefromgif($file); break;
				case IMAGETYPE_JPEG: $img = @imagecreatefromjpeg($file); break;
				case IMAGETYPE_PNG:  $img = @imagecreatefrompng($file); break;
			}
			// creamos una copia de la imagen con el tamaño que necesitamos
			$thumb = imagecreatetruecolor($width, $height);
			// enviamos al encabezado que esta es una imágen y le decimos el tipo
			header('Content-type: ' . image_type_to_mime_type($image_data[2]));
			imagecopyresampled($thumb, $img, 0, 0, 0, 0, $width, $height, $image_data[0], $image_data[1]);
			// obtenemos los datos del archivo binario dependiendo de su tipo

			switch ( $image_data[2] ) {
				case IMAGETYPE_GIF:  imagegif($thumb); break;
				case IMAGETYPE_JPEG: imagejpeg($thumb); break;
				case IMAGETYPE_PNG:  imagepng($thumb); break;
			}
			imagedestroy($thumb);
		}
	}

}
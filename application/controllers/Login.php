<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','email','security'));
		$this->load->model( array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');
	}

	public function index(){
		if( 
			check_rol_view($this->session->userdata('roles'), 1) ||
			check_rol_view($this->session->userdata('roles'), 2) ||
			check_rol_view($this->session->userdata('roles'), 3) ||
			check_rol_view($this->session->userdata('roles'), 4) ||
			check_rol_view($this->session->userdata('roles'), 5) ||
			check_rol_view($this->session->userdata('roles'), 6)
		){
			redirect( base_url().'dashboard');
		}else{
			$data = $this->view_alerts();
			$banner_image = $this->Thememodel->get_value('options',array('option_name' => 'banner_image'));
			$data['banner_image'] = $banner_image->option_value;
			cargar_dashboard_nomenu('dashboard/login/login', $data);
		}
	}

	public function login_in( $info = null ){
		$data = $this->view_alerts();
		$this->session->set_userdata('info', $info);
		$num_maximo_intentos = 3;
		$tiempo_espera = 300;
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('password','contraseña','trim|required');
		if( $this->session->userdata('intentos') >= $num_maximo_intentos){
			if( strtotime('now') < ($this->session->userdata('time_ultimo_intento')+$tiempo_espera) ){
				$data['error'] = 'Ha intenteado iniciar sesión demasiadas veces.<br> Pruebe dentro de '.($tiempo_espera/60).' minutos';
				cargar_dashboard_nomenu('dashboard/login/login',$data);
				return;
			}
		}
		if($this->form_validation->run() == FALSE){
			$data['error'] = validation_errors();
			cargar_dashboard_nomenu('dashboard/login/login',$data);
			return;
		}else{
			$usuario = $this->_is_registered($this->input->post('email'),$this->input->post('password'));
			if( $usuario ){
				if($usuario->status != 'activo'){
					$data['error'] = 'Su usuario no está activo contacte con el adminitrador';
					cargar_dashboard_nomenu('dashboard/login/login');
					return;
				}
				$roles = $this->Thememodel->get_select('id_rol','users',array('user_id'=>$id));
				$rol = $roles[0];
				if($rol){
					$permisos = $this->Thememodel->get_select('permisos','roles',array('id'=>$rol->id_rol));
					$roles_actuales = array();
					foreach($roles as $key){
						$roles_actuales[] = $key->id;
					}
				}
				$this->session->unset_userdata('intentos');
				$this->session->set_userdata('roles', $roles_actuales );
				$this->session->set_userdata('user_id', $usuario->id);
				$this->session->set_userdata('email', $usuario->email);
				$this->session->set_userdata('type', $usuario->type );
				$this->session->set_userdata('nicename', $usuario->nicename );
				$this->session->set_userdata('image', $usuario->image );
				$this->session->unset_userdata('intentos');
				$this->Thememodel->save_value('user',array('datelastlogin' => date('Y-m-d H:i:s')), array('id' => $usuario->id ));

				if($usuario->name != ''){
					$this->session->set_userdata('info', "Hola $usuario->name. Bienvenido !!");
				}else{
					$this->session->set_userdata('info', "Hola $usuario->nicename. Bienvenido !!");
				}
	/*
				$cookie = array(
					'name'   => 'Usuario',
					'value'  => $usuario->nicename,
					'expire' => '86500',
					'domain' => base_url(),
					'path'   => '/',
					'prefix' => 'miguelgomezsa.com',
					'secure' => TRUE
				);
				$this->input->set_cookie( $cookie );
	*/
				redirect(base_url().'dashboard');
			}else{
				if(empty($this->session->userdata('intentos'))){
					$this->session->set_userdata('intentos',1);
					$this->session->set_userdata('time_ultimo_intento',strtotime('now'));
				}else{
					$intentos = $this->session->userdata('intentos')+1;
					$this->session->set_userdata('intentos',$intentos);
					$this->session->set_userdata('time_ultimo_intento',strtotime('now'));
				}
				$numero_intentos_restantes = $num_maximo_intentos-$this->session->userdata('intentos');
				if($numero_intentos_restantes == 0){
					$data['error'] = 'No le quedan más intentos, espere '.($tiempo_espera/60).'minutos';
				}else{
					$data['error'] = 'Por favor introduzca un usuario y contraseña válidos. <br> Número de intentos restantes: '.$numero_intentos_restantes;
				}
				cargar_dashboard_nomenu('dashboard/login/login',$data);
			}
		}
	}


	

	public function _is_registered( $email,$password ){
		$usuario = $this->Thememodel->get_value('users', array('email' => $email));
		if( !empty($usuario) && ($usuario->pass == md5( $password ))){
			/*$id = $usuario->id;
			$type = $usuario->type;
			return $id.'-'.$type;
			*/
			return $usuario;
		}
		else{
			return FALSE;
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect( base_url().'login');
	}
	private function view_alerts(){
		$data = array();
		if($this->session->userdata('info')){
			$data['info'] = $this->session->userdata('info');
			$this->session->unset_userdata('info');
		}
		if($this->session->userdata('error')){
			$data['error'] = $this->session->userdata('error');
			$this->session->unset_userdata('error');
		}
		return $data;
	}

	public function validarEmail(){
		
	}

}
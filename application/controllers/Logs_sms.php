<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Logs_sms extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email','security'));
		$this->load->model(array('Thememodel'));
		$this->load->library('form_validation');
		$this->lang->load('formulario','castellano');
    check_rol($this->session->userdata('roles'), array(1,2,3,4,5,6));
	}


	public function index(){
		$versions = $this->Thememodel->get_all_values('versiones');
		$data['last_version'] = end($versions);
		cargar_dashboard('dashboard/logs_sms', $data);
	}


	public function obtenerLogsSms(){
		
		 $logs_sms = $this->Thememodel->get_query("SELECT * FROM `logs_sms` ORDER BY `id` DESC");
         foreach ($logs_sms as $key => $value) {
         
          //Response api
          $descripcion = json_decode($value->descripcion);
          
          if(!empty($descripcion->result[0]->sms_id)){
              $sms_id =  $descripcion->result[0]->sms_id;
          }else{
               $sms_id = "";
          } 

          if(!empty($descripcion->result[0]->error_id)){
              $error_id = $descripcion->result[0]->error_id;
          }else{
              $error_id = "";
          }

          if(!empty($descripcion->result[0]->error_msg)){
              $error_msg = $descripcion->result[0]->error_msg;
          }else{
              $error_msg = "";
          }                                           // date('d-m-y H:i:s', strtotime($value->fecha))


           $data[] =  array('id'=>$value->id,'fecha'=>date("d-m-y H:i:s", strtotime($value->fecha)),'status' => $value->status, 'terminal'=>$value->telefono, 'codigo_verificacion'=>$value->codigo_verificacion,'id_prestamo'=>$value->id_prestamo,'sms_id' => $sms_id,'error_id'=>$error_id, 'error_msg'=>$error_msg);

           
          
         }
         if(!empty($data)){
          echo json_encode(array('data'=>$data));
          }else{
              $data['data']=[];
              echo json_encode($data);
          }

	}



	public function activarEnvioSmsManual(){
		$sms_manual = $this->input->post('sms_manual');
    $id = $this->input->post('id_log');
         if(isset($sms_manual)){
           $data['status'] = 'send_manual';
           $this->Thememodel->save_value('logs_sms',$data,array('id' => $id ));
		   echo json_encode(array("status" => TRUE));
		 }else{

         $log_sms = $this->Thememodel->get_query("SELECT * FROM `logs_sms` where `id` = $id");
         $descripcion = json_decode($log_sms[0]->descripcion);
         $data['status'] = $descripcion->result[0]->status;
         $this->Thememodel->save_value('logs_sms',$data,array('id' => $id ));
		   echo json_encode(array("status" => FALSE));
		 }
	}


 

}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','file','file','security'));
		$this->load->model(array('Thememodel','Emailmodel'));
		check_rol($this->session->userdata('roles'),array(2,5));
	}

	public function index($id){
	}

	function test($id){
		if(is_numeric($id)){
			$data = $this->load_data();
			$this->load->library('dompdf_gen');

			$join = array(array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
//				array('table' => 'prestamos_estados_historico','join'=>'prestamos_estados_historico.idprestamo = prestamos.id_prestamo'));
			$where = array('prestamos.id_prestamo' => $id);
			$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);

			$data['estados'] = $this->Thememodel->get_all_values('prestamos_estados');
			$join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos_estados_historico.idestado'));
			$where = array('idprestamo' => $id);
			$data['estados_prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos_estados_historico',$join,$where,'idestado','ASC',99);

			$html = $this->load->view('template/header',$data, true);
			$html .= $this->load->view('template/gestion/prestamo',$data, true);
			$html .= $this->load->view('template/footer',$data, true);

			$html = '
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
			"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"><head>
			<title></title>
		</head>
		<body>
			<table>
				<tr>
					<td><img src="images/dompdf_simple.png"/></td>
					<td>Some Text</td>
				</tr>
				<tr>
					<td>More Text</td>
					<td>Blah</td>
				</tr>
			</table>

		</body>
		</html>
		';

			/*$dompdf = new Dompdf();
			$dompdf->loadHtml('hello world');

			// (Optional) Setup the paper size and orientation
			$dompdf->setPaper('A4', 'landscape');

			// Render the HTML as PDF
			$dompdf->render();

			// Output the generated PDF to Browser
			$dompdf->stream();*/


		// Get output html
		//	$html = $this->output->get_output();

		// Load library

		// Convert to PDF
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream("welcome.pdf");
		}
	}

	function prestamo($id){
		if(is_numeric($id)){
			$data = $this->load_data();
			$this->load->library('dompdf_gen');

			$join = array(array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
//				array('table' => 'prestamos_estados_historico','join'=>'prestamos_estados_historico.idprestamo = prestamos.id_prestamo'));
			$where = array('prestamos.id_prestamo' => $id);
			$data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);

			$data['estados'] = $this->Thememodel->get_all_values('prestamos_estados');
			$join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos_estados_historico.idestado'));
			$where = array('idprestamo' => $id);
			$data['estados_prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos_estados_historico',$join,$where,'idestado','ASC',99);

			$html = '';
			$html .= $this->load->view('template/header',$data, true);
			$html .= $this->load->view('template/gestion/prestamo',$data, true);
			$html .= $this->load->view('template/footer',$data, true);
			$this->dompdf->load_html($html);
			$this->dompdf->render();
			$this->dompdf->stream('prestamo-clicredit-'.$id.'.pdf');

		}
		// page info here, db calls, etc.
		//	$html = $this->load->view('controller/viewfile', $data, true);
		//	or
		/*
		$data = pdf_create($html, '', false);
		write_file('name', $data);
		if you want to write it to disk and/or send it as an attachment
		*/
	}
	private function load_data(){
		$id = $this->session->userdata('user_id');
		include 'Themeoptions.php';
		return $data;
	}
}
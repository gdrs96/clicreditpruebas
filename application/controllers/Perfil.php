<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email'));
		$this->load->model(array('Thememodel','Emailmodel'));
	}

	public function index(){
		$this->only_login();
		$data = $this->load_data();
		$id = $this->session->userdata('user_id');


		cargar_template('template/perfil', $data);
	}

	function guardar_perfil(){
		$this->only_login();
		$data = $this->load_data();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name','Nombre','trim|required|strip_tags|prep_for_form');
		$this->form_validation->set_rules('telefono','Teléfono','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('provincia','Provincia','trim|strip_tags|prep_for_form');
	//	$this->form_validation->set_rules('codpostal','Código postal','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('facebook','Facebook','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('twitter','Twitter','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('instagram','Instagram','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('mostrar_datos','opción de mostrar teléfono y email','trim|strip_tags|prep_for_form');
		//$this->form_validation->set_rules('email','Email','trim|required|valid_email|strip_tags');
		if($this->form_validation->run() == FALSE){
			$this->session->set_userdata('error',validation_errors());
			redirect(base_url().'perfil');
		}else{
			$datos['name'] = $this->input->post('name');
		//	$datos['email'] = $this->input->post('email');;
			$datos['telefono'] = $this->input->post('telefono');
			$datos['provincia'] = $this->input->post('provincia');
			$datos['direccion'] = $this->input->post('direccion');
			//$datos['codpostal'] = $this->input->post('codpostal');
			$datos['facebook'] = $this->input->post('facebook');
			$datos['twitter'] = $this->input->post('twitter');
			$datos['instagram'] = $this->input->post('instagram');
			$datos['mostrar_datos'] = $this->input->post('mostrar_datos');
			$id = $this->session->userdata('user_id');
			if(!empty($this->input->post('pass')) && !empty( $this->input->post('pass1'))){
				if($this->input->post('pass') == $this->input->post('pass1')){
					$datos['pass'] = md5($this->input->post('pass'));
				}
			}
			if( $this->Thememodel->save_value('user',$datos,array('id' =>$id)) ){
				$this->session->set_userdata('info', 'Su información se ha actualizado');
			}
			$config['upload_path'] = 'img/users/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size'] = '4096';
			$config['overwrite'] = TRUE;
			$this->load->library('upload', $config);
			$this->guardarImg($config,$id,'user','image');
			redirect(base_url().'perfil');
		}
	}
// Candidatos
	function nueva_candidatura(){
		$this->only_login();
		$total = sizeof( $this->Thememodel->get_all_values_where('candidaturas', array('id_user' => $this->session->userdata('user_id'))));
		if($total > 1){
			$this->session->set_userdata('error', 'No puede añadir más candidaturas');
			redirect(base_url().'perfil');
		}
		$data['etiquetas'] = $this->Thememodel->get_all_values('tags');
		$data['categorias'] = $this->Thememodel->get_all_values('ofertas_categorias');
		$data = $this->load_data();
		//$data['total'] = $data['total'] + sizeof( $this->Thememodel->get_all_values_where('oferta', array('id_user' => $this->session->userdata('user_id'),'destacado' => '1')));
		cargar_template('template/candidatura-nueva', $data);
	}

	function guardar_nueva_candidatura(){
		$this->only_login();
		if($total > 1){
			$this->session->set_userdata('error', 'No puede añadir más candidaturas');
			redirect(base_url().'perfil');
		}
		$datos = $this->obtener_datos_candidatura();
		if($datos == FALSE){
			redirect(base_url().'perfil/nueva_candidatura');
		}
		$datos['fecha_alta'] = mdate('%Y-%m-%d %H:%i:%s', time());
		$datos['fecha_modificacion'] = mdate('%Y-%m-%d %H:%i:%s', time());
		$datos['id_user'] = $this->session->userdata('user_id');
		if( $datos['nombre'] != '' && $this->Thememodel->insert_value('candidaturas',$datos) ){
			$id = $this->db->insert_id();
			$config['upload_path'] = 'img/candidaturas/';
			$config['allowed_types'] = 'jpg|png';
			$config['max_size'] = '4096'; // EN KB
			$config['overwrite'] = TRUE;
			$this->load->library('upload', $config);
			$this->guardarImg($config,$id,'candidaturas','image');
			$this->session->set_userdata('info', 'Información guardada correctamente');
		}else{
			$this->session->set_userdata('error', 'No se ha guardado, por favor compruebe los datos');
		}
		redirect( base_url().'perfil');
	}
	function editar_candidatura(){

		$this->only_login();
		$data = $this->load_data();
		$id = $this->uri->segment(3);
		//comprobamos que la candidatura es nuestra o somos administradores
		if( $this->session->userdata('type') == 'administrador' ){
			$data['candidatura'] = $this->Thememodel->get_value('candidaturas', array('id' => $id ));
		}else{
			$data['candidatura'] = $this->Thememodel->get_value('candidaturas', array('id_user' => $this->session->userdata('user_id'), 'id' => $id ));
		}
		$data['total'] = sizeof( $this->Thememodel->get_all_values_where('candidaturas', array('id_user' => $this->session->userdata('user_id'),'destacada' => '1')));
		$data['categorias'] = $this->Thememodel->get_all_values('ofertas_categorias');
		$data['etiquetas'] = $this->Thememodel->get_all_values('tags');
		$etiquetas_candidaturas = $this->Thememodel->get_select('id_tag','candidaturas_tags',array('id_candidatura'=>$id));
		foreach ($etiquetas_candidaturas as $key) {
			$data['etiquetas_candidaturas'][] = $key->id_tag;
		}
		if(empty($data['candidatura'])){redirect(base_url().'perfil');}
		cargar_template('template/candidatura-editar', $data);
	}
	function guardar_candidatura(){
		$this->only_login();
		$datos = $this->obtener_datos_candidatura();

		if(!empty($datos['etiquetas'])){
			$etiquetas = $datos['etiquetas'];
		}else{
			$etiquetas = array();
		}
		unset($datos['etiquetas']);
		$this->db->trans_start();
		if($datos == FALSE){
			redirect(base_url().'perfil/editar-candidatura');
		}
		$datos['fecha_modificacion'] = mdate('%Y-%m-%d %H:%i:%s', time());
		$id = $this->input->post('id');
		if( $this->Thememodel->save_value('candidaturas',$datos,array('id' =>$id)) ){
			$result = $this->Thememodel->get_select('id_tag','candidaturas_tags',array('id_candidatura'=>$id));
			if($result){
				$etiquetas_anterior = array();
				foreach($result as $key){
					$etiquetas_anterior[] = $key->id_tag;
				}
			}else{
				$etiquetas_anterior = array();
			}
			$crear = array_diff($etiquetas,$etiquetas_anterior);
			$eliminar = array_diff($etiquetas_anterior,$etiquetas);
			foreach($crear as $key){
				$this->Thememodel->insert_value('candidaturas_tags',array('id_candidatura' =>$id,'id_tag'=>$key));
			}
			foreach($eliminar as $key){
				$this->Thememodel->del_value('candidaturas_tags',array('id_candidatura' =>$id,'id_tag'=>$key));
			}
			$this->db->trans_complete();
			$this->session->set_userdata('info', 'Información guardada correctamente');
		}
		$config['upload_path'] = 'img/candidaturas/';
		$config['allowed_types'] = 'jpg|png';
		$config['max_size'] = '4096'; // EN KB
		$config['overwrite'] = TRUE;
		$this->load->library('upload', $config);
		$this->guardarImg($config,$id,'candidaturas','image1');
		$this->guardarImg($config,$id,'candidaturas','image2');
		$this->guardarImg($config,$id,'candidaturas','image3');
		$this->guardarImg($config,$id,'candidaturas','image4');
		$this->guardarImg($config,$id,'candidaturas','image5');
		$this->guardarImg($config,$id,'candidaturas','image6');

		redirect(base_url().'perfil/editar-candidatura/'.$id);
	}
	function borrar_candidatura(){
		$this->only_login();
		if(!empty($this->uri->segment(3))){
			$id = $this->uri->segment(3);
		}
		if( $this->session->userdata('type') == 'administrador' ){
			$this->borrarImgAllcandidatura($id);
			$this->Thememodel->del_value('candidatura', array('id' => $id ));
			$this->Thememodel->del_value('id_candidatura', array('candidaturas_tags' => $id ));
		}else{
			if( !empty( $this->Thememodel->get_value('candidatura', array('id_user' => $this->session->userdata('user_id'), 'id' => $id ))) ){
				$this->borrarImgAllcandidatura($id);
				$this->Thememodel->del_value('candidatura', array('id' => $id ));
			}
		}
		$this->session->set_userdata('info', 'Se ha borrado correctamente');
		redirect(base_url().'perfil');
	}
	function borrarImgcandidatura(){
		$this->only_login();
		$num_IMG = $this->uri->segment(3);
		$id = $this->uri->segment(4);
		$datos['image'.$num_IMG] = '';
		if( $this->session->userdata('type') == 'administrador' ){
			$candidatura = $this->Thememodel->get_select('image'.$num_IMG,'candidaturas',array('id' => $id ));
		}else{
			$candidatura = $this->Thememodel->get_select('image'.$num_IMG,'candidaturas',array('id' => $id, 'id_user' => $this->session->userdata('user_id') ));
		}
		if(!empty($candidatura)){
			if($num_IMG == 1){unlink($candidatura[0]->petimg1);}
			$this->Thememodel->save_value('candidaturas',$datos,array('id' =>$id));
			$this->session->set_userdata('info', 'Se ha borrado la imagen con éxito');
		}
		redirect(base_url().'perfil');
	}

// Ofertas
	function nueva_oferta(){
		$this->only_login();
		$data = $this->load_data();
		$data['total'] = sizeof( $this->Thememodel->get_all_values_where('ofertas', array('id_user' => $this->session->userdata('user_id'),'destacada' => '1')));
		$data['categorias'] = $this->Thememodel->get_all_values('ofertas_categorias');
		$data['etiquetas'] = $this->Thememodel->get_all_values('tags');
		cargar_template('template/oferta-nueva', $data);
	}
	function editar_oferta(){
		$this->only_login();
		$data = $this->load_data();
		$id = $this->uri->segment(3);
		//comprobamos que la oferta es nuestra o somos administradores
		if( $this->session->userdata('type') == 'administrador' ){
			$data['oferta'] = $this->Thememodel->get_value('ofertas', array('id' => $id ));
		}else{
			$data['oferta'] = $this->Thememodel->get_value('ofertas', array('id_user' => $this->session->userdata('user_id'), 'id' => $id ));
		}
		$data['total'] = sizeof( $this->Thememodel->get_all_values_where('ofertas', array('id_user' => $this->session->userdata('user_id'),'destacada' => '1')));
		$data['categorias'] = $this->Thememodel->get_all_values('ofertas_categorias');
		$data['etiquetas'] = $this->Thememodel->get_all_values('tags');
		$etiquetas_ofertas = $this->Thememodel->get_select('id_tag','ofertas_tags',array('id_oferta'=>$id));
		foreach ($etiquetas_ofertas as $key) {
			$data['etiquetas_ofertas'][] = $key->id_tag;
		}
		if(empty($data['oferta'])){redirect(base_url().'perfil');}
		cargar_template('template/oferta-editar', $data);
	}
	function guardar_nueva_oferta(){
		$this->only_login();
		$datos = $this->obtener_datos_oferta();
		if($datos == FALSE){
			redirect(base_url().'perfil/nueva-oferta');
		}
		$datos['fecha_alta'] = mdate('%Y-%m-%d %H:%i:%s', time());
		$datos['fecha_modificacion'] = mdate('%Y-%m-%d %H:%i:%s', time());
		$datos['id_user'] = $this->session->userdata('user_id');
		$etiquetas = $datos['etiquetas'];
		unset($datos['etiquetas']);
		$this->db->trans_start();
		if( $datos['nombre'] != '' && $this->Thememodel->insert_value('ofertas',$datos)){
			$id = $this->db->insert_id();
			foreach($etiquetas as $key){
				$this->Thememodel->insert_value('ofertas_tags',array('id_oferta' =>$id,'id_tag'=>$key));
			}
			$this->db->trans_complete();
			$this->session->set_userdata('info', 'Información guardada correctamente');
		}else{
			$this->session->set_userdata('error', 'No se ha guardado, por favor compruebe los datos');
		}
		redirect( base_url().'perfil');
	}
	function guardar_oferta(){
		$this->only_login();
		$datos = $this->obtener_datos_oferta();
		if(!empty($datos['etiquetas'])){
			$etiquetas = $datos['etiquetas'];
		}else{
			$etiquetas = array();
		}
		unset($datos['etiquetas']);
		$this->db->trans_start();
		if($datos == FALSE){
			redirect(base_url().'perfil/editar-oferta');
		}
		$datos['fecha_modificacion'] = mdate('%Y-%m-%d %H:%i:%s', time());
		$id = $this->input->post('id');
		if( $this->Thememodel->save_value('ofertas',$datos,array('id' =>$id)) ){
			$result = $this->Thememodel->get_select('id_tag','ofertas_tags',array('id_oferta'=>$id));
			if($result){
				$etiquetas_anterior = array();
				foreach($result as $key){
					$etiquetas_anterior[] = $key->id_tag;
				}
			}else{
				$etiquetas_anterior = array();
			}
			// resultado de etiquetas a añadir
			$crear = array_diff($etiquetas,$etiquetas_anterior);
			// resultado de etiquetas a eliminar
			$eliminar = array_diff($etiquetas_anterior,$etiquetas);

			foreach($crear as $key){
				$this->Thememodel->insert_value('ofertas_tags',array('id_oferta' =>$id,'id_tag'=>$key));
			}
			foreach($eliminar as $key){
				$this->Thememodel->del_value('ofertas_tags',array('id_oferta' =>$id,'id_tag'=>$key));
			}
			$this->db->trans_complete();
			$this->session->set_userdata('info', 'Información guardada correctamente');
		}
		redirect(base_url().'perfil/editar-oferta/'.$id);
	}
	function borrar_oferta(){
		$this->only_login();
		$id = $this->uri->segment(3);
		if( $this->session->userdata('type') == 'administrador' ){
			$this->Thememodel->del_value('ofertas', array('id' => $id ));
			$this->Thememodel->del_value('ofertas_tags', array('id_oferta' => $id ));
		}else{
			if( !empty( $this->Thememodel->get_value('ofertas', array('id_user' => $this->session->userdata('user_id'), 'id' => $id ))) ){
				$this->borrarImgAlloferta($id);
				$this->Thememodel->del_value('ofertas', array('id' => $id ));
			}
		}
		$this->session->set_userdata('info', 'Se ha borrado correctamente');
		redirect(base_url().'perfil');
	}

	function baja(){
		$this->only_login();
		$datos = array('status' => '');
		$this->Thememodel->save_value('user',$datos,array('id' => $this->session->userdata('user_id')));
		$this->session->set_userdata('info', 'Su usuario se ha desactivado, pronto será borrado por completo');
		redirect(base_url().'acceso');
	}

// Ajax
	function delcandidatura(){
		$this->only_login();
		$id = $this->input->post('id',TRUE);
		$table = 'candidatura';

		if( $this->session->userdata('type') == 'administrador' ){
			$this->borrarImgAllcandidatura($id);
			$this->Thememodel->del_value($table, array('id' => $id ));
		}else{
			if( !empty( $this->Thememodel->get_value($table, array('id_user' => $this->session->userdata('user_id'), 'id' => $id ))) ){
				$this->borrarImgAllcandidatura($id);
				$this->Thememodel->del_value($table, array('id' => $id ));
			}
		}
		echo 'Se ha eliminado correctamente';
	}
	function deloferta(){
		$this->only_login();
		$id = $this->input->post('id',TRUE);
		$table = 'oferta';

		if( $this->session->userdata('type') == 'administrador' ){
			$this->borrarImgAlloferta($id);
			$this->Thememodel->del_value($table, array('id' => $id ));
		}else{
			if( !empty( $this->Thememodel->get_value($table, array('id_user' => $this->session->userdata('user_id'), 'id' => $id ))) ){
				$this->borrarImgAlloferta($id);
				$this->Thememodel->del_value($table, array('id' => $id ));
			}
		}
		echo 'Se ha eliminado correctamente';
	}

// Categorías
	function crear_categoria(){
		$datos['nombre'] = $this->input->post('nombre');
		$datos['slug'] = $this->input->post('slug');
		$datos['keywords'] = $this->input->post('keywords');
		$datos['descripcion'] = $this->input->post('descripcion');
		if( $datos['nombre'] != '' && $this->Thememodel->insert_value('ofertas_categorias',$datos) ){
			$this->session->set_userdata('info', 'Información guardada correctamente');
		}else{
			$this->session->set_userdata('error', 'No se ha guardado, por favor compruebe los datos');
		}
		redirect( base_url().'perfil');
	}
	function editar_categoria(){
		if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
		}else{
			redirect(base_url());
		}
		$data = $this->load_data();
		//comprobamos que la oferta es nuestra o somos administradores
		$data['categoria'] = $this->Thememodel->get_value('ofertas_categorias', array('id' => $id ));
		cargar_template('template/categoria-editar', $data);
	}
	function guardar_categoria(){
		$datos['nombre'] = $this->input->post('nombre');
		$datos['slug'] = $this->input->post('slug');
		$datos['keywords'] = $this->input->post('keywords');
		$datos['descripcion'] = $this->input->post('descripcion');
		if($datos == FALSE){
			redirect(base_url().'perfil');
		}
		$id = $this->input->post('id');
		if( $this->Thememodel->save_value('ofertas_categorias',$datos,array('id' =>$id)) ){
			$this->session->set_userdata('info', 'Información guardada correctamente');
		}
		redirect(base_url().'perfil/editar-categoria/'.$id);
	}
	function borrar_categoria(){
		$id = $this->uri->segment(3);
		$this->Thememodel->del_value('ofertas_categorias', array('id' => $id ));
		$this->session->set_userdata('info', 'Se ha borrado correctamente');
		redirect(base_url().'perfil');
	}
// Etiquetas
	function crear_etiqueta(){
		$datos['nombre'] = $this->input->post('nombre');
		$datos['slug'] = $this->input->post('slug');
		$datos['meta_descripcion'] = $this->input->post('meta_descripcion');
		$datos['descripcion'] = $this->input->post('descripcion');
		if( $datos['nombre'] != '' && $this->Thememodel->insert_value('tags',$datos) ){
			$this->session->set_userdata('info', 'Información guardada correctamente');
		}else{
			$this->session->set_userdata('error', 'No se ha guardado, por favor compruebe los datos');
		}
		redirect( base_url().'perfil');
	}
	function editar_etiqueta(){
		if($this->uri->segment(3)){
			$id = $this->uri->segment(3);
		}else{
			redirect(base_url());
		}
		$data = $this->load_data();
		//comprobamos que la oferta es nuestra o somos administradores
		$data['etiqueta'] = $this->Thememodel->get_value('tags', array('id' => $id ));
		cargar_template('template/etiqueta-editar', $data);
	}
	function guardar_etiqueta(){
		$datos['nombre'] = $this->input->post('nombre');
		$datos['slug'] = $this->input->post('slug');
		$datos['meta_descripcion'] = $this->input->post('meta_descripcion');
		$datos['descripcion'] = $this->input->post('descripcion');
		if($datos == FALSE){
			redirect(base_url().'perfil');
		}
		$id = $this->input->post('id');
		if( $this->Thememodel->save_value('tags',$datos,array('id' =>$id)) ){
			$this->session->set_userdata('info', 'Información guardada correctamente');
		}
		redirect(base_url().'perfil/editar-etiqueta/'.$id);
	}
	function borrar_etiqueta(){
		$id = $this->uri->segment(3);
		$this->Thememodel->del_value('tags', array('id' => $id ));
		$this->session->set_userdata('info', 'Se ha borrado correctamente');
		redirect(base_url().'perfil');
	}

// INICIO funciones auxiliares

	private function obtener_datos_candidatura(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('candidatura','','trim|strip_tags');
		$this->form_validation->set_rules('nombre','nombre','trim|required|strip_tags|prep_for_form');
		$this->form_validation->set_rules('n_identificador','Número de Cédula o Pasaporte','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('id_categoria','categoria','trim|strip_tags');
		$this->form_validation->set_rules('etiquetas[]','etiquetas','trim|strip_tags|callback_etiquetas_check');
		$this->form_validation->set_rules('email','email','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('direccion','dirección','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('descripcion','descripción','trim|strip_tags');
		$this->form_validation->set_rules('telefono1','telefono principal','trim|strip_tags');
		$this->form_validation->set_rules('telefono2','telefono principal','trim|strip_tags');
		$this->form_validation->set_rules('facebook','facebook','trim|strip_tags');
		$this->form_validation->set_rules('twitter','twitter','trim|strip_tags');
		$this->form_validation->set_rules('instagram','instagram','trim|strip_tags');
		if($this->form_validation->run() == FALSE){
			$this->session->set_userdata('error',validation_errors().'<br><a href="javascript:history.back()">Pulse aquí para rellenar correctamente los campos</a>');
			return FALSE;
		}else{
			$datos['nombre'] = $this->input->post('nombre');
			$datos['n_identificador'] = $this->input->post('n_identificador');
			$datos['id_categoria'] = $this->input->post('categoria');
			$datos['email'] = $this->input->post('email');
			$datos['etiquetas'] = $this->input->post('etiquetas');
			$datos['direccion'] = $this->input->post('direccion');
			$datos['descripcion'] = $this->input->post('descripcion');
			$datos['telefono1'] = $this->input->post('telefono1');
			$datos['telefono2'] = $this->input->post('telefono2');
			$datos['facebook'] = $this->input->post('facebook');
			$datos['twitter'] = $this->input->post('twitter');
			$datos['instagram'] = $this->input->post('instagram');
		}
		return $datos;
	}
	private function obtener_datos_oferta(){
		$this->load->library('form_validation');
		$this->form_validation->set_rules('oferta','','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('nombre','nombre','trim|required|strip_tags|prep_for_form');
		$this->form_validation->set_rules('subtitulo','subtitulo','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('id_categoria','categoria','trim|strip_tags');
		$this->form_validation->set_rules('etiquetas[]','etiquetas','trim|strip_tags|callback_etiquetas_check');
		$this->form_validation->set_rules('descripcion','descripción','trim|strip_tags');
		$this->form_validation->set_rules('direccion','direccion','trim|strip_tags|prep_for_form');
		$this->form_validation->set_rules('destacada','destacada','trim|strip_tags|prep_for_form');
		if($this->form_validation->run() == FALSE){
			$this->session->set_userdata('error',validation_errors().'<br><a href="javascript:history.back()">Pulse aquí para rellenar correctamente los campos</a>');
			return FALSE;
		}else{
			$datos['nombre'] = $this->input->post('nombre');
			$datos['subtitulo'] = $this->input->post('subtitulo');
			$datos['id_categoria'] = $this->input->post('categoria');
			$datos['etiquetas'] = $this->input->post('etiquetas');
			$datos['descripcion'] = $this->input->post('descripcion');
			$datos['direccion'] = $this->input->post('direccion');
			$datos['destacada'] = $this->input->post('destacada');
			if(!$datos['destacada']){$datos['destacada'] = 0;}
		}
		return $datos;
	}
	function etiquetas_check($key){
		if(is_numeric($key) && $key != '0' || $key == ''){
			return TRUE;
		}else{
			return FALSE;
		}
	}

	private function guardarImg($config,$id,$table,$nameField){
		if(!empty( $_FILES[$nameField]['name'] )){
			if($_FILES[$nameField]['type'] == 'image/jpeg'){
				$config['file_name'] = $id.'-'.$nameField.'.jpg';
			}else{
				$config['file_name'] = $id.'-'.$nameField.'.png';
			}
			$this->upload->initialize($config);
			if(!$this->upload->do_upload($nameField) ){
				$data['error'] = $this->upload->display_errors();
				if(!empty($data['error']) ){
					$this->session->set_userdata('error', $data['error']);
				}
			}else{
				$config_img['image_library'] = 'gd2';
				$config_img['source_image']	= $config['upload_path'].$config['file_name'];
				$config_img['new_image'] = $config['upload_path'].$config['file_name'];
				$config_img['maintain_ratio'] = TRUE;
				$config_img['width'] = 600;
				$config_img['height'] = 400;
				$this->load->library('image_lib',$config_img);
				$this->image_lib->initialize($config_img);
				$this->image_lib->resize();
				$this->image_lib->clear();
				$imagen = array( $nameField => $config['upload_path'].$config['file_name']);
				$this->Thememodel->save_value($table,$imagen,array('id' =>$id));
			}
		}
	}
	private function borrarImgAllcandidatura($id){
		$candidaturas = $this->Thememodel->get_select('petimg1','candidaturas',array('id' => $id));
		if( !empty($candidaturas[0]->petimg1)){
			unlink( $candidaturas[0]->petimg1);
		}
	}
	private function borrarImgAlloferta($id){
		$ofertas = $this->Thememodel->get_select('image','oferta',array('id' => $id));
		if( !empty($ofertas[0]->img1)){
			unlink( $ofertas[0]->img1);
		}
		if( !empty($ofertas[0]->img2)){
			unlink( $ofertas[0]->img2);
		}
	}
	public function borrar_valoracion($id){
		$this->only_login();
		$id = $this->uri->segment(3);
		if( $this->session->userdata('type') == 'administrador' ){
			$this->Thememodel->del_value('candidaturas_valoraciones', array('id' => $id ));
		}else{
			if( !empty( $this->Thememodel->get_value('ofertas', array('id_user' => $this->session->userdata('user_id'), 'id' => $id ))) ){
				$this->Thememodel->del_value('candidaturas_valoraciones', array('id' => $id ));
			}
		}
		$this->session->set_userdata('info', 'Se ha borrado correctamente');
		redirect(base_url().'perfil');
	}
	private function load_data(){
		$id = $this->session->userdata('user_id');
		$data['user'] = $this->Thememodel->get_value('user', array('id' => $id ));
		//$data['footer'] = $this->Thememodel->get_all_values_where('post', array('type' => 'footer' ));
		//$data['ofertas_categorias'] = $this->Thememodel->get_all_values('ofertas_categorias');
		//$data['etiquetas'] = $this->Thememodel->get_all_values('tags');
		include 'Themeoptions.php';
		return $data;
	}

	function only_login(){
		if(empty($this->session->userdata('type'))){
			$this->session->set_userdata('info', 'Sólo puedes acceder si estás registrado. <a href="'.base_url().'acceso/registro">Regístrate, pincha aquí es muy fácil y rápido.</a>');
			redirect(base_url().'acceso');
		}
	}
	function only_admin(){
		if(empty($this->session->userdata('type')) && $this->session->userdata('type') == 'administrador'){
			$this->session->set_userdata('error', 'Tienes que ser administrador.');
			redirect(base_url().'perfil');
		}
	}

}
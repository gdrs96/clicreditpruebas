<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil_captacion extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email','security'));
		$this->load->model(array('Thememodel'));
		$this->load->library('form_validation');
		$this->lang->load('formulario','castellano');
        check_rol($this->session->userdata('roles'), array(1,2,3,4,5,6));
	}


	public function index(){

		$data = null;
		cargar_dashboard('dashboard/algoritmo/perfil_captacion', $data);
	}


	public function obtenerPerfilesCaptacion(){
       $data['data'] = $this->Thememodel->get_query("SELECT pc.id,pc.descripcion,pc.valor,pc.observacion,pc.status, DATE_FORMAT(pc.TS_timestamp,'%d-%m-%Y %H:%i:%s') as ultima_actualizacion, CONCAT(us.name,' ',us.primer_apellido) as name from perfil_captacion as pc LEFT JOIN users as us ON pc.TS_user = us.id order by pc.id");
		   if(!empty($data)){
              echo json_encode($data);
          }else{
              $data['data']=[];
              echo json_encode($data);
          }
		
	}

	public function crearPerfilesCaptacion(){
		
	 $this->form_validation->set_rules('descripcion','Nombre','trim|strip_tags|html_escape|required');
	 $this->form_validation->set_rules('valor','Pts > mínimo','trim|strip_tags|html_escape|required');
	 if($this->form_validation->run()){
	 	$data = array('descripcion'=>$this->input->post('descripcion'),
	 		          'valor' => $this->input->post('valor'),
	                  'observacion' => $this->input->post('observacion'),
	                  'TS_user' => $this->session->userdata('user_id'),
	                  'TS_timestamp' => date("Y-m-d H:i:s"));
         $this->Thememodel->insert_value('perfil_captacion',$data);
         echo json_encode(array("status" => TRUE));

	 }else{
	 	echo json_encode(array("status" => FALSE));
	 }
	}


	public function actualizarPerfilesCaptacion(){
		
	 $this->form_validation->set_rules('descripcion','Nombre','trim|strip_tags|html_escape|required');
	 $this->form_validation->set_rules('valor','Pts > mínimo','trim|strip_tags|html_escape|required');
	 if($this->form_validation->run()){
     $cantidad_prestamos_perfil =  $this->Thememodel->total_where('prestamos',array('perfil_captacion_id' => $this->input->post('id_perfil')));
     if($cantidad_prestamos_perfil==0){

	 	$data = array('descripcion'=>$this->input->post('descripcion'),
	 		          'valor' => $this->input->post('valor'),
	                  'observacion' => $this->input->post('observacion'),
	                  'TS_user' => $this->session->userdata('user_id'),
	                  'TS_timestamp' => date("Y-m-d H:i:s"));
        $this->Thememodel->save_value('perfil_captacion',$data,array('id' => $this->input->post('id_perfil')));
         echo json_encode(array("status" => TRUE));
     }else{
     	 echo json_encode(array("status" => "PERFIL EN USO"));
     }

	 }else{
	 	echo json_encode(array("status" => FALSE));
	 }

		
	}

	public function eliminarPerfilCaptacion($id_perfil){
		 $cantidad_prestamos_perfil =  $this->Thememodel->total_where('prestamos',array('perfil_captacion_id' => $id_perfil ));
		 if($cantidad_prestamos_perfil==0){
		 	$this->Thememodel->del_value('perfil_captacion',array('id'=>$id_perfil));
		 	echo json_encode(array("status" => TRUE));
		 }else{
		 	echo json_encode(array("status" => "PERFIL EN USO"));
		 }


	}

}
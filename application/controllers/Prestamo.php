<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Prestamo extends CI_Controller {


var $listaDatosMacroeconomicos;
var $listaVariablesPrimariasCompleta;

	function __construct(){
		parent::__construct();
		//date_default_timezone_set('Europe/Madrid');
		$this->load->helper(array('url','util','date','email','file','security'));
     $this->load->helper('cookie');
  
		$this->load->model(array('Thememodel','Emailmodel','Algoritmo'));
		$this->load->library('form_validation');
    $this->load->model('Algoritmo');
      $this->load->model('DatosMacroeconomicos'); // --- Datos macroeconomicos
      $this->listaDatosMacroeconomicos = $this->DatosMacroeconomicos->GetDatosMacroeconomicos();
       $this->load->model('Macroeconomicas' );
        $this->load->model('VariablesPrimarias');
      $this->listaVariablesPrimariasCompleta=$this->VariablesPrimarias->ProcessVariablesPrimarias();
		//check_rol($this->session->userdata('roles'),array(2,3));
     //SELECT id,data from sessions where TIME_TO_SEC(TIMEDIFF(NOW(),from_unixtime(timestamp)))<1500

	}

// Función encargada de eliminar los prestamos que se encuentran en estado incompleto y que llevan más de 48 horas sin completar
	public function eliminarPrestamos(){


    /*$emails =  $this->Thememodel->get_query("SELECT email FROM users as us LEFT JOIN prestamos as p ON p.id_cliente = us.id WHERE p.prestamo_estado_id=9 and p.ultima_actualizacion > DATE_ADD(p.ultima_actualizacion, INTERVAL -48 HOUR)");

     $delete_prestamos =   $this->Thememodel->execute_sql("DELETE FROM `prestamos` WHERE ultima_actualizacion > DATE_ADD(ultima_actualizacion, INTERVAL -48 HOUR) AND prestamo_estado_id = 9");
       


     if($delete_prestamos){
         $list_emails = array();
         foreach ($emails as $key => $value) { 
            array_push($list_emails,$value->email);    
         }
          $data['email_template'] = $this->Thememodel->get_all_values('email_template');
          $web = 'Cambio de estado, Clicredit.com';
          $from = "info@pa.clicredit.com";
          $data['email'] = $data['email_template'][7];
          $asunto = $data['email']->asunto;
          $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$list_emails);
      }
  }*/


  //echo '<script>alert("hola");</script>';
      // $file = "files/prestamos/prestamo_231/1png.png";
        //$directory = dirname($file);
        //substr($file,0,-1);
      // var_dump($directory);
     //  $this->remove_directory($directory);
/*$query =  $this->Thememodel->get_query("SELECT doc.fichero FROM documentacion_asociada as doc LEFT JOIN prestamos as p ON p.id_prestamo = doc.prestamo_id WHERE p.prestamo_estado_id = 9 and p.ultima_actualizacion < DATE_SUB(NOW(), INTERVAL 48 HOUR) GROUP by (p.id_prestamo)");
var_dump($query);*/

$files = array('a' => "");
$a = array ('test' => 1, 'hello' => NULL, 'pie' => array('a' => 'apple'));
$hoy       = gmdate('d/m/Y');



      $hoy       = gmdate('d/m/Y'); // --- Hoy
     // $fecha            = date_create($dato_respuesta);
      $fecha_antiguedad = "1/2/2015";
       
      $difDias = (float) strDMYdiff( $fecha_antiguedad, $hoy );
       $cc = round( $difDias / 365, 2 );

 // $a =     substr('SimuA7', 0, 4 );
 $query =  $this->Thememodel->get_query("SELECT * FROM `logs_sms` ORDER BY `id` ASC");

    $where = array('prestamos.id_prestamo' => 244);
      $params['prestamo'] = $this->Thememodel->get_query("SELECT * from prestamos where id_prestamo = 244");

    /*   $json_calculos = json_decode( $params['prestamo'][0]->calculos, true );
     var_dump($json_calculos[ '4_2_vp' ]);  
     if($json_calculos[ '4_2_vp' ]!=0){
      echo "0";
     }else{
      echo "1";
     }*/
    $id_perfil = 0;
    $perfilAp = $this->Thememodel->get_value('perfil_captacion', array('id' => $id_perfil));
   // $params['prestamo'] = $this->Thememodel->get_value('prestamos', array('id_prestamo' => 256));
   // $json_calculos = json_decode( $params['prestamo']->calculos, true );
   // $perfilAp = $json_calculos['perfil_aprobacion'];

     //$respuesta_algoritmo = $this->Algoritmo->getArbolDeAprobacion(259);
     $data['email_template'] = $this->Thememodel->get_all_values('email_template');
   
   /* $where = array('email'=>'chaerj91@gmail.com');
    $total = $this->Thememodel->total_where('users',$where);*/
    /* $datos_usuario = $this->Thememodel->get_value('users', array('id' => 77));
        $nombre_usuario = $datos_usuario->name .' '. $datos_usuario->segundo_nombre;
      echo $nombre_usuario;*/
     $join = array(array('table'=>'users','join'=>'users.id = prestamos.id_cliente'), array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'));
      // array('table' => 'prestamos_estados_historico','join'=>'prestamos_estados_historico.idprestamo = prestamos.id_prestamo'));
      $where = array('prestamos.id_prestamo' => 243);

      $data['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);
      $sel = json_decode($data['prestamo'][0]->datos, true);
      $provincias = $this->Thememodel->get_all_values_where('data_page', 'question="4_1"');
      $select['4_1'] = isset($sel['4_1']) ? $provincias[intval(substr($sel['4_1'], 2))-1]->description : null;

      $bancos = $this->Thememodel->get_all_values_where_pag('data_page','question="5_2"',50,0);
      $select['banco'] = isset($sel['5_2']) ? $bancos[intval($sel['5_2'])]->description : null;
      //var_dump($select['banco']);

        $roles = $this->Thememodel->get_select('id_rol','users',array('id'=>4));
        //var_dump($roles);

        $query =  $this->Thememodel->get_query("SELECT datos FROM `prestamos` ORDER BY `id_prestamo` ASC");
         $sel = json_decode($query[214]->datos, true);
         $sel1 =  $this->Thememodel->total_where('prestamos',array('perfil_captacion_id' => 1));
         $query1 = $this->Thememodel->get_query("SELECT * FROM `logs_sms` ORDER BY `id` ASC");
         foreach ($query1 as $key => $value) {
           $var1 = json_decode($value->descripcion);
          
          if(!empty($var1->result[0]->sms_id)){
              $sms_id =  $var1->result[0]->sms_id;
          }else{
               $sms_id = null;
          } 

          if(!empty($var1->result[0]->error_id)){
              $error_id = $var1->result[0]->error_id;
          }else{
              $error_id = null;
          }

          if(!empty($var1->result[0]->error_msg)){
              $error_msg = $var1->result[0]->error_msg;
          }else{
              $error_msg = null;
          }  


           /*$arr['data'] =  array('fecha'=>$value->fecha,'status' => $var1->result[0]->status, 'terminal'=>$value->telefono, 'codigo_verificacion'=>$value->codigo_verificacion,'id_prestamo'=>$value->id_prestamo,'sms_id' => $sms_id,'error_id'=>$error_id, 'error_msg'=>$error_msg);*/
           
           //echo json_encode($arr['data']);
         }

         $prestamo_anterior = $this->Thememodel->get_query("SELECT p.id_prestamo FROM prestamos as p LEFT JOIN prestamos_estados as pe ON pe.id = p.prestamo_estado_id where id_cliente = 56 AND p.prestamo_actual=1 ORDER by p.id_prestamo DESC LIMIT 1");

       // var_dump($prestamo_anterior[0]->id_prestamo);
        // var_dump( $this->listaDatosMacroeconomicos);

          $VariablesPrimarias = $this->Thememodel->get_query("SELECT valor from variables_primarias where pregunta = 'CC8' ");

         $admin_vars = $this->listaDatosMacroeconomicos;
         //var_dump($this->Macroeconomicas->formulaCC8(null,$this->listaVariablesPrimariasCompleta,null,null,null, $admin_vars));
        /* $url = "'.base_url().'/acceso/recovery_password/";
        echo '<a style="color: #7AAA38; font-weight: bold; text-decoration: underline;" href="'.base_url().'">Cambiar contraseña</a>';*/

        //  var_dump($VariablesPrimarias);
        $bancos = $this->Thememodel->get_all_values_where_pag('data_page','question="5_2"',50,0);
        //$select['5_2'] = isset($sel['5_2']) ? $bancos[16]->description : null;
        //var_dump($select['5_2']);
       // var_dump($bancos[44]->description);
   // $users = $this->Thememodel->get_all_values_where_order_pag('sessions',array('timestamp >' => time()-1800 ),'timestamp','DESC',null,null);
       //var_dump($this->session->session_id);
       //$this->session->sess_destroy($this->session->session_id);

    $users = $this->Thememodel->get_query("SELECT id,data from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))<1500");
  //  $users = $this->Thememodel->get_query("SELECT id,data from sessions where TIME_TO_SEC(TIMEDIFF(NOW(),from_unixtime(timestamp)))<1500");
     $users1 = $this->Thememodel->get_query("SELECT id,data from sessions");
  $id_session = $this->session->session_id;
  $data_session = $this->Thememodel->get_query("SELECT id,data,timestamp FROM sessions WHERE id = '$id_session'");
  $new_time = (time()+1500);
  $this->Thememodel->save_value('sessions',array('timestamp' => $new_time),array('id' =>$id_session));
  //$time = time() - $data_session[0]->timestamp;
 //var_dump($time);
 /* var_dump($new_time);*/

    //var_dump($users);
  // $users = array($users[0]);
    // $data['users_online'] = array();
      
     /* foreach ($users as $key){
         $session_data = $key->data;
         if(!empty($session_data)){
         $offset = 0;
         $return_data = array();
       while ($offset < strlen($session_data)) {
         if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
     // var_dump($key->data);
      $return_data['id'] = $key->id;
      //var_dump($return_data);
       }

      }*/
   //var_dump($array);
    //var_dump($data['users_online']);
   // $users_online =  $data['users_online'];
    /*foreach ($users_online as $key => $user) {
       if($user['user_id']==60){
       return true;
     }else{
      return false;
     }
    }*/
    /*var_dump($users1);
    echo '<br>';
    var_dump($users);*/

   // $users1 = $this->Thememodel->get_query("SELECT * from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))<1500");
     
  /*  var_dump($users);
     var_dump($users);
    '<br>';*/
   //var_dump($data['users_online'][0]["user_id"]);
   //var_dump( $data['users_online']);
    
   // $id = $this->session->userdata('user_id');
    //var_dump($id);
  //var_dump($this->session->session_id);
  var_dump(date("Y-m-d H:i:s"));
 // var_dump($this->session->userdata('info1'));
 // echo '<script type="text/javascript">alert("' . $this->session->userdata('info1') . '");</script>';

/*$cookie_name = 'pontikis_net_php_cookie';
$cookie_value = 'test_cookie_set_with_php';
setcookie($cookie_name, $cookie_value, time() + (86400 * 30), '/'); // 86400 = 1 day

//$cookie_name = 'pontikis_net_php_cookie';
unset($_COOKIE[$cookie_name]);
if(!isset($_COOKIE[$cookie_name])) {
  var_dump('Cookie with name "' . $cookie_name . '" does not exist...');
} else {
  var_dump('Cookie with name "' . $cookie_name . '" value is: ' . $_COOKIE[$cookie_name]);
}*/
//unset($_COOKIE['cookie_session']);
//var_dump($_COOKIE['cookie_session']);
//var_dump(BROADCAST_URL);
//var_dump((time() + (86400 * 30)));
/*echo '<link rel="stylesheet" type="text/css" href="'.base_url().'css/sweetalert.css">
      <script type="text/javascript" src="'.base_url().'js/jquery.js"></script>
      <script src="'.base_url().'js/sweetalert.min.js" type="text/javascript"></script>
      <script type="text/javascript"> jQuery(function(){
      swal({
                              title: "Se ha cerrado su sesión anterior",
                              text: "",
                              type: "success"
                          });});

      </script>';*/
$uri = str_replace ("//", "/", $_SERVER['REQUEST_URI']);
//var_dump($uri);
/*var_dump($this->verificarSession11());
echo '<br>';
echo '<br>';
var_dump($this->verificarSession(60));
echo '<br>';
echo '<br>';*/
//var_dump($this->obtenerUsuario(34));
//$user22 = $this->verificarSession();
//var_dump($user22[0]);
//var_dump($this->listaUsuariosInactivos1());
//var_dump($this->session_id());
/* $id = $this->session->userdata('user_id');
 $prestamo_actual = $this->Thememodel->get_query("SELECT id_prestamo from `prestamos` WHERE id_cliente =   $id AND prestamo_actual = 1");
 var_dump($prestamo_actual);*/
}

public function ver(){
  $cedula_cliente =  $this->Thememodel->get_query("SELECT fichero from documentacion_asociada where tipo_documento_id = 1 and prestamo_id=295 ");
  var_dump($cedula_cliente[0]->fichero);
}


public function verificarSession11(){
    //$users = $this->Thememodel->get_query("SELECT id,data from sessions where TIME_TO_SEC(TIMEDIFF(NOW(),from_unixtime(timestamp)))<1500");
    $users = $this->Thememodel->get_query("SELECT id,data from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))<1500 order by data DESC");
     $return_data = array();
       $listUsers = array();
     foreach ($users as $key => $value){
         $session_data = $value->data;
        if(!empty($session_data)){
        
         $offset = 0;
      while ($offset < strlen($session_data)) {
        if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
  
     // $return_data['id'] = $value->id;
     // $return_data['index'] = $key;
     // $listUsers[$key] = $return_data;
      var_dump($return_data);
    }
    
}
    //return $data;
    return $listUsers;
     /*$users_online =  $listUsers;
   
     foreach ($users_online as $key => $user1) {
     
      if($user1[$key]["user_id"]==$id_user){
        return [
        "id_session" => $user1[$key]['id'],
        "status" => true
      ];
     }else{
       return [
        "status" => false
      ];
     }
    }*/
    
 }

 public function users(){
  $listUsers = $this->listaUsuariosInactivos();
    foreach ($listUsers as $key => $value) {
      
      $session_id = $value['id'];
      if(!empty($value['user_id'])){
        $user_id = $value['user_id'];
        var_dump($value);
      }
      //$this->Thememodel->execute_sql("UPDATE prestamos SET `prestamo_estado_id`=9  where  `id_cliente`= $user_id AND `prestamo_estado_id`=8");

       //$this->Thememodel->execute_sql("UPDATE users SET logged_in = 0 WHERE  id=$user_id");

      // $this->Thememodel->execute_sql("DELETE FROM sessions WHERE id='$session_id'");

    }
 // $users = $this->Thememodel->get_query("SELECT * FROM `users` WHERE `logged_in`=1");
  //var_dump($users);
 }

 public function users1(){
    $users = $this->Thememodel->get_query("SELECT * FROM `users` WHERE `logged_in`=1");
    var_dump($users);
 }
 public function usersonline(){
  var_dump($this->listaUsuariosInactivos1());
 }

 public function deleteSession($id){
     $this->Thememodel->execute_sql("DELETE FROM sessions WHERE id='$id'");

 }

 public function verificarSession($id_user){
   
   $users = $this->Thememodel->get_query("SELECT id,data from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))<1800 order by data DESC");
       $return_data = array();
       $listUsers = array();
     foreach ($users as $key => $value){
         $session_data = $value->data;
        
         if(!empty($session_data)){
        
         $offset = 0;
      while ($offset < strlen($session_data)) {
        if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
  
      $return_data['id'] = $value->id;
      $listUsers[$key] = $return_data;
       
    }
    
}
   
     $users_online =  $listUsers;
   
     foreach ($users_online as $key => $user1) {
     
      if($user1["user_id"]==$id_user){
        return [
        "id_session" => $user1['id'],
        "status" => true
      ];
     }
    }

 }


 public function obtenerUsuario($id_user){
  $users_online =  $this->verificarSession11();
     
     for ($i=0; $i<count($users_online); $i++) {
      var_dump($users_online[$i]["user_id"]);
      /*if($users_online[$i]["user_id"]==$id_user){
        return [
        "id_session" => $users_online[$i]['id'],
        "status" => true
      ];
     }else{
       return [
        "status" => false
      ];
     }*/
     
   }

    
 }

 public function cerrarsess(){

    $listUsers = $this->is_logged_in();
    foreach ($listUsers as $key => $value) {
      //var_dump($value['user_id']);
      $user_id = $value['user_id'];
       var_dump($value['id']);
      //$this->Thememodel->execute_sql("UPDATE prestamos SET `prestamo_estado_id`=9  where  `id_cliente`= $user_id AND `prestamo_estado_id`=8");
    }

 }

 public function is_logged_in()
    {
      $users = $this->Thememodel->get_query("SELECT * from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))<1500");
       $listUsers = array();
     foreach ($users as $key){
         $session_data = $key->data;
         
         if(!empty($session_data)){
         $return_data = array();
         $offset = 0;
      while ($offset < strlen($session_data)) {
        if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
  
      $return_data['id'] = $key->id;
       $listUsers[] = $return_data;

       
    }
    
   }
    return $listUsers;
    
    
    }


 /*public function formulaCC8( $_, $listaVPsCompleta, $_, $_, $_, $adminVars ){
      $datos = $adminVars[ 'CC8' ]."&nbsp;%"; // PIB
      $dato_respuesta = $datos <  -1                ? 1 : (
                        $datos >= -1 && $datos <= 0 ? 2 : (
                        $datos >=  0 && $datos <= 2 ? 3 : (
                        $datos >=  2 && $datos <= 6 ? 4 : (
                        $datos >=  6                ? 5 : -1
                      ))));

      $n_pregunta_normalizada = 'CC8';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => "",
        "datos"     => $datos,
        "vp"        => $vp
      ];
    }*/





public function obtenerCodigoVerificacion($id_prestamo){
  $params['prestamo'] = $this->Thememodel->get_value('prestamos', array('id_prestamo' => $id_prestamo));
  $json_datos = json_decode( $params['prestamo']->datos, true );
  var_dump($json_datos['codigo_verificacion']);
}


public function show(){
                $data['user'] = $this->Thememodel->get_value('users',array('id'=> 56));
                $respuesta_algoritmo = $this->Algoritmo->getArbolDeAprobacion(259);
                $respuesta = $respuesta_algoritmo['Respuesta'];
                $data['prestamo'] = 259;
                $data['respuesta_algoritmo'] = $respuesta_algoritmo;
                $asunto = "Respuesta a" . $data['user']->name . 259 . $respuesta;
                $destinatario = "alf@pa.clicredit.com";
                $from = "alf@pa.clicredit.com";
                $web = 'CliCredit.com';
                $template =  $this->load->view("emails/email_alf", $data);
               // $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
}


public function listaUsuariosInactivos12(){
   
 $users = $this->Thememodel->get_query("SELECT * from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))>1500");
       $return_data = array();
       $listUsers = array();
     foreach ($users as $key => $value){
         $session_data = $value->data;
        
         if(!empty($session_data)){
        
         $offset = 0;
      while ($offset < strlen($session_data)) {
        if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
  
      $return_data['id'] = $value->id;
      $listUsers[$key] = $return_data;
       
    }
    
}



   
    return $listUsers;

 }


 public function listaUsuariosInactivos1(){
   
 $users = $this->Thememodel->get_query("SELECT * from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))<1500");
       $return_data = array();
       $listUsers = array();
     foreach ($users as $key => $value){
         $session_data = $value->data;
        
         if(!empty($session_data)){
        
         $offset = 0;
      while ($offset < strlen($session_data)) {
        if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
  
      $return_data['id'] = $value->id;
      $listUsers[$key] = $return_data;
       
    }
    
}



   
    return $listUsers;

 }


 // funcion que retorna la lista de usuarios que tienen una sesion de 25 minutos de inactividad 
  public function listaUsuariosInactivos(){
   $users = $this->Thememodel->get_query("SELECT * from sessions where TIME_TO_SEC(TIMEDIFF(CONVERT_TZ(NOW(),'-05:00','-05:00'),CONVERT_TZ(from_unixtime(timestamp),'-05:00','-05:00')))>1500 and data<>'' ");
       $return_data = array();
       $listUsers = array();
     /*foreach ($users as $key => $value){
         $session_data = $value->data;
        
         if(!empty($session_data)){
        
         $offset = 0;
      while ($offset < strlen($session_data)) {
        if (!strstr(substr($session_data, $offset), "|")) {
          throw new Exception("invalid data, remaining: " . substr($session_data, $offset));
        }
        $pos = strpos($session_data, "|", $offset);
        $num = $pos - $offset;
        $varname = substr($session_data, $offset, $num);
        $offset += $num + 1;
        $datos = unserialize(substr($session_data, $offset));
        $return_data[$varname] = $datos;
        $offset += strlen(serialize($datos));
      }
  
      $return_data['id'] = $value->id;
      $listUsers[$key] = $return_data;
       
    }
    
}*/
var_dump($users);
   
   // return $listUsers;

 }








}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Preview extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('util','xml','text','url','security'));
		$this->load->model(array('Thememodel'));
		check_rol($this->session->userdata('roles'),array(2,3));
	}
	public function index(){
		redirect(base_url().'login');
	}

	public function ver(){
		if($this->uri->segment(3)){
			$data = array_merge($this->view_alerts(),$this->load_data());
			$where = array('posts.slug' => $this->uri->segment(3));
			$join = array(array('table'=>'users','join'=>'users.id = posts.idauthor'));
			$data['posts'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('posts.id,posts.slug,posts.title,posts.type,posts.content,posts.keywords,posts.description,posts.image,posts.datepublish,posts.datemodify,users.name','posts',$join,$where,'datepublish','DESC',10,0);
			if(empty($data['posts'])){
				redirect(base_url().'dashboard');
			}else{
				$where = array('posts.datepublish >' => $data['posts'][0]->datepublish,'posts.type' => 'Post','posts.status'=>'publicado');
				$post_siguiente = $this->Thememodel->get_all_values_where_order_pag('posts',$where,'posts.datepublish','ASC',1,0);
				if(!empty($post_siguiente)){
					$data['post_siguiente'] = $post_siguiente[0];
				}
				$where = array('posts.datepublish <' => $data['posts'][0]->datepublish,'posts.type' => 'Post','posts.status'=>'publicado');
				$post_anterior = $this->Thememodel->get_all_values_where_order_pag('posts',$where,'posts.datepublish','DESC',1,0);
				if(!empty($post_anterior)){
					$data['post_anterior'] = $post_anterior[0];
				}
				$data['title'] = $data['posts'][0]->title;
				$data['keywords'] = $data['posts'][0]->keywords;
				$data['description'] = $data['posts'][0]->description;
				$meta = $this->Thememodel->get_all_values_where('posts_metas',array('idpost'=> $data['posts'][0]->id ));
				foreach ($meta as $key) {
					if($key->metakey == 'ratingValue') $data['ratingValue'] = $key->metavalue;
					if($key->metakey == 'reviewCount') $data['reviewCount'] = $key->metavalue;
				}
				cargar_template('template/blog-post', $data);
			}
		}else{
			$this->session->set_userdata('url',$this->uri->uri_string());
			redirect(base_url().'dashboard/posts');
		}
	}

	private function load_data(){
		include 'Themeoptions.php';
		$data['categorias'] = $this->Thememodel->get_all_values_where('posts',array('type'=>'Categoría'));
		return $data;
	}
	private function view_alerts(){
		$data = array();
		if($this->session->userdata('info')){
			$data['info'] = $this->session->userdata('info');
			$this->session->unset_userdata('info');
		}
		if($this->session->userdata('error')){
			$data['error'] = $this->session->userdata('error');
			$this->session->unset_userdata('error');
		}
		return $data;
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *'); 

class Reporte_pago extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email','security','form'));
		$this->load->model(array('Thememodel','Emailmodel'));
		$this->load->library('form_validation');
		$this->lang->load('formulario','castellano');
		

	}

   
/******************************************************************************
    Función para mostrar los pagos reportados asociados a un prestamo
******************************************************************************/
   	public function obtenerReportePagos(){

		$id_prestamo = $this->uri->segment(3); //id del prestamo
		 if($id_prestamo!=null){
		 	
             $data['data'] =  $this->Thememodel->get_query("SELECT rp.id, DATE_FORMAT(rp.fecha_subida_recibo,'%d-%m-%Y') as fecha_subida_recibo ,DATE_FORMAT(rp.fecha_efecto,'%d-%m-%Y') as fecha_efecto,FORMAT(rp.importe, 2) as importe,  rp.imagen_recibo,rpe.id as id_reporte_pago_status, rpe.nombre, rpe.color FROM prestamos as p LEFT JOIN reporte_pagos as rp ON rp.id_prestamo = p.id_prestamo LEFT JOIN reporte_pagos_estados as rpe ON rpe.id = rp.reporte_pagos_estado_id WHERE rp.id_prestamo =  $id_prestamo and rp.status=1");

               echo json_encode($data);
            }
          else{
          	

              $data['data']=[];
              echo json_encode($data);
    	       
          }

	}


  private function set_upload_options($id_prestamo)
  {   
      $config = array();
      $path = 'files/reportePagos/'.'prestamo_'.$id_prestamo;
      if (!is_dir($path)) {
           mkdir('./files/reportePagos/'.'prestamo_'.$id_prestamo, 0777, TRUE);
      }
      $config['upload_path'] = $path;
      $config['allowed_types'] = 'gif|jpg|png|pdf';
      return $config;
  }


  //Parámetros de configuración para el ajuste de imagenes
    private function resizeImage($filename){
        $config['image_library'] = 'gd2';
        $config['source_image'] = $filename;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['new_image']= $filename;
        $config['quality'] = 60;  
        $config['width'] = 400;  
        $config['height'] = 300;
        return $config;
      
    }





/******************************************************************************
    Función encargada de registrar un pago
******************************************************************************/
  public function reportarPago()  
  {  
      		
           if(isset($_FILES["imagen_recibo"]["name"]))  
           {  
               $id_prestamo = $this->input->post("id_prestamo");
               $this->load->library('upload');
               $this->load->library('image_lib');
               $this->upload->initialize($this->set_upload_options($id_prestamo));
               
         
                if(!$this->upload->do_upload('imagen_recibo'))  
                {  
                	 echo json_encode(array("status" => FALSE));
                }  
                else  
                {  
                	$datos = array();
					        $path = $this->set_upload_options($id_prestamo)['upload_path'];
                  $file_name = $this->upload->data();
                  $url_imagen = $path.'/'.$file_name['file_name'];
                  $extension = end((explode(".", $file_name['file_name'])));
                  
                  if($extension!='pdf'){
                    $this->image_lib->initialize($this->resizeImage($url_imagen)); 
                    $this->image_lib->resize();
                  }

                  $datos['imagen_recibo'] =  $url_imagen;
			            $datos['fecha_subida_recibo'] = date("Y-m-d H:i:s");
			            $datos['TS_user'] = $this->session->user_id;
			            $datos['id_prestamo'] = $id_prestamo;
                  $datos['reporte_pagos_estado_id'] = 1;
                  $datos['status'] = 1;
			            $this->Thememodel->insert_value('reporte_pagos',$datos);
                  $this->enviarMailReportePagoAdmin($this->input->post("idCliente_report"),$id_prestamo);
                  $this->enviarMailReportePago($this->input->post("idCliente_report"));
                  echo json_encode(array("status" => TRUE));
                  //redirect(base_url().'gestion/prestamo/'.$id_prestamo);
                }  
           }  
      }



/******************************************************************************
    Función para actualizar un reporte de pago del lado del administrador
******************************************************************************/
  public function actualizarReportePago(){
     check_rol($this->session->userdata('roles'),array(1));
          $data = array(
                'importe' => $this->input->post('importe'), //obtenemos el importe cancelado
                'fecha_efecto' => date("Y-m-d H:i:s", strtotime($this->input->post('fecha_efecto'))), //obtenemos la fecha de efecto 
                'reporte_pagos_estado_id' => $this->input->post('selectStatusReporte')
                );
          
         $this->Thememodel->save_value('reporte_pagos',$data,array('id' => $this->input->post('id_pago')));
          echo json_encode(array("status" => TRUE));
      }




/**********************************************************************************************
   Función para actualizar y enviar un reporte de pago dependiendo del estado seleccionado
***********************************************************************************************/
  public function enviarReportePago(){
    check_rol($this->session->userdata('roles'),array(1));
   $data = array(
        'importe' => $this->input->post('importe'), //obtenemos el importe cancelado
        'fecha_efecto' => date("Y-m-d H:i:s", strtotime($this->input->post('fecha_efecto'))), //obtenemos la fecha de efecto 
        'reporte_pagos_estado_id' => $this->input->post('selectStatusReporte'));
   $saveReporte =  $this->Thememodel->save_value('reporte_pagos',$data,array('id' => $this->input->post('id_pago')));
     if($saveReporte==TRUE){
     $id = $this->input->post('idCliente');
     $reporte_pagos_estado_id = $this->input->post('selectStatusReporte');
     $data['user'] = $this->Thememodel->get_value('users', array('id' => $id ));
     $destinatario = $data['user']->email;
     $data['email_template'] = $this->Thememodel->get_all_values('email_template');
     $web = 'CliCREDIT Panamá';
     $from = "reportepagos@pa.clicredit.com";
  
    switch ($reporte_pagos_estado_id) {
       case '1': //estado procesando
          $data['email'] = $data['email_template'][0];
          $asunto = $data['email']->asunto;
          $template =  $this->load->view("emails/email_notification", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
          echo json_encode(array("status" => TRUE));
        break;
        case '2': //estado procesado con exito
          $data['email'] = $data['email_template'][1];
          $asunto = $data['email']->asunto;
          $template =  $this->load->view("emails/email_notification", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
          echo json_encode(array("status" => TRUE));
        break;
        case '3': //estado slip ilegible
          $data['email'] = $data['email_template'][2];
          $asunto = $data['email']->asunto;
          $template =  $this->load->view("emails/email_notification", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
          echo json_encode(array("status" => TRUE));
        break;
        case '4': //estado reporte incorrecto
          $data['email'] = $data['email_template'][3];
          $asunto = $data['email']->asunto;
          $template =  $this->load->view("emails/email_notification", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
          echo json_encode(array("status" => TRUE));
        break;
        case '5': // estado imposible identificar deposito
          $data['email'] = $data['email_template'][4];
          $asunto = $data['email']->asunto;
          $template =  $this->load->view("emails/email_notification", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
          echo json_encode(array("status" => TRUE));
        break;
        case '6': // estado localizando al cliente
          $data['email'] = $data['email_template'][5];
          $asunto = $data['email']->asunto;
          $template =  $this->load->view("emails/email_notification", $data, true);
          $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
          echo json_encode(array("status" => TRUE));
        break;
       }
      
     }


  }

/****************************************************************************************
Función encargada de enviar el email al administrador para notificar acerca de un pago
*****************************************************************************************/
  public function enviarMailReportePagoAdmin($user_id, $id_prestamo){
     
     $data['user'] = $this->Thememodel->get_value('users', array('id' => $user_id ));
     $data['prestamo'] = $id_prestamo;
     $asunto = "Reporte de " . $data['user']->name;
     $destinatario = "reportepagos@pa.clicredit.com";
     $from = "reportepagos@pa.clicredit.com";
     $web = 'CliCredit.com';
     $template =  $this->load->view("emails/email_notification_admin", $data, true);
     $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);

  }

/****************************************************************************************
Función encargada de enviar el email al usuario para notificar que el reporte se encuentra estado procesado 
*****************************************************************************************/
   public function enviarMailReportePago($user_id){
     
     $data['user'] = $this->Thememodel->get_value('users', array('id' => $user_id ));
     $data['email_template'] = $this->Thememodel->get_all_values('email_template');
     $data['email'] = $data['email_template'][0];
     $asunto = $data['email']->asunto;
     $destinatario = $data['user']->email;
     $from = "reportepagos@pa.clicredit.com";
     $web = 'CliCredit.com';
     $template =  $this->load->view("emails/email_notification", $data, true);
     $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);

  }



/********************************************************************************************
    Función para eliminar un pago asociado a un prestamo
*********************************************************************************************/
      public function eliminarPago(){
       check_rol($this->session->userdata('roles'),array(1));
      $id = $this->input->post('id_pago');
         $data = array(
                'status' => 0,);
          
          $this->Thememodel->save_value('reporte_pagos',$data,array('id' =>$id));
          echo json_encode(array("status" => TRUE));
        
      }




      public function getStatusReportePago(){

        $data['data'] = $this->Thememodel->get_all_values('reporte_pagos_estados');
        echo json_encode($data);
    
      }
  
	
	
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Seccion extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','security'));
		$this->load->model(array('Thememodel'));
	}

	public function index(){
		redirect(base_url());
	}

	public function contacto(){
		$data = $this->load_data();
		$data['title'] = 'Contacto | CliCREDIT Panamá';
		$data['description'] = 'Contáctanos si tienes alguna duda o comentario';
		cargar_template('template/contacto',$data);
	}
	public function como_funciona(){
		$data = $this->load_data();
		$data['title'] = '¿Cómo funcionan los préstamos CliCREDIT Panamá?';
		$data['description'] = 'Selecciona cantidad y tiempo en que deseas devolverlo, completa el formulario; ¡eso es todo! El sistema analizará la solicitud y devolverá una respuesta en 5 minutos.';
		cargar_template('template/como-funciona',$data);
	}
	public function quienes_somos(){
		$data = $this->load_data();
		$data['title'] = 'CliCREDIT Panamá. Plataforma de micro préstamos colaborativa';
		$data['description'] = 'Nuevos enfoques en la gestión del riesgo de crédito. Desde CliCREDIT Panamá trabajamos aplicando técnicas de innovación social que mejoren la inclusión financiera, aprovechado todo el poder de la tecnología.';
		cargar_template('template/quienes-somos',$data);
	}
	public function prestamos(){
		$data = $this->load_data();
		$data['title'] = 'Préstamos en linea - Préstamos Rápidos en 30 Minutos en Panamá';
		$data['description'] = 'Préstamos en línea, rápidos y sin complicaciones. Dinero en 30 minutos, hasta 500$, sin papeleos ni APC, respuesta 7 días 24 horas con garantía de calidad en el servicio.';
		cargar_template('template/prestamos',$data);
	}
	public function ayuda_preguntas_frecuentes(){
		$data = $this->load_data();
		$data['title'] = 'Ayuda, resuelve las dudas sobre préstamos rápidos en CliCREDIT Panamá';
		$data['description'] = 'Si tienes dudas de como solicitar un préstamo CliCREDIT para una urgencia o necesidad, consulta la lista de preguntas frecuentes o contáctanos: ayuda@pa.clicredit.com';
		cargar_template('template/blank-ayuda',$data);
	}
	public function empleate(){
		$data['content'] = $this->load->view('template/empleate', NULL, TRUE);
		$data['title'] = 'Empleate | CliCREDIT Panamá';
		$data['description'] = 'La vida es una aventura atrevida o nada, siempre parece imposible hasta que se logra. Si te interesa nuestra aventura, estamos buscando compañeros!!, escríbenos';
		cargar_template('template/blank', $data);
	}
	public function devolucion(){
		$data = $this->load_data();
		$data['content'] = $this->load->view('template/devolucion', NULL, TRUE);
		$data['title'] = 'Como devolver un préstamo rápido | CliCREDIT Panamá';
		$data['description'] = '¿Quieres devolver el crédito anticipadamente? Pagaras menos intereses. ¿Se venció el préstamo?, ingresa a tu cuenta CliCREDIT Panamá o contáctanos para poder ayudarte. Es importante para nosotros conocer su situación y ofrecerte la mejor solución.';
		cargar_template('template/blank', $data);
	}
	public function politicas(){
		$data['content'] = $this->load->view('template/politicas', NULL, TRUE);
		$data['title'] = 'Privacidad | CliCREDIT Panamá';
		$data['description'] = 'Políticas y aviso de privacidad en CliCREDIT Panamá';
		cargar_template('template/blank', $data);
	}
	public function terminos(){
		$data['content'] = $this->load->view('template/terminos', NULL, TRUE);
		$data['title'] = 'Términos y condiciones | CliCREDIT Panamá';
		$data['description'] = 'Términos de uso de los préstamos inmediatos CliCREDIT Panamá.';
		cargar_template('template/blank', $data);
	}

	public function nuevo_testimonio(){
		$data = $this->load_data();
		cargar_template('template/testimonio-new',$data);
	}
	public function testimonios(){
		$data = $this->load_data();
		$data['testimonios'] = $this->Thememodel->get_all_values('testimonio');
		cargar_template('template/testimonios',$data);
	}

	public function content(){
		$data = $this->load_data();
		switch ($this->uri->segment(2)) {
			case 'contacto':
			$this->contacto();
			break;
			case 'ayuda-preguntas-frecuentes':
			$this->ayuda_preguntas_frecuentes();
			break;
			case 'como-funciona':
			$this->como_funciona();
			break;
			case 'que-son-los-prestamos':
			$this->prestamos();
			break;
			case 'quienes-somos':
			$this->quienes_somos();
			break;
			case 'empleate':
			$this->empleate();
			break;
			case 'politica-privacidad':
			$this->politicas();
			break;
			case 'terminos-condiciones':
			$this->terminos();
			break;
			case 'devoluciones':
			$this->devolucion();
			break;
			default:
			cargar_template('template/404');
		}
	}

	private function load_data(){
		include 'Themeoptions.php';
		$data['cta'] = true;
		return $data;
	}
	private function view_alerts(){
		$data = '';
		if($this->session->userdata('info')){
			$data['info'] = $this->session->userdata('info');
			$this->session->unset_userdata('info');
		}
		if($this->session->userdata('error')){
			$data['error'] = $this->session->userdata('error');
			$this->session->unset_userdata('error');
		}
		$data['messages'] = $this->Thememodel->get_all_values('message');
		return $data;
	}

}
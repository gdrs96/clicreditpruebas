<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SendSms extends SendSmsCenter {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$userId = $this->input->post('usuario');
		$this->reSendSms($userId);
	}

	public function newPhone()
	{
		$userId = $this->input->post('usuario');
		$telefono = $this->input->post('newPhone');
		$this->sendNewPhone($userId,$telefono);
	}

	

}

/* End of file SendSms.php */
/* Location: ./application/controllers/SendSms.php */
<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Simulador extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date'));
		$this->load->model(array('Thememodel'));
	}

	public function index(){
		$data = $this->load_data();
		cargar_template('template/simulador',$data);

	}
	private function load_data(){
		$data = array();
		include 'Themeoptions.php';
		return $data;
	}
}

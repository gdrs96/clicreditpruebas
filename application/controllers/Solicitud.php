<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
	The controller is created to manage all the request after an step is complete during the lending process
*/
class Solicitud extends SendSmsCenter {	

	/*
			Version:
			31-12-2017 Guillermo: __construct, paso_4_check
	*/
 

	/*
		Constructor method. Calling to necessary model to manage the lending process
	*/
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url','util','date','email','security','file','form'));
		$this->load->model('Thememodel');
		//$this->load->model(array('Emailmodel'));
		$this->load->model('Emailmodel');
		$this->load->library('form_validation');
		$this->load->model('PerfilSocial');
		$this->load->model('DataMaster');
		$this->load->library('user_agent');

		// --- jjy
		$this->load->model('Algoritmo');
	}

	public function index(){
		redirect(base_url());
	}

	public function paso_1(){
		$data = $this->load_data();		
		$id=$this->session->userdata('user_id');
        
		if(!empty($id)){
			$where = array('prestamos.id_cliente' => $id, 'prestamo_actual'=>1);
			$where_in = 'prestamos.prestamo_estado_id';
			$where_ids = array(1,2,3,4,5,6,10,11,12,13);		 
			$cantidad_prestamos = $this->Thememodel->total_where_where_in('prestamos',$where,$where_in,$where_ids);
			if ($cantidad_prestamos>=1)
			{
				redirect(base_url().'solicitud/validacion');
			}

			$where = array('prestamos.id_cliente' => $id);
			$where_in = 'prestamos.prestamo_estado_id';
			$where_ids = array(8,9);		 
			$cantidad_prestamos_P_I = $this->Thememodel->total_where_where_in('prestamos',$where,$where_in,$where_ids);	

          if ($cantidad_prestamos_P_I>=1) {
          	 $ajustarQuincena = $this->input->post('ajustarQuincena');
				if($ajustarQuincena==null){
					$ajustarQuincena=0;
				}
				$this->session->set_userdata('ajustar_quincenas', $ajustarQuincena);
      	       $this->solicitud_prestamo();
           }
		}
		
		if( ! empty($this->input->post('amount'))){
			$this->form_validation->set_rules('amount','Monto','trim|required|strip_tags|html_escape');
			$this->form_validation->set_rules('duration','Teléfono','trim|strip_tags|html_escape');
			if($this->form_validation->run() == FALSE){
				$this->session->set_userdata('error',validation_errors());
				redirect(base_url());
			}else{
				$datos_solicitud = array('amount' => $this->input->post('amount'),'duration' => $this->input->post('duration'));
				$this->session->set_userdata('datos_solicitud', $datos_solicitud);
				$ajustarQuincena = $this->input->post('ajustarQuincena');
				if($ajustarQuincena==null){
					$ajustarQuincena=0;
				}
				$this->session->set_userdata('ajustar_quincenas', $ajustarQuincena);
			}
		}
		if($this->session->userdata('user_id')){
			$data['user'] = $this->Thememodel->get_value('users',array('id' => $this->session->userdata('user_id')));
		}
		if($this->session->userdata('email')){
			/*$datos_solicitud = array();

			$datos_solicitud['duration'] = $this->input->post('duration');
			$datos_solicitud['amount'] = $this->input->post('amount');
			$datos_solicitud['ip'] = $this->input->ip_address();
			$datos_solicitud['user_agent'] = $this->input->user_agent();
			$datos_solicitud['paso_1_completado'] = 1;

			$datos['datos'] = json_encode( $datos_solicitud );
			$datos['cantidad'] = $datos_solicitud['amount'];
			$datos['tiempo'] = $datos_solicitud['duration'];
			$datos['id_cliente'] = $this->session->userdata('user_id');
			$datos['created_at'] = date('Y-m-d H:i:s');
			$datos['updated_at'] = date('Y-m-d H:i:s');
			$id_prestamo = $this->Thememodel->insert_value('prestamos',$datos);

			$datos_solicitud['id'] = $id_prestamo;

			$this->session->set_userdata('id_prestamo', $id_prestamo);

			$datos['datos'] = json_encode( $datos_solicitud );
			$this->Thememodel->save_value('prestamos',$datos,array('id_prestamo' => $id_prestamo));

			$this->session->set_userdata('datos_solicitud', $datos_solicitud);*/
			$this->max_solicitar($this->session->userdata('user_id'));
			redirect(base_url().'solicitud/paso-2');
		}
		$data['nivel'] = 'paso-1';
		$data['wizard'] = $this->load->view('template/solicitud/wizard', $data, TRUE);
		cargar_template('template/solicitud/solicitar-1',$data);
	}
	public function paso_1_check(){
		$data = $this->load_data();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('name','Nombre','trim|strip_tags|html_escape|required');
		$this->form_validation->set_rules('segundo_nombre','Segundo nombre','trim|strip_tags|html_escape');
		$this->form_validation->set_rules('primer_apellido','Primer Apellido','trim|strip_tags|html_escape|required');
		$this->form_validation->set_rules('segundo_apellido','Código postal','trim|strip_tags|html_escape');
		$this->form_validation->set_rules('dia_fecha_nacimiento','Día de fecha de nacimiento','trim|strip_tags|html_escape|required');
		$this->form_validation->set_rules('mes_fecha_nacimiento','Mes de fecha de nacimiento','trim|strip_tags|html_escape|required');
		$this->form_validation->set_rules('year_fecha_nacimiento','Año de fecha de nacimiento','trim|strip_tags|html_escape|required');
		$this->form_validation->set_rules('telefono','Teléfono','trim|strip_tags|html_escape');
		$this->form_validation->set_rules('email','Email','trim|strip_tags|html_escape|valid_email|is_unique[users.email]|required', array('is_unique' => 'Ya existe un usuario registrado con este correo.'));
		$this->form_validation->set_rules('email_check','Repita su Email','trim|matches[email]');
		$this->form_validation->set_rules('n_identificacion','Cedula','trim|strip_tags|html_escape|is_unique[users.n_identificacion]', array('is_unique' => 'Ya existe un usuario registrado con este número de Documento.'));
		$this->form_validation->set_rules('newsletter','Newsletter','trim|strip_tags|html_escape');
         
		if($this->form_validation->run() == FALSE){
			$this->session->set_userdata('error',validation_errors());
			$data['nivel'] = 'paso-1';
			$data['wizard'] = $this->load->view('template/solicitud/wizard', $data, TRUE);
			cargar_template('template/solicitud/solicitar-1',$data);
		} else{
			if( ! $this->session->userdata('user_id')){
				$datos['email'] = $this->input->post('email');
				$datos['tipo_documento'] = $this->input->post('tipo_documento');
				$datos['n_identificacion'] = $this->input->post('n_identificacion');
				$datos['pass'] = md5($this->input->post('pass'));
			}
			$datos['name'] = $this->input->post('name');
			$datos['segundo_nombre'] = $this->input->post('segundo_nombre');
			$datos['primer_apellido'] = $this->input->post('primer_apellido');
			$datos['segundo_apellido'] = $this->input->post('segundo_apellido');
			
			$dia_birthdate = $this->input->post('dia_fecha_nacimiento');
			if(strlen($dia_birthdate)==1)
			{
				$dia_birthdate = "0".$dia_birthdate;
			}
						
			$mes_birthdate = $this->input->post('mes_fecha_nacimiento');
			if(strlen($mes_birthdate)==1)
			{
				$mes_birthdate = "0".$mes_birthdate;
			}
			
			$datos['fecha_nacimiento'] = $this->input->post('year_fecha_nacimiento')."-".$mes_birthdate."-".$dia_birthdate;
			$datos['red_social'] = $this->input->post('red_social');
			$datos['telefono'] = $this->input->post('telefono');
			$datos['newsletter'] = ($this->input->post('newsletter')) ? $this->input->post('newsletter') : '';
			$datos['datelastlogin'] = date('Y-m-d H:i:s');
			$datos['image'] = "-/img-users/usuario-default.svg";
			
			if($this->session->userdata('user_id')){
				$this->max_solicitar($this->session->userdata('user_id'));
				if( $this->Thememodel->save_value('users',$datos,array('id' => $this->session->userdata('user_id'))) ){
					redirect(base_url().'solicitud/paso-2');
				}
			}else{
				$datos['status'] = 'activo';
				$datos['dateregistered'] = date('Y-m-d H:i:s');
				$datos['datelastlogin'] = date('Y-m-d H:i:s');
				$datos['id_rol'] = 2;
				$id_user = $this->Thememodel->insert_value('users',$datos);
				$this->session->set_userdata('roles', array('0') );
				$this->session->set_userdata('user_id', $id_user);
				$this->session->set_userdata('email', $datos['email']);
				$this->session->set_userdata('nicename', $datos['name'] );
	
				$this->enviarMailBienvenida($this->Thememodel->get_value('users',array('email' => $datos['email'])));
				$this->max_solicitar($id_user);
				redirect(base_url().'solicitud/paso-2');	
			}
		}
	}

//Datos iniciales del prestamo
	public function guardarDatosPrestamo(){
		if($this->session->userdata('email')){
			$datos_solicitud = array();
            $id_cliente = $this->session->userdata('user_id');
			$datos_solicitud['duration'] = $this->input->post('duration');
			$datos_solicitud['amount'] = $this->input->post('amount');
			$datos_solicitud['ip'] = $this->input->ip_address();
			$accessKey='fcf407c8c441b7d4b08cc15168c6597a';
			$url ='http://api.ipstack.com/'.$datos_solicitud['ip'].'?access_key='.$accessKey;     
			$datos_solicitud['user_agent'] = $this->agent->platform();
			$datos_solicitud['paso_1_completado'] = 1;

			$datos['datos'] = json_encode( $datos_solicitud );
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			curl_close($ch);
			$datos['datos_ip'] = $response;
			$datos['cantidad'] = $datos_solicitud['amount'];
			$datos['tiempo'] = $datos_solicitud['duration'];
			$datos['ajustar_quincena'] = $this->session->userdata('ajustar_quincenas');
			$datos['id_cliente'] = $id_cliente;
			$datos['created_at'] = date('Y-m-d H:i:s');
			$datos['updated_at'] = date('Y-m-d H:i:s');
			$datos['ultima_actualizacion'] = date('Y-m-d H:i:s');
			$datos['fecha_efectivo_prestamo'] = date('Y-m-d H:i:s');
			$datos['status_alerta'] = 1;
           //En caso de tener un prestamo cancelado actualizamos las posicion actual a 0 para poder reflejar el prestamo actualmente solicitado
			$prestamo_anterior = $this->Thememodel->get_query("SELECT id_prestamo FROM prestamos where `prestamo_estado_id` = 7 AND `id_cliente` = $id_cliente AND `prestamo_actual`=1 ORDER by id_prestamo DESC LIMIT 1");
            if(!empty($prestamo_anterior)){
                $id_prestamo_anterior = $prestamo_anterior[0]->id_prestamo;
            	$datos_prestamo_anterior['prestamo_actual'] = 0;
            	$this->Thememodel->save_value('prestamos',$datos_prestamo_anterior,array('id_prestamo' => $id_prestamo_anterior));
            	$datos['prestamo_actual'] = 1;
            }else{
            	$datos['prestamo_actual'] = 1;
            }

			

			$id_prestamo = $this->Thememodel->insert_value('prestamos',$datos);

			$datos_solicitud['id'] = $id_prestamo;

			$this->session->set_userdata('id_prestamo', $id_prestamo);

			$datos['datos'] = json_encode( $datos_solicitud );
			$this->Thememodel->save_value('prestamos',$datos,array('id_prestamo' => $id_prestamo));

			$this->session->set_userdata('datos_solicitud', $datos_solicitud);

		}
	}
	
	public function paso_2(){
		check_rol($this->session->userdata('roles'),array(0));
	
        $this->verificar_pasos();
		$this->load->model('VariablesPrimarias');
		$this->listaVariablesPrimariasCompleta=$this->VariablesPrimarias->ProcessVariablesPrimarias();
		
		
		
		//var_dump($this->VariablesPrimarias->GetVariablePrimaria($this->listaVariablesPrimariasCompleta,"CC6","1"));
		//var_dump($this->listaVariablesPrimariasCompleta);
		
		$data = $this->load_data();
		$data['num_hijos'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_1"',20,0);
		$data['lugares_nac'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_2"',20,0);
		$data['clases_sociales'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_3"',20,0);
		$data['generos'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_4"',20,0);
		$data['personas_dep'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_5"',20,0);
		$data['estados_civil'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_6"',20,0);
		$data['primeros_mat'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_6_1"',20,0);
		$data['tienen_trab'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_6_2"',20,0);
		$data['en_que_trab'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_6_3"',20,0);
		$data['mant_pensiones'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_7"',20,0);
		$data['lic_manejar'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_8"',20,0);
		$data['tie_veh'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_9"',20,0);
		$data['tipo_veh'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_9_1"',20,0);
		$data['prop_veh'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_9_4"',20,0);
		$data['tip_cont_cel'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_15"',20,0);
		$data['cot_seg_social'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_16"',20,0);
		$data['nivel_estudios'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_18"',20,0);
		$data['fin_progr'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_18_1"',20,0);
		$data['donde_estud'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_18_2"',20,0);
		
		// --- jjy - 1.57
		$data['pantalla_rota']   = $this->Thememodel->get_all_values_where_pag('data_page','question="2_11"',20,0);
		$data['tomar_decision']  = $this->Thememodel->get_all_values_where_pag('data_page','question="2_19"',20,0);
		$data['correr_riesgo']   = $this->Thememodel->get_all_values_where_pag('data_page','question="2_22"',20,0);
		$data['interes_trabajo'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_23"',20,0);

		// --- jjy - 1.58
		// --- 2_13 sin lista de valores en excel
		$data['varios_celulares'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_14"',20,0);
		// --- 2_17 sin lista de valores en excel
		$data['amigos_fallan'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_20"',20,0);
		$data['al_frente']     = $this->Thememodel->get_all_values_where_pag('data_page','question="2_21"',20,0);

		// --- jjy - 1.59
		$data['sit_sociales']  = $this->Thememodel->get_all_values_where_pag('data_page','question="2_24"',20,0);
		$data['gusto_trabajo'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_25"',20,0);
		$data['energia']       = $this->Thememodel->get_all_values_where_pag('data_page','question="2_26"',20,0);

		// --- jjy - 1.61
		// --- 2_10 sin lista de valores en excel
		$data['cuantos_cel']  = $this->Thememodel->get_all_values_where_pag('data_page','question="2_12"',20,0);
		$data['sector_vives'] = $this->Thememodel->get_all_values_where_pag('data_page','question="2_17_1"',null,0);

		$data['preguntas'] = $this->DataMaster->GetDataStep("2");
		$data['nivel'] = 'paso-2';
		$data['wizard'] = $this->load->view('template/solicitud/wizard', $data, TRUE);
		cargar_template('template/solicitud/solicitar-2',$data);
		
	}
	public function paso_2_check(){
		
		//$this->load->library('form_validation');
		//var_dump($this->form_validation->run());
		check_rol($this->session->userdata('roles'),array(0));
		$data = $this->load_data();
		$this->load->library('form_validation');
		$this->form_validation->set_rules('2_1','¿Cuántos hijos tienes?','trim|strip_tags|html_escape|required');
		
       
		$datos_solicitud = $this->input->post();

		if($this->session->userdata('datos_solicitud') ){
			if(empty($this->session->userdata('datos_solicitud')["paso_2_completado"]) && !empty($datos_solicitud)){
            $this->guardarDatosPrestamo();
			$datos_solicitud = array_merge($datos_solicitud, $this->session->userdata('datos_solicitud'));

			//envio SMS
			//quitamos -
			$datos_solicitud['2_10_telefono'] = str_replace('-', '', $datos_solicitud['2_10_telefono']);


			//Actualizamos en el array de los datos de la solicitud, la variable de monto y duracion del prestamo respectivamente
			$datos_solicitud['amount'] = $this->input->post('amount');
			$datos_solicitud['duration'] = $this->input->post('duration');
			$datos_solicitud['ultima_actualizacion'] = date('Y-m-d H:i:s');


			if( $datos_solicitud['2_10_telefono'] ){
				$datos_solicitud['codigo_verificacion'] = substr( md5( $datos_solicitud['id'].date('Y-m-d H:i:s') ), -5 );

		  
           
		    //llamamos al controlador correspondiente para enviar el codigo por SMS.
             $this->sendSms($datos_solicitud['id'],$datos_solicitud['codigo_verificacion'],$datos_solicitud['2_10_telefono']);
			}
            $datos_solicitud['paso_2_completado'] = 1;
            $this->session->set_userdata('datos_solicitud', $datos_solicitud);



			$datos['datos'] = json_encode( $datos_solicitud );
			$this->Thememodel->save_value('prestamos',$datos,array('id_prestamo' => $datos_solicitud['id']));
		
           if ($this->form_validation->run() == FALSE)
           {
           	redirect(base_url().'solicitud/paso-2');
           }else{
		      check_rol($this->session->userdata('roles'),array(0));
		      $data = $this->load_data();
			  
			  $data['sits_laboral'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_1"',20,0);
			  $data['acts_laboral'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_2"',20,0);
			  $data['tipos_contrato'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_4"',20,0);
			  $data['tams_empresa'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_8"',20,0);
			  $data['has_credits_card'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_14"',20,0);
			  $data['how_credits'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_14_1"',20,0);
			  
			  // --- jjy - 1.56
			  $data['ingresos_mes']     = $this->Thememodel->get_all_values_where_pag('data_page','question="3_10"',20,0);
			  // 3_11 - sin lista de valores en excel
			  // 3_5  - sin lista de valores en excel
			  $data['otros_creditos']   = $this->Thememodel->get_all_values_where_pag('data_page','question="3_15_3"',20,0);
			  $data['dcto_directo']     = $this->Thememodel->get_all_values_where_pag('data_page','question="3_13"',20,0);
			  $data['destino_prestamo'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_12"',20,0);
			  $data['deudas_vencidas']  = $this->Thememodel->get_all_values_where_pag('data_page','question="3_15_2"',20,0);
			  
			  // --- jjy - 1.57
			  $data['viejos_habitos'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_18"',20,0);
			  $data['falta_social']   = $this->Thememodel->get_all_values_where_pag('data_page','question="3_19"',20,0);

			  // --- jjy - 1.58
			  $data['callar']       = $this->Thememodel->get_all_values_where_pag('data_page','question="3_16"',20,0);
			  $data['temo_castigo'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_22"',20,0);
			  $data['jefe_razon']   = $this->Thememodel->get_all_values_where_pag('data_page','question="3_23"',20,0);

			  // --- jjy - 1.59
			  $data['estabilidad'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_17"',20,0);
			  $data['vacaciones']  = $this->Thememodel->get_all_values_where_pag('data_page','question="3_20"',20,0);
			  $data['capacidad']   = $this->Thememodel->get_all_values_where_pag('data_page','question="3_21"',20,0);

			  // --- jjy - 1.60
			  // 3_9  - sin lista de valores en excel

			  // --- jjy - 1.61
			  $data['tienes_deudas'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_15"',20,0);
			  $data['periodo_pago']  = $this->Thememodel->get_all_values_where_pag('data_page','question="3_7"',20,0);
			  
				$data['preguntas'] = $this->DataMaster->GetDataStep("3");
				$data['nivel'] = 'paso-3';
				$data['wizard'] = $this->load->view('template/solicitud/wizard', $data, TRUE);
				//var_dump($this->session->userdata('ajustar_quincenas'));
		        //cargar_template('template/solicitud/solicitar-3',$data);
				redirect(base_url().'solicitud/paso-3');
		   }
		}else{
			$this->verificar_pasos();
		}

		}else{
			
			redirect(base_url());
		}
	}
	public function paso_3(){
  
		
		//NO BORRRAR ESTE CODIGO EVITA LOS SALTOS ENTRE PASOS EN LA SOLICITUD
		/*if ($this->form_validation->run() == FALSE)
		    {
			    redirect(base_url().'solicitud/paso-2');
		    }
		else{*/
		
		
			
		    check_rol($this->session->userdata('roles'),array(0));
		    $data = $this->load_data();
			$this->verificar_pasos();
			
			// --- jjy - 1.56
			$data['ingresos_mes']     = $this->Thememodel->get_all_values_where_pag('data_page','question="3_10"',20,0);
			// 3_11 - sin lista de valores en excel
			// 3_5  - sin lista de valores en excel
			$data['otros_creditos']   = $this->Thememodel->get_all_values_where_pag('data_page','question="3_15_3"',20,0);
			$data['dcto_directo']     = $this->Thememodel->get_all_values_where_pag('data_page','question="3_13"',20,0);
			$data['destino_prestamo'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_12"',20,0);
			$data['deudas_vencidas']  = $this->Thememodel->get_all_values_where_pag('data_page','question="3_15_2"',20,0);	

			// --- jjy - 1.57
			$data['viejos_habitos'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_18"',20,0);
			$data['falta_social']   = $this->Thememodel->get_all_values_where_pag('data_page','question="3_19"',20,0);

			// --- jjy - 1.58
			$data['callar']       = $this->Thememodel->get_all_values_where_pag('data_page','question="3_16"',20,0);
			$data['temo_castigo'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_22"',20,0);
			$data['jefe_razon']   = $this->Thememodel->get_all_values_where_pag('data_page','question="3_23"',20,0);

			// --- jjy - 1.59
			$data['estabilidad'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_17"',20,0);
			$data['vacaciones']  = $this->Thememodel->get_all_values_where_pag('data_page','question="3_20"',20,0);
			$data['capacidad']   = $this->Thememodel->get_all_values_where_pag('data_page','question="3_21"',20,0);
			
			// --- jjy - 1.60
			// 3_9  - sin lista de valores en excel

			// --- jjy - 1.61
			$data['tienes_deudas'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_15"',20,0);
			$data['periodo_pago']  = $this->Thememodel->get_all_values_where_pag('data_page','question="3_7"',20,0);

			$data['sits_laboral'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_1"',20,0);
			$data['acts_laboral'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_2"',20,0);
			$data['tipos_contrato'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_4"',20,0);
			$data['tams_empresa'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_8"',20,0);
			$data['has_credits_card'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_14"',20,0);
			$data['how_credits'] = $this->Thememodel->get_all_values_where_pag('data_page','question="3_14_1"',20,0);
			
			$data['preguntas'] = $this->DataMaster->GetDataStep("3");
			$data['nivel'] = 'paso-3';
			$data['wizard'] = $this->load->view('template/solicitud/wizard', $data, TRUE);
		  cargar_template('template/solicitud/solicitar-3',$data);
		//}	
		
	}
	//var_dump($this->session->userdata('datos_solicitud')["paso_2_completado"]);
	public function paso_3_check(){
		check_rol($this->session->userdata('roles'),array(0));
		$data = $this->load_data();
		$datos_solicitud = $this->input->post();

		if($this->session->userdata('datos_solicitud')){
			if(!empty($datos_solicitud)){
			$datos_solicitud = array_merge($datos_solicitud, $this->session->userdata('datos_solicitud'));
			
			$datos_solicitud['paso_3_completado'] = 1;
			$datos_solicitud['amount'] = $this->input->post('amount');
			$datos_solicitud['duration'] = $this->input->post('duration');
			$datos_solicitud['ultima_actualizacion'] = date('Y-m-d H:i:s');

			//Actualizamos en el array de los datos de la solicitud, la variable de monto y duracion del prestamo respectivamente
			
			$datos_json = json_encode( $datos_solicitud );
            $datos =  array('cantidad' => $datos_solicitud['amount'], 'tiempo' => $datos_solicitud['duration'], 'datos'=> $datos_json, 'ultima_actualizacion'=>$datos_solicitud['ultima_actualizacion'],'fecha_efectivo_prestamo'=>date('Y-m-d H:i:s'), 'ajustar_quincena'=>$this->session->userdata('ajustar_quincenas'), 'status_alerta'=>1);
		
			$this->Thememodel->save_value('prestamos',$datos,array('id_prestamo' => $datos_solicitud['id']));

			$this->session->set_userdata('datos_solicitud', $datos_solicitud);
		}else{
			$this->verificar_pasos();
		}
		}else{
			redirect(base_url());
			
		}
        // var_dump($this->session->userdata('paso_2_completado'));
		redirect(base_url().'solicitud/paso-4');
	}
	public function paso_4(){
		check_rol($this->session->userdata('roles'),array(0));
		$this->verificar_pasos();
		$data = $this->load_data();
		

		$provincias = $this->Thememodel->get_all_values_where('data_page', 'question="4_1"');
		foreach ($provincias as $provincia) {
			$provincia->distritos = $this->Thememodel->get_all_values_where('data_page', "question='4_2' AND rel_col='$provincia->id_page'");
			foreach ($provincia->distritos as $distrito) {
				$distrito->corregimientos = $this->Thememodel->get_all_values_where('data_page', "question='4_3' AND rel_col='$distrito->id_page'");
				foreach ($distrito->corregimientos as $corregimiento) {
					$corregimiento->sectores = $this->Thememodel->get_all_values_where('data_page', "question='4_4' AND rel_col='$corregimiento->id_page'");
					foreach ($corregimiento->sectores as $sector) {
						$sector->barrios = $this->Thememodel->get_all_values_where('data_page', "question='4_5' AND rel_col='$sector->id_page'");
					}
				}
			}
		}
		$data['provincias'] = $provincias;
		$data['cambios_dom'] = $this->Thememodel->get_all_values_where_pag('data_page','question="4_10"',20,0);
		
		// --- jjy - 1.56
		$data['tipos_vivienda']   = $this->Thememodel->get_all_values_where_pag('data_page','question="4_9"',20,0);

		// --- jjy - 1.57
		$data['interes_mismo_hys'] = $this->Thememodel->get_all_values_where_pag('data_page','question="4_14"',20,0);
		$data['gente_autoritaria'] = $this->Thememodel->get_all_values_where_pag('data_page','question="4_15"',20,0);
		$data['dormir_bien']       = $this->Thememodel->get_all_values_where_pag('data_page','question="4_16"',20,0);

		// --- jjy - 1.58
		$data['opinion_padres'] = $this->Thememodel->get_all_values_where_pag('data_page','question="4_17"',20,0);

		// --- jjy - 1.59
		$data['gozar_vida']      = $this->Thememodel->get_all_values_where_pag('data_page','question="4_11"',20,0);
		$data['interesante_ser'] = $this->Thememodel->get_all_values_where_pag('data_page','question="4_12"',20,0);
		$data['dificultades']    = $this->Thememodel->get_all_values_where_pag('data_page','question="4_13"',20,0);
		
		$data['preguntas'] = $this->DataMaster->GetDataStep("4");
		$data['nivel'] = 'paso-4';
		$data['wizard'] = $this->load->view('template/solicitud/wizard', $data, TRUE);
		cargar_template('template/solicitud/solicitar-4',$data);
	}
	/*
		Method to check information set for the customer in step 4. After the method, all the information necessary to the lending should be filled.
	*/
	public function paso_4_check(){
		/*
			31-12-2017 Guillermo: Added calling to Perfil_Social and saved the result in the correspondence table
		*/
		check_rol($this->session->userdata('roles'),array(0));
		$data = $this->load_data();
		$datos_solicitud = $this->input->post();
		if($this->session->userdata('datos_solicitud')){
			if(!empty($datos_solicitud)){
			$datos_solicitud = array_merge($datos_solicitud, $this->session->userdata('datos_solicitud'));
			
			$datos_solicitud['paso_4_completado'] = 1;


			//Actualizamos en el array de los datos de la solicitud, la variable de monto y duracion del prestamo respectivamente
			$datos_solicitud['amount'] = $this->input->post('amount');
			$datos_solicitud['duration'] = $this->input->post('duration');
			$datos_solicitud['ultima_actualizacion'] = date('Y-m-d H:i:s');
			
			$fechaNacimientoUsuario = $this->Thememodel->get_select('fecha_nacimiento','users',array('id' => $this->session->user_id ));
			$datos_solicitud['fecha_nacimiento'] = $fechaNacimientoUsuario[0]->fecha_nacimiento;
			$this->session->set_userdata('datos_solicitud', $datos_solicitud);

			// --- jjy --- Trello 1.122 ...

			//    $datos_solicitud=$this->PerfilSocial->hacerCalculosPersonales($datos_solicitud);
			
			$idPrestamo = $datos_solicitud['id'];

			// -- se devuelve el detalle de los cálculos al pasar los parámetros 2 y 3 como TRUE, TRUE
			$datos_json = json_encode( $datos_solicitud );
          $datos = array(
            	'cantidad'               => $datos_solicitud['amount'],
            	'tiempo'   				 => $datos_solicitud['duration'],
            	'datos'    				 => $datos_json,
            	'ultima_actualizacion'   => $datos_solicitud['ultima_actualizacion'],
            	'fecha_efectivo_prestamo'=> date('Y-m-d H:i:s'),
            	'ajustar_quincena'       => $this->session->userdata('ajustar_quincenas'),
            	'status_alerta'          => 1
           	);
            // Atención: Al parecer se incluyó un nuevo dato - fecha_efectivo_prestamo - se debe revisar el algoritmo para los valores que toman fecha-ultima-actualización --- jjy

			$this->Thememodel->save_value('prestamos',$datos,array('id_prestamo' => $datos_solicitud['id']));

			$this->session->set_userdata('datos_solicitud', $datos_solicitud);
		}else{
			$this->verificar_pasos();
		}
		}else{
			redirect(base_url());
		}
		redirect(base_url().'solicitud/paso-5');
	}
	public function paso_5(){
		check_rol($this->session->userdata('roles'),array(0));
		$this->verificar_pasos();
		$data = $this->load_data();
		$user = $this->Thememodel->get_value('users',array('id' => $this->session->userdata('user_id')));
		$data['calificacion'] = $this->Thememodel->get_value('calificacion',array('id' => $user->id_calificacion));
		$data['bancos']   = $this->Thememodel->get_all_values_where_pag('data_page','question="5_2"',50,0);
		$data['preguntas'] = $this->DataMaster->GetDataStep("5"); // --- jjy
		$data['nivel'] = 'paso-5';
		$data['wizard'] = $this->load->view('template/solicitud/wizard', $data, TRUE);
		cargar_template('template/solicitud/solicitar-5',$data);
	}
	

  public function paso_5_check(){
		check_rol($this->session->userdata('roles'),array(0));
		$data = $this->load_data();
		$datos_solicitud = $this->input->post();
		if($this->session->userdata('datos_solicitud')){
       if(!empty($datos_solicitud)){
        $datos_solicitud = array_merge($datos_solicitud, $this->session->userdata('datos_solicitud'));
        $codigo_verificacion = $this->session->userdata('datos_solicitud')['codigo_verificacion'];
        //verificamos el codigo de validación sms
        if($codigo_verificacion==$this->input->post('codigo_verificacion_telefono')){
			$datos_solicitud['paso_5_completado'] = 1;
			$datos_solicitud['amount'] = $this->input->post('amount');
			$datos_solicitud['duration'] = $this->input->post('duration');
			$datos_solicitud['ultima_actualizacion'] = date('Y-m-d H:i:s');


			$id_prestamo = $datos_solicitud['id'];
			
            $usuario = $this->Thememodel->get_value('users',array('id'=> $this->session->userdata('user_id')));

            $comprobar_imagen_cedula = $this->guardarImgCedula('imagen_cedula',$id_prestamo);
            $comprobar_imagen_talonarios = $this->guardarImgMultiple('imagen_talonarios',$id_prestamo);
            $comprobar_imagen_recibos = $this->guardarImgMultiple('imagen_recibos',$id_prestamo);
            $comprobar_imagen_seguro_social = $this->guardarImg('imagen_seguro_social',$id_prestamo);

            if(!empty($_FILES['imagen_cedula']) &&  $comprobar_imagen_cedula  && $comprobar_imagen_talonarios  &&  $comprobar_imagen_recibos  &&  $comprobar_imagen_seguro_social){
               
          
               
			//Actualizamos en el array de los datos de la solicitud, la variable de monto y duracion del prestamo respectivamente
			
		
           
			$datos['datos'] = json_encode( $datos_solicitud );
			$datos['updated_at'] = date('Y-m-d H:i:s');
			
			
	          $datos_json = json_encode( $datos_solicitud );
			  $dataPrestamo = array(
            	'cantidad'               => $datos_solicitud['amount'],
            	'tiempo'   				 => $datos_solicitud['duration'],
            	'datos'    				 => $datos_json,
            	'ultima_actualizacion'   => $datos_solicitud['ultima_actualizacion'],
            	'fecha_efectivo_prestamo'=> date('Y-m-d H:i:s'),
            	'ajustar_quincena'       => $this->session->userdata('ajustar_quincenas'),
            	'status_alerta'          => 0
           	);

		

			$this->Thememodel->save_value('prestamos',$dataPrestamo,array('id_prestamo' => $id_prestamo));

			$calculos_perfil_social     = $this->Algoritmo->getCalculosVariables( $id_prestamo, 'Perfil social', TRUE, TRUE );
			$calculos_honestidad        = $this->Algoritmo->getCalculosVariables( $id_prestamo, 'Honestidad', TRUE, TRUE );
			$calculos_responsabilidad   = $this->Algoritmo->getCalculosVariables( $id_prestamo, 'Responsabilidad', TRUE, TRUE );
			$calculos_trabajo           = $this->Algoritmo->getCalculosVariables( $id_prestamo, 'Trabajo', TRUE, TRUE );
			$calculos_capacidad_de_pago = $this->Algoritmo->getCalculosVariables( $id_prestamo, 'Capacidad de Pago', TRUE, TRUE );
			$calculos_macroeconomicas   = $this->Algoritmo->getCalculosVariables( $id_prestamo, 'Macroeconomicas', TRUE, TRUE );

			// --- jjy --- Guardando calculos de todos los GV en bbdd ( _cc y _vp )
			$calculos_solicitud = array_merge(
				$calculos_perfil_social,
				$calculos_honestidad,
				$calculos_responsabilidad,
				$calculos_trabajo,
				$calculos_capacidad_de_pago,
				$calculos_macroeconomicas
			);

			// --------------------------------------------------------------------------------

		
			$calculos_json = json_encode( $calculos_solicitud ); // --- jjy
            
           $dataPrestamo = array(
            	'calculos' => $calculos_json,
            );

		   $datos_prestamo = $this->Thememodel->save_value('prestamos',$dataPrestamo,array('id_prestamo' => $id_prestamo));
            
        if($datos_prestamo){
		       $respuesta_algoritmo = $this->Algoritmo->getArbolDeAprobacion($id_prestamo);
               $respuesta = $respuesta_algoritmo['Respuesta'];
               $data['user'] = $usuario;
               $destinatario = $data['user']->email;
               $data['email_template'] = $this->Thememodel->get_all_values('email_template');
               $web = 'CliCREDIT Panamá';
               $from = "info@pa.clicredit.com";
              switch ($respuesta) {
            	case 'PRE-APROBADO':
            	  $data_estado = array('prestamo_estado_id' => 12);
            	  $this->Thememodel->save_value('prestamos',$data_estado,array('id_prestamo' => $id_prestamo));
          	      $data['email'] = $data['email_template'][10];
                  $asunto = $data['email']->asunto;
                  $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
                  $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
          	   break;
          	   case 'EN REVISION':
          	      $data_estado = array('prestamo_estado_id' => 11);
            	  $this->Thememodel->save_value('prestamos',$data_estado,array('id_prestamo' => $id_prestamo));
          	      $data['email'] = $data['email_template'][9];
                  $asunto = $data['email']->asunto;
                  $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
                  $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
               break;
               case 'DENEGADO':
                  $data_estado = array('prestamo_estado_id' => 10);
            	  $this->Thememodel->save_value('prestamos',$data_estado,array('id_prestamo' => $id_prestamo));
          	      $data['email'] = $data['email_template'][8];
                  $asunto = $data['email']->asunto;
                  $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
                  $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
               break;
              }
                $data['prestamo'] = $id_prestamo;
                $data['respuesta_algoritmo'] = $respuesta_algoritmo;
                $asunto = "Respuesta a " .' '.$data['user']->name .' '.$id_prestamo .' '.$respuesta;
                $destinatario = "info.clicredit@gmail.com";
                $web = 'CliCredit.com';
                $template =  $this->load->view("emails/email_alf", $data, true);
                $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
            

               echo json_encode(array("status" => TRUE)); 
			}else{
			   echo json_encode(array("status" => FALSE)); 
			}

			
		}else{
			
			 	 echo json_encode(array("status" => "ERROR_IMAGEN")); 
			
		
			
		}
	   }else{
	   	  echo json_encode(array("status" => "ERROR_CODIGO_VERIFICACION"));
	   }
	    }else{
             $this->verificar_pasos();
		}
	  }
	 
		
		
	}




  private function set_upload_options($id_prestamo)
  {   
      $config = array();
      $path = 'files/prestamos/'.'prestamo_'.$id_prestamo;
      if (!is_dir($path)) {
           mkdir('./files/prestamos/'.'prestamo_'.$id_prestamo, 0777, TRUE);
      }
      $config['upload_path'] = $path;
      $config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';
      return $config;
  }

//Parámetros de configuración para el ajuste de imagenes
    private function resizeImage($filename){
        $config['image_library'] = 'gd2';
        $config['source_image'] = $filename;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['new_image']= $filename;
        $config['quality'] = 60;  
        $config['width'] = 400;  
        $config['height'] = 300;
        $config['max_size'] = '8388608';
        return $config;
      
    }

    //Parámetros de configuración para el ajuste de imagenes
    private function resizeImageCedula($filename){
        $config['image_library'] = 'gd2';
        $config['source_image'] = $filename;
        $config['create_thumb'] = FALSE;
        $config['maintain_ratio'] = TRUE;
        $config['new_image']= $filename;
        $config['width'] = 400;  
        $config['height'] = 300;
        $config['max_size'] = '8388608';
        return $config;
      
    }



   private function set_upload_options_user($id_user)
   {   
      $config = array();
      $path = 'files/usuarios/'.'usuario_'.$id_user;
      if (!is_dir($path)) {
           mkdir('./files/usuarios/'.'usuario_'.$id_user, 0777, TRUE);
      }
      $config['upload_path'] = $path;
      $config['allowed_types'] = 'gif|jpg|png|pdf|jpeg';
      return $config;
   }


//Función encargada de guardar multiples imagenes o archivos de la documentacion asociada al prestamo  
 public function guardarImgMultiple($nameField,$id_prestamo)
 {
    
    
    if(isset($_FILES[$nameField])){
    if(count($_FILES[$nameField]['size'])<=3){
    $files = array($nameField => $_FILES[$nameField]);
    $this->load->library('image_lib');
    foreach($files as $key => $value)
    {
        for($i=0; $i<count($files[$nameField]['size']); $i++)
        {
            $_FILES[$nameField]['name']     = $value['name'][$i];
            $_FILES[$nameField]['type']     = $value['type'][$i];
            $_FILES[$nameField]['tmp_name'] = $value['tmp_name'][$i];
            $_FILES[$nameField]['error']    = $value['error'][$i];
            $_FILES[$nameField]['size']     = $value['size'][$i]; 
            
            $this->load->library('upload');
            
            $this->upload->initialize($this->set_upload_options($id_prestamo));
             
            if(!$this->upload->do_upload($nameField))  
            { 
              
            	return false;
            }  
             else
             {  
                 $datos = array();
                 $path = $this->set_upload_options($id_prestamo)['upload_path'];
                  $file_name = $this->upload->data();
                  $url_imagen = $path.'/'.$file_name['file_name'];
                  $extension = end((explode(".", $file_name['file_name'])));
                  
                  
                  $datos['fichero'] =  $url_imagen;
                  $datos['fecha_subida'] = date("Y-m-d H:i:s");
                  $datos['descripcion'] = $file_name['file_name'];
                  $datos['prestamo_id'] = $id_prestamo;
                  $documentacion = $this->Thememodel->insert_value('documentacion_asociada',$datos);
                  if(($i+1)==count($files[$nameField]['size'])){
                  	return true;
                  }
             }  
           }
         } 
    }else{
    	return false;
    }
    }else{
    	return true;
    }
  }


  public function guardarImg($nameField,$id_prestamo){
		
    if(isset($_FILES[$nameField]["name"]))  
     {  
         $this->load->library('upload');
         $this->load->library('image_lib');
         $this->upload->initialize($this->set_upload_options($id_prestamo));
        
          if(!$this->upload->do_upload($nameField))  
           {  
             return false;
           }
            else  
               {  
               	  $datos = array();
                  $path = $this->set_upload_options($id_prestamo)['upload_path'];
                  $file_name = $this->upload->data();
                  $url_imagen = $path.'/'.$file_name['file_name'];
                  $extension = end((explode(".", $file_name['file_name'])));
                  
                 /* if($extension!='pdf'){
                  $this->image_lib->initialize($this->resizeImage($url_imagen)); 
                  $this->image_lib->resize();
                 }*/
                  $datos['fichero'] =  $url_imagen;
                  $datos['fecha_subida'] = date("Y-m-d H:i:s");
                  $datos['descripcion'] = $file_name['file_name'];
                  $datos['prestamo_id'] = $id_prestamo;
                  $datos['tipo_documento_id'] = 0;
                  $documentacion = $this->Thememodel->insert_value('documentacion_asociada',$datos);
                  return true;
               }  
       }else{
       	return true;
       }  
	}



  public function guardarImgCedula($nameField,$id_prestamo){
		
    if(isset($_FILES[$nameField]["name"]))  
     {  
         $this->load->library('upload');
         $this->load->library('image_lib');
        $this->upload->initialize($this->set_upload_options($id_prestamo));
         
          if(!$this->upload->do_upload($nameField))  
           {  
              //echo json_encode(array("status" => "ERROR_IMAGEN")); 
           	 return false;
           }
            else  
               {  
               	  $datos = array();
                  $path = $this->set_upload_options($id_prestamo)['upload_path'];
                  $file_name = $this->upload->data();
                  $url_imagen = $path.'/'.$file_name['file_name'];
                  $extension = end((explode(".", $file_name['file_name'])));
                  /*if($extension!='pdf'){
                     $this->image_lib->initialize($this->resizeImageCedula($url_imagen)); 
                     $this->image_lib->resize();
                 }*/
                  $datos['fichero'] =  $url_imagen;
                  $datos['fecha_subida'] = date("Y-m-d H:i:s");
                  $datos['descripcion'] = $file_name['file_name'];
                  $datos['prestamo_id'] = $id_prestamo;
                  $datos['tipo_documento_id'] = 1;
                  $documentacion = $this->Thememodel->insert_value('documentacion_asociada',$datos);
                  return true;
                  
               }  
       }  
	}
	/*
	Campo calculado
	Valor primaria
	Variable secundaria
	*/





	public function recalcular_prestamo($id){
		$prestamo = $this->Thememodel->get_value('prestamos',array('id_prestamo' => $id));
		$puntuacion = 0;
		if(isset($prestamo->datos)){

			$datos = json_decode( $prestamo->datos );
/*
			var_dump($prestamo->datos );
			$datos = get_object_vars($datos);
			var_dump($datos);
			if(isset($datos['2_1'])){
				switch ($datos['2_1']) {
					case '1';
					case '2';
					$puntuacion = $puntuacion + 85;
					break;
					case '3':
					$puntuacion = $puntuacion + 75;
					break;
					case '4';
					case '5';
					$puntuacion = $puntuacion + 65;
					break;
					case ($datos['2_1'] > '5'):
					$puntuacion = $puntuacion + 5;
					break;
				}
			}
			if(isset($datos['2_2'])){
				switch ($datos['2_2']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					case '3':
					case '4';
					case '12';
					$puntuacion = $puntuacion + 80;
					break;
					case '5';
					case '6';
					case '7';
					$puntuacion = $puntuacion + 75;
					break;
					$puntuacion = $puntuacion + 65;
					case '8';
					case '9';
					case '10';
					case '11';
					$puntuacion = $puntuacion + 10;
					break;
					case '13';
					$puntuacion = $puntuacion + 45;
					break;
				}
			}
			if(isset($datos['2_5'])){
				switch ($datos['2_5']) {
					case ($datos['2_5'] >= 0 && $datos['2_5'] <= 3);
					$puntuacion = $puntuacion + 90;
					break;
					case ($datos['2_5'] > 3 && $datos['2_5'] <= 5);
					$puntuacion = $puntuacion + 60;
					break;
					case ($datos['2_5'] > '5'):
					$puntuacion = $puntuacion + 30;
					break;
				}
			}
			if(isset($datos['2_6'])){
				switch ($datos['2_6']) {
					case '1';
					case '5':
					$puntuacion = $puntuacion + 60;
					break;
					case '2';
					$puntuacion = $puntuacion + 70;
					break;
					case '3':
					case '4';
					$puntuacion = $puntuacion + 50;
					break;
				}
			}
			if(isset($datos['2_6_1'])){
				switch ($datos['2_6_1']) {
					case '1';
					$puntuacion = $puntuacion + 70;
					break;
					case '2';
					$puntuacion = $puntuacion + 50;
					break;
				}
			}
			if(isset($datos['2_6_2'])){
				switch ($datos['2_6_2']) {
					case '1';
					$puntuacion = $puntuacion + 60;
					break;
					case '2';
					$puntuacion = $puntuacion + 70;
					break;
				}
			}
			if(isset($datos['2_6_3'])){
				switch ($datos['2_6_3']) {
					case '1';
					$puntuacion = $puntuacion + 40;
					break;
					case '2';
					$puntuacion = $puntuacion + 80;
					break;
				}
			}
			if(isset($datos['2_7'])){
				switch ($datos['2_7']) {
					case '1';
					$puntuacion = $puntuacion + 40;
					break;
					case '2';
					$puntuacion = $puntuacion + 80;
					break;
				}
			}
			if(isset($datos['2_8'])){
				switch ($datos['2_8']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 60;
					break;
				}
			}
			if(isset($datos['2_9'])){
				switch ($datos['2_9']) {
					case '1';
					$puntuacion = $puntuacion + 80;
					break;
					case '2';
					$puntuacion = $puntuacion + 40;
					break;
				}
			}
			if(isset($datos['2_9_1'])){
				switch ($datos['2_9_1']) {
					case '1';
					case '4';
					$puntuacion = $puntuacion + 50;
					break;
					case '2';
					$puntuacion = $puntuacion + 80;
					break;
					case '3';
					$puntuacion = $puntuacion + 70;
					break;
				}
			}
			if(isset($datos['2_9_4'])){
				switch ($datos['2_9_4']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 70;
					break;
				}
			}
			if(isset($datos['2_9_3'])){
				switch ($datos['2_9_3']) {
					case ($datos['2_9_3'] >= 2013);
					$puntuacion = $puntuacion + 85;
					break;
					case ($datos['2_9_3'] >= 2010 && $datos['2_9_3'] < 2013);
					$puntuacion = $puntuacion + 70;
					break;
					case ($datos['2_9_3'] >= 2000 && $datos['2_9_3'] < 2010);
					$puntuacion = $puntuacion + 60;
					break;
					case ($datos['2_9_3'] >= 1990 && $datos['2_9_3'] < 2000);
					$puntuacion = $puntuacion + 50;
					break;
				}
			}
			if(isset($datos['2_11'])){
				switch ($datos['2_11']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 50;
					break;
					case '3';
					$puntuacion = $puntuacion + 70;
					break;
					case '4';
					$puntuacion = $puntuacion + 30;
					break;
				}
			}
			if(isset($datos['2_12'])){
				switch ($datos['2_12']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 85;
					break;
					case '3';
					$puntuacion = $puntuacion + 60;
					break;
					case '4';
					$puntuacion = $puntuacion + 30;
					break;
					case '5';
					$puntuacion = $puntuacion + 10;
					break;
				}
			}
			if(isset($datos['2_13_telefono2']) != ''){
				$puntuacion = $puntuacion + 80;
			}else{
				$puntuacion = $puntuacion + 30;
			}
			if(isset($datos['2_14'])){
				switch ($datos['2_14']) {
					case '1';
					$puntuacion = $puntuacion + 20;
					break;
					case '2';
					$puntuacion = $puntuacion + 95;
					break;
					case '3';
					$puntuacion = $puntuacion + 60;
					break;
					case '4';
					case '6';
					$puntuacion = $puntuacion + 85;
					break;
					case '5';
					$puntuacion = $puntuacion + 50;
					break;
				}
			}
			if(isset($datos['2_15'])){
				switch ($datos['2_15']) {
					case '1';
					case '3';
					$puntuacion = $puntuacion + 40;
					break;
					case '2';
					$puntuacion = $puntuacion + 80;
					break;
				}
			}
			if(isset($datos['2_16'])){
				switch ($datos['2_16']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 60;
					break;
				}
			}
			if(isset($datos['2_17_telefono_referencia']) != ''){
				$puntuacion = $puntuacion + 80;
			}else{
				$puntuacion = $puntuacion + 30;
			}
			// 2.17.1 PENDIENTE 2_17_1
			if(isset($datos['2_18'])){
				switch ($datos['2_18']) {
					case '1';
					$puntuacion = $puntuacion + 30;
					break;
					case '2';
					$puntuacion = $puntuacion + 60;
					break;
					case '3';
					$puntuacion = $puntuacion + 70;
					break;
					case '4';
					$puntuacion = $puntuacion + 85;
					break;
					case '5';
					case '6';
					$puntuacion = $puntuacion + 90;
					break;
				}
			}
			if(isset($datos['2_18_1'])){
				switch ($datos['2_18_1']) {
					case '1';
					$puntuacion = $puntuacion + 80;
					break;
					case '2';
					$puntuacion = $puntuacion + 40;
					break;
				}
			}
			if(isset($datos['2_18_2'])){
				switch ($datos['2_18_2']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 70;
					break;
				}
			}
			if(isset($datos['2_19'])){
				switch ($datos['2_19']) {
					case '1';
					$puntuacion = $puntuacion + 30;
					break;
					case '2';
					$puntuacion = $puntuacion + 20;
					break;
					case '2';
					$puntuacion = $puntuacion + 80;
					break;
				}
			}
			if(isset($datos['2_20'])){
				switch ($datos['2_20']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 50;
					break;
					case '2';
					$puntuacion = $puntuacion + 10;
					break;
				}
			}
			if(isset($datos['2_21'])){
				switch ($datos['2_21']) {
					case '1';
					$puntuacion = $puntuacion + 10;
					break;
					case '2';
					$puntuacion = $puntuacion + 40;
					break;
					case '2';
					$puntuacion = $puntuacion + 80;
					break;
				}
			}
			if(isset($datos['2_22'])){
				switch ($datos['2_22']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 45;
					break;
					case '2';
					$puntuacion = $puntuacion + 30;
					break;
				}
			}
			if(isset($datos['2_23'])){
				switch ($datos['2_23']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 45;
					break;
					case '2';
					$puntuacion = $puntuacion + 70;
					break;
				}
			}
			if(isset($datos['2_24'])){
				switch ($datos['2_24']) {
					case '1';
					$puntuacion = $puntuacion + 90;
					break;
					case '2';
					$puntuacion = $puntuacion + 45;
					break;
					case '2';
					$puntuacion = $puntuacion + 20;
					break;
				}
			}
			if(isset($datos['2_25'])){
				switch ($datos['2_25']) {
					case '1';
					$puntuacion = $puntuacion + 20;
					break;
					case '2';
					$puntuacion = $puntuacion + 45;
					break;
					case '2';
					$puntuacion = $puntuacion + 90;
					break;
				}
			}
			if(isset($datos['2_26'])){
				switch ($datos['2_26']) {
					case '1';
					$puntuacion = $puntuacion + 70;
					break;
					case '2';
					$puntuacion = $puntuacion + 30;
					break;
					case '2';
					$puntuacion = $puntuacion + 10;
					break;
				}
			}




			3_1
			3_2
			3_4
			3_7
			3_8
			3_10
			3_12
			3_13
			3_14
			3_14_1
			3_15
			3_15_1
			3_15_2
			3_15_3
			3_16
			3_17
			3_18
			3_19
			3_20
			3_21
			3_22
			3_23

			5_2
			5_3
			5_4
			5_4_1
			4_1
			4_2
			4_3
			4_4
			4_5
			4_6
			4_8
			4_9
			4_10
			4_11
			4_12
			4_13
			4_14
			4_15
			4_16
			4_17
			2_10_telefono
			2_17_telefono_referencia
			2_8_1_codigo_licencia
			3_3_nombre_empresa
			3_5_cantidad_dinero_minima
			3_6 (fecha)
			3_9_telefono_empresa
			3_11_gastos_mensualea
			5_1_numero_cuenta
			5_1_1_nombre_cuenta
			bancos_propios
			5_4_2_numero_tarjeta
			ccv
			nombre_tarjeta
			4_7_mes
			4_7_year

			if(isset($datos['amount'])){
				switch ($datos['amount']) {
					case ($datos['amount'] > 20 && $datos['amount'] <= 100);
					$puntuacion = $puntuacion + 90;
					break;
					case ($datos['amount'] > 100 && $datos['amount'] <= 200);
					$puntuacion = $puntuacion + 80;
					break;
					case ($datos['amount'] > 200 && $datos['amount'] <= 300);
					$puntuacion = $puntuacion + 70;
					break;
					case ($datos['amount'] > 300);
					$puntuacion = $puntuacion + 50;
					break;
				}
			}
			if(isset($datos['duration'])){
				switch ($datos['duration']) {
					case ($datos['duration'] < 15);
					$puntuacion = $puntuacion + 95;
					break;
					case ($datos['duration'] >= 15 && $datos['duration'] < 30);
					$puntuacion = $puntuacion + 85;
					break;
					case ($datos['duration'] >= 30 && $datos['duration'] < 60);
					$puntuacion = $puntuacion + 75;
					break;
					case ($datos['duration'] >= 60 && $datos['duration'] <= 90);
					$puntuacion = $puntuacion + 55;
					break;
				}
			}
			$this->Thememodel->save_value('prestamos',array('puntuacion' => $puntuacion ),array('id_prestamo' => $id));
				$this->session->set_userdata('info', 'Se ha actualizado el Scoring');
			}else{
				$this->session->set_userdata('error', 'Ha ocurrido un error al actualizar');
			}
			redirect(base_url().'gestion/prestamo/'.$id);*/
		}
	}


// funcion para verificar en que paso se encuentra la solicitud del prestamo y redireccionar al mismo
	public function solicitud_prestamo(){
        
        $id = $this->session->userdata('user_id');
        if(!empty($id))
		{
        $join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
         $where = array('prestamos_estados.id' => 8,'users.id' => $id);

		$data['prestamos'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);
		if(!empty($data['prestamos'])){
		    $datos = json_decode($data['prestamos'][0]->datos, true);
		    $cantidad = $data['prestamos'][0]->cantidad;
        	$tiempo =  $data['prestamos'][0]->tiempo;
        	$id = $data['prestamos'][0]->id_prestamo;
        	
        	
        	$datos_solicitud['amount'] = $cantidad;
			$datos_solicitud['duration'] = $tiempo;
			$datos_solicitud['id'] = $id;
        	$datos_solicitud = $datos;
        	$this->session->set_userdata('datos_solicitud', $datos_solicitud);
           
       if(!empty($datos['paso_2_completado']) && empty($datos['paso_3_completado'])) {
       	   
       	       redirect(base_url().'solicitud/paso-3');
       	      
        	}
        elseif(!empty($datos['paso_3_completado']) && empty($datos['paso_4_completado'])) {
        	 
        	 redirect(base_url().'solicitud/paso-4');
        }
        elseif(!empty($datos['paso_4_completado']) && empty($datos['paso_5_completado'])) {
		  
        	redirect(base_url().'solicitud/paso-5');
        }
           elseif(!empty($datos['paso_5_completado'])) {
		  
        	redirect(base_url().'solicitud/finalizado');
        }
      }else{
      	$this->session->sess_destroy();
		redirect(base_url());
      }
     }
    }


    public function verificar_pasos(){

        $id = $this->session->userdata('user_id');
        if(!empty($id))
		{

          $where = array('prestamos.id_cliente' => $id, 'prestamo_actual'=>1);
			$where_in = 'prestamos.prestamo_estado_id';
			$where_ids = array(1,2,3,4,5,6,10,11,12,13);		 
			$cantidad_prestamos = $this->Thememodel->total_where_where_in('prestamos',$where,$where_in,$where_ids);
			if ($cantidad_prestamos>=1)
			{
				redirect(base_url().'solicitud/validacion');
			}else{ 

			  $join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
                    $where = array('prestamos_estados.id' => 8,'users.id' => $id);

		      $data['prestamos'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);
		     if(!empty($data['prestamos'])){
		     $datos = json_decode($data['prestamos'][0]->datos, true);
		     $cantidad = $data['prestamos'][0]->cantidad;
        	 $tiempo =  $data['prestamos'][0]->tiempo;
        	 $id = $data['prestamos'][0]->id_prestamo;
        	
        	
        	 $datos_solicitud['amount'] = $cantidad;
			 $datos_solicitud['duration'] = $tiempo;
			 $datos_solicitud['id'] = $id;
        	 $datos_solicitud = $datos;
        	 $this->session->set_userdata('datos_solicitud', $datos_solicitud);
           
            if(!empty($datos['paso_2_completado']) && empty($datos['paso_3_completado'])) {
          // echo	base_url(uri_string());
       	     if(base_url(uri_string())!=base_url().'solicitud/paso-3'){
       	        redirect(base_url().'solicitud/paso-3');
       	      }
        	}
            elseif(!empty($datos['paso_3_completado']) && empty($datos['paso_4_completado'])) {
        	     if(base_url(uri_string())!=base_url().'solicitud/paso-4'){
        	     redirect(base_url().'solicitud/paso-4');
        	  }
        	}
            elseif(!empty($datos['paso_4_completado']) && empty($datos['paso_5_completado'])) {
		         if(base_url(uri_string())!=base_url().'solicitud/paso-5'){
        	      redirect(base_url().'solicitud/paso-5');

             }
            }
            elseif(!empty($datos['paso_5_completado'])) {
		        if(base_url(uri_string())!=base_url().'solicitud/finalizado'){
        	    redirect(base_url().'solicitud/finalizado');
        	}
        }
      }else{
      		if(empty($datos['paso_2_completado'])){
      			if(base_url(uri_string())!=base_url().'solicitud/paso-2'){
      			//return true;
      			//echo base_url(uri_string());
      			redirect(base_url().'solicitud/paso-2');
      	         }

      	     }
      	   }
         }
          
       }
    }

 
    public function actualizarDatosPrestamo(){
    	
    	$id_prestamo = $this->input->post("id_prestamo");
        if($this->input->post("fecha_deposito")){
            $datos['fecha_efectivo_prestamo'] = date("Y-m-d H:i:s", strtotime( $this->input->post('fecha_deposito')));
			$this->Thememodel->update_value('prestamos', $datos, array('id_prestamo' => $id_prestamo));
		}else if($this->input->post('cantidad')) {
           	$datos['cantidad'] = $this->input->post('cantidad');
        	$this->Thememodel->update_value('prestamos', $datos, array('id_prestamo' => $id_prestamo));
		}else if($this->input->post('tiempo')) {
            $datos['tiempo'] = $this->input->post('tiempo');
        	$this->Thememodel->update_value('prestamos', $datos, array('id_prestamo' => $id_prestamo));
		}
	}

	public function nuevosDatosPrestamo($id) {                                 
		//$response = $this->Thememodel->get_select(array('tiempo', 'cantidad', 'fecha_efectivo_prestamo'), 'prestamos', array('id_prestamo' => $id));
		$response = $this->Thememodel->get_query("SELECT tiempo, cantidad,fecha_efectivo_prestamo, DATE_FORMAT(fecha_efectivo_prestamo,'%d/%m/%Y') as fecha_efecto  from prestamos where id_prestamo=$id");
		echo json_encode( $response );
	}

    public function actualizarAjusteQuincena(){
        
        $id_prestamo = $this->input->post("id_prestamo");
    	$ajustarQuincena = $this->input->post('ajustar_quincena');
				if($ajustarQuincena==null){
					$ajustarQuincena=0;
				}
		$datos['ajustar_quincena'] = $ajustarQuincena;
		$this->Thememodel->update_value('prestamos', $datos, array('id_prestamo' => $id_prestamo));
		echo json_encode(array("status" => TRUE));
	    //redirect(base_url().'gestion/prestamo/'.$id_prestamo);
    }


	public function finalizado(){
		check_rol($this->session->userdata('roles'),array(0));
		$data = $this->load_data();
		cargar_template('template/solicitud/solicitud-finalizada',$data);
	}

	public function cambiarEmail(){
       
		$datos['email'] = $this->input->post('nuevo_email');
		$this->session->set_userdata('email', $datos['email']);
		$datos_solicitud = $this->session->userdata('datos_solicitud');
	    $where = array('email'=>$this->input->post('nuevo_email'));
	    // verificamos si existe el email ingresado por el usuario
        $cantidad_email = $this->Thememodel->total_where('users',$where);
	   //si no existe actualizamos el email de lo contrario retornamos un mensaje a nivel de la vista
	   if($cantidad_email==0){
	   $usuario = $this->Thememodel->get_value('users',array('id'=> $this->session->userdata('user_id')));
		$datos_email = $this->Thememodel->update_value('users', $datos, array('id' => $usuario->id));
		if($this->session->userdata('datos_solicitud')){
		if($datos_email){
			   $id_prestamo = $datos_solicitud['id'];
		       $respuesta_algoritmo = $this->Algoritmo->getArbolDeAprobacion($id_prestamo);
               $respuesta = $respuesta_algoritmo['Respuesta'];
               $data['user'] = $usuario;
               $destinatario = $data['user']->email;
               $data['email_template'] = $this->Thememodel->get_all_values('email_template');
               $web = 'CliCREDIT Panamá';
               $from = "info@pa.clicredit.com";
              switch ($respuesta) {
            	case 'PRE-APROBADO':
            	  $data_estado = array('prestamo_estado_id' => 12);
            	  $this->Thememodel->save_value('prestamos',$data_estado,array('id_prestamo' => $id_prestamo));
          	      $data['email'] = $data['email_template'][10];
                  $asunto = $data['email']->asunto;
                  $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
                  $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
          	   break;
          	   case 'EN REVISION':
          	      $data_estado = array('prestamo_estado_id' => 11);
            	  $this->Thememodel->save_value('prestamos',$data_estado,array('id_prestamo' => $id_prestamo));
          	      $data['email'] = $data['email_template'][9];
                  $asunto = $data['email']->asunto;
                  $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
                  $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
               break;
               case 'DENEGADO':
                  $data_estado = array('prestamo_estado_id' => 10);
            	  $this->Thememodel->save_value('prestamos',$data_estado,array('id_prestamo' => $id_prestamo));
          	      $data['email'] = $data['email_template'][8];
                  $asunto = $data['email']->asunto;
                  $template =  $this->load->view("emails/email_estado_prestamo", $data, true);
                  $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);
               break;
              }
              
            }
          }
          echo json_encode(array("status" => TRUE)); 

		}else{
			echo json_encode(array("status" => FALSE)); 
		}
	}


	public function enviarMailBienvenida($datosUsuario){

     $data['user'] = $datosUsuario;
     $data['email_template'] = $this->Thememodel->get_all_values('email_template');
     $data['email'] = $data['email_template'][17];
     $asunto = $data['email']->asunto;
     $destinatario = $data['user']->email;
     $from = "info@pa.clicredit.com";
     $web = 'CliCREDIT Panamá';
     $template =  $this->load->view("emails/email_notification", $data, true);
     $this->Emailmodel->send_Email_Notification($asunto,$template,$from,$web,$destinatario);

	}
	public function max_solicitar($id) {
		//validacion de maximo a solicitar
		$where = array('prestamos.id_cliente' => $id);
		$where_in = 'prestamos.prestamo_estado_id';
		$where_ids = array(7);
		$prestamo_cancelado = $this->Thememodel->total_where_where_in('prestamos',$where,$where_in,$where_ids);
		if(! $prestamo_cancelado) {
			$cantidad = $this->session->userdata('datos_solicitud')['amount'];
			$duracion = $this->session->userdata('datos_solicitud')['duration'];
			if($cantidad > 250) {
				$cantidad = 250;
				$this->session->set_userdata('msg_val_max', 'Por ser su primer préstamo el monto a solicitar ha sido ajustado al máximo permitido');
			}
			$datos_solicitud = array('amount' => $cantidad,'duration' => $duracion);
			$this->session->set_userdata('datos_solicitud', $datos_solicitud);
			$this->session->set_userdata('max_amount', 250);
		}
	}

	

   

	public function validacion(){
		check_rol($this->session->userdata('roles'),array(0));
		$id_user = $this->session->userdata('user_id');
		$join = array(array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'), array('table'=>'users','join'=>'users.id = prestamos.id_cliente'));
        $where = array('prestamo_actual' => 1,'users.id' => $id_user);

		$prestamo = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);
        
        $where = array('id' => $prestamo[0]->prestamo_estado_id);
		$data['estado_prestamo'] = $this->Thememodel->get_value('prestamos_estados',$where);
		cargar_template('template/solicitud/solicitud-validacion',$data);
	}
	private function load_data(){
		$data = array();
		include 'Themeoptions.php';
		return $data;
	}
}
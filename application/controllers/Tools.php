<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Tools extends CI_Controller {
	function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->model(array('Thememodel'));
	}
	public function index(){
	}

	public function ping(){
		$url = strtolower(uri_string());
		$url = explode('enviarurl/ping/',$url);

		$this->load->library('xmlrpc');

		$this->xmlrpc->server('http://rpc.pingomatic.com/', 80);
		$this->xmlrpc->method('weblogUpdates.ping');

		$post = $this->Thememodel->get_value('post',array('slug' => $this->uri->segment(4) ));

		$request = array($post->title, base_url( $url[1]) );
		$this->xmlrpc->request($request);

		if ( ! $this->xmlrpc->send_request()){
			echo $this->xmlrpc->display_error();
		}else{
			echo 'OK';
		}
	}
	public function sitemap_create(){
/*
No identar
*/
$sitemap ='<?xml version="1.0" encoding="UTF-8"?>
<urlset
xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
$sitemap .='<url>
<loc>'.base_url().'</loc>
<changefreq>always</changefreq>
<priority>0.80</priority>
</url>
<url>
	<loc>'.base_url().'contacto</loc>
	<changefreq>always</changefreq>
	<priority>0.80</priority>
</url>';

$where = array('post.type' => 'Post','post.status'=>'publicado', 'datepublish <' => date("Y-m-d H:i:s"));
$posts = $this->Thememodel->get_all_values_where('post',$where);

foreach ($posts as $key) {
	$sitemap .='<url>
	<loc>'.base_url().'blog/'.$key->slug.'</loc>
	<lastmod>'.date('Y-m-d', strtotime($key->datepublish)).'</lastmod>
	<changefreq>monthly</changefreq>
	<priority>0.8</priority>
</url>';
}
$where = array('post.type' => 'Sección','post.status'=>'publicado', 'datepublish <' => date("Y-m-d H:i:s"));
$secciones = $this->Thememodel->get_all_values_where('post',$where);

foreach ($secciones as $key) {
	$sitemap .='<url>
	<loc>'.base_url().'seccion/'.$key->slug.'</loc>
	<lastmod>'.date('Y-m-d', strtotime($key->datepublish)).'</lastmod>
	<changefreq>monthly</changefreq>
	<priority>0.8</priority>
</url>';
}
$sitemap .= '</urlset>';
file_put_contents('sitemaptest.xml', $sitemap) or die('ERROR: No es posible escribir en el archivo');
}


}
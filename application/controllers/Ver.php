<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Ver extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->load->helper(array('url'));
		$this->load->model(array('Thememodel'));
	}

	public function index(){
		redirect(base_url());
	}
	public function redirect(){
		if(!empty($this->session->userdata('url'))){
			$ruta = $this->session->userdata('url');
			$this->session->unset_userdata('url');
		}elseif(!empty($this->uri->uri_string())){
			$ruta = $this->uri->uri_string();
		}else{
			redirect(base_url());
		}
		if(!empty($ruta)){
			$data['enrutado'] = $this->Thememodel->get_value('redirects',array('url_origin'=>$ruta));
			if(!empty($data['enrutado']) && $data['enrutado'] != NULL){
				redirect($data['enrutado']->url_destiny);
			}else{
				redirect(base_url());
			}
		}
	}
}
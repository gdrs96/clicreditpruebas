<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Xmlrpc extends CI_Controller {

	public function index(){
		$this->load->library('xmlrpc');

		$this->xmlrpc->server('http://rpc.pingomatic.com/', 80);
		$this->xmlrpc->method('weblogUpdates.ping');

		$request = array('miguelgomezsa.com/blog', 'http://www.miguelgomezsa.com/');
		$this->xmlrpc->request($request);

		if ( ! $this->xmlrpc->send_request()){
			echo $this->xmlrpc->display_error();
		}else{
			echo 'OK';
		}
	}
}
?>
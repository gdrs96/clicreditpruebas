<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SendSmsCenter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Thememodel');
		$this->load->helper(array('url'));
	}

	public function sendSms($prestamoId,$verificationCode, $telefono)
	{
        
        $request = '{
			"api_key":"b85aa64d5437ea71e38dbe382c494026",
			"report_url":"'.base_url().'solicitud/enviarSMS",
			"concat":0,
			"encoding":"UCS2",
			"messages":[{
				"from":"CliCREDIT",
				"to":"+507'.$telefono.'",
				"text":"Clicredit Panamá: use el siguiente código, '.$verificationCode.' en el paso 5.",
				"send_at":"'.date('Y-m-d H:i:s').'"
			}]
		}';
		$headers = array('Content-Type:application/json');

		$ch = curl_init('https://api.gateway360.com/api/3.0/sms/send');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);


		$result = curl_exec($ch);
        $response = json_decode($result);
		if (curl_errno($ch) != 0 ){
			
			 $datos['telefono'] = $telefono;
             $datos['codigo_verificacion'] = $verificationCode;
             $datos['fecha'] = date('Y-m-d H:i:s');
             $datos['codigo_error'] = curl_errno($ch);
             $datos['descripcion'] = $result;
             $datos['id_prestamo'] = $prestamoId;
             $datos['status'] = $response->result[0]->status;
			$this->Thememodel->insert_value('logs_sms',$datos);
		}else{
			 $datos['telefono'] = $telefono;
             $datos['codigo_verificacion'] = $verificationCode;
             $datos['fecha'] = date('Y-m-d H:i:s');
             $datos['descripcion'] = $result;
             $datos['id_prestamo'] = $prestamoId;
             $datos['status'] = $response->result[0]->status;
			$this->Thememodel->insert_value('logs_sms',$datos);
		}
	}

	public function reSendSms($userId)
	{
		$telefono;
		$verificationCode;

		$result = $this->Thememodel->get_query('Select id_prestamo, datos from prestamos where prestamo_estado_id = 8 and id_cliente = "'.$userId.'"');

		$resultDecode = json_decode($result['0']->datos);

		$prestamoId = $result['0']->id_prestamo;


		foreach ($resultDecode as $key => $valor) 
		{
			if ($key == '2_10_telefono') 
			{
				$telefono = $valor;
			}
			if ($key == 'codigo_verificacion') 
			{
				$verificationCode = $valor;
			}
		}

	    $request = '{
			"api_key":"b85aa64d5437ea71e38dbe382c494026",
			"report_url":"'.base_url().'solicitud/enviarSMS",
			"concat":0,
			"encoding":"UCS2",
			"messages":[{
				"from":"CliCREDIT",
				"to":"+507'.$telefono.'",
				"text":"Clicredit Panamá: use el siguiente código,'.$verificationCode.' en el paso 5.",
				"send_at":"'.date('Y-m-d H:i:s').'"
			}]
		}';
		$headers = array('Content-Type: application/json');

		$ch = curl_init('https://api.gateway360.com/api/3.0/sms/send');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

		$result = curl_exec($ch);
        $response = json_decode($result);
		if (curl_errno($ch) != 0 ){
			 $datos['telefono'] = $telefono;
             $datos['codigo_verificacion'] = $verificationCode;
             $datos['fecha'] = date('Y-m-d H:i:s');
             $datos['codigo'] = curl_errno($ch);
             $datos['descripcion'] = $result;
             $datos['id_prestamo'] = $prestamoId;
             $datos['status'] = $response->result[0]->status;
			$this->Thememodel->insert_value('logs_sms',$datos);
			echo "El SMS no pudo ser enviado";
		} else {
			 $datos['telefono'] = $telefono;
             $datos['codigo_verificacion'] = $verificationCode;
             $datos['fecha'] = date('Y-m-d H:i:s');
             $datos['id_prestamo'] = $prestamoId;
             $datos['descripcion'] = $result;
             $datos['status'] = $response->result[0]->status;
			$this->Thememodel->insert_value('logs_sms',$datos);
			echo "SMS enviado con éxito al número ".$telefono;
		}

	}

	public function sendNewPhone($userId,$telefono)
	{
		$verificationCode;

		$result = $this->Thememodel->get_query('Select id_prestamo, datos from prestamos where prestamo_estado_id = 8 and id_cliente = "'.$userId.'"');

		$resultDecode = json_decode($result['0']->datos);

		$prestamoId = $result['0']->id_prestamo;


		foreach ($resultDecode as $key => $valor) 
		{
			if ($key == 'codigo_verificacion') 
			{
				$verificationCode = $valor;
			}
		}

       $request = '{
			"api_key":"b85aa64d5437ea71e38dbe382c494026",
			"report_url":"'.base_url().'solicitud/enviarSMS",
			"concat":0,
			"encoding":"UCS2",
			"messages":[{
				"from":"CliCREDIT",
				"to":"+507'.$telefono.'",
				"text":"Clicredit Panamá: use el siguiente código,'.$verificationCode.' en el paso 5.",
				"send_at":"'.date('Y-m-d H:i:s').'"
			}]
		}';
		$headers = array('Content-Type: application/json');

		$ch = curl_init('https://api.gateway360.com/api/3.0/sms/send');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $request);

		$result = curl_exec($ch);
        $response = json_decode($result);
		if (curl_errno($ch) != 0 ){
			 
			 $datos['telefono'] = $telefono;
             $datos['codigo_verificacion'] = $verificationCode;
             $datos['fecha'] = date('Y-m-d H:i:s');
             $datos['codigo_error'] = curl_errno($ch);
             $datos['descripcion_error'] = curl_error($ch);
             $datos['id_prestamo'] = $prestamoId;
             $datos['status'] = $response->result[0]->status;
			$this->Thememodel->insert_value('logs_sms',$datos);
			echo "El SMS no pudo ser enviado";

			//die("curl error: ".curl_errno($ch));
		} else {
			 $datos['telefono'] = $telefono;
             $datos['codigo_verificacion'] = $verificationCode;
             $datos['fecha'] = date('Y-m-d H:i:s');
             $datos['id_prestamo'] = $prestamoId;
             $datos['status'] = $response->result[0]->status;
			$this->Thememodel->insert_value('logs_sms',$datos);
			echo "SMS enviado con éxito al número ".$telefono;
		}

	}

}

/* End of file MY_Controller_SendSms.php */
/* Location: ./application/core/MY_Controller_SendSms.php */
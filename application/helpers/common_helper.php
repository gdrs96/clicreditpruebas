<?php defined('BASEPATH') OR exit('No direct script access allowed');

function strDMYdiff( $DMY1, $DMY2, $format = '%r%a' ){
  // ambas fechas deben tener el formato DD/MM/YYYY -- jjy
  $f1 = explode('/', $DMY1 );
  $f2 = explode('/', $DMY2 );
  $realDate1 = new DateTime( "{$f1[2]}-{$f1[1]}-{$f1[0]}");
  $realDate2 = new DateTime( "{$f2[2]}-{$f2[1]}-{$f2[0]}");
  $n = date_diff($realDate1, $realDate2);
  $d = $n->format( $format );
  return $d;
}

function prp( $v, $end = FALSE ){
   // --- funcion utilitaria --- jjy
   echo "\n<pre>\n";
   if( is_array($v) || is_object($v) ){ print_r( $v ); }
   else { echo( $v ); }
   echo "\n</pre>\n";
   $end && die();
}
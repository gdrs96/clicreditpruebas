<?php defined('BASEPATH') OR exit('No direct script access allowed');
function check_rol($rol_permissison, $section_id){
	if( ! is_array($rol_permissison)){
		redirect( base_url());
	}
	$check = false;
	foreach ($rol_permissison as $key) {
		if( in_array($key,$section_id)){
			$check = true;
		}
	}
	if( ! $check)redirect(base_url());
}
function check_rol_view($rol_permissison,$section_id){
	$check = false;
	if(is_array($rol_permissison) && is_array($section_id)){
		foreach ($rol_permissison as $key) {
			if( in_array($key,$section_id)){
				return true;
			}
		}
	}
}
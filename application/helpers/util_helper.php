<?php defined('BASEPATH') OR exit('No direct script access allowed');

function cargar_template($nombreVista,$datos=array()){
	$CI =& get_instance();
	$CI->load->view('template/header',$datos);
	$CI->load->view( $nombreVista,$datos);
	$CI->load->view( 'template/footer',$datos);
}
function cargar_dashboard($nombreVista,$datos=array()){
	$CI =& get_instance();
	$CI->load->view('dashboard/header',$datos);
	$CI->load->view('dashboard/menu',$datos);
	$CI->load->view( $nombreVista,$datos);
	$CI->load->view('dashboard/footer',$datos);
}
function cargar_dashboard_nomenu($nombreVista,$datos=array()){
	$CI =& get_instance();
	$CI->load->view('dashboard/header',$datos);
	$CI->load->view( $nombreVista,$datos);
	$CI->load->view('dashboard/footer',$datos);
}
function last_version($file){
	return $file ."?v=". filemtime($file);
}

function remove_directory($directory, $empty=FALSE)
{
       if(substr($directory,-4) == '/') {
            $directory = substr($directory,0,-1);
        }
        if(!file_exists($directory) || !is_dir($directory)) {
            return FALSE;
        } elseif(!is_readable($directory)) {

        return FALSE;

        } else {

            $handle = opendir($directory);
            while (FALSE !== ($item = readdir($handle)))
            {
                if($item != '.' && $item != '..') {
                    $path = $directory.'/'.$item;
                    if(is_dir($path)) {
                        remove_directory($path);
                    }else{
                        unlink($path);
                    }
                }
            }
            closedir($handle);
            if($empty == FALSE)
            {
                if(!rmdir($directory))
                {
                    return FALSE;
                }
            }
        return TRUE;
        }
    }

function calcularQuincenas($fecha) {
           $quincenas = array();
            $ultimoDia = new Date($fecha->format("Y"), $fecha->format("m") + 1, 0);
            if($fecha.getDate() < 11) {
                array_push($quincenas,$this->quincena($fecha,0));
                array_push($quincenas,$this->finDeMes($fecha,0));
                array_push($quincenas,$this->quincena($fecha,1));
                array_push($quincenas,$this->finDeMes($fecha,1));
                array_push($quincenas,$this->quincena($fecha,2));
                array_push($quincenas,$this->finDeMes($fecha,2));
                array_push($quincenas,$this->quincena($fecha,3));
               
            } else if(fecha.getDate() < (ultimoDia.getDate()-4)) {
                array_push($quincenas,$this->finDeMes($fecha,0));
                array_push($quincenas,$this->quincena($fecha,1));
                array_push($quincenas,$this->finDeMes($fecha,1));
                array_push($quincenas,$this->quincena($fecha,2));
                array_push($quincenas,$this->finDeMes($fecha,2));
                array_push($quincenas,$this->quincena($fecha,3));
                array_push($quincenas,$this->finDeMes($fecha,3));
            } else {
                array_push($quincenas,$this->quincena($fecha,0));
                array_push($quincenas,$this->finDeMes($fecha,0));
                array_push($quincenas,$this->quincena($fecha,1));
                array_push($quincenas,$this->finDeMes($fecha,1));
                array_push($quincenas,$this->quincena($fecha,2));
                array_push($quincenas,$this->finDeMes($fecha,2));
                array_push($quincenas,$this->quincena($fecha,3));
                array_push($quincenas,$this->finDeMes($fecha,3));
                array_push($quincenas,$this->quincena($fecha,4));
            }
            return $quincenas;
        }
        function quincena($fecha, $numero) {
            return new Date($fecha->format("Y"), $fecha->format("m") + $numero, 15);
        }
        function finDeMes($fecha, $numero) {
            return new Date($fecha->format("Y"), $fecha->format("m") + $numero + 1, 0);
        }

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
   This class is defined to model Algoritmo where some variables are calculated to manage the final result of the different questions
*/
/**
   OJO: Este modelo se puede optimizar más ... jjy
**/

class Algoritmo extends CI_Model{ // --- 1.84 trello --- jjy
   /*
      Version:
      03-2018 jjyepez --- deriverd from Guillermo's - PerfilSocial
   */
   var $relacionFormulas;
   //list to save Coeficientes de Inferencia saved in the database
   var $listaMaestrosInferencia;
   //list to save Coeficientes saved in the database
   var $listaCoeficientes;
   //lista de Datos Macroeconomicos
   var $listaDatosMacroeconomicos;

   //list to save Lista de Variables Primarias saved in the database
   private $listaVariablesPrimariasCompleta;
    
   // --- Construct method, list with information required to calculate information is retrieve from the database
   public function __construct()
    {
      parent::__construct();

      // load the model for CoeficientesInferencia and get the correspondence data from the database
      $this->load->model('CoeficientesInferencia');
      $this->listaMaestrosInferencia = $this->CoeficientesInferencia->GetMaestrosInferencia();     
            
      // load the model for Coeficientes and get the correspondence data from the database
      $this->load->model('Coeficientes');
      //$this->listaCoeficientes = $this->Coeficientes->ProcessCoeficientes("Perfil social");     
      
      // load the model for VariablesPrimarias and get the correspondence data from the database
      $this->load->model('VariablesPrimarias');
      $this->listaVariablesPrimariasCompleta=$this->VariablesPrimarias->ProcessVariablesPrimarias();     

      $this->load->model('Thememodel'); // --- modelo base DAL
      $this->load->model('DataMaster'); // --- modelo base Datos del Prestamo

      $this->load->model('DatosMacroeconomicos'); // --- Datos macroeconomicos
      $this->listaDatosMacroeconomicos = $this->DatosMacroeconomicos->GetDatosMacroeconomicos();

      $this->load->model('PerfilSocial' );    // -- calculos para el GdV PerfilSocial
      $this->load->model('Honestidad' );      // -- calculos para el GdV Honestidad
      $this->load->model('Responsabilidad' ); // -- calculos para el GdV Responsabilidad
      $this->load->model('Trabajo' );         // -- calculos para el GdV trabajo
      $this->load->model('CapacidadDePago' ); // -- calculos para el GdV CapacidadDePago
      $this->load->model('Macroeconomicas' ); // -- calculos para el GdV Macroeconomicas
   }

   /**
      METODOS para el cálculo de los Valores Secundarios y Promedio total final ... para 
      el Grupo de Variables (*cualquiera*) ... de acuerdo al algoritmode Excel --- jjy // --- no es definitivo aun
   **/
   public function debugging( $params = [] ){
      $this->load->view( '../views/depuracion', Array( 'variables' => $params ) );
   }

   // --- calculo fundamental para el algoritmo .. VPs para cada pregunta segun n_pregunta y grupo de vars
   private function getCalculoVP( $n_pregunta, $titulo_grupo, $dato_respuesta, $listaVPsCompleta, $infoCliente, $infoSimu, $todasLasRespuestas, $datosMacroeconomicos ){
      // --- valores por omision
      $vp      = -1;
      $formula = 'n/a';

      $grupo_variables = str_replace( ' ', '', ucwords( $titulo_grupo ) ); // --- se conv el titulo a CamelCase

      // Grupos Habilitados para usar el algoritmo
      if( in_array( $grupo_variables, [
         'Honestidad',
         'Responsabilidad',
         'Trabajo',
         'CapacidadDePago',
         'Macroeconomicas',
         'PerfilSocial'
      ] ) ) { // --- modelos de grupos cargados
         eval('$formula = $this->'.$grupo_variables.'->getFormulaVP("'.$n_pregunta.'");');
         eval('$vp      = $this->'.$grupo_variables.'->'.$formula.'("'.$dato_respuesta[1].'", $listaVPsCompleta, $infoCliente, $infoSimu, $todasLasRespuestas, $datosMacroeconomicos );' );
         // Se pasan TODOS los datos que pudieran requerirse para calcular VP!
      }
      return $vp;
   }

   /* Obtiene los valores VS relativos al Arbol de Aprobacion ... segun el algoritmo excel */
   public function getArbolDeAprobacion( $idPrestamo ){
      $gvs = [
         'Responsabilidad',
         'Honestidad',
         'Trabajo',
         'Perfil social',
         'Capacidad de Pago',
         'Macroeconomicas',
      ];
      $salida = [];
      foreach( $gvs as $tituloGrupo ){
         // solo las VSs calculados
         $salida[ $tituloGrupo ] = $this->getCalculosVariables( $idPrestamo, $tituloGrupo, TRUE ); 
      }

      // Calculo de las Medias Aritmeticas por niveles ... excel
      $salida[ 'ma Nivel4'] = round( ( $salida[ 'Responsabilidad' ]   + $salida[ 'Honestidad' ]  + $salida[ 'Trabajo' ] ) / 3); // se aplica redondeo simple a entero
      $salida[ 'ma Nivel3'] = round( ( $salida[ 'Perfil social' ]     + $salida[ 'ma Nivel4' ] ) / 2); // se aplica redondeo simple a entero
      // ma Nivel 2 ... segun excel
      $salida[ 'Probabilidad pago'] = round( ( $salida[ 'Capacidad de Pago' ] + $salida[ 'ma Nivel3' ] ) / 2); // se aplica redondeo simple a entero

      $salida[ 'Minimo requerido'] = 100 - $salida[ 'Macroeconomicas' ];
      $params['prestamo'] = $this->Thememodel->get_value('prestamos', array('id_prestamo' => $idPrestamo));
      $json_calculos = json_decode( $params['prestamo']->calculos, true );
      if(!empty( $json_calculos['perfil_aprobacion']))
      {   
         //obtenemos el perfil de captacion que se encontraba seleccionado en el momento que se realizó la solicitud del préstamo 
          $id_perfil =  $json_calculos['perfil_aprobacion'];
       }else{
          $id_perfil = $this->listaDatosMacroeconomicos[ 'Perfil' ];
       }
       $perfilesAP = $this->Thememodel->get_value('perfil_captacion', array('id' => $id_perfil ));
       // se refiere al perfil seleccionado (agresivo, estandar o conservador) que se obtiene del campo cálculos de la tabla prestamos
       $perfilSeleccionado = $perfilesAP->valor;

       $salida[ 'Perfil aprobacion' ] = intval($salida['Minimo requerido'] + $perfilSeleccionado);
       if($salida[ 'Perfil aprobacion' ] > 100){
          $salida[ 'Perfil aprobacion' ] = 100;
       }
     
    //margen de puntuación para considerar EN REVISION un préstamo
     $margen = $this->listaDatosMacroeconomicos[ 'Margen' ];
     if($id_perfil==1){
      // para el caso del perfil agresivo se considera el préstamo en revisión con 10 puntos por encima del perfil de aprobación   
          $respuesta = $salida[ 'Probabilidad pago' ] > ( intval( $salida[ 'Perfil aprobacion' ] ) + $margen) ? 'PRE-APROBADO' : (
                   $salida[ 'Probabilidad pago' ] >=   intval( $salida[ 'Perfil aprobacion' ]) ? 'EN REVISION' : 'DENEGADO'
                   );
      }else{
      // para el caso del perfil estandar o conservador se considera el préstamo en revisión con 10 puntos por encima del perfil de aprobación  y por 10 puntos por debajo del perfil de aprobación
      $respuesta = $salida[ 'Probabilidad pago' ] > ( intval( $salida[ 'Perfil aprobacion' ] ) + $margen ) ? 'PRE-APROBADO' : (
                   $salida[ 'Probabilidad pago' ] >= ( intval( $salida[ 'Perfil aprobacion' ]) - $margen ) ? 'EN REVISION' : 'DENEGADO'
                   );
     }
      $salida[ 'Respuesta' ] = $respuesta;

     return $salida;
   }

   /* Obtiene el detalle y los calculos del Grupo de Valores de respuestasGrupoVars segun algoritmo excel*/
   public function getCalculosVariables(
      $idPrestamo,
      $titulo_grupo,
      $devolverSoloElTotal  = FALSE,
      $devolverDetalleCCyVP = FALSE
   ){

      // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      //
      //     IMPORTANTE: Cuando el parametro $devolverSoloElTotal es TRUE
      //     Se realizan todos los calculos del algoritmo pero NO se genera un HTML
      //     ... solo se devuelve el valor VS correspondiente al grupo de variables
      //     Cuando es FALSE se devuelve un HTML con los detalles para el informe --- jjy
      //
      // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      $params  = [];
      $detalle = [];

      // Preparando datos necesarios ... traido de Gestion.php (controller)
      // --- se colocan acá para independizar el modelo del controlador de Gestion

      $join  = array(array('table'=>'users','join'=>'users.id = prestamos.id_cliente'), array('table'=>'prestamos_estados','join'=>'prestamos_estados.id = prestamos.prestamo_estado_id'));
      $where = array('prestamos.id_prestamo' => $idPrestamo);
      $params['prestamo'] = $this->Thememodel->get_all_values_leftjoin_where_order_pag('*','prestamos',$join,$where,'id_prestamo','ASC',10,0);

      $id_cliente         = $params['prestamo'][0]->id_cliente;
      $params['cliente']  = $this->Thememodel->get_value('users', array('id' => $id_cliente));

      $params['preguntas']  = $this->DataMaster->GetData();
      $params['respuestas'] = $this->DataMaster->GetDataResponses($params['prestamo']);

      // --- datos del prestamo (formulario de solicitud)
      $datos_prestamo = $params['prestamo'][0];

      $info_cliente = json_decode( json_encode( $params['cliente'] ), true ); // de stdClass object a array

      $admin_vars = $this->listaDatosMacroeconomicos; // Se usan basicamente en Macroeconomicas // consultar Guillermo
      $infoSimu = Array(
         "cantidad"     => $datos_prestamo->cantidad,
         "duracion"     => $datos_prestamo->tiempo,
         // Preguntar si es esta la fecha de inicio correcta! No parece haber otra mas ideal, en el excel se asume la fecha de Hoy.
         "fecha_inicio" => $datos_prestamo->ultima_actualizacion,
         "dias"         => $datos_prestamo->tiempo * $admin_vars['FDur'] // factor de duracion = 15 (para Qnas)
      );

      // --- al pasar otro titulo de Grupo de Valores de la tabla coeficientes funciona ok
      $this->listaCoeficientes = $this->Coeficientes->ProcessCoeficientes( $titulo_grupo );

      // --- obtener respuestas del prestamo segun los id de pregunta de la lista de Coeficientes
      $respuestasGrupoVars = $this->getRespuestasPrestamo(
         array_keys ( $this->listaCoeficientes ), // --- arreglo de id_preguntas
         $datos_prestamo->datos,   // --- campo datos de la tabla prestamo en la bbdd
         $datos_prestamo->calculos // --- campo calculos de la tabla prestamo en la bbdd
      );
      
      $col_A  = $this->listaCoeficientes;
      $col_B  = $params['preguntas'];
      $col_H  = $respuestasGrupoVars;
      $vp_all = $this->listaVariablesPrimariasCompleta;
      $col_U  = $this->listaMaestrosInferencia;

      if( !$devolverSoloElTotal ){ // en caso de sólo requerir el cálculo de valores pero no el HTML del informe

      $titulo_bloque = strtoupper( $titulo_grupo );
      $salida = "<p class='blue font-white py-2 pl-2 mb-0 mt-3 toggleVP'><b class='enlace'><i class='mr-3 fa fa-chevron-down variables'></i>{$titulo_bloque}</b><i class='fa fa-exclamation-circle pull-right mr-3 pt-1' data-toggle='popover'></i></p>";
      $salida .= "<table class='table table-hover  font-blue tablesVP mt-0' cellpadding='5' border='0' style='overflow:hidden'>";

      $salida .=<<<fdb
      <thead>
            <tr>
                  <th width='65'>Cod.</th>
                  <th width='460'>Pregunta</th>
                  <th width='60'>Datos</th>
                  <th width='60'>CC</th>
                  <th width='150'>Respuesta</th>
                  <th width='65'>V.P</th>
                  <th width='40'> &nbsp; </th>
                  <th width='60'>C.Inf.</th>
                  <th width='60'>V.S</th>
            </tr>
     </thead>
     <tbody>
fdb;
      }

      $prom_vp    = 0;
      $prom_vs    = 0;

      $suma_vps   = 0;
      $n_vps      = 0;
      $suma_vss   = 0;
      $n_vss      = 0;

      if( !$devolverSoloElTotal ){
         $hints = Array(
            "#en blanco#"     => "En blanco:\nExiste la respuesta en prestamo->datos pero está en blanco",
            "#Sin Pregunta#"  => "Sin Pregunta:\nNo trae la pregunta en \$this->DataMaster->GetData();",
            "#Sin Respuesta#" => "Sin Respuesta:\nNo existe el valor en prestamo->datos con este n_pregunta",
            "#SF#"            => "SF / Sin fórmula:\nAún no se ha codificado el algoritmo para esta pregunta",
            "#SC#"            => "SC / Sin coeficiente:#\nNo tiene definido valor en la tabla coeficientes",
            "-1"              => "VP -1:\nPresenta errores en los datos almacenados para esta pregunta.",
            "-2"              => "VP -2:\nno tiene sufijo _vp asociado, no tiene valor de respuesta asociado a n_preg o no puede identificarse VP"
         );
      }

      foreach( $col_A as $n_pregunta => $col_L ){
         
         $sw_no_rsp  = FALSE;

         $vp    = -1; // es obligatorio 
         $cc    = '&nbsp;'; // para evitar error en el td
         $datos = '&nbsp;'; // para evitar error en el td

         $rsp       = isset( $col_H[ $n_pregunta ] ) ? $col_H[ $n_pregunta ] : ['#Sin Respuesta#',-1];
         $respuesta = $rsp[0]; // -- revisar ... array? por?

         $hint_pregunta = ''; // --- temporal mientras se revisa
         $hint_rsp      = ''; // --- temporal mientras se revisa

         // --- Importante ... estos valores no tienen respuesta directa de prestamo porque son datos calculados
         if( $respuesta == '#Sin Respuesta#' ) {
            if( substr( $n_pregunta, 0, 2 ) === 'CC' || substr( $n_pregunta, 0, 4 ) === 'Simu' || substr( $n_pregunta, 0, 5 ) === 'Resum'){ // --- Los campos CC son campos calculados
               // Los campos calculados CCx tienen formulas del algoritmo
               // .. asi que no se consideran sin respuesta
               $sw_no_rsp = FALSE;
               $respuesta = '#CC#';
            } else if (!is_numeric( substr( $n_pregunta, 0, 2 ) ) )  { // -- Simu, An, Dato, Resum ... campos raros!
               // Los campos especiales tienen formulas del algoritmo / aún cuando sólo devuelvan un dato X
               // .. asi que no se consideran sin respuesta
               $sw_no_rsp = FALSE;
               $respuesta = ''; // no debera ser tomada en cuenta para el informe
            } else {
               // --- todos los demás son campos sin respuesta en prestamo->dato
               $sw_no_rsp = TRUE;

               if( !$devolverSoloElTotal ){
                  $hint_rsp  = "style='cursor:help' title='{$hints[ $respuesta ]}'";
               }
               $respuesta    = '';  // no debera ser tomada en cuenta para el informe
            }
         }else if( $respuesta == '' ) {
            // --- existe en prestamo->dato pero esta en blanco
            $sw_no_rsp = TRUE;

            if( !$devolverSoloElTotal ){
               $hint_rsp  = "style='cursor:help' title='{$hints['#en blanco#']}'";
            }
            //$respuesta    = "<font color='grey'>#en blanco#</font>";
            $respuesta = ''; // no debera ser tomada en cuenta para el informe
         }
         
         // --- si no hay respuesta no se incluye en el informe ni en el calculo del promedio VP
         if( $respuesta !== '' && $respuesta !== '#Sin Respuesta#'
             // ---- || TRUE // TRUE solo para depuracion // REVISAR
            ){ 

            $tn_pregunta = str_replace( '_', '.', $n_pregunta );
            $pregunta    = '';
            if( isset( $col_B[ $n_pregunta ] ) ){
               $pregunta = $col_B[ $n_pregunta ];
            } else {
               if( !$devolverSoloElTotal ){
                  $pregunta      = "{$n_pregunta} <font color='grey'>#Sin Pregunta#</font>";
                  $hint_pregunta = "style='cursor:help' title='{$hints['#Sin Pregunta#']}'";
               }
            }

            $json_datos    = json_decode( $datos_prestamo->datos, true );
            $json_calculos = json_decode( $datos_prestamo->calculos, true );
            if( is_array( $json_calculos ) ) {
               $json_datos_full = array_merge( $json_datos, $json_calculos ); // merge!
            } else {
               $json_datos_full = $json_datos;
            }


            $respuestas_normalizadas = $this->normalizarRespuestasPrestamo( $json_datos_full ); // TODAS las respuestas // ---- incluyen los datos calculados con sufijo _cc y _vp ... si los tuviera!

            $vp_fija = '';
            $cc_fija = '';
            if( isset( $json_datos_full[ $n_pregunta.'_vp' ] ) ) // --- jjy ... ojo con esto ..
            // --- sufijos _vp priman por sobre el algoritmo
            {
               $vp      = $json_datos_full[ $n_pregunta.'_vp' ];
               $vp_fija = $vp;
            }
            if( isset( $json_datos_full[ $n_pregunta.'_cc' ] ) ) // --- jjy ... ojo con esto ..
            // --- sufijos _cc priman por sobre el algoritmo
            {
               $cc      = $json_datos_full[ $n_pregunta.'_cc' ];
               $cc_fija = $cc;
            }

            if( !$sw_no_rsp ){
               // --- se pasarán TODOS los datos que pudieran requerirse (o no) para el cálculo de VP
               $vp = $this->getCalculoVP( // --- Calculo fundamental para el algoritmo ! -- jjy
                           $n_pregunta,
                           $titulo_grupo,
                           $rsp,
                           $this->listaVariablesPrimariasCompleta,
                           $info_cliente,
                           $infoSimu,
                           $respuestas_normalizadas, // el resto de respuestas ... principalmente para CCs
                           $admin_vars               // datos macroeconomicos
                        );
               // --- el resultado puede ser solo $vp .. o un arreglo asociativo
               if( is_array( $vp ) ){
                  // extrae del arreglo segun indice asociativo en variables individuales
                  extract( $vp );
                  // --- valores esperados: vp, cc, datos ... obligatorio solo vp
               }
            } else {
               // --- no tiene sufijo _vp asociado
               // --- no tiene valor de respuesta asociado a lista
               // --- no puede identificarse VP
               $vp = -2;
            }

            if( $vp === -1 ){
               $vp = 'solo-mostrar'; // hay errores en los datos guardados en prestamo
               $respuesta = "<font color='red'>inválida</font>";
            }
                        // RESTRICCION indicada por Guillermo --- en el informe solo deben mostrarse _vp .. de lo contrario es un error
            if( $devolverSoloElTotal == FALSE && $vp_fija == '' && $tn_pregunta!='3.11' && $tn_pregunta!='Resum'  && $tn_pregunta!='4.2' && $tn_pregunta!='4.3'){
               $sw_no_rsp == TRUE;
               $respuesta = "<font color='red'>error</font>";
               $vp = 'solo-mostrar';
            }

            // al haber vp_fija .. remplaza al resultado del algoritmo .. pero permanecen los otros datos
            $es_vp_fija = FALSE;
            if( $vp_fija != '' ){
               $vp         = $vp_fija;
               $es_vp_fija = TRUE;
            }
            // al haber cc_fija .. remplaza al resultado del algoritmo .. pero permanecen los otros datos
            $es_cc_fija = FALSE;
            if( $cc_fija != '' ){
               $cc         = $cc_fija;
               $es_cc_fija = TRUE;
            }

            $col_M = $col_L >= 1 ? $col_U[ $col_L - 1 ] : '#SC#'; // --- coef infer segun tabla y valor de $col_L
            if( $col_M == '#SC#' ){
               if( in_array( $n_pregunta, ['4_2','4_3','4_4'] ) ){ // es distrito / corregimiento / sector .. caso especial
                  // caso particular para distrito / corregimiento / sector! .. muy raro! ver excel!
                  $col_M = '';
               }
            }
            $vs = '';

            $color = $vp === '#SF#' ? 'grey !important;cursor:help' 
                     : ($vp < 0 ? 'red !important;cursor:help' // --- no es definitiva
                     : '#265c96'); 

            if( !$devolverSoloElTotal ){
               $hint_vp   = isset( $hints[$vp] )    ? "title='{$hints[$vp]}'" : '';
               $hint_coef = isset( $hints[$col_M] ) ? "style='cursor:help;color:grey' title='{$hints[$col_M]}'" : '';
            }
           
            
               /*if(substr($json_datos_full['4_2_vp' ],0,1)!=0)
               {
                  echo "si es diferente";
                  echo "<br>";
               }else{
                  echo "no es diferente";
                  echo "<br>";
               }*/
          /*echo $vp;
          echo "<br>";*/

            // --- ATENCION .. hay valores del informe que solo se usan para calcular otros datos, solo deben
            //     mostrarse en el informe pero no tienen VP / Coef / VS que deban afectar el calculo final
            //     del algoritmo -- Consultar detalles con Hugo / Guillermo
            if( $vp !== 'solo-mostrar'){
              
               $n_vps++;
               $suma_vps += $vp;
               $n_vss++;
               
                if( isset( $vs4_2 ) ){
                  $vs = $vs4_2; // caso particular para distrito / corregimiento / sector! .. muy raro! ver excel!
                  unset( $vs4_2);
               } else {
                  $vs = round( $col_M * $vp );
               }
                  
                  $suma_vss += $vs;
            } else {
               // --- solo se muestra en el informe .. pero no afecta el resultado
               $vp    = '';
               $col_L = '';
               $col_M = '';
               $vs    = '';
            }

            if( $devolverDetalleCCyVP ){ // prepara el detalle de calculos para la salida
               if( !in_array( trim($cc), ['', '&nbsp;'] ) ) $detalle[ $n_pregunta.'_cc' ] = $cc;
               if( !in_array( trim($vp), ['', '&nbsp;'] ) ) $detalle[ $n_pregunta.'_vp' ] = $vp;
            }

            if( !$devolverSoloElTotal ){
               $vp = $vp_fija === '' ? $vp : "<font color='darkblue'>{$vp}</font>";
               $cc = $cc_fija === '' ? $cc : "<font color='darkblue'>{$cc}</font>";

               $html_vs = "<td>{$vs}</td>";
                
              
               $salida .=<<<fdb2
                  <tr>
                     <td>
                        $tn_pregunta
                     </td>
                     <td {$hint_pregunta} width='460'>
                        <div style='white-space:nowrap;width:inherit;overflow:hidden;text-overflow:ellipsis'>
                           {$pregunta}
                        </div>
                     </td>
                     <td> {$datos} </td>
                     <td> {$cc} </td>
                     <td {$hint_rsp} width='150'>
                        <div style='white-space:nowrap;width:inherit; overflow:hidden;text-overflow:ellipsis'>
                           {$respuesta}
                        </div>
                     </td>
                     <td {$hint_vp} width='65' style='color:{$color};overflow:hidden;text-overflow:ellipsis'> 
                        {$vp}
                     </td>
                     <td> 
                        {$col_L}
                     </td>
                     <td {$hint_coef}> 
                        {$col_M}
                     </td>
                     {$html_vs}
                  </tr>
fdb2;
            }
         }
      }

      if( $n_vps > 0 ) $prom_vp = round( $suma_vps / $n_vps );
      if( $n_vss > 0 ) $prom_vs = round( $suma_vss / $n_vss );

      if( !$devolverSoloElTotal ){
            
         // Atención acá: La puntuación guardada corresponde al promedio de las variables secundarias VS
         //     mientras que los valores individuales guardadaos corresponden a las _VPs y _CCs
         // --- esto deberá revisarse ... jjy
         $style_pvs = '';
         if( isset( $respuestas_normalizadas[ 'puntuacion_'.str_replace(' ','_',$titulo_grupo) ] ) ){
            $prom_vs = $respuestas_normalizadas[ 'puntuacion_'.str_replace(' ','_',$titulo_grupo) ];
         } else {
            $style_pvs = 'color: red;'; // no existe el campo guardado
         }

         $salida .=<<<fdb3
         <tfoot>
            <tr>
               <td colspan='4'> &nbsp; </td>
               <td> Promedio:</td>
               <td> {$prom_vp} </td>
               <td> &nbsp; </td>
               <td> &nbsp; </td>
               <td style='{$style_pvs}'> {$prom_vs} </td>
            </tr>
         </tfoot>
fdb3;
         $salida .= "</table>";
         $salida .= "</table>";
      }

      if( $devolverSoloElTotal ){
         $salida = $prom_vs;
      }

      // --- Devolviendo calculos _cc y _vp para cada pregunta (persistencia) .. 1.122 trello
      if( $devolverDetalleCCyVP ){
         if( is_array( $salida ) ){
            $salida = array_merge( $salida, $detalle );
         } elseif ( is_string( $salida ) ){
            $salida = array_merge( [ 'html' => $salida ], $detalle );
         } else {
            $salida = array_merge( [ 'puntuacion_'.str_replace( ' ','_', $titulo_grupo ) => $prom_vs, 'perfil_aprobacion'=>$this->listaDatosMacroeconomicos[ 'Perfil' ]], $detalle );
         }
      }

      return $salida;
   }

   /* Obtiene las respuestas para un grupo de Variables dadas */
   private function getRespuestasPrestamo( $grupo_variables, $prestamo_campo_datos, $prestamo_campo_calculos ){

      $salida = [];
      
      $json_respuestas       = json_decode( $prestamo_campo_datos, true );
      $json_calculados       = json_decode( $prestamo_campo_calculos, true );
      if( is_array( $json_calculados ) ){
         $json_respuestas_todas = array_merge( $json_respuestas, $json_calculados );
      } else {
         $json_respuestas_todas = $json_respuestas;
      }

      $json_respuestas_normalizadas = $this->normalizarRespuestasPrestamo( $json_respuestas_todas );

      $respuestas = $json_respuestas_normalizadas;

      foreach( $grupo_variables as $id_pregunta )
      {

         $id_pregunta_normalizada = $this->normalizarIDpregunta( $id_pregunta );

         // --- si el id_pregunta coincide con alguna respuesta
         // --- OJO .. hay IDs que tienen cola! ... deben normalizarse antes de este if
         if ( isset( $respuestas[ $id_pregunta_normalizada ] ) ) {

            $strAuxDatos = $respuestas[ $id_pregunta ];

            // --- datos de ubicacion, sector, corregimiento, etc ....
            if (
                  ($id_pregunta == "4_1")
                  || ($id_pregunta == "4_2")
                  || ($id_pregunta == "4_3")
                  || ($id_pregunta == "4_4")
                  || ($id_pregunta == "4_5")
               )
            {
               $strAuxDatos = substr( $respuestas[ $id_pregunta ], 2 ); // --- a partir de la pos 2
            }

            $salida[ $id_pregunta ][0] = $this->descRespuesta( $id_pregunta_normalizada, $strAuxDatos );
            $salida[ $id_pregunta ][1] = $strAuxDatos;
         }
      }
      return $salida; 
   }

   private function descRespuesta( $id_pregunta, $id_respuesta ){

      $salida = $this->Thememodel->get_select(
         'description' , // --- Campos del SELECT
         'data_page'   , // --- FROM
         'question = "'.$id_pregunta.'" and id_page = "'.$id_respuesta.'"' // --- WHERE
      );

      if( !isset( $salida[0] ) ) {
         return $id_respuesta; // --- si no hay coincidencia, se devuelve el valor original
      }
      return $salida[0]->description;
   }

   private function normalizarIDpregunta( $id_pregunta ){
      
      // --- Este método debe complementarse con un procedimiento más formal y genérico ..
      // --- que sirva para cualquier código recibido

      $arrEquivalenciasID = Array (
         "5_1_numero_cuenta"        => "5_1",
         "5_1_1_nombre_cuenta"      => "5_1_1",
         "5_4_2_numero_tarjeta"     => "5_4_2",
         "2_10_telefono"            => "2_10",
         "4_7_dia"                  => "4_7", // --- revisar
         "4_7_mes"                  => "4_7", // --- revisar
         "4_7_year"                 => "4_7", // --- revisar
         "3_5_cantidad_dinero_minima" => "3_5",
         "3_6_dia"                  => "3_6", // --- revisar
         "3_6_mes"                  => "3_6", // --- revisar
         "3_6_year"                 => "3_6", // --- revisar
         "3_9_telefono_empresa"     => "3_9",
         "3_3_nombre_empresa"       => "3_3",
         "3_11_gastos_mensuales"    => "3_11",
         "2_13_telefono2"           => "2_13",
         "2_17_telefono_referencia" => "2_17",
         "2_8_1_codigo_licencia"    => "2_8_1"
      );

      $salida = $id_pregunta;

      if( isset( $arrEquivalenciasID[ $id_pregunta ] ) ){
         $salida = $arrEquivalenciasID[ $id_pregunta ];
      }

      return $salida;
   }

   private function normalizarRespuestasPrestamo( $json_datos ){
      
      // --- Normalizar tanto los id de preguntas como las respuestas en ciertos casos 4_7 y 3_6

      $salida = [];
      foreach( $json_datos as $id_pregunta => $respuesta ){

         $id_pregunta_normalizada = $this->normalizarIDpregunta( $id_pregunta );

         if( $id_pregunta_normalizada == '4_7' ||
             $id_pregunta_normalizada == '3_6' ) {

            // --- se redefine la respuesta para crear una fecha completa
            if( !isset( $salida[ $id_pregunta_normalizada ] ) 
               && $json_datos[ $id_pregunta_normalizada.'_dia' ] != ''
            )
            {
               $respuesta = $json_datos[ $id_pregunta_normalizada.'_dia' ].'/'.$json_datos[ $id_pregunta_normalizada.'_mes' ].'/'.$json_datos[ $id_pregunta_normalizada.'_year' ];
               $salida[ $id_pregunta_normalizada ] = $respuesta;
            }
         }

         if( !isset( $salida[ $id_pregunta_normalizada ] ) ) {
            $salida[ $id_pregunta_normalizada ] = $respuesta;
         }

      }
      // arreglo de respuestas normalizadas e id_preguntas normalizados
      return $salida;
   }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
   This class is defined to model CapacidadDePago where some variables are calculated to manage the final result of the different questions
*/

class CapacidadDePago extends CI_Model{ // --- 1.84 trello --- jjy
   /*
      Version:
      03-2018 jjyepez
   */
    private $formulasHabilitadas;

    public function __construct()
    {
      parent::__construct();

      // --- si no está en el array es que no se ha definido fórmula para la pregunta dada
      $this->formulasHabilitadas = [
        "4_9","3_12","3_13","3_2","3_15_2","3_15_3","2_9_3","2_9_4","3_10","3_11","3_5","3_6",

        // --- A falta de un nombre mas adecuado usamos la referemcia al excel
        "SimuA6","SimuA7","A10",
        "CC1", "Resum", "CC3", "CC2",
        "3_1","4_7"

      ];
	  }

    // --- Obtiene la formula para cada pregunta - si la hay - evita errores
    public function getFormulaVP( $n_pregunta_normalizada ){
      return in_array( $n_pregunta_normalizada, $this->formulasHabilitadas )
              ? 'formula'.$n_pregunta_normalizada
              : 'sinFormula';
    }

    // --- Forma mas generica / reutilizable para VPs basados en lista
    public function formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta ){
      $vp     = -1; // --- valor de VP por defecto
      $id_rsp = $dato_respuesta;
      if( is_numeric( $dato_respuesta ) ){
        $id_rsp = intval( $dato_respuesta );
      }
      // --- simplificada
      if( isset( $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ] ) ) {
        return $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ];
      } else {
        return $id_rsp;
      }
    }

    // --- fallback para cuando no se ha definido la formula para una pregunta dada
    public function sinFormula( $params = Array() ){
      return '#SF#';
    }

    // ---- FORMULAS EN RELACION DIRECTA CON EL EXCEL DEL ALGORITMO ----- //
    // -- pudieran cambiar (optimizarse) o unificarse luego

    public function formula2_9_4( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_9_4';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula4_9( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '4_9';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_1( $dato_respuesta, $listaVPsCompleta ){
      // --- deberia venir como 3_1_vp
      $n_pregunta_normalizada = '3_1';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_12( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_12';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_13( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_13';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_15_2( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_15_2';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_15_3( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_15_3';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_2( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_2';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_5( $dato_respuesta, $listaVPsCompleta, $_, $_, $todasLasRespuestas ){
      // Este campo es en teoria el RESUMEN de los datos principales del simulador !!! y más !
      $datos_simuA6    = $this->formulaSimuA6( null, null, null, null, $todasLasRespuestas );
      $cuanto_necesita = $datos_simuA6[ 'respuesta' ];

      $cc = $cuanto_necesita - $dato_respuesta;

      $dato_respuesta = $cc <   0               ? 1 : (
                        $cc ==  0               ? 2 : ( // --- en el excel dice >= pero la logica indica que es == 0
                        $cc >   0 && $cc <= 100 ? 3 : (
                        $cc > 100 && $cc <= 200 ? 4 : (
                        $cc > 200               ? 5 : -1
                        ))));

      $n_pregunta_normalizada = '3_5';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "cc" => $cc,
        "vp" => $vp
      ];
    }

    public function formula3_6( $dato_respuesta, $listaVPsCompleta, $infoCliente, $_, $todasLasRespuestas ){
      $hoy       = gmdate('d/m/Y'); // --- Hoy

      $fecha_antiguedad = $dato_respuesta;

      $difDias = (float) strDMYdiff( $fecha_antiguedad, $hoy );

      // se necesita la diferencia en años (con decimales si es necesario ...) dias diff / 365
      $cc = round( $difDias / 365, 2 );
      $dato_respuesta = $cc <  1               ? 1 : (
                        $cc >= 1  && $cc <  2  ? 2 : (
                        $cc >= 2  && $cc <  5  ? 3 : (
                        $cc >= 5  && $cc <= 10 ? 4 : (
                        $cc >  10              ? 5 : -1
                        )))); // el else : es innecesario acá pero se deja para respetar la lógica del excel

      $n_pregunta_normalizada = '3_6';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => $fecha_antiguedad,
        "cc"        => $cc,
        "vp"        => $vp
      ];
    }


    public function formula3_10( $dato_respuesta, $listaVPsCompleta ){ // --- Ingresos mensuales
      $dato_orig = $dato_respuesta;
      $dato_respuesta = $dato_orig >   0   && $dato_orig <  550 ? 1 : (
                        $dato_orig >=  550 && $dato_orig <= 650 ? 2 : (
                        $dato_orig >=  650 && $dato_orig <= 850 ? 3 : (
                        $dato_orig >=  850 && $dato_orig <=1050 ? 4 : (
                        $dato_orig >= 1050 && $dato_orig <=1550 ? 5 : (
                        $dato_orig >= 1500 && $dato_orig <=2000 ? 6 : (
                        $dato_orig >  2000                      ? 7 : -1
                        ))))));
      
      $n_pregunta_normalizada = '3_10';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_9_3( $dato_respuesta, $listaVPsCompleta ){ // --- Año del vehiculo
      $year = $dato_respuesta;
      $dato_respuesta = $year >= 1990 && $year < 2000 ? 1 : (
                        $year >= 2000 && $year < 2010 ? 2 : (
                        $year >= 2010 && $year < 2013 ? 3 : (
                        $year >= 2013                 ? 4 : -1
                        )));
      // --- OJO .. esta validación debería hacerse relativa al año actual ... no de 1990 a 2013
      // --- y adicionalmente .. que pasaría si el año es menor que 1990?

      $n_pregunta_normalizada = '2_9_3';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_11(){ // --- Gastos mensuales
      // Este es un campo especial --- solo usado para calcular otros datos ... 
      // --- no tiene formula asociada y su vp no debe afectar los resultados

      $vp = "solo-mostrar";
       return $vp;
    }

    public function formulaSimuA6( $_, $listaVPsCompleta, $_, $_, $todasLasRespuestas ){ // -- Cuanto necesitas
      // Este es un campo especial --- viene del simulador: cantidad solicitada
      $cantidad = $todasLasRespuestas[ 'amount' ];

      $dato_respuesta = $cantidad >= 20 && $cantidad <= 100 ? 1 : (
                        $cantidad > 100 && $cantidad <= 200 ? 2 : (
                        $cantidad > 200 && $cantidad <= 300 ? 3 : (
                        $cantidad > 300                     ? 4 : -1
                        )));

      $n_pregunta_normalizada = 'SimuA6';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "tn_pregunta" => 'Simu', // --- es el codigo que tiene en el excel
        "respuesta"   => $cantidad,
        "vp"          => $vp
      ];
    }

    public function formulaSimuA7( $_, $listaVPsCompleta, $_, $_, $todasLasRespuestas, $datosMacroeconomicos ){  // -- tiempo para devolverlo
        // Este es un campo especial --- viene del simulador
      $duracion        = $todasLasRespuestas[ 'duration' ];
      $factor_duracion = $datosMacroeconomicos[ 'FDur' ]; // --- FDur .. 15 para quincena

     
      $dato_respuesta = $duracion;

      $n_pregunta_normalizada = 'SimuA7';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "tn_pregunta" => 'Simu', // --- es el codigo que tiene en el excel
        "respuesta"   => $duracion,
        "vp"          => $vp
      ];
    }

    public function formulaSimuA8( $_, $_, $_, $_, $todasLasRespuestas ){ // --- fecha inicio prestamo
      // Este es un campo especial --- viene del simulador
      $dato_respuesta_orig = $todasLasRespuestas[ 'ultima_actualizacion' ];
      $fecha               = date_create( $dato_respuesta_orig );
      $dato_respuesta      = date_format( $fecha, "d/m/Y" ); // --- se le da el formato estandar

      $vp = "solo-mostrar";
      return [
        "n_pregunta"  => '',
        "tn_pregunta" => '', // --- es el codigo que tiene en el excel
        "respuesta"   => $dato_respuesta,
        "vp"          => $vp
      ];
    }

    public function formulaSimuA9( $_, $_, $_, $_, $todasLasRespuestas, $datosMacroeconomicos ){ // --- dias
      // Este es un campo especial --- viene del simulador
      $factor_duracion = $datosMacroeconomicos[ 'FDur' ]; // --- FDur .. 15 para quincena
      $dato_respuesta  = $todasLasRespuestas[ 'duration' ] * $factor_duracion; // n quincenas * 15
      $vp = "solo-mostrar";
      // --- aparentemento tiene un error en el excel ... no asocia VP ni VS
      return [
         "n_pregunta"  => '',
         "tn_pregunta" => '', // --- es el codigo que tiene en el excel
         "respuesta"   => $dato_respuesta,
         "vp"          => $vp
      ];
    }

    public function formulaA10( $_, $_, $_, $_, $todasLasRespuestas ){ // --- dias
      if( isset($todasLasRespuestas[ '3_10' ]) && isset($todasLasRespuestas[ '3_5' ]) ){  // son requeridos
        $ingresos_mensuales = $todasLasRespuestas[ '3_10' ];
        $cant_util_hoy      = $todasLasRespuestas[ '3_11' ];

        $cc = $ingresos_mensuales - $cant_util_hoy;
        $vp = "solo-mostrar";
      } else {
        $cc = '';
        $vp = -1;
      }
      return [
        "tn_pregunta" => '', // --- en el excel esta celda no tiene codigo de pregunta
        "cc"          => $cc,
        "respuesta"   => '',
        "vp"          => $vp
      ];
    }

    public function formulaCC1( $_, $listaVPsCompleta, $_, $_, $todasLasRespuestas ){
      $datos_A10           = $this->formulaA10( null, null, null, null, $todasLasRespuestas );
      
      if( $datos_A10 != -1 && isset($todasLasRespuestas[ '3_10' ]) ){ // son requeridos
        $capacidad_pago_calc = $datos_A10[ 'cc' ];
        $ingreso_mensual     = $todasLasRespuestas[ '3_10' ];

        $cc = round( ( $capacidad_pago_calc / $ingreso_mensual ) * 100, 2); // --- * 100 por ser %
        $cc_con_formato = $cc.'&nbsp;%';

        $dato_respuesta = $cc <= 0              ? 1 : (
                          $cc >  0 && $cc <= 10 ? 2 : (
                          $cc > 10 && $cc <= 20 ? 3 : (
                          $cc > 20 && $cc <= 30 ? 4 : (
                          $cc > 30 && $cc <= 50 ? 5 : (
                          $cc > 50              ? 6 : -1
                        )))));
        
        $n_pregunta_normalizada = 'CC1';
        $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      } else {
        $cc_con_formato = '';
        $vp = -1;
      }
      return [
        "respuesta" => '',
        "cc"        => $cc_con_formato,
        "vp"        => $vp
      ];
    }

    public function formulaResum( $_, $_, $infoCliente, $_, $todasLasRespuestas, $datosMacroeconomicos ){
      // Este campo es en teoria el RESUMEN de los datos principales del simulador !!! y más !

      // --- Luego de conversar los detalles e inconvenientes que se observaron en este campo la conclusión es
      //     que el excel está equivocado en los campos del simulador, y la forma de calcular en total aplazado
      //     con intereses, legal y manejo .. y que en su lugar deberá ser calculado con la siguiente formula

      /*
        @Guillermo vía Slack 03022018

        (columna cantidad de la tabla prestamos) * (columna tiempo de la tabla prestamos) * 15 * (columna tasa
        de interes tabla calificacion)
        dentro de la tabla de usuarios, hay un id_calificacion para poder atacar a la tabla calificacion
      */

      $cantidad_solicitada = $todasLasRespuestas[ 'amount' ];
      $id_calificacion     = $infoCliente[ 'id_calificacion' ];

      $rs_obj  = $this->Thememodel->get_select( 'tasa_interes', 'calificacion', 'id = "'.$id_calificacion.'"' );
      $rs      = json_decode( json_encode( $rs_obj ), true );

      // el excel está equivocado en este campo según indicaciones de Guillermo
      $porc_interes    = $rs[0]['tasa_interes'];

      $factor_duracion = $datosMacroeconomicos[ 'FDur' ]; // --- FDur .. 15 para quincena

      $datos_simuA7    = $this->formulaSimuA7( null, null, null, null, $todasLasRespuestas, $datosMacroeconomicos );
      $duracion_qnas   = $datos_simuA7[ 'respuesta' ];
      
      $coste = $cantidad_solicitada * $duracion_qnas * $factor_duracion * $porc_interes;
      $monto_total_prestamo = $cantidad_solicitada+$coste;
      if(!empty($todasLasRespuestas[ 'total_aplazado' ])){
          $total_aplazado  = $todasLasRespuestas[ 'total_aplazado' ];
        }else{
          $total_aplazado  = round($monto_total_prestamo/$duracion_qnas);;
        }

    
      //round($monto_total_prestamo/$duracion_qnas); //calculo actualizado
     // $cantidad_solicitada * $duracion_qnas * $factor_duracion * $porc_interes; calculo anterior 
      $respuesta       = $total_aplazado.' &nbsp; '.$duracion_qnas.' Qnas.';

      $cc = $duracion_qnas == 1 ? $total_aplazado : (
            $duracion_qnas >= 2 ? $total_aplazado * 2 : -1
            );
      
      return [
        "respuesta" => $respuesta,
        "cc"        => $cc,
        "vp"        => "solo-mostrar"
      ];
    }

    public function formulaCC3( $_, $listaVPsCompleta, $infoCliente, $_, $todasLasRespuestas ){
      $hoy       = gmdate('d/m/Y'); // --- Hoy

      $fecha_nacimiento = $infoCliente[ 'fecha_nacimiento' ];
      $fecha            = date_create( $fecha_nacimiento );
      $fecha_formato    = date_format( $fecha, "d/m/Y" ); // --- se le da el formato estandar

      $difDias = (float) strDMYdiff( $fecha_formato, $hoy );

      // se necesita la diferencia en años (con decimales si es necesario ...) dias diff / 365
      $cc = round( $difDias / 365, 2 );
      $dato_respuesta = $cc <  18             ? 1 : (
                        $cc >= 18 && $cc < 21 ? 2 : (
                        $cc >= 21 && $cc < 28 ? 3 : (
                        $cc >= 28 && $cc < 35 ? 4 : (
                        $cc >= 35 && $cc < 60 ? 5 : (
                        $cc >  60             ? 6 : -1
                        ))))); // el else : es innecesario acá pero se deja para respetar la lógica del excel

      $n_pregunta_normalizada = 'CC3';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => $fecha_formato,
        "cc"        => $cc,
        "vp"        => $vp
      ];
    }

    public function formulaCC2( $_, $listaVPsCompleta, $infoCliente, $_, $todasLasRespuestas, $datosMacroeconomicos ){
      $datos_A10           = $this->formulaA10( null, null, null, null, $todasLasRespuestas );

      if( $datos_A10 != -1 &&  isset( $todasLasRespuestas[ '3_10' ] ) ){ // son requeridos
        $capacidad_pago_calc = $datos_A10[ 'cc' ];

        $datos_Resum    = $this->formulaResum( null, null, $infoCliente, null, $todasLasRespuestas, $datosMacroeconomicos );
        $total_aplazado = $datos_Resum[ 'cc' ];

        $ingreso_mensual = $todasLasRespuestas[ '3_10' ];

        $cc = round( ( ( $capacidad_pago_calc - $total_aplazado ) / $ingreso_mensual ) * 100, 2 ); // --- * 100 por ser %
        $cc_con_formato = $cc.'&nbsp;%';
        //$total_aplazado;
        

        $dato_respuesta = $cc <= 0              ? 1 : (
                          $cc >  0 && $cc <= 10 ? 2 : (
                          $cc > 10 && $cc <= 20 ? 3 : (
                          $cc > 20 && $cc <= 30 ? 4 : (
                          $cc > 30 && $cc <= 50 ? 5 : (
                          $cc > 50              ? 6 : -1
                        )))));
        
        $n_pregunta_normalizada = 'CC2';
        $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      } else {
        $cc_con_formato = '';
        $vp = -1;
      }
      return [
        "respuesta" => '',
        "cc"        => $cc_con_formato,
        "vp"        => $vp
      ];
    }

    public function formula4_7( $dato_respuesta, $listaVPsCompleta, $infoCliente, $_, $todasLasRespuestas ){
      $hoy       = gmdate('d/m/Y'); // --- Hoy
     // $fecha            = date_create($dato_respuesta);
      $fecha_antiguedad = $dato_respuesta;
       
      $difDias = (float) strDMYdiff( $fecha_antiguedad, $hoy );

      // se necesita la diferencia en años (con decimales si es necesario ...) dias diff / 365
      $cc = round( $difDias / 365, 2 );
       $dato_respuesta = $cc < 1                ? 1 : (
                         $cc >= 1 && $cc < 2    ? 2 : (
                         $cc >= 2 && $cc < 5    ? 3 : (
                         $cc >= 5 && $cc <= 10  ? 4 : (
                         $cc > 10               ? 5 : -1
                        ))));
      $cc = round( $difDias / 365, 2 );
      $n_pregunta_normalizada = '4_7'; // no se calcula porque tiene vp fijo en los datos
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => $fecha_antiguedad,
        "cc"        => $cc,
        "vp"        => $vp
      ];
    }
}

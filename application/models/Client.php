<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once('ripcord/ripcord.php');

class Client extends CI_Model{
   
	private $url;

	private $uid;

	private $user;

	private $database;

	private $password;
	
	private $client;
	
	private $common;
	
	private $models;
   
   
   
   function __construct(){
		
		parent::__construct();
		
	}
	
	
	public function setUrl($url)
	{
		$this->url = $url;		
	}
	
	public function setDatabase($database)
	{
		$this->database = $database;		
	}
	
	public function setUser($user)
	{
		$this->user = $user;		
	}
	
	public function setPassword($password)
	{
		$this->password = $password;		
	}
		
	private function setUid()
	{
		if ($this->uid === null) {
			$this->common = ripcord::client($this->url."/xmlrpc/2/common");
			$this->models = ripcord::client($this->url."/xmlrpc/2/object");
			$this->uid = $this->common->authenticate($this->database, $this->user, $this->password, array());
		}
	}
   
   public function createCustomer($data)
   {
	   
		/*$data = [
		'name' => 'John Doe',
		'email' => 'foo@bar.com',
		];
		*/

		$this ->setUid();

		$id = $this->models->execute_kw($this->database, 
							    $this->uid, 
								$this->password,
								'res.partner', 
								'create',
								array($data));
								
		return $id;
	
	
	}
	
	public function editCustomer($id, $data)
   {
	   
		/*$data = [
		'name' => 'John Doe',
		'email' => 'foo@bar.com',
		];
		*/

		$this ->setUid();
		

		$this->models->execute_kw($this->database, 
							    $this->uid, 
								$this->password, 
								'res.partner', 
								'write',
								array(array($id), $data));									
	
	}
	
	public function searchCustomer($id)
	{
		
		$this ->setUid();
		
		$data = $this->models->execute_kw($this->database, 
						  $this->uid, 
						  $this->password,
						  'res.partner',
						  'search',
							array(array(array('id', '=', $id)))
							);
						   
		return $data;
		
	}
	
	
   
}
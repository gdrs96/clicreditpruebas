<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
	This class is defined to model Coeficientes. Get the information from the database and format to be used easily
*/
class Coeficientes extends CI_Model{
	/*
		Version:
		31-12-2017 Guillermo: New
	*/
	
	/*
		This method get the information from the database and decode from json
	*/
	public function ProcessCoeficientes($descripcion)
	{
		/*
			Version:
			31-12-2017 Guillermo: New
		*/	
		$coeficientes = $this->Thememodel->get_value('coeficientes',array('description' => $descripcion));
		return json_decode($coeficientes->valor,true);
	}
	
	/*
		This method get and specific Coeficiente by identifier
	*/
	public function GetCoeficiente($listaCompletaCoeficientes, $id)
	{
		/*
			Version:
			31-12-2017 Guillermo: New
		*/
		return $listaCompletaCoeficientes[0][$id];
	}

	
 
}

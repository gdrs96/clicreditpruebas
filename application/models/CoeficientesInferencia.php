<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
	This class is defined to model CoeficienteInferencia. Get the information from the database and format to be used easily
*/
class CoeficientesInferencia extends CI_Model{
	/*
		Version:
		31-12-2017 Guillermo: New
	*/
	
	/*
		Get the information from the database and format to an array to use easily
	*/
	public function GetMaestrosInferencia()
	{
		/*
			Version:
			31-12-2017 Guillermo: New
		*/
		$query = $this->db->select('id,coeficiente')->from('maestro_coeficiente_inferencia');
		$sql = $query->get_compiled_select();
		$maestrosInferencia = $this->Thememodel->get_query($sql);
		$listaMaestrosInferencia = $this->FormatToCorrectOutput($maestrosInferencia);
		return $listaMaestrosInferencia;
	}
	
	/*
		Format the information retrieve from the database to an array
	*/
	private function FormatToCorrectOutput($maestrosInferencia)
	{
		/*
			Version:
			31-12-2017 Guillermo: New
		*/
		$listaMaestrosInferencia = array();
		foreach ($maestrosInferencia as $valor) {
			array_push($listaMaestrosInferencia, $valor->coeficiente);
		}
		return $listaMaestrosInferencia;
	}

}

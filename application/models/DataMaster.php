<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
	
*/
class DataMaster extends CI_Model{
	/*
		Version:
		 Guillermo: New
	*/
	
	/*
		Process the info for the table and format to correct output
	*/
	
	public function GetDataResponses($datos_prestamo_completos)
	{
		
		// --- OJO ...
		//     Debe ser modificado para incluir los demas GRUPOS DE VARIABLES no solo PerfilSocial
		// --- jjy
		
		$perfilSocialIds = array("2_1","2_5","2_6", "2_6_1", "2_6_2", "2_6_3", "2_7", "2_8", "2_9", "2_9_1", "2_15", "2_16", "2_18", "2_18_1", "2_18_2", "3_1" , "3_2", "3_4", "3_6", "3_8", "3_14", "4_2" ,"4_3", "4_4", "4_7", "4_10","CC3");
		
		
		// "CC6":"5","CC3":"7"
		
		$lista = array();
		
		$datos_prestamo = json_decode($datos_prestamo_completos[0]->datos,true);
		
		foreach($perfilSocialIds as $key)
		{
			if (!empty($datos_prestamo[$key]))
			{
				$strAuxDatos = $datos_prestamo[$key];
				if (($key == "4_1") || ($key == "4_2") || ($key == "4_3") || ($key == "4_4") || ($key == "4_5") )
				{
					$strAuxDatos = substr($datos_prestamo[$key],2);
				}
				
				$lista[$key] = $this->GetDataMasterRetrieval($key,$strAuxDatos);
			}
		}
		
		return $lista; 
		
	}
	
	
	private function GetDataMasterRetrieval($question, $respuesta)
	{
		/*
			Version:
			31-12-2017 Guillermo: New
		*/
		
		return $this->Thememodel->get_select(('description'),'data_page','question="'.$question.'" and id_page="'.$respuesta.'"');
				
	}
	
	
	public function GetDataStep($paso)
	{
		$lista = array();
		
		$datos = $this->Thememodel->get_select(array('question_id','question'),'data_preguntas','paso = "'.$paso.'"');
		
		
		foreach($datos as $key)
		{
			
			$lista[$key->question_id] = $key->question;
		
		}
		
		return $lista;
		
	}
	
	public function GetData()
	{
		$lista = array();
		
		$datos = $this->Thememodel->get_select(array('question_id','question'),'data_preguntas','1 = 1');
		
		
		foreach($datos as $key)
		{
			
			$lista[$key->question_id] = $key->question;
		
		}
		
		return $lista;
		
	}

	
 
}
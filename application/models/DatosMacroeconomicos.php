<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
	This class is defined to model DatosMacroeconomicos. Get the information from the database and format to be used easily
*/
class DatosMacroeconomicos extends CI_Model{
	/*
		Version:
		02-04-2018 jjyepez: New
	*/
	
	/*
		Get the information from the database and format to an array to use easily
	*/
	public function GetDatosMacroeconomicos()
	{
		/*
			Version:
			31-12-2017 Guillermo: New
		*/
		$query = $this->db->select('question_id, descripcion, dato')->from('datos_macroeconomicos');
		$sql = $query->get_compiled_select();
		$datosMacroeconomicos = $this->Thememodel->get_query($sql);
		$listaDatosMacroeconomicos = $this->FormatToCorrectOutput($datosMacroeconomicos);

		return $listaDatosMacroeconomicos;
	}
	/*
		Format the information retrieve from the database to an array
	*/
	private function FormatToCorrectOutput($datosMacroeconomicos)
	{
		/*
			Version:
			31-12-2017 Guillermo: New / mod jjyepez
		*/
		$listaDatosMacroeconomicos = array();
		foreach ($datosMacroeconomicos as $valor) {
			$listaDatosMacroeconomicos[ $valor->question_id ] = $valor->dato;
		}
		return $listaDatosMacroeconomicos;
	}
}

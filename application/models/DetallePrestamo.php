<?php defined('BASEPATH') OR exit('No direct script access allowed');

class DetallePrestamo extends CI_Model {

    public function calcularQuincenas($cantidad, $tiempo, $tasa_interes){
    	$total = ($cantidad*$tasa_interes) + $cantidad;
    	$cuota = $total/$tiempo;
    	return $cuota;

    }


}
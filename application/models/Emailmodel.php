<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Emailmodel extends CI_Model {
	var $email_ = 'info@clicredit.com';
	var $web = 'CliCredit.com';

	function __construct(){
		parent::__construct();
	}

	function send_email($from, $namefrom, $to, $cc, $ccoo, $asunto, $data, $reply_to = null){
		// aqui metemos la plantilla, los datos y los destinatarios
		$header = '
		<html lang="es-PA" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
		<head>
			<meta charset="utf-8">
			<meta name="viewport" content="width=device-width">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="x-apple-disable-message-reformatting">
			<title>'.$asunto.'</title>
			<meta name="viewport" content="width=device-width, initial-scale=1.0" />
			<style>
				body{background-color:#F2F2F2";font-family:Verdana}
				b{font-weight:800;color:#265C96}
				a{font-weight:800;color:#5EAC25}
			</style>
		</head>
		<body style="background:#F2F2F2">
		<table border="0" cellspacing="0" cellpadding="0" style="min-width:600px;background-color:#F2F2F2;margin:auto"><tr><td style="padding:0;margin:0"><a style="padding:0;margin:0" href="http://pa.clicredit.com/v0/" target="_blank"><img src="http://pa.clicredit.com/v0/img/clicredit-logo-mini.png" width="125" height="25" style="padding:0;margin:0"></a></td></tr></table>
		<table style="min-width:600px;background-color:#F2F2F2;margin:auto" border="0">
			<tr><td style="padding:0"></td>
		</tr><tr><td style="padding:0">';
		$footer = '<p><a href="http://clicredit.com/v0"><b style="font-weight:800;color:#265C96">Clicredit.com</b></a>, tu plata al instante, donde tú estes.<br>#daleclicysalvaeldia</p><br><hr style="background-color:#265c96;height:2px">
		Si tienes dudas o preguntas, contactanos a: <br><b style="font-weight:800;color:#265C96">ayuda@pa.clicredit.com</b>, escribenos por <b style="font-weight:800;color:#265C96">whatsapp</b> o llanamos al <b style="font-weight:800;color:#265C96">telelefono 260-8951</b><br></td></tr></table></body></html>';
		$data = $header.$data.$footer;

		$this->load->library('email');

		$config['mailtype'] = 'html';

		$this->email->initialize($config);

		//$this->email->from('ceo@carsharingspain.com', 'Miguel Gómez - CEO Car Sharing');
		if($reply_to != null){
			$this->email->reply_to($reply_to, $namefrom);
		}
		$this->email->from($from, $namefrom );
		$this->email->to( $to );
		$this->email->cc( $cc );
		$this->email->bcc( $ccoo );
		$this->email->subject( $asunto );
		$this->email->message( $data );

		if( $this->email->send()){
			return TRUE;
		}else{
			echo $this->email->print_debugger();
		}
	// para depurar y diseñar el email
	// echo $this->email->print_debugger();
	}


	function send_email_bienvenida( $datosUsuario ){

		$ruta_validacion = $this->direction_for_confirm_email( $datosUsuario );

		if( ! empty( $datosUsuario->name )){
			$nombre = $datosUsuario->name;
		}else{
			$nombre = $datosUsuario->username;
		}

		$data = '<h3><b style="font-weight:800;color:#265C96"> Hola!, '.$nombre.'</b></h3>
		<p>Bienvenido a nuestra comunidad.</p>
		<p>Necesitamos validar su Email. Para validar el email soló debes de hacer clic en el botón.</p>

		<a href="'.$ruta_validacion.'"> Validar email </a>
		<p>De ahora en adelante, para realizar cualquier gestión, dirígete a la sección de Mi Cuenta e identifícate con tu cédula o email y tu contraseña. Podrás ver el estado de tu prestamo o cambiar tus datos personales.</p>

		<a href="http://pa.clicredit.com/v0/cuenta" style="font-weight:800;color:#265C96">Ir a mi cuenta</a>
		';
		$this->send_email( $this->email_, $this->web, $datosUsuario->email, '', '', 'Por favor confirme su email',$data );
	}

	function send_email_for_confirm_email( $datosUsuario ){

		$ruta_validacion = $this->direction_for_confirm_email( $datosUsuario );

		if( ! empty( $datosUsuario->name )){
			$nombre = $datosUsuario->name;
		}else{
			$nombre = $datosUsuario->username;
		}

		$data = '<h3 <b style="font-weight:800;color:#265C96"> Gracias '.$nombre.'</b></h3>
		<p>Este es un mensaje para verificar su email.
		<br>Simplemente pinche en este enlace
		<br><a href="'.$ruta_validacion.'" style="padding:9px 19px;background:#5eac25;font-weight 800;color:#fff"> Validar email </a>
		<p>Por favor a continuación compruebe en su pertil de miguelgomezsa.com que se ha validado correctamente. Gracias</p>';

		$this->send_email( $this->email_, $this->web, $datosUsuario->email, '', '', 'Por favor confirme su email',$data );
	}



	function send_email_for_recover_password( $datosUsuario, $nuevaPass ){
		$data = '<h3><b style="font-weight:800;color:#265C96"> Hola!, '.$datosUsuario->name.'</b></h3>
		<p>Hemos recibido una petición de recuperación de contraseña.
		<br>Esta es su nueva contrasea: <b>'.$nuevaPass.'</b>
		<br>No olivde cambiar la contraseña al entrar
		<br>';

		$this->send_email( $this->email_, $this->web, $datosUsuario->email, '', '', 'Restablecimiento de contraseña',$data );
	}


		// Funciones auxiliares

	function direction_for_confirm_email( $datosUsuario ){
		$controlador = 'login';
		return base_url().$controlador.'/validarEmail/'.$datosUsuario->id.'/'.md5($datosUsuario->dateregistered).'/'.md5($datosUsuario->email);
	}



 function email_template($name_user,$content,$footer){
     
    $header = '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <!--<link rel="stylesheet" media="all" href="<?= base_url() ?>css/emailStyles.css">-->

    <style>
 /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
* {
    margin: 0;
    padding: 0;
    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
    color :#000000;
    text-align: justify;
}

img {
    max-width: 100%;
}

body {
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: none;
    width: 100% !important;
    height: 100%;
    line-height: 1.6;
}

table td {
    vertical-align: top;
}

/* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
body {
    background-color: #f6f6f6;
}
h1, h2, h3 {
    font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    color: #000;
    margin: 15px 0 0;
    line-height: 1.2;
    font-weight: 400;
}

h1 {
    font-size: 32px;
    font-weight: 500;
}

h2 {
    font-size: 24px;
}

h3 {
    font-size: 18px !important;
    color:#4872B0 !important;
    font-weight: bold !important;
}

h4 {
    font-size: 14px;
    font-weight: 600;
}

p, ul, ol {
    margin-bottom: 10px;
    font-weight: normal;
}
p li, ul li, ol li {
    margin-left: 5px;
    list-style-position: inside;
}
@media only screen and (min-width: 500px) {
    h1, h2, h3, h4 {
        font-weight: bold !important;
        margin: 20px 0 5px !important;
    }

    h1 {
        font-size: 22px !important;
    }

    h2 {
        font-size: 18px !important;
    }

    h3 {
        color:#265c96 !important;
        font-size: 16px !important;
        font-weight: bold !important;
    }

    .container {
        width: 100% !important;
    }

    .content, .content-wrap {
        padding: 12px !important;

    }

    .invoice {
        width: 100% !important;
    }
  } .text-footer {
   
    font-size: 12px;
    color:#FFFFFF;
    
    
}

.text-blue-bold{
    color: #265c96;
    font-weight: bold;
}

.text-red-bold{
    color: #FF0000;
    font-weight: bold;
}

.text-highlight{
      color:#FFFFFF !important;
      font-size: 14px !important;
}

hr{
    height: 1px;
    border:none;
    background-color: #4872B0;

}

.padding-td{
    padding-bottom: 0px;

}

.padding-td-text{
    padding-top: -50px;
}


tfoot{
    background-color: #4872B0;
}

.content-wrap-tfoot-admin{
    padding: 4px;
}

a.class1 {
    color: #7AAA38 !important;
    font-weight: bold !important;
    text-decoration: underline !important;
}


a.class2 {
     color:#FFFFFF !important;
    font-weight: bold !important;
   
}
</style>
</head>';

$body = '<body>
<table style="background-color: #f6f6f6;
    width: 100%;">
    <tr>
      <td></td>
        <td class="container" style="display: block !important; max-width: 600px !important;
                                     margin: 0 auto !important; clear: both !important;" width="600">
            <div class="content" style="max-width: 600px;
                        margin: 0 auto;
                        display: block;
                        padding: 20px;">
            
               <table style="background: #fff;
                    border: 1px solid #e9e9e9;
                           border-radius: 3px;
                           background-color:#F2F2F2;
                           margin:auto;" width="100%" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td class="content-wrap" style="padding: 17px;">
                            <table  cellpadding="0" cellspacing="0" >
                                <tr style="padding: 0 0 20px;">
                                    <td>
                                       <img src="http://pa.clicredit.com/v0/img/clicredit-logo-email.png">
                                       <br>
                                       <hr style=" height: 1px; border:none; background-color: #4872B0;">
                                    </td>

                                </tr>
                                 
                                <tr style="padding: 0 0 20px;">
                                    <td>
                                        <h3>Hola!, '.$name_user.'</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 0 20px;">
                                        '.$content.'
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 0 20px;">
                                    </td>
                                </tr>
                                
                            </table>
                             </td>
                          </tr>
                            <tfoot style="background-color: #4872B0">
                               <tr>
                                 <td style="padding: 17px;">
                                    '.$footer.'
                                 </td>
                                </tr>
                          </tfoot>
                </table>
            </td>
        <td></td>
    </tr>
</table>

</body>
</html>';

$data = $header.$body;
return $data;

}


function email_recovery_password($datosUsuario,$token){
     $header = '<html lang="es-PA" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="x-apple-disable-message-reformatting">
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <style>
 /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
* {
    margin: 0;
    padding: 0;
    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
    color :#000000;
    text-align: justify;
}

img {
    max-width: 100%;
}

body {
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: none;
    width: 100% !important;
    height: 100%;
    line-height: 1.6;
}

table td {
    vertical-align: top;
}

/* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
body {
    background-color: #f6f6f6;
}
h1, h2, h3 {
    font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    color: #000;
    margin: 15px 0 0;
    line-height: 1.2;
    font-weight: 400;
}

h1 {
    font-size: 32px;
    font-weight: 500;
}

h2 {
    font-size: 24px;
}

h3 {
    font-size: 18px !important;
    color:#4872B0 !important;
    font-weight: bold !important;
}

h4 {
    font-size: 14px;
    font-weight: 600;
}

p, ul, ol {
    margin-bottom: 10px;
    font-weight: normal;
}
p li, ul li, ol li {
    margin-left: 5px;
    list-style-position: inside;
}
@media only screen and (min-width: 500px) {
    h1, h2, h3, h4 {
        font-weight: bold !important;
        margin: 20px 0 5px !important;
    }

    h1 {
        font-size: 22px !important;
    }

    h2 {
        font-size: 18px !important;
    }

    h3 {
        color:#265c96 !important;
        font-size: 16px !important;
        font-weight: bold !important;
    }

    .container {
        width: 100% !important;
    }

    .content, .content-wrap {
        padding: 12px !important;

    }

    

.text-blue-bold{
    color: #265c96;
    font-weight: bold;
}

.text-red-bold{
    color: #FF0000;
    font-weight: bold;
}

hr{
    height: 1px;
    border:none;
    background-color: #4872B0;

}

.padding-td{
    padding-bottom: 0px;

}

.padding-td-text{
    padding-top: -50px;
}


tfoot{
    background-color: #4872B0;
}

.content-wrap-tfoot-admin{
    padding: 4px;
}



</style>
</head>';

$body = '<body>
<table style="background-color: #f6f6f6;
    width: 100%;">
    <tr>
      <td></td>
        <td class="container" style="display: block !important; max-width: 600px !important;
                                     margin: 0 auto !important; clear: both !important;" width="600">
            <div class="content" style="max-width: 600px;
                        margin: 0 auto;
                        display: block;
                        padding: 20px;">
            
               <table style="background: #fff;
                    border: 1px solid #e9e9e9;
                           border-radius: 3px;
                           background-color:#F2F2F2;
                           margin:auto;" width="100%" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td class="content-wrap" style="padding: 17px;">
                            <table  cellpadding="0" cellspacing="0" >
                                <tr style="padding: 0 0 20px;">
                                    <td>
                                       <img src="http://pa.clicredit.com/img/clicredit-logo-email.png">
                                       <br>
                                       <hr style=" height: 1px; border:none; background-color: #4872B0;">
                                    </td>

                                </tr>
                                
                                <tr style="padding: 0 0 20px;">
                                    <td>
                                        <h3>Hola!, '.$datosUsuario->name.'</h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 0 20px;">
                                        Hemos recibido una petición de recuperación de contraseña. Por favor visita el siguiente enlace para cambiar tu contraseña
                                        <br>
                                        <br>
                                        <a style="color: #7AAA38; font-weight: bold; text-decoration: underline;" href="'.base_url().'/acceso/recovery_password/'.$token.'">Cambiar contraseña</a>
                                        <br>
                                        <br>
                                        Si el enlace anterior no funciona copia y pega la siguiente url en tu navegador '.base_url().'/acceso/recovery_password/'.$token.'
                                        <br>
                                        <br>
                                        Los enlaces anteriores expiran en 1 hora
                                        <br>
                                        <br>

                                        <a style="color: #7AAA38; font-weight: bold; text-decoration: underline;" href="'.base_url().'">Clicredit.com</a>, tu plata al instante, donde tú estes.<br><b>#daleclicysalvaeldia</b>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 0 20px;">
                                    </td>
                                </tr>
                                
                            </table>
                             </td>
                          </tr>
                            <tfoot style="background-color: #4872B0">
                               <tr>
                                 <td style="padding: 17px;">
                                    <p style=" font-size: 12px; color:#FFFFFF;">Si tienes dudas o preguntas, contáctanos a:<br> <a style="color:#FFFFFF;font-weight: bold;">ayuda@pa.clicredit.com</a>, escríbenos por <b style="color:#FFFFFF; font-size:14px;">WhatsApp</b> o llámanos al <a style="color:#FFFFFF;font-weight: bold;" href="tel:+5072608951" title="llamar a Clicredit">260-8951</a></p>
                                 </td>
                                </tr>
                          </tfoot>
                </table>
            </td>
        <td></td>
    </tr>
</table>

</body>
</html>';

$data = $header.$body;
$subject = "Restablecimiento de contraseña";
$template = $data;
$from = "info@pa.clicredit.com";
$namefrom = "CliCREDIT Panamá";
$receiver = $datosUsuario->email;
$this->send_Email_Notification($subject,$template,$from,$namefrom,$receiver);

}



  function send_Email_Notification($subject,$template,$from,$namefrom,$receiver){

		
   $config = array(
      'mailtype'  => 'html',
      'charset'   => 'utf-8',
      'newline' => '\r\n',
      'crlf' => '\n'
    //'wordwrap' => TRUE
     );
        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->set_newline("rn");

        $this->email->to($receiver);
        $this->email->from($from, $namefrom);
        $this->email->subject($subject);
        $this->email->message($template);

        //Send email
        if( $this->email->send()){
			return TRUE;
		}else{
			//echo $this->email->print_debugger();
			echo "No envio";
		}
	// para depurar y diseñar el email
	 //  $this->email->clear(TRUE);
	   echo $this->email->print_debugger();
    }



    


 



}

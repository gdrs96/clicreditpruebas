<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
   This class is defined to model Honestidad where some variables are calculated to manage the final result of the different questions
*/

class Honestidad extends CI_Model{ // --- 1.84 trello --- jjy
   /*
      Version:
      03-2018 jjyepez
   */
    private $formulasHabilitadas;

    public function __construct()
    {
      parent::__construct();

      // --- si no está en el array es que no se ha definido fórmula para la pregunta dada
      $this->formulasHabilitadas = [
        "2_13","2_14","2_17","5_1_1","2_20","2_21","3_16","3_22","3_23","4_17"
      ];
	  }

    // --- Obtiene la formula para cada pregunta - si la hay - evita errores
    public function getFormulaVP( $n_pregunta_normalizada ){
      return in_array( $n_pregunta_normalizada, $this->formulasHabilitadas )
              ? 'formula'.$n_pregunta_normalizada
              : 'sinFormula';
    }

    // --- Forma mas generica / reutilizable para VPs basados en lista
    public function formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta ){
      $vp     = -1; // --- valor de VP por defecto
      $id_rsp = $dato_respuesta;
      if( is_numeric( $dato_respuesta ) ){
        $id_rsp = intval( $dato_respuesta );
      }
      // --- simplificada
      if( isset( $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ] ) ) {
        return $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ];
      } else {
        return $id_rsp;
      }
    }

    // --- fallback para cuando no se ha definido la formula para una pregunta dada
    public function sinFormula( $params = Array() ){
      return '#SF#';
    }

    // ---- FORMULAS EN RELACION DIRECTA CON EL EXCEL DEL ALGORITMO ----- //
    // -- pudieran cambiar (optimizarse) o unificarse luego

    public function formula2_13( $dato_respuesta, $listaVPsCompleta ){
      $dato_respuesta_orig = $dato_respuesta;
      $dato_respuesta = is_numeric( $dato_respuesta_orig ) === TRUE ? 1 : 2; // 1:80 .. 2:30 ... se traen de la bbdd

      $n_pregunta_normalizada = '2_13';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_14( $dato_respuesta, $listaVPsCompleta ){
    	$n_pregunta_normalizada = '2_14';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_17( $dato_respuesta, $listaVPsCompleta ){
      $dato_respuesta_orig = $dato_respuesta;
      $dato_respuesta = is_numeric( $dato_respuesta_orig ) === TRUE ? 1 : 2; // 1:90 .. 2:60 ... se traen de la bbdd

      $n_pregunta_normalizada = '2_17';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula5_1_1( $dato_respuesta, $listaVPsCompleta, $infoCliente ){
      $vp = -1; // --- valor de VP por defecto

      $nombreCompleto = $infoCliente['name']
      .' '.$infoCliente['segundo_nombre']
      .' '.$infoCliente['primer_apellido']
      .' '.$infoCliente['segundo_apellido'];
      
      $nombreCompleto = trim( str_replace( '  ', ' ', replaceAccents($nombreCompleto) ) );

      $dato_respuesta = trim( str_replace( '  ', ' ', replaceAccents($dato_respuesta) ) );

      $dato_respuesta_orig = $nombreCompleto;
      $dato_respuesta = strtoupper( $nombreCompleto ) === strtoupper( $dato_respuesta ) ? 1 : 0; // 1:75 .. 0:1 ... se traen de la bbdd

      $n_pregunta_normalizada = '5_1_1';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_20( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_20';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_21( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_21';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_16( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_16';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_22( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_22';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_23( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_23';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula4_17( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '4_17';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }
}

// source - https://stackoverflow.com/questions/10054818/convert-accented-characters-to-their-plain-ascii-equivalents
function replaceAccents($str) {
  $search = explode(",","ç,æ,œ,á,é,í,ó,ú,à,è,ì,ò,ù,ä,ë,ï,ö,ü,ÿ,â,ê,î,ô,û,å,ø,Ø,Å,Á,À,Â,Ä,È,É,Ê,Ë,Í,Î,Ï,Ì,Ò,Ó,Ô,Ö,Ú,Ù,Û,Ü,Ÿ,Ç,Æ,Œ");
  $replace = explode(",","c,ae,oe,a,e,i,o,u,a,e,i,o,u,a,e,i,o,u,y,a,e,i,o,u,a,o,O,A,A,A,A,A,E,E,E,E,I,I,I,I,O,O,O,O,U,U,U,U,Y,C,AE,OE");
  return str_replace($search, $replace, $str);
}
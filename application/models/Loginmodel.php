<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Loginmodel extends CI_Model {

	function __construct()
	{
	parent::__construct();
	}

	function make_login( $email ){
		$usuario = $this->db->get_where('user', array('email' => $email));
		if( $usuario->num_rows()==1){
			return $usuario->row();
		}
		if( $usuario->num_rows()>1){
			//TODO : notificar usuario duplicado
			return FALSE;
		}
	}

	function get_user_by_id( $user_id ){
		$usuario = $this->db->get_where('user', array('id' => $user_id));

		if($usuario->num_rows()==1){
			return $usuario->row();
		}
		if($usuario->num_rows()>1){
			//TODO : notificar usuario duplicado
			return FALSE;
		}
	}

	function add_usuario( $username, $password , $email){

		if($this->db->query('INSERT INTO `users` (`username`, `password`, `email`, `dateRegister`) VALUES (?,?,?,?)', array($username, md5($password), $email, mdate( '%Y/%m/%d', time()) ))){
			return $this->db->insert_id();
			 // devuelve el id
		}
		else{
			return FALSE;
		}
	}

	function check_email( $email ){

		foreach($this->db->get('users')->result() as $fila){
			if( $email == $fila->email){
				return FALSE;
				break;
			}
		}
		return TRUE;
	}

	function get_usuario( $username ){

		$usuarios = $this->db->query('SELECT * FROM `users` WHERE username=?', array($username));

		if($usuarios->num_rows()==1){
			return $usuarios->row();
		}
		if($usuarios->num_rows()>1){
			//TODO : notificar usuario duplicado
			return FALSE;
		}
	}

	function get_user_by_email( $email ){

		$usuarios = $this->db->query('SELECT * FROM `users` WHERE email=?', array($email));

		if($usuarios->num_rows()==1){
			return $usuarios->row();
		}
		if($usuarios->num_rows()>1){
			//TODO : notificar usuario duplicado
			return FALSE;
		}
	}

	function change_password( $pass, $user_id ){
		$pass = "'$pass'";
		$this->db->set('password', $pass, FALSE);
		$this->db->where('id', $user_id);
		if ($this->db->update('users')){
			return TRUE;
		}
		else{
			return FALSE;
		}

	}

	function save_datelastlogin( $user_id ){
		$time =  mdate( '%Y-%m-%d %H:%i:%s', time() );
		$datos_a_actualizar = array('datelastlogin' => $time );
		$str = $this->db->update_string('user', $datos_a_actualizar, array('id' => $user_id ));
		$this->db->query( $str );
	}

// hacemos un conteo de los mensajes al loguear, y de futuras notificaciones
	function count_message( $user_id ){
		$this->db->where( array('id_receiver' => $user_id, 'viewed' => '0' ));
		$this->db->from('talk');
		return $this->db->count_all_results();
	}



}
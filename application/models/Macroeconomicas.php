<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
   This class is defined to model Macroeconomicas where some variables are calculated to manage the final result of the different questions
*/

class Macroeconomicas extends CI_Model{ // --- 1.84 trello --- jjy
   /*
      Version:
      03-2018 jjyepez
   */
    private $formulasHabilitadas;

    public function __construct()
    {
      parent::__construct();

      // --- si no está en el array es que no se ha definido fórmula para la pregunta dada
      $this->formulasHabilitadas = [
        "2_17", "3_1", "3_9", "CC5", "CC8", "CC9", "CC10", "Dato", "CC11","4_4"
        // --- Pendiente: n

        // n pasa directo n_vp

        // --- ATENCION con las preguntas que tienen predefinido vp ... como la 4_7_vp .. no pasan por acá
        //     su valor VP pasa directo desde prestamos->datos
      ];
	  }

    // --- Obtiene la formula para cada pregunta - si la hay - evita errores
    public function getFormulaVP( $n_pregunta_normalizada ){
      return in_array( $n_pregunta_normalizada, $this->formulasHabilitadas )
              ? 'formula'.$n_pregunta_normalizada
              : 'sinFormula';
    }

    // --- Forma mas generica / reutilizable para VPs basados en lista
    public function formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta ){
      $vp     = -1; // --- valor de VP por defecto
      $id_rsp = $dato_respuesta;
      if( is_numeric( $dato_respuesta ) ){
        $id_rsp = intval( $dato_respuesta );
      }
      // --- simplificada
      if( isset( $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ] ) ) {
        return $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ];
      } else {
        return $id_rsp;
      }
    }

    // --- fallback para cuando no se ha definido la formula para una pregunta dada
    public function sinFormula( $params = Array() ){
      return '#SF#';
    }

    // ---- FORMULAS EN RELACION DIRECTA CON EL EXCEL DEL ALGORITMO ----- //
    // -- pudieran cambiar (optimizarse) o unificarse luego

    public function formulaCC8( $_, $listaVPsCompleta, $_, $_, $_, $adminVars ){
      $datos = $adminVars[ 'CC8' ]."&nbsp;%"; // PIB
      $dato_respuesta = $datos <  -1                ? 1 : (
                        $datos >= -1 && $datos <= 0 ? 2 : (
                        $datos >=  0 && $datos <= 2 ? 3 : (
                        $datos >=  2 && $datos <= 6 ? 4 : (
                        $datos >=  6                ? 5 : -1
                      ))));

      $n_pregunta_normalizada = 'CC8';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => "",
        "datos"     => $datos,
        "vp"        => $vp
      ];
    }

    public function formulaCC9( $_, $listaVPsCompleta, $_, $_, $_, $adminVars ){
      $datos = $adminVars[ 'CC9' ]."&nbsp;%"; // Desempleo
      $dato_respuesta = $datos <  -5                ? 1 : (
                        $datos >= -5 && $datos < -1 ? 2 : (
                        $datos >= -1 && $datos <= 0 ? 3 : (
                        $datos >   0 && $datos <= 2 ? 4 : (
                        $datos >   2 && $datos <= 5 ? 5 : (
                        $datos >   5                ? 6 : -1
                      )))));

      $n_pregunta_normalizada = 'CC9';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => "",
        "datos"     => $datos,
        "vp"        => $vp
      ];
    }

    public function formulaCC10( $_, $listaVPsCompleta, $_, $_, $_, $adminVars ){
      $datos = $adminVars[ 'CC10' ]."&nbsp;%"; // IPC
      $dato_respuesta = $datos <  -2                ? 1 : (
                        $datos >= -2 && $datos <  0 ? 2 : (
                        $datos >=  0 && $datos <= 1 ? 3 : (
                        $datos >   1 && $datos <= 2 ? 4 : (
                        $datos >   2 && $datos <= 6 ? 5 : (
                        $datos >   6                ? 6 : -1
                      )))));

      $n_pregunta_normalizada = 'CC10';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => "",
        "datos"     => $datos,
        "vp"        => $vp
      ];
    }

    public function formulaCC11( $_, $_, $_, $_, $_, $adminVars ){
      $datos = $adminVars[ 'CC11' ]; // Mínimo requerido
      $vp    = "solo-mostrar";
      return [
        "respuesta" => "",
        "datos"     => $datos,
        "vp"        => $vp
      ];
    }

    public function formulaDato( $_, $_, $_, $_, $_, $adminVars ){
      $datos = $adminVars[ 'Dato' ]; // IPC
      $vp    = "solo-mostrar";
      return [
        "respuesta" => "",
        "datos"     => $datos,
        "vp"        => $vp
      ];
    }

    public function formulaCC5( $_, $listaVPsCompleta, $_, $_, $todasLasRespuestas, $datosMacroeconomicos ){
      // IMPORTANTE: El dato de respuesta 3_10 ha sido cambiado de Lista a texto libre
      if( isset( $todasLasRespuestas[ '3_10' ] )){ // es requerido
        $dato_respuesta_orig = $todasLasRespuestas[ '3_10' ];    // --- Ingresos mensuales
        $salario_promedio    = $datosMacroeconomicos[ 'Dato' ]; // DatosMacroeconomicos / excel GV Macroeconomicas F175
        $cc                  = $dato_respuesta_orig - $salario_promedio;

        $dato_respuesta = $cc <  -200               ? 1 : (
                          $cc >= -200 && $cc < -100 ? 2 : (
                          $cc >= -100 && $cc <  0   ? 3 : (
                          $cc >=  0   && $cc <  100 ? 4 : (
                          $cc >=  100 && $cc <= 200 ? 5 : (
                          $cc >   200               ? 6 : -1
                          )))));
        
        $n_pregunta_normalizada = 'CC5';
        $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      } else {
        $dato_respuesta_orig = '';
        $cc = '';
        $vp = -1;
      }
      return [ 
        // --- "datos"     => $salario_promedio,
        "cc"        => $cc,
        "respuesta" => $dato_respuesta_orig,
        "vp"        => $vp,
      ];
    }

    public function formula2_17( $dato_respuesta, $listaVPsCompleta ){
      $dato_respuesta_orig = $dato_respuesta;
      $dato_respuesta = is_numeric( $dato_respuesta_orig ) === TRUE ? 1 : 2;

      $n_pregunta_normalizada = '2_17';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_9( $dato_respuesta, $listaVPsCompleta ){
      $dato_respuesta_orig = $dato_respuesta;
      $dato_respuesta = is_numeric( $dato_respuesta_orig ) === TRUE ? 1 : 2;

      $n_pregunta_normalizada = '3_9';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula4_4( $dato_respuesta, $listaVPsCompleta, $_, $_, $todasLasRespuestas ){
      $n_pregunta_normalizada = '4_4';
      $dato_respuesta_orig = $todasLasRespuestas[ '4_4' ]; // con el prefijo original ... S_ C_ D_ etc
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta_orig, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_1( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_1';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }
}

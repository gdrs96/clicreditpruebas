<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
   This class is defined to model PerfilSocial where some variables are calculated to manage the final result of the different questions
*/

class PerfilSocial extends CI_Model{ // --- 1.84 trello --- jjy
   /*
      Version:
      04-2018 jjyepez
   */
    private $formulasHabilitadas;

    public function __construct()
    {
      parent::__construct();

      // --- si no está en el array es que no se ha definido fórmula para la pregunta dada
      $this->formulasHabilitadas = [
      	"CC3","2_6_2","2_6_3","2_9_1","3_4","3_2","3_8","3_6","4_2","4_3","4_4",
        // --- Pendiente: n

        // n pasa directo n_vp // por lo tanto no se aplica una formula
		    "2_18","2_18_1","2_18_2","2_6","2_6_1","2_1","2_5","2_7","2_15","2_16","2_8","2_9","3_14","3_1","4_7","4_10","CC6"

        // --- ATENCION con las preguntas que tienen predefinido vp ... como la 4_7_vp .. no pasan por acá
        //     su valor VP pasa directo desde prestamos->datos
      ];
	  }

    // --- Obtiene la formula para cada pregunta - si la hay - evita errores
    public function getFormulaVP( $n_pregunta_normalizada ){
      return in_array( $n_pregunta_normalizada, $this->formulasHabilitadas )
              ? 'formula'.$n_pregunta_normalizada
              : 'sinFormula';
    }

    // --- Forma mas generica / reutilizable para VPs basados en lista
    public function formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta ){
      $vp     = -1; // --- valor de VP por defecto
      $id_rsp = $dato_respuesta;
      if( is_numeric( $dato_respuesta ) ){
        $id_rsp = intval( $dato_respuesta );
      }
      // --- simplificada
      if( isset( $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ] ) ) {
        return $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ];
      } else {
        return $id_rsp;
      }
    }

    // --- fallback para cuando no se ha definido la formula para una pregunta dada
    public function sinFormula( $params = Array() ){
      return '#SF#';
    }

    // ---- FORMULAS EN RELACION DIRECTA CON EL EXCEL DEL ALGORITMO ----- //
    // -- pudieran cambiar (optimizarse) o unificarse luego

    public function formulaCC3( $_, $listaVPsCompleta, $infoCliente, $_, $todasLasRespuestas ){
      $hoy       = gmdate('d/m/Y'); // --- Hoy

      $fecha_nacimiento = $infoCliente[ 'fecha_nacimiento' ];
      $fecha            = date_create( $fecha_nacimiento );
      $fecha_formato    = date_format( $fecha, "d/m/Y" ); // --- se le da el formato estandar

      $difDias = (float) strDMYdiff( $fecha_formato, $hoy );

      // se necesita la diferencia en años (con decimales si es necesario ...) dias diff / 365
      $cc = round( $difDias / 365, 2 );
      $dato_respuesta = $cc <  18             ? 1 : (
                        $cc >= 18 && $cc < 21 ? 2 : (
                        $cc >= 21 && $cc < 28 ? 3 : (
                        $cc >= 28 && $cc < 35 ? 4 : (
                        $cc >= 35 && $cc < 60 ? 5 : (
                        $cc >  60             ? 6 : -1
                        ))))); // el else : es innecesario acá pero se deja para respetar la lógica del excel

      $n_pregunta_normalizada = 'CC3';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => $fecha_formato,
        "cc"        => $cc,
        "vp"        => $vp
      ];
    }

    public function formula2_18( $dato_respuesta, $listaVPsCompleta ){
      // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_18';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_18_1( $dato_respuesta, $listaVPsCompleta ){
      // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_18_1';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_18_2( $dato_respuesta, $listaVPsCompleta ){
      // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_18_2';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_6( $dato_respuesta, $listaVPsCompleta ){
      // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_6';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_6_1( $dato_respuesta, $listaVPsCompleta ){
      // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_6_1';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_6_2( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_6_2';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

	public function formula2_6_3( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_6_3';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

	public function formula2_1( $dato_respuesta, $listaVPsCompleta ){
	  // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_1';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

	public function formula2_5( $dato_respuesta, $listaVPsCompleta ){
      // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_5';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

	public function formula2_7( $dato_respuesta, $listaVPsCompleta ){
	  // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_7';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_15( $dato_respuesta, $listaVPsCompleta ){
      // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_15';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

	  public function formula2_16( $dato_respuesta, $listaVPsCompleta ){
	  // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_16';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }
	
	  public function formula2_8( $dato_respuesta, $listaVPsCompleta ){
	  // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_8';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_9( $dato_respuesta, $listaVPsCompleta ){
	  // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_9';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_9_1( $dato_respuesta, $listaVPsCompleta ){
	  // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '2_9_1';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_14( $dato_respuesta, $listaVPsCompleta ){
	  // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '3_14';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_1( $dato_respuesta, $listaVPsCompleta ){
	  // --- tiene prefijo vp en los datos
      $n_pregunta_normalizada = '3_1';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

	  public function formula3_4( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_4';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_2( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_2';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_8( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_8';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

	  public function formula3_6( $dato_respuesta, $listaVPsCompleta, $infoCliente, $_, $todasLasRespuestas ){
      $hoy       = gmdate('d/m/Y'); // --- Hoy

      $fecha_antiguedad = $dato_respuesta;

      $difDias = (float) strDMYdiff( $fecha_antiguedad, $hoy );

      // se necesita la diferencia en años (con decimales si es necesario ...) dias diff / 365
      $cc = round( $difDias / 365, 2 );
      $dato_respuesta = $cc <  1               ? 1 : (
                        $cc >= 1  && $cc <  2  ? 2 : (
                        $cc >= 2  && $cc <  5  ? 3 : (
                        $cc >= 5  && $cc <= 10 ? 4 : (
                        $cc >  10              ? 5 : -1
                        )))); // el else : es innecesario acá pero se deja para respetar la lógica del excel

      $n_pregunta_normalizada = '3_6';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => $fecha_antiguedad,
        "cc"        => $cc,
        "vp"        => $vp
      ];
    }

    public function formula4_2( $dato_respuesta, $listaVPsCompleta, $_, $_, $todasLasRespuestas ){
      // caso particular para distrito / corregimiento / sector! .. muy raro! ver excel!
      $dato_respuesta4_2 = $todasLasRespuestas[ '4_2' ];
      $dato_respuesta4_3 = $todasLasRespuestas[ '4_3' ]; // tiene prefijo C_
      $dato_respuesta4_4 = $todasLasRespuestas[ '4_4' ]; // tiene prefijo S_

      // --- VS es la suma de los 3 vp de 4_2, 4_3 y 4_4
      $n_pregunta_normalizada = '4_2';
      $vp4_2 = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta4_2, $listaVPsCompleta );
      $n_pregunta_normalizada = '4_3';
      $vp4_3 = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta4_3, $listaVPsCompleta );
      $n_pregunta_normalizada = '4_4';
      $vp4_4 = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta4_4, $listaVPsCompleta );
	  
	  // --- VS es la suma de los 3 vp de 4_2, 4_3 y 4_4
      $vs = $vp4_2 + $vp4_3 + $vp4_4;
      if($vp4_2==0){
        $vp4_2 = "solo-mostrar";
      }
      return [
      	"vs4_2" => $vs, // no es lo mejor pero este caso es demasiado particular!
        "vp4_2" => $vp4_2,
      	"vp"    => $vp4_2
      ];
    }

    /*public function formula4_3( $dato_respuesta, $listaVPsCompleta ){ 
      // --- VS es la suma de los 3 vp de 4_2, 4_3 y 4_4
      $n_pregunta_normalizada = '4_3';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }*/
    public function formula4_3( $dato_respuesta, $listaVPsCompleta, $_, $_, $todasLasRespuestas ){
      $n_pregunta_normalizada = '4_3';
      $dato_respuesta_orig = $todasLasRespuestas[ '4_3' ]; // con el prefijo original ... S_ C_ D_ etc
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta_orig, $listaVPsCompleta );
       if($vp==0){
        $vp = "solo-mostrar";
      }
      return[
        "vp" => $vp,
        "vp4_3" => $vp
        ];
    }
   

   public function formula4_4( $dato_respuesta, $listaVPsCompleta, $_, $_, $todasLasRespuestas ){
      $n_pregunta_normalizada = '4_4';
      $dato_respuesta_orig = $todasLasRespuestas[ '4_4' ]; // con el prefijo original ... S_ C_ D_ etc
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta_orig, $listaVPsCompleta );
      return[
        "vp" => $vp,
        "vp4_4" => $vp
        ];
    }

   /* public function formula4_4( $dato_respuesta, $listaVPsCompleta ){
      // --- VS es la suma de los 3 vp de 4_2, 4_3 y 4_4
      $n_pregunta_normalizada = '4_4';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }*/

	  public function formula4_7( $dato_respuesta, $listaVPsCompleta, $infoCliente, $_, $todasLasRespuestas ){
      $hoy       = gmdate('d/m/Y'); // --- Hoy
      // $fecha            = date_create($dato_respuesta);
       $fecha_antiguedad = $dato_respuesta;
       
       $difDias = (float) strDMYdiff( $fecha_antiguedad, $hoy );

      // se necesita la diferencia en años (con decimales si es necesario ...) dias diff / 365
       $cc = round( $difDias / 365, 2 );
       $dato_respuesta = $cc < 1                ? 1 : (
                         $cc >= 1 && $cc < 2    ? 2 : (
                         $cc >= 2 && $cc < 5    ? 3 : (
                         $cc >= 5 && $cc <= 10  ? 4 : (
                         $cc > 10               ? 5 : -1
                        ))));
      $cc = round( $difDias / 365, 2 );
      $n_pregunta_normalizada = '4_7'; // no se calcula porque tiene vp fijo en los datos
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => $fecha_antiguedad,
        "cc"        => $cc,
        "vp"        => $vp
      ];
    }

    public function formula4_10( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '4_10';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formulaCC6( $_, $listaVPsCompleta, $_, $_, $todasLasRespuestas, $datosMacroeconomicos ){
      if( isset($todasLasRespuestas[ '2_2' ]) ){  // son requeridos
        $arr_IDH = json_decode( $datosMacroeconomicos['IDH'], true );
        
        $dato_respuesta = $todasLasRespuestas['2_2'];

        $respuesta = $arr_IDH[ $dato_respuesta ];
        $n_pregunta_normalizada = 'CC6';
        $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );


      } else {
        $respuesta = '';
        $vp = -1;
      }
      return [
        'respuesta' => $respuesta,
        'vp' => $vp
      ];
    }

}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
   This class is defined to model Responsabilidad where some variables are calculated to manage the final result of the different questions
*/

class Responsabilidad extends CI_Model{ // --- 1.84 trello --- jjy
   /*
      Version:
      03-2018 jjyepez
   */
    private $formulasHabilitadas;

    public function __construct()
    {
      parent::__construct();

      // --- si no está en el array es que no se ha definido fórmula para la pregunta dada
      $this->formulasHabilitadas = [
        "2_11","3_12","3_15_2","2_19","2_22","2_23","3_18","3_19","4_14","4_15","4_16","3_4",
        "3_5", // cc
        "3_6", // cc
        "2_1","2_5","2_7","4_7",
      ];
	  }

    // --- Obtiene la formula para cada pregunta - si la hay - evita errores
    public function getFormulaVP( $n_pregunta_normalizada ){
      return in_array( $n_pregunta_normalizada, $this->formulasHabilitadas )
              ? 'formula'.$n_pregunta_normalizada
              : 'sinFormula';
    }

    // --- Forma mas generica / reutilizable para VPs basados en lista
    public function formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta ){
      $vp     = -1; // --- valor de VP por defecto
      $id_rsp = $dato_respuesta;
      if( is_numeric( $dato_respuesta ) ){
        $id_rsp = intval( $dato_respuesta );
      }
      // --- simplificada
      if( isset( $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ] ) ) {
        return $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ];
      } else {
        return $id_rsp;
      }
    }

    // --- fallback para cuando no se ha definido la formula para una pregunta dada
    public function sinFormula( $params = Array() ){
      return '#SF#';
    }

    // ---- FORMULAS EN RELACION DIRECTA CON EL EXCEL DEL ALGORITMO ----- //
    // -- pudieran cambiar (optimizarse) o unificarse luego

    public function formula2_1( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_1';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_5( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_5';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_7( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_7';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_11( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_11';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_12( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_12';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_15_2( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_15_2';
      // --- OJO valores en bbdd no son identicos al excel para respuesta 1 -- 70 vs 95 --- jjy
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_19( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_19';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_22( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_22';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_23( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_23';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_18( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_18';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_19( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_19';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_4( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_4';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_5( $dato_respuesta, $listaVPsCompleta, $infoCliente, $infoSimu ){
      $n_pregunta_normalizada = '3_5';

      $cc = -1;
      $cc = floatval( $infoSimu['cantidad'] ) - floatval( $dato_respuesta );
      $dato_respuesta = $cc <  0                 ? 1 : (
                        $cc == 0                 ? 2 : ( // OJO ... en el excel dice >= ... pero la lógica indica que debe ser sólo ==
                        $cc >  0   && $cc <= 100 ? 3 : (
                        $cc >  100 && $cc <= 200 ? 4 : (
                        $cc >  200               ? 5 : -1
                        )))); // el else : es innecesario acá pero se deja para respetar la lógica del excel

      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );

      return [ "vp" => $vp, "cc" => $cc ]; // devuelve mas de un valor ... posibles: vp, cc y datos ... vp es obligatorio
    }

   public function formula3_6( $dato_respuesta, $listaVPsCompleta, $infoCliente, $_, $todasLasRespuestas ){
      $hoy       = gmdate('d/m/Y'); // --- Hoy

      $fecha_antiguedad = $dato_respuesta;

      $difDias = (float) strDMYdiff( $fecha_antiguedad, $hoy );

      // se necesita la diferencia en años (con decimales si es necesario ...) dias diff / 365
      $cc = round( $difDias / 365, 2 );
      $dato_respuesta = $cc <  1               ? 1 : (
                        $cc >= 1  && $cc <  2  ? 2 : (
                        $cc >= 2  && $cc <  5  ? 3 : (
                        $cc >= 5  && $cc <= 10 ? 4 : (
                        $cc >  10              ? 5 : -1
                        )))); // el else : es innecesario acá pero se deja para respetar la lógica del excel

      $n_pregunta_normalizada = '3_6';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => $fecha_antiguedad,
        "cc"        => $cc,
        "vp"        => $vp
      ];
    }


    public function formula4_14( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '4_14';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula4_15( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '4_15';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula4_16( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '4_16';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula4_7( $dato_respuesta, $listaVPsCompleta, $infoCliente, $_, $todasLasRespuestas ){
      $hoy       = gmdate('d/m/Y'); // --- Hoy
      // $fecha            = date_create($dato_respuesta);
       $fecha_antiguedad = $dato_respuesta;
       
       $difDias = (float) strDMYdiff( $fecha_antiguedad, $hoy );

      // se necesita la diferencia en años (con decimales si es necesario ...) dias diff / 365
       $cc = round( $difDias / 365, 2 );
       $dato_respuesta = $cc < 1                ? 1 : (
                         $cc >= 1 && $cc < 2    ? 2 : (
                         $cc >= 2 && $cc < 5    ? 3 : (
                         $cc >= 5 && $cc <= 10  ? 4 : (
                         $cc > 10               ? 5 : -1
                        ))));
      $cc = round( $difDias / 365, 2 );
      $n_pregunta_normalizada = '4_7'; // no se calcula porque tiene vp fijo en los datos
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return [
        "respuesta" => $fecha_antiguedad,
        "cc"        => $cc,
        "vp"        => $vp
      ];
    }
}
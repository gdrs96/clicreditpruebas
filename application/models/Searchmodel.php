<?php defined('BASEPATH') OR exit('bye bye');

class Searchmodel extends CI_Model {

	function __construct(){
		parent::__construct();
	}
	function get_all_values_or_like_order_pag($table,$columns,$search,$order,$way,$limit,$offset=0){
		if(empty($offset))$offset=0;
		foreach ($search as $text) {
			foreach ($columns as $column) {
				$query = $this->db->or_like($column, $text, 'both');
			}
		}
		$query = $this->db->order_by($order,$way);
		$query = $this->db->limit($limit,$offset);
		if($query = $this->db->get($table)){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function get_all_values_like_order_pag($table,$columns,$search,$order,$way,$limit,$offset=0){
		if(empty($offset))$offset=0;
		foreach ($columns as $column) {
			$query = $this->db->or_like($column, $search, 'both');
		}
		$query = $this->db->order_by($order,$way);
		$query = $this->db->limit($limit,$offset);
		if($query = $this->db->get($table)){
			return $query->result();
		}else{
			return FALSE;
		}
	}

	function get_query($sql){
		if(empty($sql))
			return FALSE;
		if($query = $this->db->query($sql)){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	function total_get_query($sql){
		if(empty($sql))
			return FALSE;
		if($query = $this->db->query($sql)){
			return $query->num_rows();
		}else{
			return FALSE;
		}
	}
	function total_where($table,$where){
		if(empty($table) || empty($where))
			return FALSE;
		if($query = $this->db->get_where($table,$where)){
			return $query->num_rows();
		}else{
			return FALSE;
		}
	}
	function total_or_where($table,$where,$or_where){
		$query = $this->db->where($where);
		$query = $this->db->or_where($or_where);
		if($query = $this->db->get($table,$where)){
			return $query->num_rows();
		}else{
			return FALSE;
		}
	}
}
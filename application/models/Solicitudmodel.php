<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
	Esta clase contiene métodos para extraer los datos de la solicitud desde la base de datos
*/
class Solicitudmodel extends CI_Model{
	/*
		Version:
		15-02-2018 @jjyepez: New
	*/
	
	/*
		This method get the information from the database and decode from json
	*/

	public function GetOpcionesPregunta( $id_pregunta )
	{
		/*
			Version:
			15-02-2018 @jjyepez: New // --- experimental beta
		*/	
		$sql =<<<EOS
			SELECT
				id_cliente,
				datos
			FROM
				prestamos
			WHERE
				id_cliente = "{$id_cliente}";
EOS;
		$datos = $this->Thememodel->get_query( $sql );
		return json_decode($datos, true);
	}
	 
}

<?php defined('BASEPATH') OR exit('bye bye');

class Thememodel extends CI_Model {

	function __construct(){
		parent::__construct();
		$this->db->reset_query();
		//$this->db->get_compiled_select()
	}
	function get_all_values($table){
		return $this->db->get($table)->result();
	}
	function get_value($table,$where){
		return $this->db->get_where($table,$where)->row();
	}
	function get_all_values_where($table,$where){
		return $this->db->get_where($table,$where)->result();
	}
	function get_all_values_order($table,$order,$way){
		$query = $this->db->order_by($order,$way);
		$query = $this->db->get($table);
		if($result = $query->result()){
			return $result;
		}else{
			return FALSE;
		}
	}
	function get_all_values_where_order($table,$where,$order,$way){
		$query = $this->db->order_by($order,$way);
		$query = $this->db->get_where($table,$where);
		if($result = $query->result()){
			return $result;
		}else{
			return FALSE;
		}
	}
	function get_select($columns,$table,$where){
		$query = $this->db->select($columns);
		if($query = $this->db->get_where($table,$where)){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	function insert_value($table,$data){
		if(empty($table) || empty($data))
			return FALSE;
		if($this->db->insert($table,$data)){
			return $this->db->insert_id();
		}else{
			return FALSE;
		}
	}
	function save_value($table,$data,$where){
		if(empty($table) || empty($data))
			return FALSE;
		if($this->db->update($table,$data,$where)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	//Metodo creado para la actualizacion de campos en las tablas de la base de datos
	function update_value($table,$data,$where)
	{
		if(empty($table) || empty($data))
			return FALSE;
		if($this->db->update($table,$data,$where)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	function del_value($table,$where){
		if(empty($table))
			return FALSE;
		$this->db->delete($table,$where);
		if( $this->db->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	function get_all_values_pag($table,$limit,$offset=0){
		if($query = $this->db->get($table,$limit,$offset))
			return $query->result();
		else
			return FALSE;
	}
	function get_all_values_where_pag($table,$where,$limit,$offset=0){
		if($query = $this->db->get_where($table,$where,$limit,$offset)){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	function get_all_values_order_pag($table,$order,$way,$limit,$offset=0){
		$query = $this->db->order_by($order,$way);
		if($query = $this->db->get($table,$limit,$offset)){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	function get_all_values_where_order_pag($table,$where,$order,$way,$limit,$offset=0){
		$query = $this->db->order_by($order,$way);
		if($query = $this->db->get_where($table,$where,$limit,$offset)){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	function get_all_values_orwhere_order_pag($table,$where,$or_where,$order,$way,$limit,$offset=0){
		$query = $this->db->where($where);
		$query = $this->db->or_where($or_where);
		$query = $this->db->order_by($order,$way);
		$query = $this->db->limit($limit,$offset);
		if($query = $this->db->get($table)){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	function get_all_values_leftjoin_order_pag($columns,$table,$join,$order,$way,$limit,$offset=0){
		$query = $this->db->select($columns);
		foreach($join as $key){
			$query = $this->db->join($key['table'],$key['join'],'left');
		}
		$query = $this->db->order_by($order,$way);
		$query = $this->db->limit($limit,$offset);
		if($query = $this->db->get($table)){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	function get_all_values_leftjoin_where_order_pag($columns,$table,$join,$where,$order,$way,$limit,$offset=0){
		$query = $this->db->select($columns);
		foreach($join as $key){
			$query = $this->db->join($key['table'],$key['join'],'left');
		}
		$query = $this->db->order_by($order,$way);
		$query = $this->db->limit($limit,$offset);
		if($query = $this->db->get_where($table,$where)){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	function get_all_values_leftjoin_where_where_in_order_pag($columns,$table,$join,$where,$where_in,$where_ids,$order,$way,$limit,$offset=0){
		$query = $this->db->select($columns);
		foreach($join as $key){
			$query = $this->db->join($key['table'],$key['join'],'left');
		}
		$query = $this->db->order_by($order,$way);
		$query = $this->db->limit($limit,$offset);
		$query = $this->db->where($where);
		$query = $this->db->where_in($where_in,$where_ids);
		if($query = $this->db->get($table)){
			return $query->result();
		}else{
			return FALSE;
		}
	}
	function get_query($sql){
		return $this->db->query($sql)->result();
	}
	function total_get_query($sql){
		return $this->db->query($sql)->num_rows();
	}
	function total($table){
		return $this->db->count_all($table);
	}
	function total_where($table,$where){
		return $this->db->where($where)->count_all_results($table);
	}
	function total_where_where_in($table,$where,$where_in,$where_ids){
		return $this->db->where($where)->where_in($where_in,$where_ids)->count_all_results($table);
	}
	function total_or_where($table,$where,$or_where){
		return $this->db->where($where)->or_where($or_where)->get($table,$where)->num_rows();
	}

	function execute_sql($sql){
	$query = $this->db->query($sql);
	 //return  $this->db->get();
		if( $this->db->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}

	}
}
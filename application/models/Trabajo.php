<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
   This class is defined to model Trabajo where some variables are calculated to manage the final result of the different questions
*/

class Trabajo extends CI_Model{ // --- 1.84 trello --- jjy
   /*
      Version:
      03-2018 jjyepez
   */
    private $formulasHabilitadas;

    public function __construct()
    {
      parent::__construct();

      // --- si no está en el array es que no se ha definido fórmula para la pregunta dada
      $this->formulasHabilitadas = [
        "3_2","3_8","3_9","2_24","2_25","2_26","3_17","3_20","3_21","4_11","4_12","4_13",
        "CC5","3_4"

        // --- ATENCION con las preguntas que tienen predefinido vp ... como la 4_7_vp .. no pasan por acá
        //     su valor VP pasa directo desde prestamos->datos
      ];
	  }

    // --- Obtiene la formula para cada pregunta - si la hay - evita errores
    public function getFormulaVP( $n_pregunta_normalizada ){
      return in_array( $n_pregunta_normalizada, $this->formulasHabilitadas )
              ? 'formula'.$n_pregunta_normalizada
              : 'sinFormula';
    }

    // --- Forma mas generica / reutilizable para VPs basados en lista
    public function formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta ){
      $vp     = -1; // --- valor de VP por defecto
      $id_rsp = $dato_respuesta;
      if( is_numeric( $dato_respuesta ) ){
        $id_rsp = intval( $dato_respuesta );
      }
      // --- simplificada
      if( isset( $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ] ) ) {
        return $listaVPsCompleta[ $n_pregunta_normalizada ][ $id_rsp ];
      } else {
        return $id_rsp;
      }
    }

    // --- fallback para cuando no se ha definido la formula para una pregunta dada
    public function sinFormula( $params = Array() ){
      return '#SF#';
    }

    // ---- FORMULAS EN RELACION DIRECTA CON EL EXCEL DEL ALGORITMO ----- //
    // -- pudieran cambiar (optimizarse) o unificarse luego

    public function formula3_2( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_2';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_4( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_4';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_8( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_8';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_9( $dato_respuesta, $listaVPsCompleta ){
      $dato_respuesta_orig = $dato_respuesta;
      $dato_respuesta = is_numeric( $dato_respuesta_orig ) === TRUE ? 1 : 2;

      $n_pregunta_normalizada = '3_9';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_24( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_24';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_25( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_25';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula2_26( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '2_26';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_17( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_17';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_20( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_20';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formula3_21( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '3_21';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }
    
    public function formula4_11( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '4_11';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }
    
    public function formula4_12( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '4_12';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }
    
    public function formula4_13( $dato_respuesta, $listaVPsCompleta ){
      $n_pregunta_normalizada = '4_13';
      $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
      return $vp;
    }

    public function formulaCC5( $_, $listaVPsCompleta, $_, $_, $todasLasRespuestas, $datosMacroeconomicos ){
      // IMPORTANTE: El dato de respuesta 3_10 ha sido cambiado de Lista a texto libre
      if( isset( $todasLasRespuestas[ '3_10' ] ) ){
        $dato_respuesta_orig = $todasLasRespuestas[ '3_10' ];    // --- Ingresos mensuales
        $salario_promedio    = $datosMacroeconomicos[ 'Dato' ]; // DatosMacroeconomicos / excel GV Macroeconomicas F175
        $cc                  = $dato_respuesta_orig - $salario_promedio;

        $dato_respuesta = $cc <  -200               ? 1 : (
                          $cc >= -200 && $cc < -100 ? 2 : (
                          $cc >= -100 && $cc <  0   ? 3 : (
                          $cc >=  0   && $cc <  100 ? 4 : (
                          $cc >=  100 && $cc <= 200 ? 5 : (
                          $cc >   200               ? 6 : -1
                          )))));

        $n_pregunta_normalizada = 'CC5';
        $vp = $this->formulaVPlista( $n_pregunta_normalizada, $dato_respuesta, $listaVPsCompleta );
        
      } else {
        $cc = '';
        $dato_respuesta_orig = '';
        $vp = -1;
      }
      return [ 
        // --- "datos"     => $salario_promedio,
        "cc"        => $cc,
        "respuesta" => $dato_respuesta_orig,
        "vp"        => $vp,
      ];
    }
}

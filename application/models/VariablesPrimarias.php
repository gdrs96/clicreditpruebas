<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
	This class is defined to model VariablesPrimarias. Get the information from the database and format to be used easily
*/
class VariablesPrimarias extends CI_Model{
	/*
		Version:
		31-12-2017 Guillermo: New
	*/
	
	/*
		Process the info for the table and format to correct output
	*/
	public function ProcessVariablesPrimarias()
	{
		/*
			Version:
			31-12-2017 Guillermo: New
		*/
		$query = $this->db->select('pregunta,valor')->from('variables_primarias');
		$sql = $query->get_compiled_select();
		$listaVariablesCompleta = $this->Thememodel->get_query($sql);
		$listaVariablesPrimarias = $this->FormatToCorrectOutput($listaVariablesCompleta);
		return $listaVariablesPrimarias;
		
	}
	/*
		Looping into list returned and format to an array
	*/
	private function FormatToCorrectOutput($listaVariablesCompleta)
	{
		/*
			Version:
			31-12-2017 Guillermo: New
		*/
		$lista = array();
		foreach ($listaVariablesCompleta as $valor) {
			$lista[$valor->pregunta] = json_decode($valor->valor,true); 
		}
		return $lista;
	}
	
	/*
		Returned specific variablePrimaria getting question and identifier
	*/
	public function GetVariablePrimaria($listaVariablesPrimarias,$pregunta, $id)
	{
		/*
			Version:
			31-12-2017 Guillermo: New
		*/
		$result = "";
		if (isset($listaVariablesPrimarias[$pregunta][$id]))	
		{
			$result = $listaVariablesPrimarias[$pregunta][$id];
		}
		return $result;
	}

	
 
}

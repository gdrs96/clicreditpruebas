<?php defined('BASEPATH') OR exit('No direct script access allowed');
if( $posts ){
	?>
	<ul>
		<?php
		foreach($posts as $key){
			?>
			<li><a href="<?= base_url() ?>dashboard/post/<?= $key->id ?>"><?= $key->title ?> | <?= $key->slug ?></a><br> <?= $key->description ?></li>
			<?php
		}
		?>
	</ul>
	<?php
}else{
	?>
	<ul><li>No hay resultados</li></ul>
	<?php
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
if( $messages ){ ?>
<ul>
	<?php
	foreach($messages as $key){
		?>
		<li><a><?= $key->name ?> | <?= $key->subject ?> | <?= $key->email ?></a><br><?= $key->message ?></li>
		<?php
	}
	?>
</ul>
<?php
}else{
	?>
	<ul><li>No hay resultados</li></ul>
	<?php
}
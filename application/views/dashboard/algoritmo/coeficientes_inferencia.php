<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
  <div class="container">
    <div class="page-content">
         <div class="single-head">
         <div class="single-head">
          <h2>Coeficientes de inferencia</h2>
         </div>
           <div class="clearfix"></div>
             <div class="page-media">
               <div class="table-responsive">
                 <table class="table table-bordered table-hover text-center" id="table-coeficientes-inferencia">
           <thead>
              <tr>
                <th class="not-mobile">Indicador</th>
                <th class="not-mobile">Última actualización</th>
                <th class="not-mobile">Etiqueta</th>
                <th class="not-mobile">C. Inf</th>
                <th class="not-mobile" style="width: 150px">Notas y observaciones</th>
                <th class="not-mobile">Status</th>
                <th class="not-mobile">Acciones</th>
              </tr>
            </thead>
           </table>
             </div>
              <div class="widget-foot">
              <span class="pull-left">
                <a class="btn btn-default" href="#" onclick="agregarCoeficienteInf();"><i class="fa fa-plus"></i> Crear nuevo coeficiente de inferencia</a> &nbsp;
              </span>
              <div class="clearfix"></div>
            </div>
         </div>
  </div>
</div>
</div>
</div>
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/datatable.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">
<link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?= base_url() ?>css/normalize.css">
<link rel="stylesheet" href="<?= base_url() ?>-/css/parsley.css">

<script type="text/javascript">
  var urlCoeficientesInferencia = '<?= base_url()?>Gestion_variables/obtenerCoeficientesInferencia';
  var urlCrearCoeficienteInf = '<?= base_url()?>Gestion_variables/crearCoeficienteInferencia';
  var urlActualizarCoeficienteInf = '<?= base_url()?>Gestion_variables/actualizarCoeficienteInferencia';
  
</script>


<script src="<?= base_url() ?>js/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>
<script src="<?= base_url() ?>dist/js/dashboard/algoritmo/coeficiente_inferencia.js" type="text/javascript"></script>

 <div class="modal modal-alert fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form action="#" id="form_coeficiente" class="form-horizontal">
                   

                      <div class="form-group">
                            <label class="control-label col-md-4">Indicador:</label>
                            <div class="col-md-9">
                                <p name="indicador" id="indicador" class="form-control"></p>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="control-label col-md-4">Etiqueta:</label>
                            <div class="col-md-9">
                                <input name="etiqueta" id="etiqueta" class="form-control" type="text" data-parsley-trigger="input" data-parsley-required="true">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-6">Coeficiente:</label>
                            <div class="col-md-9">
                                <input name="coeficiente" id="coeficiente" placeholder="" class="form-control" type="text" data-parsley-required="true" data-parsley-trigger="keyup" data-parsley-type="number" onchange="validacionCoeficienteInput();">
                                <span id="message"></span>
                            </div>
                         </div>

                     
                        <div class="form-group">
                            <label class="control-label col-md-6">Notas:</label>
                            <div class="col-md-9">
                                <textarea  rows="2" name="observacion" id="observacion" placeholder="" class="form-control" data-parsley-required="true"></textarea>
                            </div>
                        </div>

                          <input type="hidden" id="cantidad_coeficiente" name="cantidad_coeficiente" value='<?= $cantidad_coeficientes[0]->cantidad  ?>'>

                          <input type="hidden" id="id_coeficiente" name="id_coeficiente">
                  </form>
            </div>
            <div class="modal-footer">
                <button id="btnSave" onclick="guardar_coeficiente()" class="btn btn-success">Guardar</button>
                <button class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
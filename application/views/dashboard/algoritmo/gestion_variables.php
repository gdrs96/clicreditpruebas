<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="">
         <div class="single-head">
         <div class="single-head">
            <h2>Gestión de variables</h2>
         </div>
           <div class="clearfix"></div>
             <div class="page-media">
            <div id="grupoTablas">
             <ul>
               <li class="active"><a href="#tab-1">Cargar versión</a></li>
               <li><a href="#tab-2">C. de inferencia</a></li>
               <li><a href="#tab-3">V. Macroeconómicas</a></li>
               <li><a href="#tab-4">Editar v. S.</a></li>
            </ul>
             <div id="tab-1">
                <?php $this->load->view('dashboard/algoritmo/historico_versiones',null); ?>
            </div>
            <div id="tab-2">
              <?php $this->load->view('dashboard/algoritmo/coeficientes_inferencia',null); ?>
           </div>
            <div id="tab-3">
              <?php $this->load->view('dashboard/algoritmo/variables_macroeconomicas',null); ?>
           </div>
         </div>
              <div class="widget-foot">
             
              <div class="clearfix"></div>
            </div>
         </div>
	</div>
</div>
</div>
</div>



<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/datatable.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>-/css/parsley.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/nav.css">
  
<script src="<?= base_url() ?>js/jquery.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/bootstrap.min.js" type="text/javascript"></script>

<script src="<?= base_url() ?>js/jquery-ui.js" type="text/javascript"></script>

<script src="<?= base_url() ?>js/select2.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>


<script>
  $( function() {
    $( "#grupoTablas" ).tabs();
  } );

  console.log("gestion var");
</script>


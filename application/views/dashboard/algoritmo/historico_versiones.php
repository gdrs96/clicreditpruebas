<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
  <div class="container">
  
         <div class="single-head">
         <div class="single-head">
          <p class="mb-0"><h3>Histórico de versiones</h3> <span class="pull-right"> Versión cargada: <?= $version_actual[0]->codigo ?></span></p>
         </div>
           <div class="clearfix"></div>
             <div class="page-media">
               <div class="table-responsive">
                 <table class="table table-bordered table-hover text-center" id="table-versiones">
           <thead>
              <tr>
                <th class="not-mobile">Código</th>
                <th class="not-mobile">Activo desde</th>
                <th class="not-mobile">Activo hasta</th>
                <th class="not-mobile">Ver. variables</th>
                <th class="not-mobile">Nombre de versión</th>
                <th class="not-mobile">Acciones</th>
              </tr>
            </thead>
           </table>
             </div>
              <div class="widget-foot">
              <span class="pull-left">
                <a class="btn btn-default" href="#" onclick=""><i class="fa fa-plus"></i>Cargar versión seleccionada</a> &nbsp;
              </span>
              <div class="clearfix"></div>
            </div>
         </div>
  </div>
</div>

</div>
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/datatable.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">
<link rel="stylesheet" href="<?= base_url() ?>css/font-awesome.min.css">
<link rel="stylesheet" href="<?= base_url() ?>css/normalize.css">
<link rel="stylesheet" href="<?= base_url() ?>-/css/parsley.css">

<script type="text/javascript">
  var urlVersionesAlgoritmo = '<?= base_url()?>Gestion_variables/obtenerVersionesAlgoritmo';
  var urlCrearCoeficienteInf = '<?= base_url()?>Gestion_variables/crearCoeficienteInferencia';
  var urlActualizarCoeficienteInf = '<?= base_url()?>Gestion_variables/actualizarCoeficienteInferencia';
  
</script>


<script src="<?= base_url() ?>js/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>
<script src="<?= base_url() ?>dist/js/dashboard/algoritmo/historico_versiones.js" type="text/javascript"></script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
         <div class="single-head">
         <div class="single-head">
          <h2>Perfiles de captación </h2>
         </div>
           <div class="clearfix"></div>
             <div class="page-media">
               <div class="table-responsive">
                 <table class="table table-bordered table-hover text-center" id="table-perfil-captacion">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Última actualización</th>
                      <th>Nombre de perfil</th>
                      <th style="width: 80px">Pts > MR</th>
                      <th style="width: 150px">Notas y observaciones</th>
                      <th>Modificado por:</th>
                      <th class="not-mobile" style="width: 150px">Acciones</th>
                   </tr>
                </thead>
               </table>
             </div>
              <div class="widget-foot">
              <span class="pull-left">
                <a class="btn btn-default" href="#" onclick="agregarPerfil()"><i class="fa fa-plus"></i> Crear nuevo perfil de captación</a> &nbsp;
              </span>
              <div class="clearfix"></div>
            </div>
         </div>
	</div>
</div>
</div>
<div class="modal modal-alert fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_perfil" class="form-horizontal">
                   
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-4">Nombre:</label>
                            <div class="col-md-9">
                                <input name="descripcion" id="descripcion" class="form-control" type="text" data-parsley-trigger="input" data-parsley-required>
                            </div>
                        </div>
                     
                          <div class="form-group">
                            <label class="control-label col-md-6">Pts > mínimo:</label>
                            <div class="col-md-9">
                                <input name="valor" id="valor" placeholder="" class="form-control" type="text" data-parsley-required data-parsley-trigger="keyup" data-parsley-type="number">
                            </div>
                         </div>

                     
                        <div class="form-group">
                            <label class="control-label col-md-6">Notas y observaciones:</label>
                            <div class="col-md-9">
                                <textarea  rows="2" name="observacion" id="observacion" placeholder="" class="form-control"></textarea>
                            </div>
                        </div>

                         <input type="hidden" id="id_perfil" name="id_perfil" value="" class="form-control" type="text">
                       
                       </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnSave" type="submit" onclick="guardar_perfil();" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

</div>






<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/datatable.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>-/css/parsley.css">



<!-- Declaración de rutas -->
<script type="text/javascript">
 var urlPerfilesCaptacion = '<?= base_url()?>perfil_captacion/obtenerPerfilesCaptacion';
 var urlAgregarPerfilCaptacion ='<?= base_url()?>perfil_captacion/crearPerfilesCaptacion';
 var urlActualizarPerfilCaptacion ='<?= base_url()?>perfil_captacion/actualizarPerfilesCaptacion';
 var urlEliminarPerfilCaptacion ='<?= base_url()?>perfil_captacion/eliminarPerfilCaptacion';
</script>

<script src="<?= base_url() ?>js/jquery.js"></script>
<script src="<?= base_url() ?>-/js/bootstrap.min.js" type="text/javascript"></script>

<script src="<?= base_url() ?>js/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script src="<?= base_url() ?>dist/js/dashboard/algoritmo/perfil_captacion.js" type="text/javascript"></script>

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link href="<?= base_url() ?>-/css/fullcalendar.css" rel="stylesheet">
<div class="main-content">
	<div class="container">
		<div class="page-content page-calendar">
			<div class="single-head">
				<h1 class="pull-left"><i class="fa fa-calendar"></i> Calendario</h1>
				<div class="clearfix"></div>
			</div>
			<iframe src="https://www.google.com/calendar/embed?src=<?= $email ?>&ctz=Europe/Madrid" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
			<!-- <div id="calendar"></div>-->
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<!--
<script src="<?= base_url() ?>-/js/fullcalendar.min.js"></script>
<script type="text/javascript">
/* ****************** */
/* Calendar */
/* ****************************************** */

	$(document).ready(function() {
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		$('#calendar').fullCalendar({
			header: {
				left: 'prev',
				center: 'title',
				right: 'month,agendaWeek,agendaDay,next'
			},
			editable: true,
			events: [
				{
					title: 'All Day Event',
					start: new Date(y, m, 1)
				},
				{
					title: 'Long Event',
					start: new Date(y, m, d-5),
					end: new Date(y, m, d-2)
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d-3, 16, 0),
					allDay: false
				},
				{
					id: 999,
					title: 'Repeating Event',
					start: new Date(y, m, d+4, 16, 0),
					allDay: false
				},
				{
					title: 'Meeting',
					start: new Date(y, m, d, 10, 30),
					allDay: false
				},
				{
					title: 'Lunch',
					start: new Date(y, m, d, 12, 0),
					end: new Date(y, m, d, 14, 0),
					allDay: false
				},
				{
					title: 'Birthday Party',
					start: new Date(y, m, d+1, 19, 0),
					end: new Date(y, m, d+1, 22, 30),
					allDay: false
				},
				{
					title: 'Click for Google',
					start: new Date(y, m, 28),
					end: new Date(y, m, 29),
					url: 'http://google.com/'
				}
			]
		});
	});
</script>
	-->
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style>
    .btn-file {
    position: relative;
    overflow: hidden;
}
.btn-file input[type=file] {
    position: absolute;
    top: 0;
    right: 0;
    min-width: 100%;
    min-height: 100%;
    font-size: 100px;
    text-align: right;
    filter: alpha(opacity=0);
    opacity: 0;
    outline: none;
    background: white;
    cursor: inherit;
    display: block;
}

#img-upload{
    width: 100%;
}
</style>


<div class="main-content">
    <div class="container">
        <div class="page-content">
            <div class="single-head">
                <h2><i class="fa fa-user"></i>Calificación</h2>
                <div class="clearfix"></div>
            </div>
            <div class="page-form">
                 <span><?php echo validation_errors(); ?></span>
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?= base_url() ?>dashboard/saveCalificacion" >
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-lg-4 control-label" for="name">Calificación</label>
                                <div class="col-lg-8">
                                    <input type="text" id="calificacion" name="calificacion" class="form-control" placeholder="Calificación" value="<?php echo $nueva_calificacion; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label" for="interes">Interés día(en número)</label>
                                <div class="col-lg-8">
                                    <input type="text" id="interes" name="interes" class="form-control" placeholder="Interés día(en número)" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-4">Cambiar Foto <i class="fa fa-pencil"></i></label>
                                 <div class="col-lg-8">
                                    <input type="file" class="form-control" id="image" name="userfile" required>
                                </div>
                            </div>
                        </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <input type="hidden" name="id" value="<?php if(isset($user->id)) echo $user->id; ?>">
                            <button class="btn btn-info" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<link href="<?= base_url() ?>-/css/select2.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
    $('.select-multiple').select2({placeholder: "Roles"});
</script>
<script type="text/javascript">
    function user_image_del(id){
        var r = confirm('Por favor confirma el borrado');
        if(r == true){
            $.ajax({
                type:   'POST',
                url:    '<?= base_url() ?>dashboard-ajax/user-image-del/'+id,
                success: function(result){
                    $.gritter.add({
                        text: result,
                        sticky: false,
                        time: '',
                        class_name: 'gritter-custom'
                    });
                    $('#user_image').slideUp('slow', function(){});
                }
            });
        }
    }
</script>

<script>
    $(document).ready( function() {
        $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
            label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [label]);
        });

        $('.btn-file :file').on('fileselect', function(event, label) {
            
            var input = $(this).parents('.input-group').find(':text'),
                log = label;
            
            if( input.length ) {
                input.val(log);
            } else {
                if( log ) alert(log);
            }
        
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                
                reader.onload = function (e) {
                    $('#img-upload').attr('src', e.target.result);
                }
                
                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#imgInp").change(function(){
            readURL(this);
        });     
    });
</script>
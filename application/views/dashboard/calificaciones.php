<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!--
This class manage the grid to show all the calificaciones in the system. Could be possible to edit every of them.
-->

<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<div class="single-head">
					<h2><i class="fa fa-user"></i> Calificaciones </h2>
					<div class="clearfix"></div>
				</div>
				<div class="page-media">
					<div class="table-responsive">
						<table class="table table-bordered table-hover text-center" id="table-users">
							<thead>
								<tr>
									<th class="text-center">Calificación</th>
									<th class="text-center">Tasa de Interés Diario (en número)</th>
									<th class="text-center">Imagen</th>
									<th class="text-center">Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($calificaciones as $key){
									?>
									<tr>
										<td><?= $key->id; ?></td>
										<td><?= $key->tasa_interes; ?></td>
										<td><img  src=<?php echo base_url(); ?><?=$key->imagen; ?> width="100px"></td>
										<td>
										  <a href="<?= base_url() ?>dashboard/editCalificacion/<?= $key->id ?>" class="btn btn-info"><i class="fa fa-pencil"></i> </a>
										</td>
										
									</tr>
										<?php } ?>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-md-12 text-center">
									<?php if( isset( $pagination )){ echo $pagination; } ?>
								</div>
							</div>
						</div>
						<div class="widget-foot">
							<span class="pull-left">
								<a href="<?= base_url() ?>dashboard/calificacion" class="btn btn-default"><i class="fa fa-plus"></i> Añadir Calificacion</a> &nbsp;
							</span>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#table-users").tablesorter();
	});
</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="main-content">
    <div class="container">
        <div class="page-content">
            <div class="single-head">
                <h2><i class="fa fa-user"></i>Calificación</h2>
                <div class="clearfix"></div>
            </div>
            <div class="page-form">
                 <span><?php echo validation_errors(); ?></span>
                  <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?= base_url() ?>dashboard/editCalificacion" >
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-lg-4 control-label" for="name">Calificación</label>
                                <div class="col-lg-8">
                                    <input type="text" id="calificacion" name="calificacion" class="form-control" placeholder="Calificación" value="<?php $valor = !empty($calificacion_edit->calificacion)?$calificacion_edit->calificacion:null; echo set_value('calificacion', $valor); ?>" disabled/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-lg-4 control-label" for="interes">Interés día (en número)</label>
                                <div class="col-lg-8">
                                    <input type="text" id="interes" name="interes" class="form-control" placeholder="Interés día(en número)" value="<?php  $valor = !empty($calificacion_edit->tasa_interes)?$calificacion_edit->tasa_interes:null; echo set_value('tasa_interes', $valor); ?>" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-lg-4">Cambiar Foto <i class="fa fa-pencil"></i></label>
                                 <div class="col-lg-8">
                                    <input type="file" class="form-control" id="image" name="userfile">
                                </div>
                            </div>
                        </div>
                           <div class="form-group col-lg-6 center-block">
                            <?php if( isset($calificacion_edit->imagen) && file_exists($calificacion_edit->imagen)){ ?>
                            <img style="max-height:335px" src="<?= base_url().$calificacion_edit->imagen ?>" alt="Imagen" class="img-responsive" id="user_image"/>
                            <br>
                        <?php }?>
                        </div>
                        
                         
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-10">
                            <input type="hidden" name="id" value="<?php if(isset($calificacion_edit->id)) echo $calificacion_edit->id; ?>"">
                            <button class="btn btn-info" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
                        </div>
                    </div>
               </form>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>




<script type="text/javascript">
     $("#end").click(function(event) {
        event.preventDefault();

    });


</script>



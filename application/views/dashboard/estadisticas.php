<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<div class="single-head">
          <h2>Preferencia de <strong>REDES SOCIALES</strong></h2>
					<div class="clearfix"></div>
				</div>
				<div class="page-media">
					<div class="table-responsive">
						<table class="table table-bordered table-hover text-center" id="table-groups">
							<thead>
								<tr>
									<th>Red social</th>
									<th>Cantidad</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(!empty($red_sociales)){
									foreach ($red_sociales as $red => $cantidad){
										?>
										<tr>
                      <td><?= $red; ?></td>
                      <td><?= $cantidad; ?></td>
										</tr>
										<?php
									}
								} ?>
							</tbody>
						</table>
					</div>
				</div>
        <div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
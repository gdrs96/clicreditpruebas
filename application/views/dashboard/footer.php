<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
	$enlace_actual = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	$cursor = substr($enlace_actual, -5);
	if ($cursor != 'login' && $cursor != 'in_in') {
?>
</div>
</div>
</div>
	<footer>
		<div class="green-line"></div>
		<div class="br-blue footer">
			<div class="container pb-5">
				<div class="row justify-content-center">
					<div class="col mt-2">
						<?php if($last_version) : ?>
							<p>© <span class="year"></span> CliCREDIT Panamá | <a href="<?= $last_version->code_version; ?>"><?= $last_version->number_version; ?></a></p>
						<?php endif; ?>
						<p>Desarrollado por <a href="#"> <b class="green" >DEV TEAM <span class="year"></span></b></a></p>
					</div>
				</div>
			</div>
		</div>
	</footer>
<?php } ?>
	<script type="text/javascript">
		var date = new Date()
		var year = date.getFullYear()
		var $years = document.getElementsByClassName('year')
		Array.prototype.map.call($years,function($year) {$year.innerText= year})
		// $year.innerText = year
	</script>
	</body>
</html>
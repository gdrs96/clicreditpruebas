<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<h1 class="pull-left"><i class="fa fa-picture-o"></i> Galería</h1>
				<div class="clearfix"></div>
			</div>
			<div class="page-gallery">
				<div id="gallery">
					<?php
					if(!empty($files)){
						foreach($files as $key){
							?>
							<div class="element">
								<a href="<?= base_url().'img/'.$key['name'] ?>" class="prettyphoto">
									<img src="<?= base_url().'img/'.$key['name'] ?>" class="img-responsive"/>
								</a>
								<div class="gall-caption">
									<ul class="list-unstyled">
										<li><strong>File name:</strong> <?= $key['name'] ?></li>
										<li><strong>File size:</strong> <?= round( $key['size'] /1024) ?> KB</li>
									</ul>
								</div>
							</div>
							<?php
						}
					}
					?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<link href="<?= base_url()?>-/css/prettyPhoto.css" rel="stylesheet">

<script type="text/javascript">
	/* PrettyPhoto for Gallery */
	/* ----------------------- */
	$(".prettyphoto").prettyPhoto({
		overlay_gallery: false, social_tools: false
	});
</script>
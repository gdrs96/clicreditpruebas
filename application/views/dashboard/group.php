<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<h2><i class="fa fa-users"></i> Rol</h2>
				<div class="clearfix"></div>
			</div>
			<div class="page-form">
				<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?= base_url() ?>dashboard/group" >
					<div class="form-group">
						<label class="col-lg-2 control-label" for="name">Nombre</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" placeholder="Nombre" name="name" value="<?php $t = !empty($group->name)?$group->name:null; echo set_value('name',$t); ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label" for="description">Descripcón</label>
						<div class="col-lg-10">
							<textarea class="form-control" name="description"><?php $t = !empty($group->description)?$group->description:null; echo set_value('description',$t); ?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Permisos</label><br>
						<input type="text" class="js-example-basic-multiple w-full ml-3">
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<input type="hidden" name="id" value="<?php $t = !empty($group->id)?$group->id:null; echo set_value('id',$t); ?>">
							<input type="hidden" name="permisos" id="permisos" value='<?php $t = !empty($group->permisos)?$group->permisos:null; echo set_value('permisos',$t); ?>'>
							<button class="btn btn-info" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<script type="text/javascript">
	$(document).ready(function() {
		$('.js-example-basic-multiple').select2({
		data:[
			{ id: 0, text: 'Cuenta'},
			{ id: 1, text: 'Usuarios'},
			{ id: 2, text: 'Roles'},
			{ id: 3, text: 'Calificaciones'},
			{ id: 4, text: 'Préstamos'},
			{ id: 5, text: 'Clientes'},
			{ id: 6, text: 'Media'},
			{ id: 7, text: 'Logs-SMS'},
			{ id: 8, text: 'Versiones'},
			{ id: 9, text: 'Estadísticas'}
		],
		multiple: true,
	});
	let permisos = $('#permisos').val()
	if(!!permisos) {
		$('.js-example-basic-multiple').select2('data', JSON.parse(permisos))
	}
	$('.js-example-basic-multiple').change(function() {
		var selections = ( JSON.stringify($('.js-example-basic-multiple').select2('data')) );
		$('#permisos').val(selections);
	});
		// console.log(JSON.parse(permisos))
		

	});
</script>
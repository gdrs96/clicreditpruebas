<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<div class="single-head">
					<h2><i class="fa fa-users"></i> Roles </h2>
					<div class="clearfix"></div>
				</div>
				<div class="page-media">
					<div class="table-responsive">
						<table class="table table-bordered table-hover text-center" id="table-groups">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Descripción</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(!empty($groups)){
									foreach ($groups as $key){
										?>
										<tr id="container_groupid_<?= $key->id ?>">
											<td><?= $key->name; ?></td>
											<td><?= $key->description; ?></td>
											<td>
												<a class="btn  btn-success" onclick="del(<?= $key->id ?>,'container_groupid_<?= $key->id ?>','groups');"><i class="fa fa-trash-o"></i> </a>
												<a href="<?= base_url() ?>dashboard/group/<?= $key->id ?>" class="btn btn-info"><i class="fa fa-pencil"></i> </a>
											</td>
										</tr>
										<?php
									}
								} ?>
							</tbody>
						</table>
					</div>
				</div>
				<div class="widget-foot">
					<span class="pull-left">
						<a href="<?= base_url() ?>dashboard/group" class="btn btn-default"><i class="fa fa-plus"></i> Añadir Rol</a> &nbsp;
					</span>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#table-groups").tablesorter();
	});
</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html lang="es-PA">
	<head>
		<meta charset="utf-8" />
		<title>Dashboard</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
		<meta name="description" content="¿Necesitas un préstamo en línea urgente sin revisar APC?. Consigue tu crédito en línea urgente con un máximo de 24 horas y conociendo su aprobación en 5 minutos con clicredit.com, dale clic y salva el día!">
		<meta name="keywords" content="Préstamos en linea, sin APC, rápido, fácil, clicredit">
	
		<link href="<?= base_url() ?>-/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?= base_url() ?>-/css/font-awesome.min.css" rel="stylesheet">
		<link href="<?= base_url() ?>-/css/style.css" rel="stylesheet">
		<link href="<?= base_url() ?>-/css/select2.css" rel="stylesheet">
		<link rel="icon" href="<?= base_url() ?>img/clicredit-favicon.png" type="image/x-icon" />
		<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">
		<script>window.jQuery || document.write('<script type="text/javascript" src="<?= base_url() ?>js/jquery.js"><\/script>')</script>
        <script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>
         <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>

		<script type="text/javascript">
			var baseURL = '<?= base_url() ?>';
			var clave_temporal = '<?= $this->config->item('clave_temporal') ?>'
		</script>
		<script type="text/javascript">
//verifica si tiene una sesión activa en caso de poseer la misma se procede a mostrar un mensaje indicando que tiene otra sesion 
function checkSessionBrowser()
	{
	   $.ajax({
			cache: false,
			type: "POST",
            dataType:'json',
            url: '<?= base_url() ?>Acceso/showMessageSession',
            success: function(response) 
            {
            	
             if(response.status == true)
             {
              swal({
                title: '<p style="font-size: 25px; ">'+response.msg+'</p>',
                text: "",
                type: "warning",
                html: true,
                confirmButtonColor: '#5eac25',
                confirmButtonText: 'Aceptar',
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

             
                if (isConfirm) {
                   $.ajax({
                    url : '<?= base_url() ?>Acceso/quitMessageSession',
                    type: "POST",
                   dataType: "JSON",
                   success: function(data)
                   {
                       if(data.status) 
                       {
                           
                           swal({
                              title: '<p style="font-size: 25px;">Se ha cerrado su sesión anterior</p>',
                              text: '',
                              html: true,
                              type: 'success',
                              showConfirmButton: false,
                              timer: 2000

                          });

                       
                       }
           },
              error: function (jqXHR, textStatus, errorThrown)
              {
            	  console.log("Error General del Sistema, Intente Más Tarde");
              }
            });
           } 
            });
                     
                  }else{
                     return;
                  }
            },
				error: function(){
					console.log("Error General del Sistema, Intente Más Tarde");
				}
});
	}
	checkSessionBrowser();

</script>

<script>
//función encargada de remover la sesión del navegador en caso de tener otra sesión abierta o de haber expirado la sesión
	function removeSessionBrowser()
	{

	   $.ajax({
			cache: false,
			//async: true,
			type: "POST",
            dataType:'json',
            url: '<?= base_url() ?>Acceso/removeSessionBrowser',
            success: function(response) 
            {
                 if(response.status)
                  {
                     swal({
                              title: '<p style="font-size: 25px;">La sesión que mantenía activa ha caducado</p>',
                              html: true,
                              text: '',
                              type: 'warning',
                              confirmButtonColor: '#5eac25',
                              confirmButtonText: 'Aceptar',
                              timer: 10000

                          });
                  }
                  else{
                       return;
                  }
            },
				error: function(){
					//alert("Error General del Sistema, Intente Más Tarde");
					console.log("Error General del Sistema, Intente Más Tarde");
				}
});
	}
removeSessionBrowser();
</script>
	</head>
<body>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="row widget1 collapse">
			<div class="col-md-12">
				<div class="current-status">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-6">
							<div class="current-status-item mt-0">
								<h6>Total usuarios</h6>
								<h3><?= sizeof($users_total) ?> <span class="fa fa-users"></span></h3>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<div class="current-status-item mt-0">
								<h6>Usuarios registrados hoy</h6>
								<h3><?= sizeof($users_today) ?> <span class="fa fa-user"></span></h3>
							</div>
						</div>
						<div class="col-md-4 col-sm-4 col-xs-6">
							<a href="<?= base_url() ?>dashboard/users-online">
								<div class="current-status-item mt-0">
									<h6>Usuarios Online</h6>
									<h3><?= sizeof($users_online_total) ?> <span class="fa fa-user-secret"></span></h3>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="<?= base_url() ?>-/js/excanvas.min.js"></script><![endif]-->
<script src="<?= base_url() ?>-/js/jquery.flot.min.js"></script>
<script src="<?= base_url() ?>-/js/jquery.flot.stack.min.js"></script>
<script src="<?= base_url() ?>-/js/jquery.flot.pie.min.js"></script>
<script src="<?= base_url() ?>-/js/jquery.flot.resize.min.js"></script>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>

<script type="text/javascript">
	setTimeout(function(){$('.widget1').collapse();},450)
	setTimeout(function(){$('.widget2').collapse();},900)
	setTimeout(function(){$('.widget3').collapse();},1100)
</script>'
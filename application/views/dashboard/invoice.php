<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
			<div class="container">

				<div class="page-content page-invoice">

					<!-- Heading -->
					<div class="single-head">
						<!-- Heading -->
						<h3 class="pull-left"><i class="fa fa-desktop lblue"></i> Invoice</h3>
						<!-- Bread crumbs -->
						<div class="breads pull-right">
							<strong>Nav</strong> : <a href="#">Home</a> / <a href="#">Sign</a> / Home
						</div>
						<div class="clearfix"></div>
					</div>

					<!-- Address -->
					<div class="row">

					  <div class="col-md-4 col-sm-4">
						<h5>Metro King</h5>
						<p>
						  19/133, New New York Street<br>
						  Near Bus Stop, Right side <br>
						  Mexico, NY - 63442<br>
						  USA
						</p>
					  </div>

					  <div class="col-md-4 col-sm-4">
						<h5>MacMedia</h5>
						<p>
						  24/133, Mexico Street<br>
						  Near Airport, Right side <br>
						  Mexico, CA - 53432<br>
						  Mexico
						</p>
					  </div>

					  <div class="col-md-4 col-sm-4">
						<h5>Invoice</h5>
						<p>
						  Invoice No : 52322<br>
						  Date : 12/34/2012<br>
						  Account No : 4290293203
						</p>
					  </div>

					</div>

					<!-- Data -->
					<div class="row">

					  <div class="col-md-12">
						<hr />
						<div class="table-responsive">
							<table class="table table-bordered">
							  <thead>
								<tr>
								  <th>#</th>
								  <th>Name</th>
								  <th>Quantity</th>
								  <th>Unit Price</th>
								  <th>Tax</th>
								  <th>Total</th>
								</tr>
							  </thead>
							  <tbody>
								<tr>
								  <td>1</td>
								  <td>Sony Xperia Pro X1242</td>
								  <td>5</td>
								  <td>$534</td>
								  <td>12.5%</td>
								  <td>$2424</td>
								</tr>
								<tr>
								  <td>2</td>
								  <td>Samsung Galaxy Pro</td>
								  <td>2</td>
								  <td>$244</td>
								  <td>12.5%</td>
								  <td>$5342</td>
								</tr>
								<tr>
								  <td>3</td>
								  <td>Apple iPhone 5G - Black</td>
								  <td>7</td>
								  <td>$434</td>
								  <td>12.5%</td>
								  <td>$34524</td>
								</tr>
								<tr>
								  <td>4</td>
								  <td>Nokia Lumina Tipo - Windows Mobile</td>
								  <td>4</td>
								  <td>$754</td>
								  <td>12.5%</td>
								  <td>$8866</td>
								</tr>
								<tr>
								  <td>5</td>
								  <td>Motorola Defy 29323</td>
								  <td>5</td>
								  <td>$644</td>
								  <td>12.5%</td>
								  <td>$88865</td>
								</tr>
								<tr>
								  <td>6</td>
								  <td>Micromax Funbook - Android Tablet</td>
								  <td>9</td>
								  <td>$434</td>
								  <td>12.5%</td>
								  <td>$53535</td>
								</tr>
								<tr>
								  <td></td>
								  <td></td>
								  <td></td>
								  <td></td>
								  <td><strong>Total</strong></td>
								  <td><strong>$2405</strong></td>
								</tr>
							  </tbody>
							</table>
						</div>
					  </div>

					</div>

						<hr />
					<div class="pull-right">
						<button type="submit" class="btn btn-info">Send Invoice</button>
						<button type="submit" class="btn btn-default">Reset</button>
					</div>
					<div class="clearfix"></div>

				</div>

			</div>
		</div>

	</div>
	<!-- Mainbar ends -->

	<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script src="<?= base_url() ?>js/jquery.js"></script>
<script src="<?= base_url() ?>-/js/jquery-ui.min.js"></script>
<script src="<?= base_url() ?>js/popper.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/bootstrap-datetimepicker.min.js"></script>
<script src="<?= base_url() ?>-/js/prettify.js"></script>
<script src="<?= base_url() ?>-/js/jquery.validate.js"></script>
<script src="<?= base_url() ?>-/js/jquery.slimscroll.min.js"></script>
<script src="<?= base_url() ?>-/js/jquery.prettyPhoto.js"></script>
<script src="<?= base_url() ?>-/js/custom.notification.js"></script>
<script src="<?= base_url() ?>-/js/custom.js"></script>
<script src="<?= base_url() ?>-/js/jquery.gritter.min.js"></script>
<script src="<?= base_url() ?>-/js/typing.min.js"></script>
<script src="<?= base_url() ?>-/js/select2.min.js"></script>

<script type="text/javascript">
	function del_all_cache(){
		$.ajax({
			type:	'POST',
			url:	'<?= base_url() ?>dashboard-ajax/del_all_cache',
			success: function(result){
				$.gritter.add({
					text: result,
					sticky: false,
					time: '',
					class_name: 'gritter-custom'
				});
			}
		});
	}
	function del(id,container,type){
		var r = confirm('Por favor confirma el borrado');
		if(r == true){
			var dataString='id='+id+'&type='+type;
			$.ajax({
				type:	'POST',
				url:	'<?= base_url() ?>dashboard-ajax/del',
				data: dataString,
				success: function(result){
					$.gritter.add({
						text: result,
						sticky: false,
						time: '',
						class_name: 'gritter-custom'
					});
					$('#'+container).slideUp('slow', function(){});
				}
			});
		}
	}
	function delpostmeta(id,container){
		var r = confirm('Por favor confirma el borrado');
		if(r == true){
			var dataString='id='+id;
			$.ajax({
				type:	'POST',
				url:	'<?= base_url() ?>dashboard-ajax/delpostmeta',
				data: dataString,
				success: function(result){
					$.gritter.add({
						text: result,
						sticky: false,
						time: '',
						class_name: 'gritter-custom'
					});
					$('#'+container).slideUp('slow', function(){});
				}
			});
		}
	}
</script>
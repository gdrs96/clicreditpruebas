<?php defined('BASEPATH') OR exit('bye bye'); ?>
<div class="container-fluid img-background-login" style="background-image: url(<?= base_url() ?>/img/<?= isset($banner_image) ? $banner_image : 'fondo4.jpg' ?>);">
	<div class="card login blue">
		<div class="card-header"> 
		   <a href="<?= base_url() ?>">
			<img class="d-block mx-auto mt-2 mb-3" style="width: 250px; height:50px" alt="Logo CliCredit.com" src="<?= base_url() ?>img/clicredit-dashboard.svg"></a>
			<ul class="nav nav-tabs card-header-tabs"  id="myTab" role="tablist">
				<li class="nav-item w-50 mr-0 text-center">
					<a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">ENTRAR</a>
				</li>
				<li class="nav-item w-50 mr-0 text-center">
					<a class="nav-link" id="recover-tab" data-toggle="tab" href="#recover" role="tab" aria-controls="recover" aria-selected="false">RECUPERAR CUENTA</a>
				</li>
			</ul>
		</div>
		<div class="card-body pb-2">
			<div class="tab-content pb-1" id="myTabContent">
				<div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
					<form role="form" method="post" id="login_form" action="<?= base_url() ?>dashboard_login/login_in">
						<div class="form-group">
							<label for="email">Email</label>
							<input type="text" class="form-control" name="email" placeholder="Email" name="email" value="<?= set_value('email') ?>" required>
						</div>
						<div class="form-group">
							<label for="password">Contraseña</label>
							<input type="password" class="form-control" id="password" placeholder="Contraseña" name="password" value="<?= set_value('password') ?>" required>
						</div>
						<?php if( !empty($info) ) : ?>
							<div class="alert alert-info"><strong><?= $info ?></strong></div>
						<?php endif; ?>
						<?php if( !empty($error) ) : ?>
							<div class="alert alert-danger"><strong><?= $error ?></strong></div>
						<?php endif; ?>
						<div class="text-center my-3">
							<button type="submit" class="btn btn-info">Entrar</button>
						</div>
					</form>
				</div>
				<div class="tab-pane fade" id="recover" role="tabpanel" aria-labelledby="recover-tab">
					<form role="form" method="post" action="<?= base_url() ?>dashboard_login/enviarEmailRecuperacion">
						<div class="form-group">
							<label for="email_contact">Email</label>
							<input type="email" class="form-control"  placeholder="Email" name="email" required>
						</div>
						<div class="form-group">
							<label for="email_contact">Por favor sume el día y el mes, <?= date('d') ?> + <?= date('m')?>:</label>
							<input type="text" class="form-control" id="digito" placeholder="Día + mes" name="digito" required>
						</div>
						<div class="text-center">
							<button type="submit" class="btn btn-info">Recuperar</button>
						</div>
					</form>
				</div>
			</div>
			<p class="text-center mt-2"><b>CLICREDIT ES,</b><span class="black"> INNOVACIÓN FINANCIERA</span></p>
		</div>
	</div>
</div>
<script src="<?= base_url().'-/js/'; ?>jquery.js"></script>
<script src="<?= base_url().'-/js/'; ?>bootstrap.min.js"></script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="well">
	<h1 class="titulo">Registro</h1>
	<form action="<?php echo base_url()?>login/do_registration" role="form" method="post" class="form-horizontal">
		<h2 class="form-signin-heading">Introduce tus datos para el registro</h2>
			<div class="form-group">
				<label for="username" class="col-sm-3 control-label">Usuario:</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="username" name="username">
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-3 control-label">Email:</label>
				<div class="col-sm-8">
					<input type="email" class="form-control" id="email" name="email">
				</div>
			</div>
			<div class="form-group">
				<label for="password" class="col-sm-3 control-label">Contraseña:</label>
				<div class="col-sm-8">
					<input type="password" class="form-control" id="password" name="password">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-8">
					<button class="btn btn-primary" type="submit"> Registrarme </button>
				</div>
			</div>
	</form>
	<br><p class="text-center">Por favor asegurate de rellenar tus datos en tu perfil una vez registrado.</p>
</div>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
         <div class="single-head">
         <div class="single-head">
          <h2><b>Logs</b> SMS</h2>
         </div>
           <div class="clearfix"></div>
             <div class="page-media">
               <div class="table-responsive">
                 <table class="table table-bordered table-hover text-center" id="table-logs" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th class="not-mobile" style="width: 200px; padding:0;">Fecha - hora de envío</th>
                      <th class="not-mobile">N° terminal</th>
                      <th class="not-mobile">C. verif.</th>
                      <th class="not-mobile">ID Prést</th>
                      <th class="not-mobile" style="width: 80px;">SMS_id</th>
                      <th class="not-mobile">Status</th>
                      <th class="not-mobile">Acciones</th>
                   </tr>
                </thead>
               </table>
             </div>
         </div>
	</div>
</div>
</div>
<div class="modal modal-alert fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
        <h3 class="modal-title"></h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_logs_sms" class="form-horizontal">
                   
                    <div class="form-body">

                      <p align="center">Se debe <b style="color:#F00;">confirmar el número de celular del cliente manualmente</b> antes de proceder a emitir un código de verificación</p>
                      <br>


                      <p class="mb-0"><b>Número de terminal:  <span class="pull-right" id="terminal"></span></b></p>
                      
                      <p class="mb-0"><b>ID Préstamo:  <span class="pull-right" id="id_prestamo"></span></b></p>  
                      
                      <p class="mb-0"><b>Código de verificación: <span class="pull-right" id="codigo_verificacion"></span></b></p>


                      <p class="mb-0"><b>Activar manualmente: <span class="pull-right"> 
                        <label class="switch">
                              <input type="checkbox" id="sms_manual" name="sms_manual">
                               <span class="slider round"></span>
                            </label></span></b></p>


                         <input type="hidden" id="id_log" name="id_log" value="" class="form-control" type="text">
                       
                       </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="btnSave" type="submit" onclick="activar_envio_manual();" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
</div>






<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/datatable.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">



<!-- Declaración de rutas -->
<script type="text/javascript">
 var urlLogsSms = '<?= base_url()?>logs_sms/obtenerLogsSms';
 var urlActivarEnvioManual = '<?= base_url()?>logs_sms/activarEnvioSmsManual';
 var routeImageOk = '<?= base_url()?>img/logs_sms/Ok.png';
 var routeImageAlert = '<?= base_url()?>img/logs_sms/Alert.png';
 var routeImageHand = '<?= base_url()?>img/logs_sms/Hand.png';
 var urlPrestamo = '<?= base_url()?>dashboard/prestamo/';
</script>

<script src="<?= base_url() ?>js/jquery.js"></script>
<script src="<?= base_url() ?>-/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/moment.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datetime-moment.js"></script>
<script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>


<script src="<?= base_url() ?>dist/js/dashboard/logs_sms.js" type="text/javascript"></script>

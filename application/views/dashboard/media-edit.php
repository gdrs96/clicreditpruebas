<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Mainbar head ends -->
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<h3 class="pull-left"><i class="fa fa-file"></i> Modificar Archivo</h3>
				<div class="clearfix"></div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<form class="form-horizontal" role="form"  method="post" action="<?php echo base_url() ?>dashboard/media-edit">
						<div class="form-group">
							<label for="name" class="col-md-2 control-label">Nombre</label>
							<div class="col-md-10">
								<input type="text" class="form-control" id="name" name="name" value="<?php
								$nombre = explode( '/', $file['server_path']);
								echo $nombre[sizeof($nombre)-1]; ?>">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-offset-2 col-md-10">
								<div class="pull-right">
									<input type="hidden" id="path" name="path" value="<?= $nombre[0]; ?>">
									<input type="hidden" id="old_name" name="old_name" value="<?= $file['server_path'] ?>">
									<button class="btn btn-info" type="submit">Guardar</button>
								</div>
								<div class="clearfix"></div>
							</div>
						</div>
					</form>
				</div>
				<div class="col-md-8">
					<img src="<?= base_url().$file['server_path'] ?>" alt="<?= $file['name'] ?>" class="img-fluid"/>
				</div>
			</div>
			<div class="widget-foot">
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>

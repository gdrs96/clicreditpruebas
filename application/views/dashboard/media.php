<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link href="<?= base_url() ?>-/css/dropzone.css" rel="stylesheet">
<link href="<?= base_url() ?>-/css/prettyPhoto.css" rel="stylesheet">
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<h2><i class="fa fa-picture-o"></i> Media</h1>
				<div class="clearfix"></div>
			</div>
			<div class="single-head">
				<div class="page-media">
					<form action="<?= base_url() ?>dashboard/uploadMedia" class="dropzone"></form>
					<br/>
					<form class="mb-3" action="<?= base_url() ?>dashboard/editBannerImage" method="POST">
						<div class="form-group">
    					<label for="banner_image">Imagen de portada</label>
    					<input type="text" class="form-control" id="banner_image" name="banner_image" value="<?= $banner_image; ?>" required>
  					</div>
						<div class="form-group">
    					<label for="banner_image_float">Imagen flotante de portada (<em>Tamaño recomendado: 210px x 57px</em>)</label>
    					<input type="text" class="form-control" id="banner_image_float" name="banner_image_float" value="<?= $banner_image_float; ?>" required>
  					</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="show_image_float" id="show_image_float1" value="true" <?php if($show_image_float) : ?>checked<?php endif; ?>>
							<label class="form-check-label" for="show_image_float1">
								Si, deseo mostrar la imagen flotante.
							</label>
						</div>
						<div class="form-check">
							<input class="form-check-input" type="radio" name="show_image_float" id="show_image_float2" value="false" <?php if(!$show_image_float) : ?>checked<?php endif; ?>>
							<label class="form-check-label" for="show_image_float2">
								No, no quiero mostrar la imagen flotante.
							</label>
						</div>
						<button type="submit" class="btn btn-info mt-3">Actualizar</button>
					</form>
					<div class="table-responsive">
						<table class="table table-bordered table-hover text-center">
							<thead>
								<tr>
									<th>Vista Previa</th>
									<th>Nombre</th>
									<th>Tamaño</th>
									<th style="width: 120px">Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(!empty($files)){
									foreach($files as $key){
										?>
										<tr id="<?= $key['size'].$key['date'] ?>">
											<td><a href="<?= base_url().'img/'.$key['name'] ?>" class="prettyphoto"><img src="<?= base_url().'img/'.$key['name'] ?>" alt="<?= $key['name'] ?>" class="img-responsive img-thumbnail"/></a></td>
											<td><a href="<?= base_url().'img/'.$key['name'] ?>" class="prettyphoto"><?= $key['name'] ?></a><p><?= 'img/'.$key['name'] ?></p></td>
											<td><?= round( $key['size'] /1024) ?> KB</td>
											<td>
												<a href="<?= base_url() .'dashboard/media-edit/'.$key['name'] ?>" class="btn btn-info"><i class="fa fa-pencil"></i> </a>
												<a onclick="deleteMedia('img/<?= $key['name'] ?>','<?= $key['size'].$key['date'] ?>');" class="btn btn-success"><i class="fa fa-trash-o"></i> </a>
											</td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<script src="<?= base_url() ?>-/js/dropzone.min.js"></script>
<?php
include 'js-files.php';
?>
<script language="javascript">
	function deleteMedia(name,container){
		var r = confirm('Por favor confirma el borrado');
		if(r == true){
			var dataString='name='+name;
			$.ajax({
				type:	'POST',
				url:	'<?= base_url() ?>/dashboard-ajax/deleteMedia',
				data: dataString,
				success: function(result){
					$.gritter.add({
						text: result,
						sticky: false,
						time: '',
						class_name: 'gritter-custom'
					});
					$('#'+container).slideUp('slow', function(){});
				}
			});
		}
	}

	$(".prettyphoto").prettyPhoto({
		overlay_gallery: false, social_tools: false
	});
</script>
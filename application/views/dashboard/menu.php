<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<nav class="navbar navbar-expand-lg navbar-light fixed-top p-0">
	<div class="container my-1">
		<a class="navbar-brand pt-0" href="<?= base_url() ?>">
			<img class="img-fluid" style="width: 250px; height:50px" alt="Logo CliCredit.com" src="<?= base_url() ?>img/clicredit-dashboard.svg">
		</a>
		<button class="navbar-toggler mr-3" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
			<span class="sr-only">Menú</span>
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="main-navbar">
			<ul class="navbar-nav align-items-center ml-auto my-3 my-lg-0">
				<?php if(check_rol_view($this->session->userdata('roles'),array(5))) : ?>
					<li class="nav-item my-2 mr-lg-3"><a href="<?= base_url() ?>dashboard/clientes" class="btn btn-mi-cuenta" style="background-color: #5EAC25">CLIENTES</a></li>
				<?php endif; ?>
				<?php if(check_rol_view($this->session->userdata('roles'),array(4))) : ?>
					<li class="nav-item my-2 mr-lg-3"><a href="<?= base_url() ?>dashboard/prestamos" class="btn btn-mi-cuenta" style="background-color: #5EAC25">PRÉSTAMOS</a></li>
				<?php endif; ?>
				<li class="nav-item mr-lg-3 my-2 my-lg-0">
					<div class="dropdown">
						<a class=" dropdown-toggle" href="#" role="button" data-toggle="dropdown" id="profile" aria-haspopup="true" aria-expanded="false">
							<?php if(file_exists($this->session->userdata('image')) ) : ?>
								<img src="<?= base_url().$this->session->userdata('image') ?>" alt="<?= $this->session->userdata('nicename') ?>" style="height:33px; width:33px" class="img-fluid rounded"/>
							<?php endif; ?>
						</a>
						<div class="dropdown-menu" aria-labelledby="profile">
							<a class="dropdown-item" href="<?= base_url() ?>dashboard/user/<?= $this->session->userdata('user_id') ?>">Editar perfil</a>
							<a class="dropdown-item" href="<?= base_url().'dashboard_login/logout'; ?>">Cerrar Sesión</a>
						</div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</nav>
<div class="container" style="margin-top:80px">
	<div class="outer">
		<div class="sidebar">
			<div class="sidey">
				<div class="side-nav mt-2">
					<div class="side-nav-block">
						<ul class="list-unstyled metismenu" id="menu">
							<?php $nivel = $this->uri->segment(2); ?>
							<li><a href="<?= base_url().'dashboard'; ?>" <?php if(!$nivel){ echo 'class="active";'; } ?>>Dashboard</a></li>
							<?php if(check_rol_view($this->session->userdata('roles'), array(9)) ) : ?>
								<li><a href="<?= base_url().'dashboard/estadisticas'; ?>" <?php if($nivel == 'estadisticas'){ echo 'class="active";'; } ?>>Estadísticas</a></li>
							<?php endif; ?>
							<?php if(check_rol_view($this->session->userdata('roles'), array(1)) ) : ?>
								<li><a href="<?= base_url().'dashboard/users'; ?>" <?php if($nivel == 'users'){ echo 'class="active";'; } ?>>Usuarios</a></li>
							<?php endif; ?>
							<?php if(check_rol_view($this->session->userdata('roles'), array(2)) ) : ?>
								<li><a href="<?= base_url().'dashboard/groups'; ?>" <?php if($nivel == 'groups'){ echo 'class="active";'; } ?>>Roles</a></li>
							<?php endif; ?>	
							<?php if(check_rol_view($this->session->userdata('roles'), array(3)) ) : ?>							
								<li><a href="<?= base_url().'dashboard/calificaciones'; ?>" <?php if($nivel == 'calificaciones'){ echo 'class="active";'; } ?> >Calificaciones</a></li>
							<?php endif; ?>
							<?php if(check_rol_view($this->session->userdata('roles'), array(6)) ) : ?>							
								<li><a href="<?= base_url() ?>dashboard/media" <?php if($nivel == 'media'){ echo 'class="active";'; } ?>>Media</a></li>
							<?php endif; ?>
							<?php if(check_rol_view($this->session->userdata('roles'), array(7)) ) : ?>							
								<li><a href="<?= base_url() ?>logs_sms">Logs sms</a></li>
							<?php endif; ?>
							<?php if(check_rol_view($this->session->userdata('roles'), array(8)) ) : ?>							
								<li><a href="<?= base_url() ?>dashboard/versiones" <?php if($nivel == 'versiones'){ echo 'class="active";'; } ?>>Versiones CliCREDIT</a></li>
							<?php endif; ?>
						</ul>
					</div>
				</div>
			</div>
		</div>
    
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/metisMenu.min.css">
 <script src="<?= base_url() ?>js/jquery.min.js"></script>
 <script src="<?= base_url() ?>js/metisMenu.js"></script>
     <script type="text/javascript">

          $("#menu").metisMenu({

  // enabled/disable the auto collapse.
  toggle: true,

  // prevent default event
  preventDefault: true,

  // default classes
  activeClass: 'active',
  collapseClass: 'collapse',
  collapseInClass: 'in',
  collapsingClass: 'collapsing',

  // .nav-link for Bootstrap 4
  triggerElement: 'a',

  // .nav-item for Bootstrap 4
  parentTrigger: 'li',

  // .nav.flex-column for Bootstrap 4
  subMenu: 'ul'

});
       
     </script>

		<div class="mainbar">
			<?php
			if(!empty($info)){
				?>
				<div class="main-content">
					<div class="container"><div class="alert alert-info"><button type="button" class="close" aria-hidden="true" data-dismiss="alert">×</button><strong><?= $info ?></strong></div></div>
				</div>
				<?php
			}
			if(!empty($error)){
				?>
				<div class="main-content">
					<div class="container"><div class="alert alert-danger"><button type="button" class="close" aria-hidden="true" data-dismiss="alert">×</button><strong><?= $error ?></strong></div></div>
				</div>
				<?php
			} 


 
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<h1 class="pull-left"><i class="fa fa-envelope"></i> Responder Mensaje</h1>
				<div class="clearfix"></div>
			</div>
			<div class="page-inbox">
				<div class="row">
					<div class="col-md-12">
						<div class="mail-write">
							<div class="mail-head">
								<h3 class="pull-left">De: <strong><?= $message[0]->name ?></strong> &lt;<a href="#"><?= $message[0]->email ?></a>&gt;</h3>
								<span class="pull-right"><strong>Recibido:</strong> <?= $message[0]->date ?></span>
								<div class="clearfix"></div>
							</div>
							<div class="mail-details">
								<?= $message[0]->message ?>
								<div class="clearfix"></div>
							</div>
							<hr/>
							<form role="form" method="post" class="form-horizontal" action="<?= base_url() ?>dashboard/sendEmail">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-1 control-label">De</label>
									<div class="col-sm-5">
										<input type="email" class="form-control" id="from" name="from" value="<?= $user[0]->email ?>" disabled />
									</div>
									<div class="col-sm-6">
										<input type="text" class="form-control" id="namefrom" name="namefrom" value="<?= $user[0]->nicename ?>" disabled />
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-1 control-label">Destinatarios</label>
									<div class="col-sm-11">
										<input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?= $message[0]->email ?>" />
									</div>
								</div>
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-1 control-label">CC</label>
									<div class="col-sm-5">
										<input type="email" class="form-control" id="email_cc" name="email_cc" placeholder="Email" />
									</div>
									<label for="inputEmail3" class="col-sm-1 control-label">CCOO</label>
									<div class="col-sm-5">
										<input type="email" class="form-control" id="email_ccoo" name="email_ccoo" placeholder="Email" />
									</div>
								</div>
								<div class="form-group">
									<label for="subject" class="col-sm-1 control-label">Asunto</label>
									<div class="col-sm-11">
										<input type="asunto" class="form-control" id="asunto" name="asunto" placeholder="Asunto" />
									</div>
								</div>
								<div class="form-group">
									<label for="message"class=" col-sm-1 control-label">Mensaje</label>
									<div class="col-sm-11">
										<textarea class="form-control edit" cols="80" rows="10" name="content"><br/><br/>"<?= $message[0]->message ?>"</textarea>
									</div>
								</div>
								<div class="form-group">
									<div class="col-lg-offset-1 col-sm-offset-1 col-lg-11">
										<button type="submit" class="btn btn-info">
											<i class="fa fa-paper-plane"></i> <span>Enviar</span>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<?php
include 'js-files.php';
?>
<script src="<?= base_url() ?>-/tinymce/tinymce.min.js"></script>

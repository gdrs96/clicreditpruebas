<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<h1 class="pull-left"><i class="fa fa-file"></i> Contenido</h1>
				<div class="clearfix"></div>
			</div>
			<form class="form-horizontal" role="form"  method="post" action="<?= base_url() ?>dashboard/post" enctype="multipart/form-data">
				<?php if(isset($post->datemodify)){ ?>
				<div class="form-group">
					<p>Última modificación: <?= $post->datemodify ?></p>
				</div>
				<?php } ?>
				<div class="form-group">
					<label for="title" class="col-md-2 control-label">Título</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="title" name="title" value="<?php $t = !empty($post->title)?$post->title:null; echo set_value('title',$t); ?>" maxlength="255">
						<span id="title_long">(0 - 255)</span> máx 65-70
					</div>
				</div>
				<div class="form-group">
					<label for="slug" class="col-md-2 control-label">Descripción</label>
					<div class="col-md-9">
						<textarea class="form-control" id="description" name="description" maxlength="255"><?php $t = !empty($post->description)?$post->description:null; echo set_value('description',$t); ?></textarea>
						<span id="description_long">(0 - 255)</span> máx 150-170
					</div>
				</div>
				<div class="form-group">
					<label for="slug" class="col-md-2 control-label">Keywords</label>
					<div class="col-md-9">
						<input type="text" class="form-control" id="keywords" name="keywords" value="<?php $t = !empty($post->keywords)?$post->keywords:null; echo set_value('keywords',$t); ?>" maxlength="255">
						<div id="keywords_long">(0 - 255)</div>
					</div>
				</div>
				<div class="form-group">
					<label for="slug" class="col-md-2 control-label">Etiquetas</label>
					<div class="col-md-9">
						<select class="form-control select-multiple select2" multiple name="tags[]" id="roles">
							<?php if(isset($tags)){ foreach ($tags as $key) { ?>
							<option value="<?= $key->id ?>" <?php if(!empty($post_tags) && in_array($key->id,$post_tags)){ echo 'selected';} ?>><?= $key->title ?></option>
							<?php } } ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="slug" class="col-md-2 control-label">Categorías</label>
					<div class="col-md-9">
						<select class="form-control select-multiple select2" multiple name="categorys[]" id="roles">
							<?php if(isset($categorys)){ foreach ($categorys as $key) { ?>
							<option value="<?= $key->id ?>" <?php if(!empty($post_categorys) && in_array($key->id,$post_categorys)){ echo 'selected';} ?>><?= $key->title ?></option>
							<?php } } ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="dropdown" class="col-md-2 control-label">Tipo</label>
					<div class="col-md-4">
						<select class="form-control" name="type">
							<option value="0">-</option>
							<?php if(isset($posttypes)){ foreach($posttypes as $key){ ?>
							<option value="<?= $key->id ?>" <?php if(isset($post->type) && $post->type == $key->id){ echo 'selected'; } ?>><?= $key->name ?></option>
							<?php } } ?>
						</select>
					</div>
				</div>
				<?php
				if(isset($post->datepublish)){
					$date = explode(' ', $post->datepublish);
					$time = $date[1];
				}
				?>
				<div class="form-group">
					<label for="dropdown" class="col-md-2 control-label">Fecha de publicación</label>
					<div class="col-md-2">
						<div id="datepublish" class="input-append input-group dtpicker">
							<input data-format="yyyy-MM-dd" type="text" class="form-control" name="datepublish" value="<?php $t = !empty($date[0])?$date[0]:date('Y-m-d'); echo $t; ?>">
							<span class="input-group-addon add-on">
								<i data-date-icon="fa fa-calendar"></i>
							</span>
						</div>
					</div>
					<div class="col-md-2">
						<div id="timepublish" class="input-append input-group dtpicker">
							<input data-format="hh:mm:ss" type="text" class="form-control" name="timepublish" value="<?php $t = !empty($date[1])?$date[1]:date('H:i:s'); echo $t; ?>">
							<span class="input-group-addon add-on">
								<i data-time-icon="fa fa-clock-o"></i>
							</span>
						</div>
					</div>
					<label for="dropdown" class="col-md-2 control-label">Estado</label>
					<div class="col-md-3">
						<select class="form-control" name="status">
							<option value="publicado" <?php if(isset($post->status) && $post->status == 'publicado' ) echo 'selected'; ?>>Publicado</option>
							<option value="borrador"<?php if(isset($post->status) && $post->status == 'borrador' ) echo 'selected'; ?>>Borrador</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="slug" class="col-md-2 control-label">Slug</label>
					<div class="col-md-4">
						<input type="text" class="form-control" id="slug" name="slug" value="<?php $t = isset($post->slug)?$post->slug:null;echo set_value('slug',$t); ?>" maxlength="50">
					</div>
				</div>
				<div class="form-group">
					<label for="slug" class="col-md-2 control-label">Imagen destacada</label>
					<div class="col-md-4">
						<input type="file" class="form-control" id="image" name="image">
					</div>
					<?php
					if(isset($post->image)){ ?>
					<div class="col-md-1">
						<a class="btn btn-danger" onclick="del_image(<?php $t = isset($post->id)?$post->id:null;echo set_value('id',$t); ?>);"><i class="fa fa-trash-o"></i></a>
					</div>
					<?php
				} ?>
				<div class="col-md-5">
					<?php
					if(isset($post->image) && file_exists( $post->image) ){ ?>
					<img src="<?= base_url().$post->image ?>" class="img-responsive" style="margin:auto;max-height:260px;" id="imagen_destacada">
					<?php
				} ?>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-1">
			</div>
			<div class="col-md-10">
				<textarea class="form-control edit" cols="80" rows="40" name="content"><?php $t = isset($post->content)?$post->content:null;echo set_value('content',$t); ?></textarea>
			</div>
		</div>
		<?php
		if(isset($postmeta)){
			foreach($postmeta as $key){
				?>
				<div class="row" style="margin-bottom: 15px;" id="rowmeta<?= $key->id ?>">
					<div class="col-md-5">
						<input type="text" class="form-control" placeholder="Meta" id="justmeta_<?= $key->id ?>" name="justmeta_<?= $key->id ?>" value="<?= $key->metakey ?>">
					</div>
					<div class="col-md-6">
						<input type="text" class="form-control" placeholder="Valor" id="justvalue_<?= $key->id ?>" name="justvalue_<?= $key->id ?>" value="<?= $key->metavalue ?>">
					</div>
					<div class="col-md-1">
						<button type="button" onclick="delpostmeta(<?= $key->id ?>,'rowmeta<?= $key->id ?>');" class="btn btn-danger"><i class="fa fa-trash-o"></i> </button>
					</div>
				</div>
				<?php
			}
		}
		?>
		<div class="form-group">
			<div id="place_form"></div>
		</div>
		<div class="form-group">
			<div class="col-md-offset-2 col-md-10">
				<button type="button" class="btn btn-default" id="nuevo_campo"><i class="fa fa-plus"></i> Añadir Meta</button>
				<?php if(isset($post->id) ){ ?>
				<button type="button" class="btn btn-default" onclick="del_cache_blog('<?= $post->slug ?>')"><i class="fa fa-trash-o"></i> Borrar cache</button>
				<a class="btn btn-default" href="<?= base_url().'preview/ver/'.$post->slug ?>" target="_blank"><i class="fa fa-eye"></i> Vista previa</a>
				<?php } ?>
				<input type="hidden" name="newmetas" id="newmetas" value="0">
				<input type="hidden" name="justvaluesin" value="<?php if(isset($postmeta)){ foreach($postmeta as $key){ echo $key->id.','; } }?>">
				<input type="hidden" name="id" value="<?php if(isset($post->id)) echo $post->id; ?>">
				<button class="btn btn-info submit" type="submit"> <i class="fa fa-save"></i> Guardar</button>
			</div>
		</div>
	</form>
	<div class="widget-foot">
	</div>
</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<link href="<?= base_url() ?>-/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="<?= base_url() ?>-/tinymce/tinymce.min.js"></script>
<link href="<?= base_url() ?>-/css/select2.css" type="text/css" rel="stylesheet" />

<script type="text/javascript">
	$(function () {
		$('#datepublish').datetimepicker({
			pickTime: false
		});
		$('#timepublish').datetimepicker({
			pickDate: false
		});
	});
	$(document).ready(function(){
		$('#description').each(function(){
			var longitud = $(this).val().length;
			var long_max = 255;
			$('#description_long').html('('+longitud+' - '+long_max+')');
			$(this).keyup(function(){
				var nueva_longitud = $(this).val().length;
				$('#description_long').html('('+nueva_longitud+' - '+long_max+')');
				if(nueva_longitud >= long_max){
					$('#description_long').css('color', '#ff0000');
				}else{
					$('#description_long').css('color', '#00F000');
				}
			});
		});
		$('#title').each(function(){
			var longitud = $(this).val().length;
			var long_max = 255;
			$('#title_long').html('('+longitud+' - '+long_max+')');
			$(this).keyup(function(){
				var nueva_longitud = $(this).val().length;
				$('#title_long').html('('+nueva_longitud+' - '+long_max+')');
				if(nueva_longitud >= long_max){
					$('#title_long').css('color', '#ff0000');
				}else{
					$('#title_long').css('color', '#00F000');
				}
			});
		});
		$('#keywords').each(function(){
			var longitud = $(this).val().length;
			var long_max = 255;
			$('#keywords_long').html('('+longitud+' - '+long_max+')');
			$(this).keyup(function(){
				var nueva_longitud = $(this).val().length;
				$('#keywords_long').html('('+nueva_longitud+' - '+long_max+')');
				if(nueva_longitud >= long_max){
					$('#keywords_long').css('color', '#ff0000');
				}else{
					$('#keywords_long').css('color', '#00F000');
				}
			});
		});
	});
	function del_image(id){
		var r = confirm('Por favor confirma el borrado');
		if(r == true){
			$.ajax({
				type:	'POST',
				url:	'<?= base_url() ?>dashboard-ajax/del_image',
				data:	'id='+id,
				success: function(result){
					$.gritter.add({
						text: result,
						sticky: false,
						time: '',
						class_name: 'gritter-custom'
					});
					$('#imagen_destacada').slideUp('slow', function(){});
				}
			});
		}
	}
	function del_cache_blog(slug){
		$.ajax({
			type:	'POST',
			url:	'<?= base_url() ?>dashboard-ajax/del-cache-blog',
			data: 'slug='+slug,
			success: function(result){
				$.gritter.add({
					text: result,
					sticky: false,
					time: '',
					class_name: 'gritter-custom'
				});
			}
		});
	}
	var confirmarSalir = true;
	window.onbeforeunload = confimarSalir;
	function confimarSalir(){
		if (confirmarSalir)
			return 'Está saliendo';
	}
</script>
<script type="text/javascript">
	$('.select-multiple').select2();
</script>

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<h1 class="pull-left"><i class="fa fa-file"></i> Contenido</h1>
				<div class="head-search pull-right">
					<form class="form" role="form">
						<div class="input-group">
							<input type="text" class="form-control" placeholder="Buscar..." id="buscar">
							<div class="result_messages oculto"></div>
							<span class="input-group-btn">
								<button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
							</span>
						</div>
					</form>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="page-posts">
				<div class="single-head">
					<div class="table-responsive">
						<table class="table table-bordered table-hover text-center" id="table-posts" >
							<thead>
								<tr>
									<th>Título</th>
									<th>Autor</th>
									<th>Tipo</th>
									<th>Fecha de publicación</th>
									<th>Estado</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach($posts as $post){
									?>
									<tr id="container_postid_<?= $post->id ?>">
										<td><strong><a href="<?= base_url().'preview/ver/'.$post->slug ?>" target="_blank"><?= $post->title ?></a></strong><br><?= $post->slug ?></td>
										<td><?= $post->name ?></td>
										<td>-
											<?php
											if($post->type == 1){
												foreach ($post->metas as $meta => $value) {
													if($value->metakey == 'ratingValue'){
														echo '<p><i class="fa fa-star yellow"></i> '.$value->metavalue.'/5 de ';
													}
													if($value->metakey == 'reviewCount'){
														echo $value->metavalue.' votos</p>';
													}
												}
											}
											?>
										</td>
										<td><?= $post->datepublish ?></td>
										<td><span class="badge badge-<?php if($post->status != 'borrador'){ echo 'success';} ?>"><?= $post->status ?></span></td>
										<td>
											<a class="btn btn-danger" onclick="delpost(<?= $post->id ?>,'container_postid_<?= $post->id ?>');"><i class="fa fa-trash-o"></i> </a>
											<input type="hidden" name="id" value="<?= $post->id ?>">
											<a class="btn btn-info" href="<?= base_url() ?>dashboard/post/<?= $post->id ?>"><i class="fa fa-pencil"></i> </a>
											<a class="btn btn-success" href="<?= base_url().'preview/ver/'.$post->slug ?>" target="_blank"><i class="fa fa-eye"></i> </a>
										</td>
									</tr>
									<?php
								}
								?>
							</tbody>
						</table>
					</div>
					<div class="row">
						<div class="col-md-12 text-center">
							<?php if( isset( $pagination )){ echo $pagination; } ?>
							<p>Mostrando <?= sizeof($posts) ?> de <?= $content_total ?></p>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#table-posts").tablesorter();
	});
</script>
<link href="<?= base_url() ?>-/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

<script type="text/javascript">
	$(document).ready(function(){
		$('#buscar').typing({
			start: function (event, $elem) {
				$('.result_messages').removeClass('oculto');
				$('.result_messages').empty();
				$('.result_messages').append('<ul><li>Buscando...</li></ul>');
			},
			stop: function (event, $elem) {
				var dataString='s='+$('#buscar').val();
				$.ajax({
					type:	'POST',
					url:	'<?= base_url() ?>/dashboard_ajax/looking_for_content',
					data: dataString,
					success: function(result){
						$('.result_messages').empty();
						$('.result_messages').removeClass('oculto');
						$('.result_messages').append(result);
					}
				});
			},
			delay: 400
		});
	});
</script>
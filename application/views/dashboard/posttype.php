<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<h1 class="pull-left"><i class="fa fa-th-large"></i> Tipo de contenido</h1>
				<div class="clearfix"></div>
			</div>
			<div class="page-form">
				<form class="form-horizontal" role="form" method="post" action="<?= base_url() ?>dashboard/posttype" >
					<div class="form-group">
						<label class="col-lg-2 control-label">Nombre</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" placeholder="Nombre" name="name" value="<?php $t = !empty($posttype->name)?$posttype->name:null; echo set_value('name',$t); ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Tipo</label>
						<div class="col-lg-10">
							<input type="type" class="form-control" name="type" value="<?php $t = !empty($posttype->type)?$posttype->type:null; echo set_value('type',$t); ?>">
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">Meta</label>
						<div class="col-lg-10">
							<input type="text" class="form-control" placeholder="value" name="value" value="<?php $t = !empty($posttype->value)?$posttype->value:null; echo set_value('value',$t); ?>">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<input type="hidden" name="id" value="<?php $t = !empty($posttype->id)?$posttype->id:null; echo set_value('id',$t); ?>">
							<button type="submit" class="btn btn-default"><i class="fa fa-save"></i> Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
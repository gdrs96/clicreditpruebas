<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<div class="single-head">
					<h1 class="pull-left"><i class="fa fa-th-large"></i> Tipos de contenido </h1>
					<div class="clearfix"></div>
				</div>
				<div class="page-media">
					<div class="table-responsive">
						<table class="table table-bordered table-hover text-center" id="table-posttype">
							<thead>
								<tr>
									<th>Nombre</th>
									<th>Tipo</th>
									<th>Meta</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if($posttypes){
									foreach ($posttypes as $key){
										?>
										<tr id="container_posttypeid_<?= $key->id ?>">
											<td><?php if( empty( $key->nicename )) echo $key->name; else echo $key->name ?></td>
											<td><?= $key->type; ?></td>
											<td><?= $key->value; ?></td>
											<td>
												<a class="btn btn-danger" onclick="del(<?= $key->id ?>,'container_posttypeid_<?= $key->id ?>','posttype');"><i class="fa fa-trash-o"></i> </a>
												<a href="<?= base_url().'dashboard/posttype/'.$key->id ?>" class="btn btn-info"><i class="fa fa-pencil"></i> </a>
											</td>
										</tr>
										<?php }
									} ?>
								</tbody>
							</table>
						</div>
					</div>
					<div class="widget-foot">
						<span class="pull-left">
							<a href="<?= base_url() ?>dashboard/posttype" class="btn btn-default"><i class="fa fa-plus"></i> Crear</a> &nbsp;
						</span>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
</div>

<?php
include 'js-files.php';
?>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#table-posttype").tablesorter();
	});
</script>

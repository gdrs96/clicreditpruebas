<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<h2><i class="fa fa-user"></i> Usuario</h2>
				<div class="clearfix"></div>
			</div>
			<div class="page-form">
				<form class="form-horizontal" role="form" method="post" enctype="multipart/form-data" action="<?= base_url() ?>dashboard/user" >
					<div class="row">
						<div class="col-lg-7">
							<div class="form-row align-items-end mb-3">
								<label class="col-lg-4 control-label" for="name">Username</label>
								<div class="col-lg-8">
									<input type="text" class="form-control" placeholder="Username" name="name" value="<?php $t = !empty($user->name)?$user->name:null; echo set_value('name',$t); ?>">
								</div>
							</div>
							<div class="form-row align-items-end mb-3">
								<label class="col-lg-4 control-label" for="nicename">Nombre</label>
								<div class="col-lg-8">
									<input type="text" class="form-control" placeholder="Nombre" name="nicename" value="<?php $t = !empty($user->nicename)?$user->nicename:null; echo set_value('nicename',$t); ?>">
								</div>
							</div>
							
								<input type="hidden" id="id_calificacion" name="id_calificacion" value="<?php $t = !empty($calificacion[0]->id)?$calificacion[0]->id:null; echo set_value('id_calificacion',$t); ?>">
							<div class="form-row align-items-end mb-3">
								<label class="col-lg-4 control-label" for="email">Email</label>
								<div class="col-lg-8">
									<input type="email" class="form-control" placeholder="email" name="email" value="<?php $t = !empty($user->email)?$user->email:null; echo set_value('email',$t); ?>">
								</div>
							</div>
							<div class="form-row align-items-end mb-3">
								<label class="col-lg-4 control-label" for="status">Estado</label>
								<div class="col-lg-8">
									<select class="form-control" name="status">
										<option value="activo" <?php if( !empty($user->status)){ if($user->status == 'activo' ){ echo 'selected'; } } ?>>activo</option>
										<option value="inactivo" <?php if( !empty($user->status)){ if($user->status == 'inactivo' ){ echo 'selected'; } } ?>>inactivo</option>
									</select>
								</div>
							</div>
							<div class="form-row align-items-end mb-3">
								<label class="col-lg-4 control-label" for="rol">Rol</label>
								<div class="col-lg-8">
									<select class="form-control" id="rol" name="rol">
										<?php foreach($roles as $role) : ?>
											<option value="<?= $role->id ?>"><?= $role->name ?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<input type="hidden" id="id_rol" value="<?php $t = !empty($user->id_rol)?$user->id_rol:null; echo set_value('id_rol',$t); ?>">
							</div>
							<div class="form-row align-items-end mb-3">
										<label class="col-lg-4 control-label" for="email">Calificación</label>
										<div class="col-lg-8">
											<select class="form-control" id="calificacion" name="calificacion">
											<?php foreach($calificaciones as $calificacion):?>
                                                <option value="<?php echo $calificacion->id;?>"><?php echo $calificacion->id;?></option>
                                               <?php endforeach; ?>
											</select>
										</div>
									</div>
							<div class="form-row align-items-end mb-3">
								<label class="control-label col-lg-4">Cambiar Foto</label>
								<div class="col-lg-8 custom-file">
									<input type="file" class="custom-file-input" id="image" name="userfile">
									<label class="custom-file-label" for="image">Actualizar imagen...</label>
								</div>
							</div>
							<div class="form-row align-items-end mb-3">
								<label class="control-label col-lg-4">Cambiar Contraseña</label>
								<div class="col-lg-8">
									<input type="password" class="form-control" placeholder="Nueva contraseña" id="pass" name="pass">
								</div>
							</div>
							<div class="form-row align-items-end mb-3">
								<div class="col-lg-4"></div>
								<div class="col-lg-8">
									<input type="password" class="form-control" placeholder="Repita su nueva contraseña" id="pass1" name="pass1">
								</div>
							</div>
						</div>
						<div class="form-group col-lg-5 center-block">
							<?php if( isset($user->image) && file_exists($user->image)){ ?>
							<img style="max-height:320px" src="<?= base_url().$user->image ?>" alt="Foto de Perfil" class="img-fluid rounded" id="user_image"/>
							<br><br>
						<?php }
						if( ! empty($user->image)){ ?>
							<button class="btn btn-success" type="button" onclick="user_image_del(<?php $t = isset($user->id)?$user->id:null;echo set_value('id',$t); ?>)"><i class="fa fa-trash"></i> Borrar foto</button>
						</div>
						<?php } ?>
					</div>
					<div class="form-group">
						<div class="col-md-offset-2 col-md-10">
							<input type="hidden" name="id" value="<?php if(isset($user->id)) echo $user->id; ?>">
							<button class="btn btn-info" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
</div>
<?php
include 'js-files.php';
?>
<link href="<?= base_url() ?>-/css/select2.css" type="text/css" rel="stylesheet" />
<script type="text/javascript">
	$('.select-multiple').select2({placeholder: "Roles"});
</script>
<script type="text/javascript">
	function user_image_del(id){
		var r = confirm('Por favor confirma el borrado');
		if(r == true){
			$.ajax({
				type:	'POST',
				url:	'<?= base_url() ?>dashboard-ajax/user-image-del/'+id,
				success: function(result){
					$.gritter.add({
						text: result,
						sticky: false,
						time: '',
						class_name: 'gritter-custom'
					});
					$('#user_image').slideUp('slow', function(){});
				}
			});
		}
	}



	   $(document).ready(function(){         
        var id_calificacion =  $('#id_calificacion').val();
        var id_rol = $('#id_rol').val();
		$('#calificacion').val(id_calificacion);
		$('#rol').val(id_rol);
       });
</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<div class="single-head">
					<h1 class="pull-left"><i class="fa fa-user"></i> Usuarios Online</h1>
					<div class="clearfix"></div>
				</div>
				<div class="page-media">
					<div class="table-responsive">
						<table class="table table-bordered table-hover text-center" id="table-users">
							<thead>
								<tr>
									<th>Email</th>
									<th>IP</th>
									<th>Última vez conectado</th>
									<th>Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($users_online as $key){
									?>
									<tr>
										<td><?= (isset($key['email'])) ? $key['email'] : 'Anónimo'; ?></td>
										<td><?= (isset($key['ip'])) ? $key['ip'] : '0.0.0.0'; ?></td>
										<td><?= (isset($key['last_activity'])) ? $key['last_activity'] : '--'; ?></td>
										<td>
											<a onclick="cerrar_sesion('<?= (isset($key['id'])) ? $key['id'] : ''; ?>','<?= (isset($key['user_id'])) ? $key['user_id'] : ''; ?>')" class="btn btn-success"><i class="fa fa-times"></i></a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
						<div class="row">
							<div class="col-md-12 text-center">
								<?php if( isset( $pagination )){ echo $pagination; } ?>
								<br/>Viendo <?= sizeof($users_online) ?> de un total de <?= $users_online_total ?>
							</div>
						</div>
					</div>
					<div class="widget-foot">
						<span class="pull-left">
							<a href="<?= base_url() ?>dashboard/users-online" class="btn btn-default"><i class="fa fa-refresh"></i> Actualizar</a> &nbsp;
						</span>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$("#table-users").tablesorter();
	});
	function cerrar_sesion(id_session,user_id){
		console.log(user_id);
		$.ajax({
			type:	'POST',
			url:	'<?= base_url() ?>dashboard-ajax/cerrar-sesion/',
			data: {'id_session':id_session, 'user_id':user_id},
			dataType : 'json',
			success: function(result){
				$.gritter.add({
					text: result,
					sticky: false,
					time: '',
					class_name: 'gritter-custom'
				});
			}
		});
	}
</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
  <div class="container">
    <div class="page-content">
      <div class="single-head">
        <div class="single-head">
          <h2><i class="fa fa-user"></i> Usuarios </h2>
          <div class="clearfix"></div>
        </div>


      
        <div class="page-media">
          <div class="table-responsive">
            <table class="table table-bordered table-hover text-center" id="table-users">
              <thead>
                <tr>
                  <th>Nombre</th>
                  <th>Roles</th>
                  <th>Email</th>
                  <th>Estado</th>
                  <th>Registrado</th>
                  <th>Último acceso</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($users as $key){ ?>

                  <?php foreach ($key as $value){ ?>
                  <tr >
                    <td><?php if( empty( $value->nicename )) echo  $value->name; else echo $value->name ?></td>
                    <td><?= $value->name_rol ?></td>
                    <td><?=  $value->email; ?></td>
                    <td><span
                      <?php if( $value->status == "activo"){ echo 'class="badge badge-success"> Activo'; }else{ echo 'class="badge badge-danger"> Inactivo'; } ?></span></td>
                      <td><?=  $value->dateregistered; ?></td>
                      <td><?=  $value->datelastlogin; ?></td>
                      <td>
                        <?php if( $value->status == "activo"){ ?>
                        <a class="btn btn-success" onclick="block_user(<?=  $value->id ?>,'container_userid_<?= $value->id ?>');"><i class="fa fa-lock"></i> </a>
                        <a href="<?= base_url() ?>dashboard/user/<?= $value->id ?>" class="btn btn-info"><i class="fa fa-pencil"></i> </a>
                        <?php } ?>
                        <?php if( $value->status == "inactivo"){ ?>
                        <a class="btn btn-success" onclick="unBlock_user(<?=  $value->id ?>,'container_userid_<?= $value->id ?>');"><i class="fa fa-unlock"></i> </a>
                        <a href="<?= base_url() ?>dashboard/user/<?= $value->id ?>" class="btn btn-info"><i class="fa fa-pencil"></i> </a>
                        <?php } ?>
                      </td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              
            </div>
            <div class="widget-foot">
              <span class="pull-left">
                <a href="<?= base_url() ?>dashboard/user" class="btn btn-default"><i class="fa fa-plus"></i> Añadir usuario</a> &nbsp;
              </span>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/datatable.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">
<script src="<?= base_url() ?>js/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
  urlBlockUser = "<?php echo site_url('dashboard_ajax/block_user')?>/";
  urlUnBlockUser = "<?php echo site_url('dashboard_ajax/unBlock_user')?>/";
</script>
<script type="text/javascript">
  $(document).ready(function() {
    //$("#table-users").tablesorter();
    var tableUsers = $('#table-users').DataTable({
               // "bPaginate": false,
                    //  "bLengthChange": false,
                   // "bSearch": false,
                   // "bFilter": false,
                    "bInfo": false,
                      "bAutoWidth": false,
                      "processing": true, 
                       //"lengthMenu":     "Show _MENU_ entries",
                      "language": {
                        "zeroRecords": "No hay usuarios",
                        "search": "Buscar:",
                        "paginate": {
                         "next":       "Siguiente",
                         "previous":   "Anterior"
    },
                     }});
  });


  function block_user(id_user){
       swal({
                title: "¿Desea bloquear este usuario?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

             
                if (isConfirm) {
                   $.ajax({
                    url : urlBlockUser+id_user,
                    type: "POST",
                    dataType: "JSON",
                   success: function(data)
                   {
                    
                      if(data.status) //if success close modal and reload ajax table
                       {


                        swal({
                        title: 'Usuario bloqueado!',
                        text: '',
                        type: 'success'
                          }, function() {
                              window.location.reload();
                          });
                       }else{


                       swal("El usuario no pudo ser bloqueado intente más tarde", "", "error");

                       }
           },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
         
 
        }
    });
         } else {
                    swal("Acción cancelada", "", "warning");
                }
            });
   
   
}


function unBlock_user(id_user){
       swal({
                title: "¿Desea desbloquear este usuario?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

             
                if (isConfirm) {
                   $.ajax({
                    url : urlUnBlockUser+id_user,
                    type: "POST",
                    //data: data,
                   dataType: "JSON",
                   success: function(data)
                   {
                    
                      if(data.status) //if success close modal and reload ajax table
                       {


                        swal({
                        title: 'Usuario desbloqueado!',
                        text: '',
                        type: 'success'
                          }, function() {
                              window.location.reload();
                          });
                       }else{


                       swal("El usuario no pudo ser desbloqueado intente más tarde", "", "error");

                       }
           },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
         
 
        }
    });
         } else {
                    swal("Acción cancelada", "", "warning");
                }
            });
   
   
}


/*busqueda sobre input codigo de transaccion*/
/*$('#search').on( 'keyup', function () {
    tableUsers.columns(0).search(this.value).draw();
});  */
</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
        <h2>
          Archivo de <strong>VERSIONES</strong> 
          <a href="<?= base_url() ?>dashboard/version" class="btn btn-mi-cuenta ml-3" style="background-color: #5EAC25">REGISTRAR nueva versión</a>
          <a href="<?= base_url() ?>dashboard/versiones" class="btn btn-mi-cuenta ml-3">HISTÓRICO de versiones</a>
        </h2>
				<div class="clearfix"></div>
			</div>
			<div class="page-form">
				<form class="form row" role="form" method="post" enctype="multipart/form-data" action="<?= base_url() ?>dashboard/version" >
					<div class="col-6">
            <div class="form-group">
              <label class="control-label" for="name">Nueva VERSIÓN:</label>
              <input type="text" class="form-control" placeholder="0.1.0" name="number_version" value="<?php $t = !empty($version->number_version)?$version->number_version:null; echo set_value('number',$t); ?>">
            </div>
            <div class="form-group">
              <label class="control-label" for="name">Nombre VERSIÓN:</label>
              <input type="text" class="form-control" placeholder="Nombre" name="name_version" value="<?php $t = !empty($version->name_version)?$version->name_version:null; echo set_value('name',$t); ?>">
            </div>
            <div class="form-group">
              <label class="control-label" for="name">VERSIÓN BD:</label>
              <input type="text" class="form-control" placeholder="2.0" name="version_db" value="<?php $t = !empty($version->version_db)?$version->version_db:null; echo set_value('db',$t); ?>">
            </div>
            <div class="form-group">
              <label class="control-label" for="name">REGISTRADO por:</label>
              <input type="text" class="form-control" placeholder="Autor" name="author_version" value="<?php $t = !empty($version->author_version)?$version->author_version:null; echo set_value('author',$t); ?>">
            </div>
          </div>
          <div class="col-6 d-flex flex-column">
            <div class="form-group">
              <label class="control-label" for="name">Enlace a CÓDIGO</label>
              <input type="text" class="form-control" placeholder="link" name="code_version" value="<?php $t = !empty($version->code_version)?$version->code_version:null; echo set_value('code',$t); ?>">
            </div>
            <div class="form-group">
              <label class="control-label" for="name">Enlace a REGISTRO DE CAMBIOS</label>
              <input type="text" class="form-control" placeholder="link" name="log_version" value="<?php $t = !empty($version->log_version)?$version->log_version:null; echo set_value('log',$t); ?>">
            </div>
            <div class="form-group mt-auto">
              <input type="hidden" name="id_version" value="<?php $t = !empty($version->id_version)?$version->id_version:null; echo set_value('id',$t); ?>">							
              <button class="btn btn-info" type="submit">Crear</button>
            </div>
          </div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<script type="text/javascript">
	$(document).ready(function() {
	});
</script>
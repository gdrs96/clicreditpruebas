<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-content">
	<div class="container">
		<div class="page-content">
			<div class="single-head">
				<div class="single-head">
					<h2>
            Archivo de <strong>VERSIONES</strong>
            <a href="<?= base_url() ?>dashboard/version" class="btn btn-mi-cuenta ml-3">REGISTRAR nueva versión</a>
            <a href="<?= base_url() ?>dashboard/versiones" class="btn btn-mi-cuenta ml-3" style="background-color: #5EAC25">HISTÓRICO de versiones</a>
          </h2>
					<div class="clearfix"></div>
				</div>
				<div class="page-media">
					<div class="table-responsive">
						<table class="table table-bordered table-hover text-center" id="table-groups">
							<thead>
								<tr>
									<th>Número de versión</th>
									<th>Fecha</th>
									<th>Nombre de versión</th>
                  <th>Registro de cambios</th>
                  <th>Versión BD</th>
                  <th>Acciones</th>
								</tr>
							</thead>
							<tbody>
								<?php
								if(!empty($versions)){
									foreach ($versions as $key){
										?>
										<tr id="container_groupid_<?= $key->id_version ?>">
											<td><a href="<?= $key->code_version; ?>"><?= $key->number_version; ?></a></td>
                      <td><?= date('d-m-y', strtotime($key->date_registered)); ?></td>
											<td><?= $key->name_version; ?></td>
                      <td><a href="<?= $key->log_version; ?>">Documentación</a></td>
                      <td><?= $key->version_db; ?></td>
											<td>
												<a href="<?= base_url() ?>dashboard/version/<?= $key->id_version ?>" class="btn btn-info"><i class="fa fa-pencil"></i> </a>
											</td>
										</tr>
										<?php
									}
								} ?>
							</tbody>
						</table>
					</div>
				</div>
        <div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
</div>
<div class="clearfix"></div>
</div>
<?php
include 'js-files.php';
?>
<script src="<?= base_url() ?>-/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
	// $(document).ready(function() {
	// 	$("#table-groups").tablesorter();
	// });
</script>
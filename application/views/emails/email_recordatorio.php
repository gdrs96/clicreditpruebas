<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="es-PA" xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="x-apple-disable-message-reformatting">
    <!--<link rel="stylesheet" media="all" href="<?= base_url() ?>css/emailStyles.css">-->

    <style>
 /* -------------------------------------
    GLOBAL
    A very basic CSS reset
------------------------------------- */
* {
    margin: 0;
    padding: 0;
    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    box-sizing: border-box;
    font-size: 14px;
    color :#000000;
    text-align: justify;
}

img {
    max-width: 100%;
}

body {
    -webkit-font-smoothing: antialiased;
    -webkit-text-size-adjust: none;
    width: 100% !important;
    height: 100%;
    line-height: 1.6;
}

/* Let's make sure all tables have defaults */
table td {
    vertical-align: top;
}

/* -------------------------------------
    BODY & CONTAINER
------------------------------------- */
body {
    background-color: #f6f6f6;
}

.body-wrap {
    background-color: #f6f6f6;
    width: 100%;
}

.container {
    display: block !important;
    max-width: 600px !important;
    margin: 0 auto !important;
    /* makes it centered */
    clear: both !important;
}

.content {
    max-width: 600px;
    margin: 0 auto;
    display: block;
    padding: 20px;
}

/* -------------------------------------
    HEADER, FOOTER, MAIN
------------------------------------- */
.main {
    background: #fff;
    border: 1px solid #e9e9e9;
    border-radius: 3px;
    background-color:#F2F2F2;
    margin:auto;
}

.content-wrap {
    padding: 17px;
}

.content-block {
    padding: 0 0 20px;
}

.header {
    width: 100%;
    margin-bottom: 20px;
}

.footer {
    width: 100%;
    clear: both;
    color: #999;
    padding: 20px;
}
.footer a {
    color: #999;
}
.footer p, .footer a, .footer unsubscribe, .footer td {
    font-size: 12px;
}

/* -------------------------------------
    TYPOGRAPHY
------------------------------------- */
h1, h2, h3 {
    font-family: "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
    color: #000;
    margin: 15px 0 0;
    line-height: 1.2;
    font-weight: 400;
}

h1 {
    font-size: 32px;
    font-weight: 500;
}

h2 {
    font-size: 24px;
}

h3 {
    font-size: 18px !important;
    color:#4872B0 !important;
    font-weight: bold !important;
}

h4 {
    font-size: 14px;
    font-weight: 600;
}

p, ul, ol {
    margin-bottom: 10px;
    font-weight: normal;
}
p li, ul li, ol li {
    margin-left: 5px;
    list-style-position: inside;
}

/* -------------------------------------
    LINKS & BUTTONS
------------------------------------- */
a.class1 {
    color: #7AAA38 !important;
    font-weight: bold !important;
    text-decoration: underline !important;
}


a.class2 {
     color:#FFFFFF !important;
    font-weight: bold !important;
   
}


.btn-primary {
    text-decoration: none;
    color: #FFF;
    background-color: #1ab394;
    border: solid #1ab394;
    border-width: 5px 10px;
    line-height: 2;
    font-weight: bold;
    text-align: center;
    cursor: pointer;
    display: inline-block;
    border-radius: 5px;
    text-transform: capitalize;
}

/* -------------------------------------
    OTHER STYLES THAT MIGHT BE USEFUL
------------------------------------- */
.last {
    margin-bottom: 0;
}

.first {
    margin-top: 0;
}

.aligncenter {
    text-align: center;
}

.alignright {
    text-align: right;
}

.alignleft {
    text-align: left;
}

.clear {
    clear: both;
}

/* -------------------------------------
    ALERTS
    Change the class depending on warning email, good email or bad email
------------------------------------- */
.alert {
    font-size: 16px;
    color: #fff;
    font-weight: 500;
    padding: 20px;
    text-align: center;
    border-radius: 3px 3px 0 0;
}
.alert a {
    color: #fff;
    text-decoration: none;
    font-weight: 500;
    font-size: 16px;
}
.alert.alert-warning {
    background: #f8ac59;
}
.alert.alert-bad {
    background: #ed5565;
}
.alert.alert-good {
    background: #1ab394;
}

/* -------------------------------------
    INVOICE
    Styles for the billing table
------------------------------------- */
.invoice {
    margin: 40px auto;
    text-align: left;
    width: 80%;
}
.invoice td {
    padding: 5px 0;
}
.invoice .invoice-items {
    width: 100%;
}
.invoice .invoice-items td {
    border-top: #eee 1px solid;
}
.invoice .invoice-items .total td {
    border-top: 2px solid #333;
    border-bottom: 2px solid #333;
    font-weight: 700;
}

/* -------------------------------------
    RESPONSIVE AND MOBILE FRIENDLY STYLES
------------------------------------- */
@media only screen and (min-width: 500px) {
    h1, h2, h3, h4 {
        font-weight: bold !important;
        margin: 20px 0 5px !important;
    }

    h1 {
        font-size: 22px !important;
    }

    h2 {
        font-size: 18px !important;
    }

    h3 {
        color:#265c96 !important;
        font-size: 16px !important;
        font-weight: bold !important;
    }

    .container {
        width: 100% !important;
    }

    .content, .content-wrap {
        padding: 12px !important;

    }
    .textbold{
         color:#265c96 !important;
        font-weight: bold !important;
    }

    .invoice {
        width: 100% !important;
    }
}

.text-footer {
   
    font-size: 12px;
    color:#FFFFFF;
    
    
}

.text-blue-bold{
    color: #265c96;
    font-weight: bold;
}

.text-red-bold{
    color: #FF0000;
    font-weight: bold;
}

.text-highlight{
      color:#FFFFFF !important;
      font-size: 14px !important;
}

hr{
    height: 1px;
    border:none;
    background-color: #4872B0;

}

.padding-td{
    padding-bottom: 0px;

}

.padding-td-text{
    padding-top: -50px;
}


tfoot{
    background-color: #4872B0;
}




.content-wrap-tfoot-admin{
    padding: 4px;
} 

</style>

</head>

<body>
<table style="background-color: #f6f6f6;
    width: 100%;">
    <tr>
      <td></td>
        <td class="container" style="display: block !important; max-width: 600px !important;
                                     margin: 0 auto !important; clear: both !important;" width="600">
            <div class="content" style="max-width: 600px;
                        margin: 0 auto;
                        display: block;
                        padding: 20px;">
            
            <table style="background: #fff;
                    border: 1px solid #e9e9e9;
                           border-radius: 3px;
                           background-color:#F2F2F2;
                           margin:auto;" width="100%" cellpadding="0" cellspacing="0" >
                    <tr>
                        <td class="content-wrap" style="padding: 17px;">
                            <table  cellpadding="0" cellspacing="0" >
                                <tr style="padding: 0 0 20px;">
                                    <td >
                                        <img src="<?= base_url() ?>img/clicredit-logo-email.png">
                                       <br>
                                       <hr>
                                    </td>

                                </tr>
                                 
                                <tr style="padding: 0 0 20px;">
                                    <td>
                                        <h3>Hola!, <?= $data->name ?></h3>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 0 20px;">
                                       Aprovechamos esta comunicación para saludarle, en primer lugar, y recordarle la importancia de hacer los pagos a tiempo. 
                                       <br>
                                       <br>
                                       El monto de la próxima cuota es de <b class="textbold"><?= $data->cuota ?> US$</b>, la cual deberá cancelarse  el  día <br> <b class="textbold"><?= date('d-m-Y', strtotime($data->fecha_vencimiento))  ?></b>.
                                       <br>
                                       <br>
                                       Nuestro sistema evalúa a cada cliente en diversos puntos. La puntualidad de los pagos es una variable importante que hará que sus próximos préstamos sean más económicos. 
                                   </td>
                                </tr>
                                <tr>
                                    <td style="padding: 0 0 20px;">
                                    </td>
                                </tr>
                            </table>
                             </td>
                          </tr>
                            <tfoot style="background-color: #4872B0">
                               <tr>
                                 <td style="padding: 17px;">
                                    <p style=" font-size: 12px; color:#FFFFFF;">Si tienes dudas o preguntas, contáctanos a:<br> <a style="color:#FFFFFF;font-weight: bold;">ayuda@pa.clicredit.com</a>, escríbenos por <b style="color:#FFFFFF; font-size:14px;">WhatsApp</b> o llámanos al <a style="color:#FFFFFF;font-weight: bold;" href="tel:+5072608951" title="llamar a Clicredit">260-8951</a></p>
                                 </td>
                                </tr>
                          </tfoot>
                </table>
            </td>
        <td></td>
    </tr>
</table>

</body>
</html>

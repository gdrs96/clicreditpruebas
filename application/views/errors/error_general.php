<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $heading; ?></title>
</head>
<body>
	<div class="emoticon" style="font-weight: bold; font-size: 200px; font-family: Arial, Helvetica, sans-serif; float: left; width: 150px; color: #0097c8; letter-spacing: -2px; position: absolute; z-index: 5;">:(</div>
	<h1 style="margin: 0px; padding-top: 42px; font-size: 90px; line-height: 90px; font-family: 'Palatino Linotype', 'Book Antiqua', Palatino, serif; color: #111111; letter-spacing: -5px; padding-left: 150px;"><?php echo $heading; ?></h1>
	<h2 style="margin: 0px; font-weight: normal; font-size: 28px; line-height: 35px; font-family: 'Lucida sans unicode', 'Lucida Grande', Arial, Helvetica, Garuda, sans-serif; text-transform: uppercase; letter-spacing: -1px; padding: 0px 0px 14px 150px; color: #0097c8;"><?php echo $message; ?><b><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']; ?>">Salir de aquí.</a></b></h2>

</body>
</html>
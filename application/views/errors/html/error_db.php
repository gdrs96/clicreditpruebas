<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo $heading; ?></title>
</head>
<body>
	<div class="emoticon" style="font-weight: bold; font-size: 200px;float: left; width: 150px; color: #0097c8; letter-spacing: -2px; position: absolute; z-index: 5;">:(</div>
	<h1><?php echo $heading; ?></h1>
	<h2><?php echo $message; ?></h2>
	<b><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']; ?>">Salir de aquí</a></b>
</body>
</html>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>An uncaught Exception was encountered</title>
</head>
<body>
	<div class="emoticon" style="font-weight: bold; font-size: 200px; float: left; width: 150px; color: #0097c8; letter-spacing: -2px; position: absolute; z-index: 5;">:(</div>
	<h1 style="margin: 0px; padding-top: 42px; font-size: 90px; line-height: 90px; color: #111111; letter-spacing: -5px; padding-left: 150px;">An uncaught Exception was encountered</h1>
	<h2 style="margin: 0px; font-weight: normal; font-size: 28px; line-height: 35px; letter-spacing: -1px; padding: 0px 0px 14px 150px; color: #0097c8;"><?php echo $message; ?><b><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']; ?>">Salir de aquí.</a></b></h2>
	<h4>An uncaught Exception was encountered</h4>
	<p>Type: <?php echo get_class($exception); ?></p>
	<p>Message: <?php echo $message; ?></p>
	<p>Filename: <?php echo $exception->getFile(); ?></p>
	<p>Line Number: <?php echo $exception->getLine(); ?></p>
	<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>
		<p>Backtrace:</p>
		<?php foreach ($exception->getTrace() as $error): ?>
			<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>
				<p style="margin-left:10px">
					File: <?php echo $error['file']; ?><br />
					Line: <?php echo $error['line']; ?><br />
					Function: <?php echo $error['function']; ?>
				</p>
			<?php endif ?>
		<?php endforeach ?>
	<?php endif ?>
</body>
</html>
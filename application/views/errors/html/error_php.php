<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Hemos encontrado un error</title>
</head>
<body>
	<h1 style="margin: 0px;font-size: 36px;color: #111111"><div style="font-weight: bold;font-size: 58px;float: right;color: #0097c8;position: relative;left: -195px;top: 65px;">:(</div> Hemos encontrado un error</h1>
	<br>
	<div style="border: 1px solid #09C;padding: 16px;">
		<h4>A PHP Error was encountered</h4>
		<p>Severity: <?php echo $severity; ?></p>
		<p>Mensaje:  <?php echo $message; ?></p>
		<p>Arhivo: <?php echo $filepath; ?></p>
		<p>Línea Número: <?php echo $line; ?></p>
	</div>
	<?php if (defined('SHOW_DEBUG_BACKTRACE') && SHOW_DEBUG_BACKTRACE === TRUE): ?>
		<p>Backtrace:</p>
		<?php foreach (debug_backtrace() as $error): ?>
			<?php if (isset($error['file']) && strpos($error['file'], realpath(BASEPATH)) !== 0): ?>
				<p style="margin-left:10px">
					File: <?php echo $error['file'] ?><br />
					Line: <?php echo $error['line'] ?><br />
					Function: <?php echo $error['function'] ?>
				</p>
			<?php endif ?>
		<?php endforeach ?>
	<?php endif ?>
	<h4><a href="<?php echo 'http://'.$_SERVER['SERVER_NAME']; ?>">Salir de aquí</a></b></h4>
</body>
</html>
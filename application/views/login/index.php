<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="outer-page">
	<div class="login-page">
		<ul class="nav nav-tabs nav-justified">
			<li class="active"><a href="#login" data-toggle="tab" class="br-lblue"><i class="fa fa-sign-in"></i> Entrar</a></li>
<?php /*
					<li><a href="#register" data-toggle="tab" class="br-lblue"><i class="fa fa-pencil"></i> Registro</a></li>
*/ ?>
<li><a href="#contact" data-toggle="tab" class="br-lblue"><i class="fa fa-envelope"></i> Contacto</a></li>
</ul>
<div class="tab-content">
	<div class="tab-pane fade active in" id="login">
		<?php
		if( isset($info) ){
			?>
			<div class="alert alert-info"><strong><?= $info ?></strong></div>
			<?php
		}
		if( isset($error) ){
			?>
			<div class="alert alert-danger"><strong><?= $error ?></strong></div>
			<?php
		}
		?>
		<form role="form" method="post" id="login_form" action="<?= base_url() ?>login/login_in">
			<div class="form-group">
				<label for="email">Email</label>
				<input type="text" class="form-control" id="email" placeholder="Email" name="email" value="<?= set_value('email') ?>" required>
			</div>
			<div class="form-group">
				<label for="password">Contraseña</label>
				<input type="password" class="form-control" id="password" placeholder="Contraseña" name="password" value="<?= set_value('password') ?>" required>
			</div>
			<div class="text-center">
				<button type="reset" class="btn btn-default">Reset</button>
				<button type="submit" class="btn btn-info">Entrar</button>
			</div>
		</form>
	</div>
	<?php
/*
					<div class="tab-pane fade" id="register">
					<form role="form" action="">
						<div class="form-group">
						<label for="name">Nombre</label>
						<input type="text" class="form-control" id="name" placeholder="Nombre">
						</div>
						<div class="form-group">
						<label for="email_register">Email</label>
						<input type="text" class="form-control" id="email_register" name="email_register" placeholder="Email">
						</div>
						<div class="form-group">
						<label for="password_register">Contraseña</label>
						<input type="password" class="form-control" id="password_register" placeholder="Contraseña" name="password_register">
						</div>
						<div class="form-group">
						<label for="password2_register">Confirmar Password</label>
						<input type="password" class="form-control" id="password2_register" placeholder="Repite tu contraseña" name="password2_register">
						</div>
						<div class="checkbox">
						<label>
							<input type="checkbox"> Acepto <a href="#">Terminos y Condiciones</a>
						</label>
						</div>
						<button type="submit" class="btn btn-info btn-sm">Registro</button>
						<button type="reset" class="btn btn-default btn-sm">Reset</button>
					</form>

					</div>
*/
					?>
					<div class="tab-pane fade" id="contact">
						<form role="form" method="post" action="<?= base_url() ?>dashboard/formcontacto2">
							<div class="form-group">
								<label for="name_contact">Nombre</label>
								<input type="text" class="form-control" id="name_contact" placeholder="Nombre" name="name_contact">
							</div>
							<div class="form-group">
								<label for="email_contact">Email</label>
								<input type="email" class="form-control" id="email_contact" placeholder="Email" name="email_contact">
							</div>
							<div class="form-group">
								<label for="asunto_contact">Asunto</label>
								<input type="text" class="form-control" id="text_contact" placeholder="Asunto" name="asunto_contact">
							</div>
							<div class="form-group">
								<label for="mensaje">Mensaje</label>
								<textarea rows="3" class="form-control" id="mensaje" name="mensaje"></textarea>
							</div>
							<div class="text-center">
								<button type="reset" class="btn btn-default">Reset</button>
								<button type="submit" class="btn btn-info">Enviar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<script src="<?= base_url().'-/js/'; ?>jquery.js"></script>
		<script src="<?= base_url().'-/js/'; ?>bootstrap.min.js"></script>
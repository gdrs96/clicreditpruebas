<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- <section class="page-content"> -->
	<div class="container container-error">
		<svg version="1.1"
		xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
		x="0px" y="0px" width="113.5px" height="164.4px" viewBox="0 0 113.5 164.4" style="enable-background:new 0 0 113.5 164.4;"
		xml:space="preserve" class="svg">
		<style type="text/css">
			.st0{opacity:0.13;fill:#265C96;}
			.st1{fill:#265C96;}
			.st2{fill:none;stroke:#265C96;stroke-miterlimit:10;stroke-dasharray:4,3;}
			.st3{fill:#FFFFFF;stroke:#265C96;stroke-miterlimit:10;}
			.st4{fill:#E4EDF4;}
			.st5{fill:none;stroke:#265C96;stroke-width:2;stroke-miterlimit:10;}
		</style>
		<defs>
		</defs>
		<g>
			<ellipse id="shadow" class="st0" cx="56.7" cy="160.6" rx="56.7" ry="3.9"/>
			<g id="eyes">
				<circle class="st1" cx="28.7" cy="59.5" r="2.8"/>
				<circle class="st1" cx="85.1" cy="59.5" r="2.8"/>
			</g>
			<rect id="mouth" x="48.8" y="88.2" class="st1" width="15.9" height="2.2"/>
			<circle class="st1" cx="33.1" cy="127.6" r="2.8"/>
			<circle class="st1" cx="96.7" cy="148.2" r="1.9"/>
			<path class="st1" d="M81.3,127.6l-1.9-1.9l1.9-1.9c0.3-0.3,0.3-0.7,0-1c-0.3-0.3-0.7-0.3-1,0l-1.9,1.9l-1.9-1.9
				c-0.3-0.3-0.7-0.3-1,0c-0.3,0.3-0.3,0.7,0,1l1.9,1.9l-1.9,1.9c-0.3,0.3-0.3,0.7,0,1c0.3,0.3,0.7,0.3,1,0l1.9-1.9l1.9,1.9
				c0.3,0.3,0.7,0.3,1,0C81.6,128.3,81.6,127.8,81.3,127.6z"/>
			<path class="st1" d="M85.9,151.2l-6.8-1.1c-0.3,0-0.5-0.3-0.6-0.6l-1.1-6.8c-0.1-0.8-1.2-0.8-1.3,0l-1.1,6.8c0,0.3-0.3,0.5-0.6,0.6
				l-6.8,1.1c-0.8,0.1-0.8,1.2,0,1.3l6.8,1.1c0.3,0,0.5,0.3,0.6,0.6l1.1,6.8c0.1,0.8,1.2,0.8,1.3,0l1.1-6.8c0-0.3,0.3-0.5,0.6-0.6
				l6.8-1.1C86.6,152.4,86.6,151.4,85.9,151.2z M78.9,152.4c-0.8,0.1-1.5,0.8-1.6,1.6l-0.5,4l-0.5-4c-0.1-0.8-0.8-1.5-1.6-1.6l-4-0.5
				l4-0.5c0.8-0.1,1.5-0.8,1.6-1.6l0.5-4l0.5,4c0.1,0.8,0.8,1.5,1.6,1.6l4,0.5L78.9,152.4z"/>
			<path class="st2" d="M20.3,128.8c0.3,5.2,10.6,5.8,14.1,6.2c11.6,1.4,23.4,1.2,35.1,1.1c10.1-0.1,21.9-1.8,31.8,1
				c4.6,1.3,2.1,3.4-1.1,4.6c-14.5,5.7-31.7,4.1-46.9,4c-2.6,0-38.2-0.3-35.2,7.1c1.2,3,7.7,3.9,10.3,4.4c7.9,1.4,16.2,1.3,24.2,1.1
				c0.9,0,1.7-0.1,2.6-0.1"/>
			<circle class="st3" cx="49.3" cy="144.7" r="4.1"/>
			<g id="XMLID_2_">
				<g>
					<path class="st4" d="M111,55v64.8H111l-10.9-12.6l-1.9,2.2c0-49.9,10-104.5-61.5-104.5C43,2.4,49.8,1,56.9,1
						C86.8,1,111,25.2,111,55z"/>
				</g>
				<g>
					<path class="st5" d="M98.2,109.4l1.9-2.2l10.9,12.6h0.1V55c0-29.9-24.2-54-54.1-54c-7.1,0-14,1.4-20.2,3.9"/>
					<path class="st5" d="M36.7,4.9C16.8,12.9,2.8,32.3,2.8,55v64.4l10.5-12.2l10.9,12.6h0.1l10.9-12.6L46,119.7l10.7-12.4l10.8,12.5
						l10.8-12.5l10.9,12.6l9-10.4"/>
					<line class="st5" x1="98.2" y1="109.4" x2="98.2" y2="109.4"/>
					<line class="st5" x1="36.7" y1="4.9" x2="36.7" y2="4.9"/>
				</g>
			</g>
		</g>
		</svg>
		<h4 class="font-blue px-4 text-center mt-4">Lo sentimos, la página que solicita no existe o no ha existido nunca</h4>
		<a href="<?= base_url() ?>" class="btn btn-mi-cuenta px-4">Ir a Inicio</a>
	</div>
<!-- </section> -->
</body>
</html>
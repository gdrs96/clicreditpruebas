<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
	<div class="container">
		<div class="row justify-content-center my-4">
			<div class="col-lg-7">
				<h1 class="font-blue linea-azul text-center">Cambiar <b>contraseña</b></h1>
				<form class="job-manager-form" method="post" action="<?= base_url() ?>acceso/update_password">
					<div class="form-group row justify-content-center">
						<div class="col-lg-10 mb-3">
							<label for="password" class="control-label"><b class="font-blue">Contraseña</b></label>
							<input type="password" class="form-control" name="password" required="">
							<br>
							<label for="password2" class="control-label"><b class="font-blue">Repita la Contraseña:</b></label>
							<input type="password" class="form-control" name="password2" required="">
							<input type="hidden" name="token" value="<?php echo $token ?>">			
						</div>
					</div>
					<div class="form-group text-center">
						<button class="btn btn-acceso btn-primary" type="submit" style="width:160px">Cambiar</button>
					</div>
				</form>

				<p class="text-center">
					Si tienes problemas para acceder <a href="#" onclick="openChat()" class="font-blue">contacta con nosotros aquí.</a>
				</p>
			</div>
		</div>
	</div>
</div>
<div class="page-content">
	<div>

	</div>
</div>
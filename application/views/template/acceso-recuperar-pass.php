<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row justify-content-center mt-2 mb-4">
		<div class="col-md-8 col-xl-6">
			<h1 class="font-blue linea-azul text-center">Recuperar <b>contraseña</b></h1>
			<p class="text-center">Introduce tu correo electronico y el código verificación y te enviaremos una nueva contraseña a tu correo.</p>
			<form class="job-manager-form" method="post" action="<?= base_url() ?>acceso/enviarEmailRecuperacion" >
				<div class="form-group row justify-content-center">
					<div class="col-lg-10">
						<label for="email" class="control-label"><b class="font-blue">Correo eletrónico</b></label>
						<input type="email" class="form-control" placeholder="email" name="email" required>
						<br>
						<label for="digito" class="control-label"><b class="font-blue">Codigo de verificación:</b></label>
						<input type="text" class="form-control" placeholder="Introduce la suma del día y el mes <?= date('d') ?> + <?= date('m')?>" name="digito" required>
						<br>
						<!--<p><b class="font-blue">Codigo de verificación:</b> Es la suma de tu dia de nacimiento y el numero de mes. <b class="font-blue">Ejemplo:</b> Para la fecha 19/02/1980, el código seria 19 + 02 = 21-->
						<p><b class="font-blue">Codigo de verificación:</b> Es la suma del día actual y el mes, es decir, <?= date('d') ?> + <?= date('m')?></p>
					</div>
					<button class="btn btn-acceso btn-primary" type="submit" style="width:160px">ENVIAR</button>
				</div>
				<br>
			</form>
			<h2 class="text-center font-blue mt-4 linea-azul" style="font-size:25px"><b>¿Todavia no estás registrado? <span class="font-green">!Regístrate!</span></b></h2>
			<a class="d-flex my-3 link-no-underlined" href="<?= base_url() ?>solicitud/paso-1"><button class="btn btn-acceso btn-primary mx-auto" style="width:160px">REGISTRARME</button></a>
			<p class="text-center">
				Si tienes problemas para acceder <a href="#" onclick="openChat()" class="font-blue">contacta con nosotros aquí.</a>
			</p>
		</div>
	</div>
</div>
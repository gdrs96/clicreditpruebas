<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div id="content">
	<section id="login">
		<div class="page-heading">
			<div class="container">
				<h1><span class="line line__left"></span><i class="fa fa-user-plus"></i> Registro </h1>
			</div>
		</div>
		<div class="container">
			<div class="spacer-lg"></div>
			<p>Regístrate rellenando el siguiente formulario para poder disfrutar a todos los servicios que Clicredit ofrece.</p>
			<p>Una vez registrado puedes completar tus datos desde tu perfil.</p>
			<div class="text-center">
				<form class="form" role="form" method="post" action="<?= base_url() ?>acceso/registrado" >
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Nombre" name="name" required autofocus>
					</div>
					<div class="form-group">
						<input type="email" class="form-control" placeholder="email" name="email" required>
					</div>
					<div class="form-group">
						<input type="password" class="form-control" placeholder="Contraseña" name="pass" required>
					</div>

					<div class="form-group">
						<button class="btn btn-lg btn-primary btn-block" type="submit" ><i class="fa fa-user-plus"></i> Registrarme</button>
					</div>
				</form>
			</div>
			<br/>
			<hr>
		</div>
	</section>
</div>
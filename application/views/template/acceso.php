<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section id="login">
	<div class="container">
		<div class="row justify-content-center mt-2 mb-4">
			<div class="col-md-8 col-xl-6">
				<h1 class="linea-azul font-blue text-center"><b>Acceder</b> a su cuenta</h1>
				<form class="form" method="post" action="<?php echo base_url(); ?>acceso/entrar" >
					<div class="form-group row justify-content-center">
						<div class="col-lg-10 mb-2">
							<label for="email" class="control-label"><b class="font-blue">Correo electrónico - Cédula</b></label>
							<input type="text" class="form-control" placeholder="email" id="email" name="email" required autofocus>
							<br>
							<label for="pass" class="control-label"><b class="font-blue">Contraseña</b></label>
							<input type="password" class="form-control" placeholder="Contraseña" name="pass" id="pass" required>
							<label class="pull-right"><a href="<?php echo base_url(); ?>acceso/recuperar-cuenta">¿Ha olvidado su contraseña?</a></label>
						</div>
						<button class="btn btn-primary btn-acceso" type="submit" style="width:160px">ENTRAR</button>
					</div>
					<br>
				</form>
				<h2 class="text-center font-blue mt-4 linea-azul" style="font-size:25px"><b>¿Todavia no estás registrado? <span class="font-green">!Regístrate!</span></b></h2>
				<a class="d-flex my-3 link-no-underlined" href="<?= base_url() ?>solicitud/paso-1"><button class="btn btn-solicitar btn-primary btn-acceso mx-auto" style="width:160px">REGISTRARME</button></a>
				<p class="text-center">
					Si tienes problemas para acceder <a href="#" onclick="openChat()" class="font-blue">contacta con nosotros aquí.</a>
				</p>
			</div>
		</div>
	</div>
</section>

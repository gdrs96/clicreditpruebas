<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row my-4">
		<div class="col-lg-8">
			<h2 class="font-blue mb-4"><b>Ayuda, </b>preguntas frecuentes (FAQ)</h2>
			<div id="accordion">
				<div class="card mb-3">
					<div class="card-header" id="heading1">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap text-left" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
								<span class="font-blue"><b>¿Cuánto me presta Clicredit.com? ¿A qué plazo?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordion">
						<div class="card-body">
							En el primer préstamo puedes pedir desde B./ 20.00 hasta B./ 250.00 dentro de un plazo de 1 a 6 Quincenas. Tú decides la combinación de préstamo/plazo que más te convenga. En cuanto pagues tu primer préstamo a tiempo, automáticamente se incrementan tus posibilidades de tener un límite de crédito más alto. Lo importante es que empieces a generar un historial en Clicredit.com.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading2">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
								<span class="font-blue"><b>¿Cuál es el horario de atención al cliente?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
						<div class="card-body">
							De Lunes a Viernes de 8:00 am a 5:00 pm (provisional).
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading3">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
								<span class="font-blue"><b>¿Qué necesito para aplicar a un préstamo Clicredit.com?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
						<div class="card-body">
							Necesitas una cuenta de banco a tu nombre, un celular propio y un correo electrónico.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading4">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
								<span class="font-blue"><b>¿Necesito tener historial de crédito para aplicar a un préstamo?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
						<div class="card-body">
							No necesitas historial de crédito. Podremos contribuir a que generes un historial positivo.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading5">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
								<span class="font-blue"><b>¿Quiénes pueden pedir un préstamo en Clicredit.com?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse5" class="collapse" aria-labelledby="heading5" data-parent="#accordion">
						<div class="card-body">
							Todos los panameños mayores de edad y los extranjeros con residencia legal en el país.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading6">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
								<span class="font-blue"><b>Una vez que aplico, ¿Cuánto tiempo se tarda para tomar una decisión?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse6" class="collapse" aria-labelledby="heading6" data-parent="#accordion">
						<div class="card-body">
							Una vez completas los formularios, recibirás en tu email una respuesta rápida, generalmente menos de 5 minutos. Si es positiva te llamaremos para completar el proceso, si estamos en horario laboral y sino serás el primero de la mañana siguiente. Completaremos el proceso, enviándote el contrato de servicio con las condiciones y verificando tu identidad, tardaremos aproximado 20 minutos y por último tu transferencia se ejecutara, dependerá de tu banco pero no deberá demorar mucho.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading7">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed" data-toggle="collapse" data-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
								<span class="font-blue"><b>¿Por qué se rechazó mi crédito?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse7" class="collapse" aria-labelledby="heading7" data-parent="#accordion">
						<div class="card-body">
							Hay varias razones por las que se rechazan solicitudes. La razón más común es porque no se han facilitado todos los datos o documentos que se piden en la solicitud. Por lo que recomendamos que se provea cuidadosamente toda la información que se pide. Hay otras razones por las que aún así se declinan. Muchas veces el equipo que toma estas decisiones busca otorgar préstamos de manera responsable, evitando endeudar de más a nuestros usuarios, o quizá simplemente el aplicante no encaja en el perfil al que normalmente autorizamos los préstamos. En cualquier situación, recomendamos regresar en dos o tres semanas para volver a solicitar un crédito porque nuestras políticas internas se modifican constantemente para adaptarnos a las necesidades y circunstancias personales de nuestros usuarios.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading8">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
								<span class="font-blue"><b>¿Cómo hace Clicredit.com para depositarme los fondos y como puedo hacer mi pago?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse8" class="collapse" aria-labelledby="heading8" data-parent="#accordion">
						<div class="card-body">
							Clicredit.com utiliza un sistema de pagos electrónicos, que directamente se depositan en una cuenta bancaria específica que debe estar a tu nombre. El pago de tu préstamo se hace a través de un depósito en nuestra cuenta bancaria o por transferencia electrónica. Tú eliges la que más te convenga.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading9">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
							<span class="font-blue"><b>¿Cuánto tiempo tarda en reflejarse el pago de ustedes en mi cuenta?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse9" class="collapse" aria-labelledby="heading9" data-parent="#accordion">
						<div class="card-body">
							Dependerá de tu banco y de la hora a la que terminamos la operación. Si tu banco y el nuestro es el mismo, el pago será instantáneo a cualquier hora, pero si los bancos son diferentes, el pago se reflejará en tu cuenta durante el día si cerramos el proceso antes de las 12:00 m. En caso de terminar el proceso después del mediodía el ingreso se reflejará en tu cuenta a la mañana siguiente.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading10">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
								<span class="font-blue"><b>¿Puedo solicitar más de un préstamo online al mismo tiempo?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
						<div class="card-body">
							No, mientras tengas un préstamo abierto no podrás solicitar uno nuevo. Una vez que hayas cerrado el préstamo podrás solicitar uno nuevo y en mejores condiciones. Sin embargo, cuando tienes un préstamo abierto y no lo puedes pagar en las fechas acordadas puedes solicitar una ampliación de crédito, te ayudaremos, para que puedas cancelar con éxito tu préstamo.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading11">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
								<span class="font-blue"><b>¿Qué pasa si no puedo hacer un pago?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
						<div class="card-body">
							En Clicredit.com valoramos la comunicación honesta y directa. Si estás en una situación en la que se te complica hacer un pago, te recomendamos que te pongas en contacto con nosotros escribiendo a <a href="mailto:ayuda@pa.clicredit.com" class="font-blue">ayuda@pa.clicredit.com</a> o nos llames a 260-8951. Cuanto más sepamos acerca de tu situación, más podremos hacer para trabajar contigo y juntos encontrar una solución. Entendemos que puede haber circunstancias que impiden hacer el pago incluso si nos quieres pagar, por lo que estamos dispuestos a trabajar contigo para encontrar una solución que funcione para ti y para nosotros.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading12">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
								<span class="font-blue"><b>¿Si pago el mismo día en que se vence mi préstamo, corro algún riesgo de caer en mora?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
						<div class="card-body">
							No, no existe el riesgo. Si el pago se realiza el mismo día en el que vence el préstamo, aunque en la cuenta bancaria aparezca reflejado un día después, nosotros respetamos el día en que se hizo el pago.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading13">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
								<span class="font-blue"><b>¿Puedo anticipar el pago de préstamo porque ya no lo necesito?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordion">
						<div class="card-body">
							Si, lo puedes cancelar en cualquier momento. Y solo abonarás el tiempo que lo has usado.
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading14">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
								<span class="font-blue"><b>He olvidado mi contraseña. ¿Qué puedo hacer?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordion">
						<div class="card-body">
							Ve a la página inicial, haz clic en el botón de "Mi Cuenta" y después "¿Ha olvidado su contraseña?".
						</div>
					</div>
				</div>
				<div class="card mb-3">
					<div class="card-header" id="heading15">
						<h5 class="mb-0">
							<button class="btn btn-link btn-wrap collapsed text-left" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
								<span class="font-blue"><b>¿A quién debo contactar si tengo un comentario, pregunta o queja?</b></span>
							</button>
						</h5>
					</div>
					<div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordion">
						<div class="card-body">
							Puedes escribirnos a <a href="mailto:ayuda@pa.clicredit.com" class="font-blue">ayuda@pa.clicredit.com</a> o llamarnos a 260-8951. Haremos todo lo posible por resolver tu inquietud en el menor tiempo posible.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="ayuda mx-auto">
				<p class="mb-0"><b>¿DUDAS? *</b></p>
				<hr class="bg-white my-2">
				<p class="mb-0">Chatea con nosotros
					<span class="pull-right">
						<a href="#" class="font-white" onclick="openChat()">Haz click aquí</a>
					</span>
				</p>
				<p class="mb-0">Llámanos <span class="pull-right">260-8951</span></p>
				<p class="mb-0">Escribenos <span class="pull-right">ayuda@pa.clicredit.com</span></p>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<h2 class="font-blue"><b>Envianos,</b> un mensaje directo</h2>
			<form id="contact-form" method="post" name="contact-form" class="form">
				<div class="row form-group font-blue">
					<div class="col-md-3 d-flex flex-column justify-content-around">
						<label class="mb-0" for="name"> Indicanos tu nombre *</label>
						<input type="text" id="name" name="name" class="form-control mb-2" required="">	
						<label class="mb-0" for="email">Correo electrónico *</label>
						<input type="email" id="email" name="email" class="form-control mb-2" required="">
						<label class="mb-0" for="phone">Teléfono de contacto</label>
						<input type="text" id="phone" name="phone" class="form-control">
					</div>
					<div class="col-md-9">
						<label for="message">Escríbenos tu mensaje *</label>
						<textarea id="message" name="message" class="form-control" cols="50" rows="8" required=""></textarea>
					</div>
				</div>
				<p class="form-group text-right">
					<button class="btn btn-solicita" onclick="sendForm()" id="enviar_form">Enviar Mensaje</button>
				</p>
				<br>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	function sendForm(){
		if($('#email').val() && $('#name').val() && $('#message').val()){
			$('#enviar_form').attr('disabled',true);
			var dataString='name='+$('#name').val()+'&email='+$('#email').val()+'&phone='+ $('#phone').val() + '&message=' + $('#message').val();
			$.ajax({
				type:	'POST',
				url:	'<?= base_url() ?>contacto/formcontacto',
				data: dataString,
				success: function(result){
					alert('Gracias por su interés, pronto tendrá respuesta.');
				}
			});
		}else{
			alert('Por favor compruebe el campo nombre, email y mensaje antes de enviar');
		}
	}
</script>
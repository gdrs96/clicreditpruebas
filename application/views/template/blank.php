<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row my-4">
		<div class="col-lg-10">
			<?php if(!empty($content)){ echo $content;} ?>
		</div>
	</div>
</div>
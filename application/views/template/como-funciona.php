<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row mt-4">
		<div class="col-lg-12 text-center text-lg-left">
			<h2 class="font-blue"><b>Cómo funcionan</b> nuestros préstamos.</h2>
		</div>
	</div>
	<div class="row funcionamiento">
		<div class="col-md-6 col-xl-3">
			<img src="<?= base_url() ?>/img/funcionamiento1.svg" class="img-fluid d-block mx-auto">
			<p class="text-center text-white">Elige la cantidad de dinero que necesitas y el plazo en el cual deseas devolver el préstamo.</p>
		</div>
		<div class="col-md-6 col-xl-3">
			<img src="<?= base_url() ?>/img/funcionamiento2.svg" class="img-fluid d-block mx-auto">
			<p class="text-center text-white">Completa el formulario en la página web. 100% en línea.</p>
		</div>
		<div class="col-md-6 col-xl-3">
			<img src="<?= base_url() ?>/img/funcionamiento3.svg" class="img-fluid d-block mx-auto">
			<p class="text-center text-white">Evaluamos tu préstamo al instante y recibirás una respuesta en tu email.<br>(+/- 30 segundos)</p>
		</div>
		<div class="col-md-6 col-xl-3">
			<img src="<?= base_url() ?>/img/funcionamiento4.svg" class="img-fluid d-block mx-auto">
			<p class="text-center text-white">... y en menos de 30 minutos, después de cerrar el proceso, disfrutarás de tu dinero.* <br><small>(depende de la hora y el banco)</small></p>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-lg-12 text-center text-lg-left">
			<h2 class="font-blue"><b>Cómo cancelar</b> tu préstamo.</h2>
		</div>
		<div class="col-12">
		<p>Si tu préstamo tiene varias quincenas para cancelarse, tendrás que cancelar periódicamente cada quincena hasta terminar de cancelar el total. El importe total a devolver nunca cambia, es el que te indicamos desde el primer momento, siempre y cuando devuelvas el préstamo en las fechas acordadas. Nosotros te avisaremos con nuestro sistema de alertas. Puedes dirigirte a la sección de <b class="font-blue">"Mi Cuenta"</b> y ver el estado general en que se encuentra tu préstamo.</p>
		<p>Si quieres cancelar el total, solo pagas por el tiempo que usaste el dinero. Vas a la sección <b class="font-blue">"Mi Cuenta"</b> y haces clic sobre el botón <b class="font-blue">"DEVOLVER"</b>, te dirá cuanto tienes que cancelar.</p>
		</div>
	</div>
	<div class="row mt-5">
		<div class="col-lg-12 text-center text-lg-left">
			<h2 class="font-blue"><b>¿Qué pasa si no puedo</b> hacer un pago a tiempo?</h2>
		</div>
		<div class="col-12">
			<p>Valoramos la comunicación honesta y directa. Si estás en una situación en la que se te complica hacer un pago, te recomendamos que te pongas en contacto con nosotros escribiendo a <a href="mailto:ayuda@pa.clicredit.com" class="font-blue">ayuda@pa.clicredit.com</a> o llamándonos al teléfono 261-8951. Cuanto más sepamos de la situación, más podremos trabajar contigo y juntos encontrar una solución. Entendemos que puede haber circunstancias que impiden el reembolso incluso si nos quieres pagar, por lo que estamos dispuestos a trabajar contigo para encontrar una solución que funcione para ti y para nosotros.</p>
		</div>
	</div>
	<div class="row mt-5 mb-5">
		<div class="col-lg-12 text-center text-lg-left">
			<h2 class="font-blue"><b>Comisiones por</b> impago total o parcial.</h2>
		</div>
		<div class="col-12">
			<p>Si al final de día de pago no se ha realizado el pago, en concepto de comisión por gestión de la posición deudora vencida, se procederá a cobrar honorarios.</p>
			<p>- Comisión por gestión de posición deudora.</p>
			<p>- Interés de demora del capital no pagado.</p>
			<p>Todos estos costes los tendrás en tu contrato de servicio, variaran seg&uacute;n el tipo de cliente que seas. Cuando creas que no podrás afrontar el pago es mejor que te pongas en contacto con nosotros y buscaremos una solución juntos.</p>
		</div>
	</div>
</div>
<br>
</div>
<script type="text/javascript">
	function sendForm(){
		if($('#email').val() && $('#name').val() && $('#message').val()){
			$('#enviar_form').attr('disabled',true);
			var dataString='name='+$('#name').val()+'&email='+$('#email').val()+'&phone='+ $('#phone').val() + '&message=' + $('#message').val();
			$.ajax({
				type:	'POST',
				url:	'contacto/formcontacto',
				data: dataString,
				success: function(result){
					alert('Gracias por su interés, pronto tendrá respuesta.');
				}
			});
		}else{
			alert('Por favor compruebe el campo nombre, email y mensaje antes de enviar');
		}
	}
</script>
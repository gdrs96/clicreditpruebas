<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row my-4">
		<div class="col-lg-8">
			<h2 class="font-blue"><b>Contáctanos</b> para cualquier duda</h2>
			<p>El equipo de CliCredit.com está a tu disposición ante cualquier duda o aclaración.</p>
			<h6 class="font-blue mb-0"><b>POR TELÉFONO AL 260-8951</b></h6> 
			<p>Nuestro horario de atención al cliente es de Lunes a Viernes de 8:00 am a 5:00 pm, (provisional).</p>

			<p><b class="font-blue">FUERA DE ESTE HORARIO</b> puedes escribirnos en el formulario de esta sección o escribirnos directamente a <a href="mailto:ayuda@pa.clicredit.com" class="font-blue">ayuda@pa.clicredit.com</a>, te contestaremos súper rápido</p>
			<h2 class="font-blue"><b>Ubicados</b> en:</h2>
			<p>En Ave. Ricardo J. Alfaro con la C/ 65 Oeste, Edificio PH The Century Tower, Ofi. 11,Ciudad de Panamá, Panamá</p>
		</div>
		<div class="col-lg-4">
			<div class="cuadro-contacto font-white">
				<h2>Escríbenos</h2>
				<form id="contact-form" method="post" name="contact-form" class="form">
					<div class="form-group">
						<input type="text" id="name" name="name" class="form-control" placeholder="Nombre *" required="">
					</div>
					<div class="form-group">
						<input type="email" id="email" name="email" class="form-control" placeholder="tucorreo@ejemplo.com *" required="">
					</div>
					<div class="form-group">
						<input type="text" id="phone" name="phone" class="form-control" placeholder="Teléfono de contacto">
					</div>
					<div class="form-group">
						<textarea id="message" name="message" class="form-control" cols="50" rows="8" placeholder="Mensaje" required=""></textarea>
					</div>
				</form>
			</div>
			<p class="form-group text-right">
				<button class="btn btn-solicita mt-3" onclick="sendForm()" id="enviar_form">Enviar Mensaje</button>
			</p>
		</div>
	</div>
</div>
<script type="text/javascript">
	function sendForm(){
		if($('#email').val() && $('#name').val() && $('#message').val()){
			$('#enviar_form').attr('disabled',true);
			var dataString='name='+$('#name').val()+'&email='+$('#email').val()+'&phone='+ $('#phone').val() + '&message=' + $('#message').val();
			$.ajax({
				type:	'POST',
				url:	'<?= base_url() ?>contacto/formcontacto',
				data: dataString,
				success: function(result){
					alert('Gracias por su interés, pronto tendrá respuesta.');
				}
			});
		}else{
			alert('Por favor compruebe el campo nombre, email y mensaje antes de enviar');
		}
	}
</script>
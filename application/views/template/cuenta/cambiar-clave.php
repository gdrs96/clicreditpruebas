<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="content cuenta">
	<div class="blue">
		<br>
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<h1 class="font-white space-bottom-20">Te prestamos <b>B./ 750,00</b> en <b>30 minutos</b>. *</h1>
				</div>
				<div class="col-lg-4 text-center">
					<a href="<?= base_url() ?>" class="btn btn-solicita space-bottom-20 space-top-20">¡ SOLICITA AHORA !</a>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-lg-2"></div>
			<div class="col-lg-7">
				<h1 class="font-blue linea-azul font-blue">Cambiar <b>contraseña</b></h1>
				<p>Introduce tu nueva contraseña</p>
				<form class="job-manager-form" method="post" action="<?= base_url() ?>cuenta/cambiar-clave-guardar">
					<div class="form-group row">
						<div class="col-lg-1">
						</div>
						<div class="col-lg-10">
							<label for="password" class="control-label"><b class="font-blue">Contraseña</b></label>
							<input type="password" class="form-control" name="password" required="">

							<label for="password2" class="control-label"><b class="font-blue">Repita la Contraseña:</b></label>
							<input type="password" class="form-control" name="password2" required="">
							<br>

							

						</div>
					</div>
					<div class="form-group text-center">
						<button class="btn btn-solicitar btn-primary" type="submit" style="width:160px">Cambiar</button>
					</div>
				</form>

				<p class="text-center">
					Si tienes problemas para acceder <a href="http://localhost/clicredit.com/contacto" class="font-blue">contacta con nosotros aquí.</a>
				</p>
				<br>
			</div>


		</div>
	</div>
</div>
</div>
<div class="page-content">
	<div>

	</div>
</div>
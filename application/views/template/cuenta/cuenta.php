<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container font-blue mb-3">
	<div class="row mt-5">
		<div class="col-lg-6">
			<h2 class="linea-azul"><b>POSICIÓN ACTUAL</b></h2>					
			<p>
				<b>Fecha de inicio:</b>
				<span class="pull-right">
					<?php 
						if(!empty($prestamos[0]->created_at))
							echo date('d-m-Y', strtotime($prestamos[0]->created_at));
						else
							echo "-"; 
					?>
				</span>
			</p>					
			<p>
				<b>Monto solicitado:</b> 
				<span class="pull-right">
					<?php 
						if(!empty($prestamos[0]->cantidad))
							echo $prestamos[0]->cantidad;
						else
							echo "-";
					?>
				</span>
			</p>
			<?php if(!empty($prestamos[0]->nombre)) : ?>
				<p>
					<b>Estado del préstamo:</b>
					<span class="pull-right badge" style="font-size:1rem; color:#<?=$prestamos[0]->color ?>">
						<b><?=  $prestamos[0]->nombre ?></b>
					</span>
				</p>
			<?php else : ?>
				<p>
					<b>Estado del préstamo:</b>
					<span class="pull-right"> - </span>
				</p>
			<?php endif ?>	
			<p>
				<b>Fecha de finalización:</b>
				<span class="pull-right">
					<?php 		
						if(!empty($fecha_finalizacion_prestamo[0]->fecha_vencimiento))
							echo date('d-m-Y', strtotime($fecha_finalizacion_prestamo[0]->fecha_vencimiento));
						else
							echo "-";
					?>
				</span>
			</p>
			<?php if($prestamos_proceso>0) : ?>
				<p class="text-right"><a href="<?= base_url() ?>solicitud/paso-1">
					<?php 
						if(!empty($mensaje_estado_prestamo->descripcion)) 
						echo $mensaje_estado_prestamo->descripcion;
					?></a>
				</p>
			<?php endif ?>
			<?php if(!empty($prestamos)) : ?>					
			<?php if($prestamos[0]->prestamo_estado_id == 1 || $prestamos[0]->prestamo_estado_id==2 || $prestamos[0]->prestamo_estado_id==3 || $prestamos[0]->prestamo_estado_id==4 || $prestamos[0]->prestamo_estado_id==5 || $prestamos[0]->prestamo_estado_id==6 || $prestamos[0]->prestamo_estado_id==7) : ?>
				<?php if($prestamos[0]->prestamo_estado_id != 7) { ?>
				<p>
					<b>Si ya no necesitas el dinero, devuélvelo y paga menos:</b>
				</p>
				<button class="btn btn-devolver w-100" id="btn_devolver">
					<span style="font-size: 1.2rem"><b style="font-weight: 400 !important">Devuélvelo</b> y paga menos!</span>
				</button>
			    <?php } ?>
				<div class="row justify-content-center mt-4">
					<div class="col-12">
						<h2 class="linea-azul mb-0"><b>CALENDARIO DE PAGOS</b></h2>
					</div>
				</div>
				<table class="table table-hover font-blue mb-0" id="table_quincenas">
					<thead>
						<tr class="text-center">
							<th scope="col">
								<b class="d-none d-lg-inline">Quincenas</b>
								<b class="d-inline d-lg-none">#</b>
							</th>
							<th scope="col">Fecha de pago</th>
							<th scope="col">Importe (B./)</th>
							<th scope="col">Status</th>
						</tr>
					</thead>
					<tfoot>
                            <tr>
                           
                              <th colspan="2" style="text-align:right">Total a cancelar:</th>
                              
                              <th></th>
                            </tr>
                          </tfoot>
				</table>
				<div class="row justify-content-center my-4 d-block d-lg-none">
					<div class="col-12">
						<p class="text-center font-2"><b>Su calificación es <?=$calificacion[0]->id?> de 10</b></p>
						<p class="text-center">Si tu calificación sube, tu próximo préstamo será más barato. <b>No te atrases!! Paga a tiempo.</b></p>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<h2 class="linea-azul"><b>REPORTA TUS PAGOS</b></h2>
						<form class="mb-1" method="post" id="upload_form" action="Reporte_pago/reportarPago" enctype="multipart/form-data">  
							<input type="file" name="imagen_recibo" id="imagen_recibo" class="btn inputfile inputfile-1-orange"  multiple style="display: none" accept="image/*"   />
							<label class="rounded" for="imagen_recibo">
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>
								<span>Reporta tu recibo de pago</span>
							</label>
							<input type="hidden" id="id_prestamo" name="id_prestamo" value="<?php $valor = !empty($prestamos[0]->id_prestamo)?$prestamos[0]->id_prestamo:null; echo set_value('id_prestamo',$valor); ?>" class="form-control" type="text">
							<input type="hidden" id="idCliente_report" name="idCliente_report" value=" <?php $valor = !empty($prestamos[0]->id_cliente)?$prestamos[0]->id_cliente:null; echo set_value('idCliente_report',$valor); ?>" class="form-control" type="text">
						</form>
					</div>
				</div>
				<table class="table table-hover font-blue" id="tableReportePago">
					<thead>
						<tr class=" text-center">
							<th>VER</th>
							<th id="no-mobile">F. subida</th>
							<th>F. efecto</th>
							<th>Importe</th>
							<th>Status <i  class="fa fa-info-circle enlace ml-2" id="infoStatus"></i></th>
						</tr>
					</thead>
				</table>
				<div class="row">
					<div class="col text-center">
					<p>Si lo prefieres, puedes enviar el reporte de tu pago por <a target="_blank" href="https://api.whatsapp.com/send?phone=50763777870" class="font-red d-inline d-sm-none">WhatsApp</a><span class="font-red d-none d-sm-inline">WhatsApp al número 6377-7870</span></p>
					</div>
				</div>	
			<?php endif ?>
			<?php endif ?>
		</div>
									
		<div class="col-lg-6">
			<div class="row d-none d-lg-flex">
				<div class="col-md-6">
					<h2 class="linea-azul"><b>CALIFICACIÓN</b></h2>
					<p>Si tu calificación sube, tu próximo préstamo será más barato.</p>
					<p>No te atrases!! Paga a tiempo.</p>
				</div>
				<div class="col-md-6 d-flex justify-content-center align-items-center">
					<img class="width85" src=<?php echo base_url(); ?><?= $calificacion[0]->imagen ?> alt="calificación">
				</div>
			</div>
			<div class="row">
				<div class="col-12">
					<h2><b>INFORMACIÓN</b></h2>
					<ul class="nav nav-pills nav-fill blue rounded p-1" id="cuentaTab">
						<li class="nav-item reverse">
							<a class="nav-link p-1 active" id="dudas-tab" data-toggle="tab" href="#dudas" role="tab" aria-controls="dudas" aria-selected="true"><b>¿DUDAS?</b></a>
						</li>
						<li class="nav-item reverse">
							<a class="nav-link p-1" id="perfil-tab" data-toggle="tab" href="#perfil" role="tab" aria-controls="perfil" aria-selected="true"><b>PERFIL</b></a>
						</li>
						<li class="nav-item reverse">
							<a class="nav-link p-1" id="histo-tab" data-toggle="tab" href="#histo" role="tab" aria-controls="histo" aria-selected="true"><b>HISTÓRICO</b></a>
						</li>
					</ul>
				</div>
				<div class="tab-content pt-3 col-12" id="myTabContent">
					<div class="tab-pane fade show active" id="dudas" role="tabpanel" aria-labelledby="dudas-tab">
						<p class="my-0"><b>Inicia un chat con nosotros. <a href="#" class="font-green font-green-hover" onclick="openChat()">!Haz Click aquí!</a></b></p>
						<p class="my-0"><b>Llámanos al número 260 - 8951</b></p>
						<p class="my-0"><b>Escríbenos por email a <a href="mailto:ayuda@pa.clicredit.com" class="font-green font-green-hover" target="_blank">ayuda@pa.clicredit.com</a></b></p>
						<p class="my-0"><b><a class="font-blue" href="<?= base_url() ?>devoluciones">¿Necesitas una prórroga?</a></b></p>
						<p class="my-0"><b><a class="font-blue" href="<?= base_url() ?>prestamo-vencido">Mi préstamo se venció ¿Qué hago?</a></b></p>
					</div>
					<div class="tab-pane fade" id="perfil" role="tabpanel" aria-labelledby="perfil-tab">
						<p><b>Usuario: <span class="pull-right"><?= $user->n_identificacion ?></span></b></p>
						<p><b>Email: <span class="pull-right"><i class="fa fa-pencil mr-3 enlace" data-toggle="modal" data-target="#editModal" data-field="email"></i><?= $user->email ?></span></b></p>
						<p><b>Celular/Teléfono: <span class="pull-right"><i class="fa fa-pencil mr-3 enlace" data-toggle="modal" data-target="#editModal" data-field="telefono"></i><?= $user->telefono ?></span></b></p>
						<p><b>Contraseña: <span class="pull-right"><i class="fa fa-pencil mr-3 enlace" data-toggle="modal" data-target="#editModal" data-field="contraseña"></i>**********</span></b></p>
					</div>
					<div class="tab-pane fade" id="histo" role="tabpanel" aria-labelledby="histo-tab">
						<table class="table table-hover font-blue">
							<thead class=" text-center">
								<th>Fecha</th>
								<th>Cantidad</th>
								<th>Quincenas</th>
								<th>Estado</th>
							</thead>
							<tbody>
								<?php
								if(!empty($prestamos_historicos) ){
									foreach ($prestamos_historicos as $key ) {
										?>
										<tr>
											<td style="font-size: 1rem"><?= date('d-m-Y', strtotime($key->ultima_actualizacion)) ?></td>
											<td style="font-size: 1rem"><?= $key->cantidad ?></td>
											<td style="font-size: 1rem"><?= $key->tiempo ?></td>
											<td><span class="pull-right badge badge-primary" style="font-size:1rem; background:#<?=$key->color ?>">
											<?=  $key->nombre ?>
										</span></td>
										</tr>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<!--Vista modal para mostrar la descripcion de los status de los reportes de pago-->
<div class="modal fade" id="modal_infoEstatus" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-infoStatus" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title"><b class="font-blue">Status</b></h3>
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true"><i class="fa fa-times-circle-o" aria-hidden="true"></i></span>
				</button>
			</div>
			<div class="modalBody">
        <table class="table table-borderless" id="tableInfoStatus">
					<thead></thead>
				</table>
			</div>            
    </div>
	</div>
</div>


<div class="modal fade" id="modal_reportePago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
  <div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title font-blue" id="image-gallery-title"><b>Recibo de pago</b></h4>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
			</div>
			<div class="modal-body">
				<form action="#" id="formPago">
					<img id="imagen" class="img-fluid d-block mx-auto" src="">
				</form>
			</div>
		</div>
	</div>
</div>

 <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content font-blue">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Editar datos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			<form id="editDataForm" action="javascript:enviarForm();" method="post">
				<div class="modal-body">
          <div class="form-group">
            <label for="field-name" class="col-form-label">Nuevo dato</label>
            <input type="text" class="form-control" id="field-name" data-parsley-required>
          </div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-azul" id="edit">Editar</button>
					<button type="button" class="btn btn-verde" data-dismiss="modal">Cerrar</button>
				</div>
			</form>
    </div>
  </div>
</div>


<div class="modal fade" id="modalDevolucionPago" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content font-blue">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title linea-azul pb-0 pl-3" id="editModalLabel">Cancela <b>AHORA</b> </h5>
				<form id="devolucionPago"  method="post">
					<?php if (!empty($prestamos[0]->prestamo_estado_id)) {  ?>
						<?php if($prestamos[0]->prestamo_estado_id==1) : ?>
							<br>
							<p class="mb-0 mt-1 pl-3">Si cancelas hoy, <b>antes de las 6:00 pm</b></p>
							<?php if(!empty($total_monto_devolucion)){ ?>
								<h3 class="mb-0 pl-3"><b>DEVUELVES:</b><b class="font-green pull-right pr-3"> B./ <?=  number_format($total_monto_devolucion,2) ?></b><h3>
							<?php } ?>
							<?php if(!empty($total_ahorro)){ ?>
								<h3 class="mb-3 pl-3" style="line-height: 1.5rem"><b>TE AHORRAS:</b><b class="font-green pull-right pr-3"> B./ <?=  number_format($total_ahorro,2) ?></b></h3>
							<?php } ?>
							<input type="button"  class="btn inputfile inputfile-1-orange"   style="display: none" />
							<label style="width: calc(100% - 2rem)" class="rounded mt-3 mx-3" for="" id="btnInfoDevolucion">
								<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"></svg>
								<span>Transferencia | Depósito a cuenta</span>
							</label>
						<?php else : ?>
							<p class="text-center mt-2 px-3">Póngase en contacto con el equipo de Clicredit.com para <b>regularizar su situación.</b> </p>
							<p class="text-center px-3">Recuerde que para nosotros es importante entender sus necesidades para poder ayudarlo y evitar intereses de demora. </p>
						<?php endif ?>
						<div class="row justify-content-center my-3">
							<?php if($prestamos[0]->prestamo_estado_id==1) : ?>	
								<div class="col-12">
									<h3 class="linea-azul pl-3">¿Dudas? <b>CONTÁCTANOS</b></h3>
								</div>
								<div class="col-12">
									<p class="mb-0 mt-2 pl-3">
										<b>Chatea con nosotros</b><b class="enlace pull-right font-green pr-3" onclick="openChat()">! Haz Click !</b>
									</p>
									<p class="pl-3">
										<b>Llámanos<b> <b class="pull-right font-green pr-3"> 260-8997</b>
									</p>
								</div>
							<?php else : ?>
								<div class="col-12 mt-3">
									<h3 class="linea-azul pl-3"> <b>CONTÁCTANOS</b> ahora</h3>
								</div>
								<div class="col-12">
									<p class="text-center">
										Contáctenos al número 
										<b>260-8951</b>, por 
										<b class="enlace" onclick="openChat()">chat</b> o <br> enviándonos un email a 
										<a class="font-blue" target="_blank" href="mailto:cobros@pa.clicredit.com"><b>cobros@pa.clicredit.com</b></a>
									</p>
								</div>
							<?php endif ?>
						</div>
					<?php } ?>
				</form>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="infoDevolucion" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content font-blue">
			<div class="modal-body text-center">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title linea-azul" id="editModalLabel">Depósito a <b>CUENTA o TRANSFERENCIA</b> </h5>
				<p class="mt-3 mb-1">Multibank:  XXXXXXXXXXXXXXXXXXXXXXXXXXX</p>
				<p class="mb-1">Banesco:    XXXXXXXXXXXXXXXXXXXXXXXXXXX</p>
				<p class="mb-1">Bac Panamá: XXXXXXXXXXXXXXXXXXXXXXXXXXX</p>
				<p class="mb-1">Coloque como Titular: XXXXXXXXXXXXXXXXXXXXXX</p>
				<br>
				<p>Favor, recuerde <b>reportar su pago</b> en la sección de  <b>MI CUENTA</b> o por whatsApp al número XXXX-XXXX</p>
				<button type="button" class="text-left btn blue font-white" id="modalRegresar">
					<i class="fa fa-arrow-left"></i>
					 Atrás
				</button>
			</div>
		</div>
  	</div>
</div>


<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/view/template/cuenta/cuenta.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/component-type-file.css">
<script src="<?= base_url() ?>js/custom-file-input.js"></script>
<script src="<?= base_url() ?>-/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>

<script src="//cdn.datatables.net/plug-ins/1.10.16/api/sum().js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>
<script>
	//validacion de edicion de datos
	$('#edit').click(function() {
		const validate = $('#editDataForm').parsley().isValid();
		if(validate) {
			enviarForm();
		}
	});
	function enviarForm(){
		$("#editDataForm").attr('action','<?= base_url() ?>cuenta/cambiarDatos');
		setTimeout(500,$("#editDataForm").submit());
	}
	//declaracion de rutas
	var id_prestamo = $("#id_prestamo").val();
	var table;
	var tableQuincenas;
	var urlReportePago = "<?php echo site_url('reporte_pago/obtenerReportePagos')?>/"+id_prestamo;
	var urlStatusReportePago = "<?php echo site_url('reporte_pago/getStatusReportePago')?>";
	var urlDetallePrestamo = "<?php echo site_url('detalle_prestamo/obtenerDetallePrestamo')?>/"+id_prestamo;
	var urlReportarPago = "<?php echo site_url('reporte_pago/reportarPago')?>";
	var url = '<?= base_url() ?>';
</script>

<script src="<?= base_url() ?>js/functions.js" type="text/javascript"></script>
<script src="<?= base_url() ?>dist/js/template/cuenta/cuenta.js" type="text/javascript"></script>
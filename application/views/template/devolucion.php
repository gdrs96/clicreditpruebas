<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h2><span class="font-blue"><strong>Devolver </strong>mi préstamo</span></h2>
<p>Para devolver tu préstamo CliCredit.com, te pueden suceder tres cosas:</p>
<p><span class="font-blue"><strong>I.- Quieres devolver tu préstamo antes que termine su plazo</strong>.</span></p>
<p>Dirígete a la sección "Mi Cuenta", haz clic en "Devolver" y allí te saldrá un mensaje que te indicara el monto y el tiempo que dispones para hacer el pago y cancelar la cuenta de una vez, o bien, la fecha y lo que tienes que cancelar en la siguiente quincena.</p>
<p><span class="font-blue"><strong>II.- Como saber cuándo tengo que hacer el próximo pago.</strong></span></p>
<p>Dirígete a la sección "Mi Cuenta", haz clic en "Devolver" y allí te saldrá un mensaje con la fecha de tu próximo pago y el monto que tienes que cancelar.</p>
<p><span class="font-blue"><strong>III.- Que formas de cancelar tengo disponibles.</strong></span></p>
<p id="devolucion">Puedes cancelar haciendo una transferencia a nuestra cuenta, hacer un depósito en nuestra cuenta, en los diferentes bancos, o a través de nuestros asociados. O bien si tienes una tarjeta autorizada de débito, para que no tengas que preocuparte por el pago.</p>
<p>&nbsp;</p>
<h2><span class="font-blue"><strong>Mi préstamo </strong>se venció</span></h2>
<p>Es importante para nosotros que te comuniques con nosotros cuanto antes, valoramos la comunicación honesta y directa.</p>
<p><span class="font-blue"><strong>I.- Todavía no se venció, pero veo que se me hace difícil.</strong></span></p>
<p>Si estas en una situación en la que se te complica hacer el pago te recomendamos que nos escribas a <a href="mailto:ayuda@pa.clicredit.com" class="font-blue">ayuda@pa.clicredit.com</a> o nos llames al 260-8951, para buscar una solución en conjunto.</p>
<p><span class="font-blue"><strong>II.- Ya se venció el plazo de pago.</strong></span></p>
<p>Al estar en situación de mora, empiezas a generar intereses por usar el dinero más tiempo del pactado y honorarios de gestión de cuentas morosas. Es importante que te pongas en contacto rápido para encontrar una solución y detener los gastos extra. Te recomendamos que nos escribas a <a href="mailto:ayuda@pa.clicredit.com" class="font-blue">ayuda@pa.clicredit.com</a> o nos llames al 260-8951, para buscar una solución en conjunto.</p>
<p><span class="font-blue"><strong>III.- Se venció y no conseguimos hablar contigo para buscar una solución.</strong></span></p>
<p>Los intereses y los honorarios de gestión de cuentas atrasadas se añadirán a tu deuda, aumentándola. Por eso es importante que colabores para evitar males mayores.</p>
<p></p>
<h2 class="font-blue"><strong>Trabaja </strong>con nosotros</h2>
<p>CliCredit.com es parte de un proyecto que está creando tecnología que permite la evaluación de riesgo crediticio en tiempo real aplicando machine learning, big data y otras novedosas tecnologías.</p>
<p>Nuestro objetivo es ofrecer productos financieros innovadores que revolucionen la experiencia de usuario, usando el poder de la tecnología.</p>
<p>Estamos construyendo un equipo de profesionales para ayudar a mejorar la vida de millones de personas desatendidas por la industria financiera tradicional.</p>
<h2 class="font-blue"><strong>Ofertas </strong>actuales</h2>
<p>Si sientes que tu perfil podría encajar con nuestra filosofía, escríbenos a <a href="mailto:hr@creditagile.com,">hr@creditagile.com,</a> adjuntando tu CV  con extensión .pdf o .doc. Buscamos personal para Centroamérica, Colombia... Presencial o remoto, según perfil.</p>
<p>No hay ofertas publicadas en este momento.</p>
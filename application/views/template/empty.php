<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<article>
	<div class="container">
		<div class="row">
		<div class="col-sm-12">
			<div class="title">
				<div class="centered"><div><h2><?= $post->title ?></h2></div></div>
			</div>
		</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<?= $post->content ?>
			</div>
		</div>
	</div>
</article>
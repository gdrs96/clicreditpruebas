<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div id="content">
	<section>
		<div class="page-heading">
			<div class="container">
				<h1><span class="line line__left"></span><i class="fa fa-plus"></i> Editar Categoría</h1>
			</div>
		</div>
		<div class="page-content">
			<div class="container">
				<div class="col-md-8 col-md-offset-2">
					<form class="job-manager-form" role="form" method="post" action="<?php echo base_url() ?>perfil/guardar-etiqueta" enctype="multipart/form-data">
						<input type="hidden" name="id" value="<?= $etiqueta->id ?>" required>
						<fieldset>
							<label>Nombre <span class="required">*</span></label>
							<div class="field">
								<input type="text" class="form-control" placeholder="Nombre" name="nombre" value="<?= $etiqueta->nombre ?>" required>
							</div>
						</fieldset>
						<fieldset>
							<label>Slug <span class="required">*</span></label>
							<div class="field">
								<input type="text" class="form-control" placeholder="Slug" name="slug" value="<?= $etiqueta->slug ?>" required>
							</div>
						</fieldset>
						<button class="btn btn-lg btn-primary btn-block" type="submit" ><i class="fa fa-floppy-o"></i> Guardar</button>
					</form>
				</div>
			</div>
			<hr />
		</div>
	</section>
</div>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<footer>
	<div class="green-line"></div>
	<div class="blue footer">
		<div class="container">
			<div class="row justify-content-center">
				<div class="mb-4 col-lg-4 text-center text-lg-left">
					<img src="<?= base_url() ?>img/clicredit-logo-footer.png" class="img-fluid" alt="CliCredit.com">
					<p><a href="<?= base_url() ?>quienes-somos">Quienes somos</a></p>
					<p><a href="https://pa.clicredit.com/blog" target="_blank">Blog</a></p>
					<p><a href="<?= base_url() ?>ayuda-preguntas-frecuentes">Preguntas frecuentes</a></p>
					<p><a href="<?= base_url() ?>contacto">Contacto</a></p>
				</div>
				<div class="mb-4 col-8 col-sm-6 col-md-5 col-lg-4 text-center text-lg-left">
					<p class="footer-bigger">ATENCION AL CLIENTE:</p>
					<div class="text-left">
						<p><b>LUNES</b><span class="pull-right">9.00 am - 8.00 pm</span></p>
						<p><b>MARTES</b><span class="pull-right">9.00 am - 8.00 pm</span></p>
						<p><b>MIERCOLES</b><span class="pull-right">9.00 am - 8.00 pm</span></p>
						<p><b>JUEVES</b><span class="pull-right">9.00 am - 8.00 pm</span></p>
						<p><b>VIERNES</b><span class="pull-right">9.00 am - 8.00 pm</span></p>
						<p><b>SABADO</b><span class="pull-right">CERRADO*</span></p>
						<p><b>DOMINGO</b><span class="pull-right">CERRADO*</span></p>
						<p class="d-none d-sm-block text-center text-lg-left"><a href="tel:260-8951" class="font-38"><i class="fa fa-volume-control-phone" aria-hidden="true"></i> 260-8951</a></p>
						<p class="d-sm-none text-center"><a href="tel:260-8951" class="font-24"><i class="fa fa-volume-control-phone" aria-hidden="true"></i> 260-8951</a></p>
					</div>
				</div>
				<div class="mb-4 col-lg-3 offset-lg-1 text-center text-lg-left">
					<p class="footer-bigger">SIGUENOS:</p>
					<p>
						<a href="https://www.youtube.com/channel/UCIN-ceY8ydWRyHGopX3pYnw" target="_blank">
							<i class="fa fa-youtube-square fa-4x mx-2" aria-hidden="true"></i>
						</a>
						<a href="https://www.instagram.com/clicredit.pa/" target="_blank">
							<i class="fa fa-instagram fa-4x mx-2" aria-hidden="true"></i>
						</a>
						<a href="https://www.facebook.com/CliCredit.pa/" target="_blank">
							<i class="fa fa-facebook-square fa-4x mx-2" aria-hidden="true"></i>
						</a>
					</p>
				</div>
			</div>
		</div>
		<br>
		<div class="green">
			<div class="container">
				<div class="row py-3">
					<div class="col-sm-12">
						<ul class="nav text-center flex-column flex-lg-row justify-content-lg-end">
							<li class="nav-item"><a class="nav-link" href="<?= base_url() ?>empleate">Empléate</a></li>
							<li class="nav-item"><a class="nav-link" href="<?= base_url() ?>politica-privacidad">Política de privacidad</a></li>
							<li class="nav-item"><a class="nav-link" href="<?= base_url() ?>terminos-condiciones">Términos y Condiciones</a></li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<a target="_blank" class="chat-wsp" href="https://api.whatsapp.com/send?phone=50763777870">
	<i class="fa fa-whatsapp" aria-hidden="true"></i>
</a>
<?php
if(!$this->session->userdata('type')){
	?>
	<div class="modal fade" id="loginModal" role="dialog" aria-labelledby="loginModal">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body font-blue">
					<h3 class="modal-title mt-4" style="line-height: 1 !important" >Acceso a <b>MI CUENTA</b></h3>
					<hr class="linea-azul-small mt-0">
					<br>
					<form class="form" method="post" action="<?= base_url() ?>acceso/entrar" >
						<div class="form-group">
							<label>Correo electrónico / Cedula<span class="required">*</span></label>
							<input type="text" class="form-control" placeholder="email" name="email" required autofocus>
						</div>
						<div class="form-group">
							<label class="pull-lef">Contraseña <span class="required">*</span></label>
							<input type="password" class="form-control" placeholder="Contraseña" name="pass" required>
						</div>
						<div class="text-center">
							<p><b><a class="font-blue" href="<?= base_url() ?>acceso/recuperar-cuenta">¿Ha olvidado su contraseña?</a></b></p>
							<p><button class="btn btn-solicita width300" type="submit"> ENTRAR </button></p>
							<p><b><a  class="font-blue" href="<?= base_url() ?>solicitud/paso-1">¿No tienes cuenta? <span class="font-green">Registrate</span></a></b></p>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<?php
}
?>

<script src="<?= base_url() ?>js/popper.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/bootstrap.min.js" type="text/javascript"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?= base_url() ?>js/ie10-viewport-bug-workaround.js" type="text/javascript"></script>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
//Activar popover & tooltip -Bootrastap -
$(function () {
  $('[data-toggle="popover"]').popover({
	trigger: 'focus'
  })
  $('[data-toggle="tooltip"]').tooltip({
		trigger: 'focus',
		placement: 'bottom'
	})
  $('[data-toggle="tooltip-top"]').tooltip({
		trigger: 'focus',
		placement: 'top'
	})
  $('[data-toggle="tooltip-hover"]').tooltip({
		trigger: 'hover',
		placement: 'bottom'
	})
	// activar y desactivar animacion simulador
	$('.rangeslider__handle').addClass('slider_animated');
	let clickeado = false;
	$('.rangeslider__handle').click(function() {
		clickeado = true;
		$('.rangeslider__handle').removeClass('slider_animated');
	})
	$('.rangeslider__handle').mouseover(function() {
		if (! clickeado)
			$(this).removeClass('slider_animated');
	})
	$('.rangeslider__handle').mouseout(function() {
		if (! clickeado)
			$(this).addClass('slider_animated');
	})
	//mensaje cuando pasas el limite
	$('#amount').change(function() {
		if($(this).val() > 250) {
			$('#msg_maximo').addClass('remark');
		} else {
			$('#msg_maximo').removeClass('remark');
		}
	});
	//mostrar modal si entras por primera vez a la beta
	var firstTime = localStorage.getItem("firstTime");
	console.log(firstTime)
	if (firstTime !== 'no') {
		swal({
			title: "Estamos abiertos en PRUEBA.",
			text:  "Debido a que estamos en proceso de pruebas, la operativa puede presentar incovenientes. \n\n ¡Agredecemos su comprensión!",
			type: "warning"
		});
		localStorage.setItem("firstTime", "no")
	}
})  
//funcion creada para abrir el chat del sistema, debe llamarse con Onclick en el link o boton respectivo
	function openChat() 
	{
		$zopim.livechat.window.show();
	}
	
	var maximo = <?php if( $this->session->userdata('max_amount') ){echo $this->session->userdata('max_amount');}else{ echo 500; }?>

	function amountPlus(){
		if($('#amount').val() < maximo ){
			$('#amount').val( parseInt( $('#amount').val() ) + 5);
		}
	}
	function amountMinus(){
		if($('#amount').val() > 20 ){
			$('#amount').val($('#amount').val() - 5);
		}
	}
	function durationPlus(){
		if($('#duration').val() < 6 ){
			$('#duration').val( parseInt( $('#duration').val() ) + 1);
		}
	}
	function durationMinus(){
		if($('#duration').val() > 1 ){
			$('#duration').val($('#duration').val() - 1);
		}
	}
	
	window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
		d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
			$.src="https://v2.zopim.com/?51KSzewxSEVvPsDhaNw6xt4hTsM27F0l";z.t=+new Date;$.
			type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
		</script>
		<!--End of Zendesk Chat Script-->
	</body>
	</html>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container mb-5">
	<div class="row mt-4">
		<div class="col">
			<h1 class="font-blue mb-0-5">Buscador de <b>CLIENTES</b></h1>
			<form role="form" class="row justify-content-between" method="post" action="<?= base_url() ?>dashboard/buscar-clientes" name="clientes_buscar" enctype="multipart/form-data">
				<div class="col-sm-8 col-md-4 col lg-5">
					<div class="form-group">
						<div class="field">
							<input class="form-control" id="cliente" name="cliente" placeholder="Id, email, cedula o celular" required>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 col-lg-5 text-right">
					<button type="submit" class="btn px-5 btn-blue"><i class="fa fa-search"></i> Buscar</button>
					<?php if(check_rol_view($this->session->userdata('roles'),array(4))) : ?>
						<a href="<?= base_url() ?>dashboard/clientes" >
							<button type="button" class="btn font-white ml-2" style="background-color: #5EAC25">CLIENTES</button>
						</a>
						<a href="<?= base_url() ?>dashboard/prestamos">
							<button type="button" class="btn font-white ml-2" style="background-color: #5EAC25">PRÉSTAMOS</button>
						</a>
					<?php endif; ?>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<h2 class="font-blue">Gestión de <b>CLIENTES</b></h2>
			<table class="table table-hover font-blue text-center">
				<thead>
					<tr class="blue font-white">
						<th>Nombre</th>
						<th>Cédula</th>
						<th>E-mail</th>
						<th>Teléfono</th>
						<th>Estado de cuenta</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td><?= $user->name.' '.$user->segundo_nombre.' '.$user->primer_apellido.' '.$user->segundo_apellido ?></td>
						<td><?php $t = !empty($user->n_identificacion)?$user->n_identificacion:null; echo set_value('cedula',$t); ?></td>
						<td><?php $t = !empty($user->email)?$user->email:null; echo set_value('email',$t); ?></td>
						<td><?php $t = !empty($user->telefono)?$user->telefono:null; echo set_value('telefono',$t); ?></td>
						<td>
							<span value="activo"
								<?php 
									if($user->status == 'activo'){ echo 'class="font-1 badge btn-green"> Activo'; }else{ echo 'class="badge btn-red"> Inactivo'; } 
								?>
							</span>
						</td>
						<td>
							<?php if (!empty($prestamo_actual)) : ?>
								<a href="<?= base_url() ?>dashboard/prestamo/<?= $prestamo_actual[0]->id_prestamo ?>" class="btn btn-blue"><i class="fa fa-money font-1"></i> </a>
							<?php endif; ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-6">
			<h2 class=" linea-azul font-blue"><b>PERFIL</b></h2>
		</div>
	</div>
	<form class="form-row form-inline mt-4" method="post" action="<?= base_url() ?>dashboard/cliente" enctype="multipart/form-data">
		<div class="col-md-6 font-blue">
			<div class="row">
				<div class="col-12 d-flex justify-content-between mb-2">
					<label><b>ID Usuario</b></label>
					<span class="pull-right mr-1"><?= $user->id ?></span>
				</div>
				<div class="col-12 d-flex justify-content-between mb-2">
					<label><b>Username</b></label>
					<input  class="form-control text-right w-75" type="text" name="nicename" value="<?php $t = !empty($user->nicename)?$user->nicename:null; echo set_value('nicename',$t); ?>">
				</div>
				<div class="col-12 d-flex justify-content-between mb-2">
					<label><b>Nombre</b></label>
					<input  class="form-control text-right w-75" type="text" name="name" value="<?php $t = !empty($user->name)?$user->name:null; echo set_value('name',$t); ?>">
				</div>
				<div class="col-12 d-flex justify-content-between mb-2">
					<label><b>Email</b></label>
					<input  class="form-control text-right w-75" type="email" name="email" value="<?php $t = !empty($user->email)?$user->email:null; echo set_value('email',$t); ?>">
				</div>
				<div class="col-12 d-flex justify-content-between mb-2">
					<label><b>Cambiar imagen</b></label>
					<div class="custom-file w-75">
						<input type="file" class="custom-file-input" id="userfile" name="userfile">
						<label class="custom-file-label" for="validatedCustomFile">Actualizar imagen...</label>
					</div>
				</div>
				<div class="col-12 d-flex justify-content-between mb-2">
					<label><b>Calificación</b></label>
					<input type="hidden" id="id_calificacion" name="id_calificacion" value="<?= $user->id_calificacion ?>">
					<select class="form-control text-right w-75" id="calificacion">
						<?php foreach($calificaciones as $calificacion):?>
							<option value="<?php echo $calificacion->id;?>"><?php echo $calificacion->id;?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-12 d-flex justify-content-between">
					<label><b>Estado de cuenta</b></label>
					<select class="form-control text-right w-75" name="status">
						<option value="activo" <?php if( !empty($user->status)){ if($user->status == 'activo' ){ echo 'selected'; } } ?>>activo</option>
						<option value="inactivo" <?php if( !empty($user->status)){ if($user->status == 'inactivo' ){ echo 'selected'; } } ?>>inactivo</option>
					</select>
				</div>
				<input type="hidden" name="id" value="<?php if(isset($user->id)) echo $user->id; ?>">
			</div>
		</div>
		<div class="col-md-6 font-blue">
			<?php if( file_exists($user->image)): ?>
				<img style="max-height:175px" src="<?= base_url().$user->image ?>" alt="Foto de Perfil" class="img-fluid d-block mx-auto" id="user_image"/>
			<?php endif; ?>
			<p class="text-center cal-text mt-2"><b>Su calificación es <strong class="cal-number"><?= $user->id_calificacion ?></strong> de <strong class="cal-number">10</strong></b></p>
			<div class="col-12 d-flex justify-content-center">
				<button class="btn btn-blue" type="submit"><i class="fa fa-floppy-o"></i> Guardar</button>
				<button class="btn btn-red ml-3" onclick="user_image_del(<?php $t = isset($user->id)?$user->id:null;echo set_value('id',$t); ?>)"><i class="fa fa-trash"></i> Borrar foto</button>
			</div>
		</div>
	</form>
</div>
<div class="page-content">
	<div>
	</div>
</div>
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<script type="text/javascript">
	function user_image_del(id){
		var r = confirm('Por favor confirma el borrado');
		if(r == true){
			$.ajax({
				type:	'POST',
				url:	'<?= base_url() ?>dashboard-ajax/user-image-del/'+id,
				success: function(result){
					$.gritter.add({
						text: result,
						sticky: false,
						time: '',
						class_name: 'gritter-custom'
					});
					$('#user_image').slideUp('slow', function(){});
				}
			});
		}
	}
	$(document).ready(function(){         
		var id_calificacion =  $('#id_calificacion').val();
		$('#calificacion').val(id_calificacion);
		$('#calificacion').change(function() {
			$('#id_calificacion').val($(this).val());
		});
		$('#userfile').change(function() {
			var value = $(this).val();
			var file = value.split("\\");
			var fileName = file[file.length-1];
			$("label[for='validatedCustomFile']").text(fileName);
		});
	});


</script>


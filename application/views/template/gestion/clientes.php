<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row mt-4 mb-5">
		<div class="col">
			<h1 class="font-blue mb-0-5">Buscador de <b>CLIENTES</b></h1>
			<form role="form" class="row justify-content-between" method="post" action="<?= base_url() ?>dashboard/buscar-clientes" name="clientes_buscar" enctype="multipart/form-data">
				<div class="col-sm-8 col-md-4 col-lg-5">
					<div class="form-group">
						<div class="field">
							<input class="form-control" id="cliente" name="cliente" placeholder="Id, email, identificación, celular o estado" required>
						</div>
					</div>
				</div>
				<div class="col-sm-4 col-md-3 col-lg-5 text-right">
					<button type="submit" class="btn px-5 btn-blue"><i class="fa fa-search"></i> Buscar</button>
					<?php if(check_rol_view($this->session->userdata('roles'),array(4))) : ?>
						<a href="<?= base_url() ?>dashboard/prestamos">
							<button type="button" class="btn font-white ml-2" style="background-color: #5EAC25">PRÉSTAMOS</button>
						</a>
					<?php endif; ?>
				</div>
			</form>
			<div class="row  mt-4 mb-2">
				<div class="col d-flex justify-content-between">
					<h2 class="font-blue mb-0">Índice de <b>CLIENTES</b></h2>
					<div class="d-flex align-items-center">
						<p class="mr-3">Mostrando <?= sizeof($users) ?> de <?php if(isset($users_total)) echo $users_total; ?> Usuarios</p>
						<?php if( isset( $pagination )){ echo $pagination; }?>
					</div>
				</div>
			</div>
			<?php if(! empty($users)): ?>
				<table class="table table-hover text-center" id="table-users">
					<thead>
						<tr class="blue font-white">
							<th>ID</th>
							<th>Identif.</th>
							<th>Nombre</th>
							<th>Email</th>
							<th>Teléfono</th>
							<th>Estado</th>
							<th>Registro</th>
							<th>Acceso</th>
							<th style="min-width: 70px">Acciones</th>
						</tr>
					</thead>
					<tbody class="font-blue">
						<?php foreach ($users as $key): ?>
							<tr id="container_userid_<?= $key->id ?>">
								<td><?= $key->id ?></td>
								<td><?= $key->n_identificacion; ?></td>
								<td><?= $key->name.' '.$key->segundo_nombre.' '.$key->primer_apellido.' '.$key->segundo_apellido ?></td>
								<td><?= $key->email; ?></td>
								<td><?= $key->telefono; ?></td>
								<td>
									<?php if($key->status == "activo") : ?>
										<span class="badge btn-green">Activo</span>
									<?php else : ?> 
										<span class="badge btn-red">Inactivo</span>
									<?php endif; ?>
								</td>
								<td><?= date('d-m-y', strtotime($key->dateregistered)); ?></td>
								<td><?= date('d-m-y H:i', strtotime($key->datelastlogin)); ?></td>
								<td class="d-flex justify-content-around">
									<a href="<?= base_url() ?>dashboard/cliente/<?= $key->id ?>" class="btn btn-blue"><i class="fa fa-user"></i> </a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php else : ?>
				<div class="alert alert-info mt-5"><button type="button" class="close" aria-hidden="true" data-dismiss="alert">×</button><strong><?= $info_search ?></strong></div>
			<?php endif; ?>
		</div>
	</div>
	<div class="page-content">
		<div>

		</div>
	</div>
</div>
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/datatable.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/select2.css">
<script src="<?= base_url() ?>js/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/moment.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datetime-moment.js"></script>
<script src="<?= base_url() ?>js/select2.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
		$('.pagination li').each(function(){
			$(this).addClass('page-item');
			$(this).find('a').addClass('page-link');
		});
		$.fn.dataTable.moment('DD-MM-YY');
		$.fn.dataTable.moment('DD-MM-YY H:m');
		$('#table-users').DataTable({
			"aoColumns": [
			/* ID */ null,
			/* identificacion */ { "bSortable": false },
			/* nombre */ null,
			/* email */ null,
			/* telefono */ { "bSortable": false },
			/* status */ { "bSortable": false },
			/* registro */ { "bSortable": true },
			/* ult. acceso */ { "bSortable": true }, 
			/* acciones */ { "bSortable": false }
			],
			"bPaginate": false,
			//  "bLengthChange": false,
			"bSearch": false,
			"bFilter": false,
			"order": [[0, 'desc']],
			"bInfo": false,
			"bAutoWidth": false,
			"processing": true, 
			//"lengthMenu":     "Show _MENU_ entries",
			"language": {
				"zeroRecords": "No hay usuarios",
				"search": "Buscar:",
				"paginate": {
					"next":       "Siguiente",
					"previous":   "Anterior"
				}
			}
		});
		$('#table-users_wrapper .row').first().remove();
		$('#table-users_wrapper .row .col-sm-12').addClass('px-0');
	});
</script>
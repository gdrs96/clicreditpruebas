<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row">
		<div class="col-lg-6 text-center">
			<a href="<?= base_url() ?>gestion/prestamos" class="btn btn-solicita space-bottom-20 space-top-20">Préstamos</a>
		</div>
		<div class="col-lg-6 text-center">
			<a href="<?= base_url() ?>gestion/clientes" class="btn btn-solicita space-bottom-20 space-top-20">Clientes</a>
		</div>
	</div>
	<div class="row">
		<div class="col text-center">
			<a href="<?= base_url() ?>gestion/prestamos-estados" class="btn space-bottom-20 space-top-20">Estados</a>
		</div>
	</div>
</div>
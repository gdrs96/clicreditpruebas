<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="content cuenta">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<br>
				<hr class="hrazul">
				<h1 class="cuenta-titulo"><b>Estado</b></h1>
				<hr class="hrazul">
				<form role="form" method="post" action="<?= base_url() ?>gestion/prestamo-estado-guardar" name="pretamo_estado" enctype="multipart/form-data">
					<div class="col-md-10 col-md-offset-1">
						<div class="form-group">
							<label>Nombre <span class="required">*</span></label>
							<div class="field">
								<input type="text" class="form-control" placeholder="Nombre" name="nombre" value="<?php $t = !empty($pretamo_estado->nombre)?$pretamo_estado->nombre:null; echo set_value('nombre',$t); ?>" required>
							</div>
						</div>
						<div class="form-group">
							<label>Color <span class="required">*</span></label>
							<div class="field">
								<input class="form-control jscolor" value="<?php $t = !empty($pretamo_estado->color)?$pretamo_estado->color:000000; echo set_value('color',$t); ?>" name="color" type="text" required>
							</div>
						</div>
						<div class="col-md-8 col-md-offset-2">
							<input type="hidden" name="id" value="<?php $t = isset($pretamo_estado->id)?$pretamo_estado->id:null;echo set_value('id',$t); ?>" >
							<p><button class="btn btn-lg btn-primary btn-block" type="submit" ><i class="fa fa-floppy-o"></i> Guardar</button></p>
							<br>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<script src="<?= base_url() ?>js/jscolor.min.js"></script>

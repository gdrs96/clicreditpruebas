<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row mt-4">
		<div class="col-md-12">
			<h1 class="font-blue mb-0-5">Buscador de  <b>PRÉSTAMOS</b></h1>
			<form role="form" class="row" method="post" action="<?= base_url() ?>dashboard/buscar-prestamos" name="prestamos_buscar" enctype="multipart/form-data">
				<div class="col-md-4">
					<div class="form-group">
						<div class="field">
							<input class="form-control" id="cliente" name="cliente" placeholder="Id, email, identificación o nombre">
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<select class="form-control" id="estado" name="estado">
						<option value="">Estado</option>
						<?php
						if(!empty($estados_busqueda)){
							foreach ($estados_busqueda as $key) {
								?>
								<option value="<?= $key->id ?>"><?= $key->nombre ?></option>
								<?php
							}
						}
						?>
					</select>
				</div>
				<div class="col-md-6 text-right">
				<button type="submit" class="btn px-5 blue font-white"><i class="fa fa-search"></i> Buscar</button>
					<?php if(check_rol_view($this->session->userdata('roles'),array(4))) : ?>
						<a href="<?= base_url() ?>dashboard/clientes" >
							<button type="button" class="btn font-white ml-2" style="background-color: #5EAC25">CLIENTES</button>
						</a>
						<a href="<?= base_url() ?>dashboard/prestamos">
							<button type="button" class="btn font-white ml-2" style="background-color: #5EAC25">PRÉSTAMOS</button>
						</a>
					<?php endif; ?>
				</div>
			</form>
		</div>
	</div>
	<div class="row">
		<div class="col">
			<h2 class="font-blue">Gestión <b>PRÉSTAMOS</b></h2>
			<table class="table table-hover font-blue">
				<thead>
					<tr class="blue font-white">
						<th>Nombre</th>
						<th>Cedula</th>
						<th>E-mail</th>
						<th>Celular</th>
						<th>2º Celular</th>
						<th>Ref. personal</th>
						<th>Teléfono fijo</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$datos = json_decode($prestamo[0]->datos, true);
					?>
					<tr>
						<td><?= $cliente->name.' '.$cliente->segundo_nombre.' '.$cliente->primer_apellido.' '.$cliente->segundo_apellido ?></td>
						<td><?php $t = !empty($cliente->n_identificacion)?$cliente->n_identificacion:null; echo set_value('cedula',$t); ?></td>
						<td><?php $t = !empty($cliente->email)?$cliente->email:null; echo set_value('email',$t); ?></td>
						<td><?php echo isset($datos['2_10_telefono'])? $datos['2_10_telefono'] : '-' ?></td>
						<td><?php echo isset($datos['2_13_telefono2'])? $datos['2_13_telefono2'] : '-' ?></td>
						<td><?php echo isset($datos['2_17_telefono_referencia'])? $datos['2_17_telefono_referencia'] : '-' ?></td>
						<td><?php $t = !empty($cliente->telefono)?$cliente->telefono:null; echo set_value('telefono',$t); ?></td>
						<td>
							<?php if (!empty($cliente)) : ?>
								<a href="<?= base_url() ?>dashboard/cliente/<?= $cliente->id ?>" class="btn btn-blue"><i class="fa fa-user"></i> </a>
								<a  class="btn btn-blue" id="ver_cedula"><i class="fa fa-eye" aria-hidden="true"></i></a>
							<?php endif; ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h2 class="font-blue"><i class="mr-3 fa fa-chevron-down enlace" id="historialPrestamoButton"></i>Historial de <b>PRÉSTAMOS</b></h2>
			<table class="table table-hover font-blue" id="historialPrestamoTable">
				<thead>
					<tr class="blue font-white">
						<th>Petición</th>
						<th>Finalizo</th>
						<th>Qnas</th>
						<th>Monto</th>
						<th>Provincia</th>
						<th>Corregimiento</th>
						<th>Sector</th>
						<th>Dispositivo</th>
						<th>IP</th>
						<th>ALF</th>
						<th>Ver</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($prestamos)){
						foreach ($prestamos as $key ) {
							$prestamo_datos = json_decode($key->datos, true)
							?>
							<tr>
								<td><?= date('d-m-Y', strtotime($key->created_at)) ?></td>
								<td><?= date('d-m-Y', strtotime($key->created_at.'+'.($key->tiempo*15).' Days')) ?></td>
								<td><?= $key->tiempo ?></td>
								<td><?= $key->cantidad ?></td>
								<td><?php echo isset($select['4_1'])? $select['4_1'] : '-' ?></td>
								<td><?php echo isset($select['4_3'])? $select['4_3'] : '-' ?></td>
								<td><?php echo isset($select['4_4'])? $select['4_4'] : '-' ?></td>
								<td><?php echo isset($prestamo_datos['user_agent']) ? $prestamo_datos['user_agent'] : '-'; ?></td>
								<td><?php echo isset($prestamo_datos['ip']) ? $prestamo_datos['ip'] : '-'; ?></td>
								<td><?php echo !empty($key->puntuacion)? $key->puntuacion : '-' ?></td>
								<td><a href="<?= base_url() ?>dashboard/prestamo/<?= $key->id_prestamo ?>"><i class="fa fa-eye font-blue"></i></a></td>
								<td><?php echo !empty($key->nombre) ?'<span class="badge font-white" style="background:#'.$key->color.'">'.$key->nombre.'</span>':'-'; ?></td>
							</tr>
							<?php
						}
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row mt-3">
		<div class="col-md-6 font-blue">
			<h2 class="linea-azul">Posición <b>ACTUAL</b></h2>
			<?php 
				if(! empty($prestamo)) : 
				$key = $prestamo[0];
				$key->datos = json_decode($key->datos);
			?>
				<p class="mb-0"><b>ID Préstamo</b> <span class="pull-right"><?= $key->id_prestamo ?></span></p>
				<p class="mb-0"><b>Fecha de simulacion:</b> <span class="pull-right">
					<?php 
						if(!empty($key->created_at))
							echo date('d/m/Y h:i:s', strtotime($key->created_at));
						else
							echo "-"; 
					?>
				</span></p>
				<p class="mb-0"><b>Última actualización:</b> <span class="pull-right">
					<?php 
						if(!empty($key->ultima_actualizacion))
							echo date('d/m/Y h:i:s', strtotime($key->ultima_actualizacion));
						else
							echo "-";
					?>
				</span></p>
				<br>
				<p class="mb-0"><b>Fecha efectiva:</b> 
					<span class="pull-right">
						<span id="new_fecha">
							<?php 
								if(!empty($key->fecha_efectivo_prestamo))
									echo date('d/m/Y', strtotime($key->fecha_efectivo_prestamo));
								else
									echo "-";
							?>
						</span>
						<?php if($key->prestamo_estado_id==13) : ?>
							<i class="fa fa-pencil ml-3 enlace" data-toggle="modal" data-target="#editModal" data-whatever="fecha" data-field="fecha"></i>
						<?php endif; ?>
					</span>	
				</p>
				<?php if($key->prestamo_estado_id==13) : ?>
					<p class="mb-0"><b>Monto solicitado: <span class="pull-right"><span id="new_cantidad">B./<?= $key->cantidad ?></span> <i class="fa fa-pencil ml-3 enlace" data-toggle="modal" data-target="#editModal" data-whatever="cantidad" data-field="cantidad" data-whatever="cantidad"></i></span></b></p>				<p class="mb-0"><b>Días del préstamo:</b> <span class="pull-right" id="total_dias"></span></p>	
					<p class="mb-0"><b>Número de quincenas: <span class="pull-right"><span id="new_tiempo"><?= $key->tiempo ?></span> <i class="fa fa-pencil ml-3 enlace" data-toggle="modal" data-target="#editModal" data-whatever="cantidad_quincenas" data-field="cantidad de quincenas" ></i></span></b></p>
					<p class="mb-0"><b>Ajuste a Qnc/Cuota:</b><span class="pull-right"><input type="checkbox"  id="ajustar_quincena" name="ajustar_quincena" value="<?= $key->ajustar_quincena ?>" ></span>
				<?php else : ?>
					<p class="mb-0"><b>Monto solicitado: <span class="pull-right">B./<?= $key->cantidad ?></span></b></p>
					<p class="mb-0"><b>Días del préstamo:</b> <span class="pull-right" id="total_dias"></span></p>
					<p class="mb-0"><b>Número de quincenas: <span class="pull-right"><?= $key->tiempo ?></span></b></p>
				<?php endif ?>
				<?php if($key->prestamo_estado_id==8 || $key->prestamo_estado_id==9 || $key->prestamo_estado_id==10 || $key->prestamo_estado_id==11 || $key->prestamo_estado_id==12 || $key->prestamo_estado_id==13) : ?>
					<p class="mb-0"><b>Total a devolver:</b> <span class="pull-right" id="total"></span></p>
				<?php else : ?>
					<p class="mb-0"><b>Total a devolver:</b> <span class="pull-right"><?= $key->monto_total_prestamo ?> </span></p>
				<?php endif ?>
				<p><b>Fecha de devolución:</b> <span class="pull-right" id="fecha_finalizacion"></span></p>
				<input type="hidden" id="id_prestamo_cliente" name="id_prestamo_cliente" value="<?= $key->id_prestamo; ?>">
				<input type="hidden" id="prestamo_estado_id" name="prestamo_estado_id" value="<?= $key->prestamo_estado_id; ?>">
				

				<input type="hidden" id="cedula_cliente"   value="<?php $valor = !empty($cedula_cliente)?$cedula_cliente[0]->fichero:"$cliente->img_cedula"; echo set_value('cedula_cliente', $valor); ?>" >

				

			<?php endif; ?>
			<h2 class="mb-0">Calendario de <b>PAGOS</b></h2>
			<?php if($key->prestamo_estado_id  == 1 || $key->prestamo_estado_id ==2 || $key->prestamo_estado_id ==3 ||  $key->prestamo_estado_id ==5 || $key->prestamo_estado_id ==6 ||$key->prestamo_estado_id ==7  || $key->prestamo_estado_id ==13) : ?>
				<a class="btn btn-blue btn-green-hover w-100" onclick="agregarQuincena()"><i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar quincena</a>
				<table class="table font-blue table-hover text-center"  id="table_quincenas">
					<thead>
						<tr>
							<th>Qncns</th>
							<th>Fecha pago</th>
							<th>Importe (B./)</th>
							<th>Status Qnc.</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tfoot>
					<tr>
					
						<th colspan="4" style="text-align:right">Total a cancelar:</th>
						<th></th>
					</tr>
					</tfoot>
				</table>
			<?php else : ?>
				<p>No existen datos para mostrar</p>
			<?php endif; ?>
			<h2 class="mb-0">Reporte de <b>PAGOS</b></h2>
			<?php if($key->prestamo_estado_id  == 1 || $key->prestamo_estado_id ==2 || $key->prestamo_estado_id ==3 ||  $key->prestamo_estado_id ==5 || $key->prestamo_estado_id ==6 ||$key->prestamo_estado_id ==7  || $key->prestamo_estado_id ==13) : ?>
				<form class="mb-0" method="post" id="upload_form" align="center" action="<?= base_url() ?>reporte_pago/reportarPago" enctype="multipart/form-data">  
					<input type="file" name="imagen_recibo" id="imagen_recibo" class="inputfile inputfile-1-blue" data-multiple-caption="{count} files selected" multiple style="display: none" />
					<label class="mb-0 rounded" for="imagen_recibo"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Reportar recibo pago</span></label>
					<input type="hidden" id="id_prestamo" name="id_prestamo" value="<?= $key->id_prestamo; ?>">
					<input type="hidden" id="idCliente_report" name="idCliente_report" value="<?=  $key->id_cliente; ?>" class="form-control" type="text">
				</form>
				<table class="table font-blue table-hover text-center" id="tableReportePago">
					<thead>
						<tr>
							<th>F. subida</th>
							<th>F. efecto</th>
							<th>Importe</th>
							<th>Status</th>
							<th>Acciones</th>
						</tr>
					</thead>
					<tfoot>
						<tr>
							<th colspan="4" style="text-align:right">Total cancelado:</th>
							<th></th>
						</tr>
					</tfoot>
				</table>
			<?php else : ?>
				<p>No existen datos para mostrar</p>
			<?php endif; ?>
		</div>
		<div class="col-md-6 font-blue">
			<h2 class="linea-azul">Cambiar <b>ESTADOS</b></h2>
			<div class="row justify-content-end">
				<div class="col-md-5 form-group">
					<label>Añadir estado</label>
					<select class="form-control" id="estado_<?= $key->id_prestamo ?>" name="estado_<?= $key->id_prestamo ?>">
						<option value="0">Seleccione un estado</option>
						<?php
						if(!empty($estados)){
							foreach ($estados as $key1) {
								?>
								<option value="<?= $key1->id ?>"><?= $key1->nombre ?></option>
								<?php
							}
						}
						?>
					</select>
				</div>
				<div class="col-md-7">
					<label>Observaciones:</label>
					<textarea rows="1" class="form-control" id="observaciones_<?= $key->id_prestamo ?>" name="observaciones_<?= $key->id_prestamo ?>"></textarea>
				</div>
				<div class="col-md-4">
					<button type="button" class="btn btn-blue w-100" onclick="añadir_estado(<?= $key->id_prestamo ?>)">AÑADIR ESTADO</button>
				</div>
			</div>
			<h2 class="linea-azul mb-0">Estado <b>ACTUAL</b></h2>
			<?php if(!empty($estados_prestamo_actual)): ?>
				<table class="table table-hover font-blue" id="">
					<?php $actual = $estados_prestamo_actual[0]; ?>
					<tbody>
						<tr>
							<td class="text-center" style="width: 130px"><?= date('d/m/Y', strtotime($actual->fecha_actualizacion)) ?></td>
							<td class="text-center" style="width: 160px"><div class="badge font-white" style="background:#<?= $actual->color ?>"><?= $actual->nombre ?></div></td>
							<td><?= $actual->observaciones ?></td>
						</tr>
					</tbody>
				</table>
			<?php else : ?>
				<p>No existen datos para mostrar</p>
			<?php endif; ?>
			<h2 class="linea-azul mb-0">Histórico de <b>ESTADOS</b></h2>
			<?php if(!empty($estados_prestamo)) : ?>
				<table class="table table-hover font-blue" id="">
					<thead>
						<tr class="text-center">
							<th style="width: 130px">Modificado</th>
							<th style="width: 160px">Estado</th>
							<th>Observaciones</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($estados_prestamo as $key2) : ?>
							<tr>
								<td class="text-center"><?= date('d/m/Y', strtotime($key2->fecha_actualizacion)) ?></td>
								<td class="text-center"><div class="badge font-white" style="background:#<?= $key2->color ?>"><?= $key2->nombre ?></div></td>
								<td><?= $key2->observaciones ?></td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php else : ?>
				<p>No existen datos para mostrar</p>
			<?php endif; ?>
			<h2 class="mb-0">Documentos <b>ENVIADOS</b></h2>
			<form class="mb-0" method="post" id="cargar_recaudos" align="center" enctype="multipart/form-data">  
				<input type="file" name="imagen_documento[]" id="imagen_documento" class="inputfile inputfile-1-blue"  multiple="multiple" style="display: none" onsubmit="return false" />
				<label class="mb-0 rounded"for="imagen_documento"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Cargar documentos</span></label>
				<input type="hidden" name="id_prestamo" value="<?= $key->id_prestamo; ?>">
			</form>
			<table class="table font-blue table-hover text-center"  id="table_documentos">
				<thead>
					<tr>
						<th></th>
						<th>F. subida</th>
						<th>Descripción</th>
						<th>Observación</th>
						<th style="min-width: 146px">Acciones</th>
					</tr>
				</thead>
			</table>
			<h2 class="linea-azul">Datos <b>ASOCIADOS</b></h2>
			<?php if(!empty($prestamo)): ?>
				<ul class="nav nav-pills nav-fill" id="myTab">
					<li class="nav-item">
						<a class="nav-link active" id="misce-tab" data-toggle="tab" href="#misce" role="tab" aria-controls="misce" aria-selected="true">MISCELÁNEO</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="contac-tab" data-toggle="tab" href="#contac" role="tab" aria-controls="contac" aria-selected="true">CONTACTO</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="desem-tab" data-toggle="tab" href="#desem" role="tab" aria-controls="desem" aria-selected="true">DESEMBOLSO</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" id="scrip-tab" data-toggle="tab" href="#scrip" role="tab" aria-controls="scrip" aria-selected="true">SCR-IP-DISP</a>
					</li>
				</ul>
				<div class="tab-content pt-3" id="myTabContent">
					<div class="tab-pane fade show active" id="misce" role="tabpanel" aria-labelledby="misce-tab">
						<p class="mb-0"><b>¿Con qué clase social te identificas?</b> <span class="pull-right"><?php echo isset($select['2_3'])? $select['2_3'] : '-' ?></span></p>
						<p class="mb-0"><b>¿Cuál es tu género?</b> <span class="pull-right"><?php echo isset($select['2_4'])? $select['2_4'] : '-' ?></span></p>
						<p class="mb-0"><b>Código de licencia:</b> <span class="pull-right"><?php echo isset($datos['2_8_1_codigo_licencia'])? $datos['2_8_1_codigo_licencia'] : '-' ?></span></p>
						<p class="mb-0"><b>Placa de vehículo:</b> <span class="pull-right"><?php echo isset($datos['2_9_2'])? $datos['2_9_2'] : '-' ?></span></p>
						<p><b>¿Dónde Naciste?</b> <span class="pull-right"><?php echo isset($select['2_2'])? $select['2_2'] : '-' ?></span></p>
						<p class="mb-0"><b>Período de cobro:</b> <span class="pull-right"><?php echo isset($select['3_7'])? $select['3_7'] : '-' ?></span></p>
						<p class="mb-0"><b>¿Tienes tarjeta de crédito?(3.14)</b> <span class="pull-right"><?php echo isset($select['3_14'])? $select['3_14'] : '-' ?></span></p>
						<p class="mb-0"><b>¿Cuántas tienes?(5.4.1)</b> <span class="pull-right"><?php echo isset($select['5_4_1'])? $select['5_4_1'] : '-' ?></span></p>
						<p class="mb-0"><b>¿Total de deudas por mes?</b> <span class="pull-right"><?php echo isset($datos['3_15_1'])? $datos['3_15_1'] : '-' ?></span></p>

					</div>
					<div class="tab-pane fade" id="contac" role="tabpanel" aria-labelledby="contac-tab">
						<p class="mb-0"><b>Teléfono de contacto (Paso 1):</b> <span class="pull-right"><?php echo isset($cliente->telefono)? $cliente->telefono : '-' ?></span></p>
						<p class="mb-0"><b>¿Cuál es tu número de celular?</b> <span class="pull-right"><?php echo isset($datos['2_10_telefono'])? $datos['2_10_telefono'] : '-' ?></span></p>
						<p class="mb-0"><b>Indícanos otro número:</b> <span class="pull-right"><?php echo isset($datos['2_13_telefono2'])? $datos['2_13_telefono2'] : '-' ?></span></p>
						<p><b>Teléfono fijo(4.8):</b> <span class="pull-right"><?php echo isset($datos['4_8_1'])? $datos['4_8_1'] : '-' ?></span></p>
						<p class="mb-0"><b>Provincia:</b> <span class="pull-right"><?php echo isset($select['4_1'])? $select['4_1'] : '-' ?></span></p>
						<p class="mb-0"><b>Distrito:</b> <span class="pull-right"><?php echo isset($select['4_2'])? $select['4_2'] : '-' ?></span></p>
						<p class="mb-0"><b>Corregimiento:</b> <span class="pull-right"><?php echo isset($select['4_3'])? $select['4_3'] : '-' ?></span></p>
						<p class="mb-0"><b>Sector:</b> <span class="pull-right"><?php echo isset($select['4_4'])? $select['4_4'] : '-' ?></span></p>
						<p><b>Referencia de la casa:</b> <span class="pull-right"><?php echo isset($datos['4_6'])? $datos['4_6'] : '-' ?></span></p>
						<p class="mb-0"><b>Nombre de ref:</b> <span class="pull-right"><?php echo isset($datos['2_17_2'])? $datos['2_17_2'] : '-' ?></span></p>
						<p class="mb-0"><b>Celular ref. personal:</b> <span class="pull-right"><?php echo isset($datos['2_17_telefono_referencia'])? $datos['2_17_telefono_referencia'] : '-' ?></span></p>
						<p><b>Sector donde vive ref:</b> <span class="pull-right"><?php echo isset($select['2_17_1'])? $select['2_17_1'] : '-' ?></span></p>
						<p class="mb-0"><b>Empresa donde trabaja:</b> <span class="pull-right"><?php echo isset($datos['3_3_nombre_empresa'])? $datos['3_3_nombre_empresa'] : '-' ?></span></p>
						<p class="mb-0"><b>Telefono de empresa:</b> <span class="pull-right"><?php echo isset($datos['3_9_telefono_empresa'])? $datos['3_9_telefono_empresa'] : '-' ?></span></p>

					</div>
					<div class="tab-pane fade" id="desem" role="tabpanel" aria-labelledby="desem-tab">
						<p class="mb-0"><b>Nombre de la cuenta:</b> <span class="pull-right"><?php echo isset($datos['5_1_numero_cuenta'])? $datos['5_1_1_nombre_cuenta'] : '-' ?></span></p>
						<p class="mb-0"><b>Número de cuenta:</b> <span class="pull-right"><?php echo isset($datos['5_1_1_nombre_cuenta'])? $datos['5_1_numero_cuenta'] : '-' ?></span></p>
						<p class="mb-0"><b>Entidad bancaria</b> <span class="pull-right"><?php echo isset($select['5_2'])? $select['5_2'] : '-' ?></span></p>

					</div>
					<div class="tab-pane fade" id="scrip" role="tabpanel" aria-labelledby="scrip-tab">
						<p class="mb-0"><b>Scoring ALF-1 (%): </b> <span class="pull-right"></span></p>
						<p><b>Mínimo requerido:</b> <span class="pull-right"></span></p>
						<?php $datos_ip = json_decode($key->datos_ip, true); ?>
						<p class="mb-0"><b>Dispositivo</b><span class="pull-right"><?php echo isset($datos['user_agent'])? $datos['user_agent'] : '-' ?></span></p>
						<p><b>IP</b><span class="pull-right"><?php echo isset($datos['ip'])? $datos['ip'] : '-' ?></span></p>
						<p class="mb-0"><b>País</b><span class="pull-right"><?php echo isset($datos_ip['country_name'])? $datos_ip['country_name'] : '-' ?></span></p>
						<p class="mb-0"><b>Región</b><span class="pull-right"><?php echo isset($datos_ip['region_name'])? $datos_ip['region_name'] : '-' ?></span></p>
						<p class="mb-0"><b>Ciudad</b><span class="pull-right"><?php echo isset($datos_ip['city'])? $datos_ip['city'] : '-' ?></span></p>
						<p class="mb-0"><b>Latitud</b><span class="pull-right"><?php echo isset($datos_ip['latitude'])? $datos_ip['latitude'] : '-' ?></span></p>
						<p class="mb-0"><b>Longitud</b><span class="pull-right"><?php echo isset($datos_ip['longitude'])? $datos_ip['longitude'] : '-' ?></span></p>
					</div>
				</div>
			<?php endif; ?>	
		</div>
	</div>
	<?php if(!empty($prestamo)) : ?>
		<?php foreach ($prestamo as $key) : ?>
			<?php
				$prestamo_datos_array = (array) $key->datos;
			?>
		<?php endforeach; ?>
		<div class="row mt-4">
			<div class="col-md-12 font-blue">
				<h2 class="mb-0">Grupos de <b>VARIABLES</b></h2>
				<form name="datos_prestamo" id="datos_prestamo" action="<?= base_url() ?>dashboard/prestamo-guardar/<?= $prestamo[0]->id ?>">
					<?php
						//INFORME de Grupos de Variables / Algoritmo --- jjy
					    echo( $variables_capacidad_pago );
						echo( $variables_perfil_social );
						echo( $variables_responsabilidad );
						echo( $variables_honestidad );
						echo( $variables_trabajo );
						echo( $variables_macroeconomicas );
                        
						// EJEMPLO DE USO - Arbol de Aprobacion segun Algoritmo excel -- jjy
						// prp( $respuesta_algoritmo ) sólo para depuración
					?>
					<h2 class="linea-azul pb-0 mb-3 mt-5">Scoring de <b>CLIENTE</b></h2>
					<div class="row">
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								V. RESPONSABILIDAD
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['Responsabilidad'] ?>
							</div>
						</div>
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								V. HONESTIDAD
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['Honestidad'] ?>
							</div>
						</div>
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								V. TRABAJO
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['Trabajo'] ?>
							</div>
						</div>
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								N4 INTEGRIDAD
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['ma Nivel4'] ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								V. PERFIL SOCIAL
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['Perfil social'] ?>
							</div>
						</div>
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								N4 INTEGRIDAD
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['ma Nivel4'] ?>
							</div>
						</div>
						<div class="col-3  offset-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								N3 PSICOSOCIAL
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['ma Nivel3'] ?>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								Capacidad de PAGO
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['Capacidad de Pago'] ?>
							</div>
						</div>
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								N3 PSICOSOCIAL
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['ma Nivel3'] ?>
							</div>
						</div>
						<div class="col-3  offset-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								N2 Probabilidad PAGO (%)
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['Probabilidad pago'] ?>
							</div>
						</div>
					</div>
					<div class="row mb-4">
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								N1 MACROECONOMICOS
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['Macroeconomicas'] ?>
							</div>
						</div>
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								Mínimo REQUERIDO
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['Minimo requerido'] ?>
							</div>
						</div>
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="textbox">
								Perfil APROBACIÓN
							</div>
							<div class="valuebox">
								<?= $respuesta_algoritmo['Perfil aprobacion']?>
							</div>
						</div>
						<div class="col-3 px-0 mb-2 d-flex justify-content-center">
							<div class="valuebox stringvalue">
								<?= $respuesta_algoritmo['Respuesta'] ?>
							</div>
						</div>
					</div>
											
				</form>
				<?php foreach ($prestamo as $key) : ?>
					<input type="hidden" name="id_prestamo" value="<?= $key->id_prestamo ?>">
					<input type="hidden" id="cantidad" name="cantidad" value="<?= $key->cantidad ?>">
					<input type="hidden" id="tiempo" name="tiempo" value="<?= $key->tiempo ?>">
					<input type="hidden" name="datos" value='<?= $datos ?>'>
					<input type="hidden" id="id_cliente" name="id_cliente" value='<?= $key->id_cliente  ?>'>
					<input type="hidden" id="cantidad_quincenas" name="cantidad_quincenas" value='<?= $total_quincenas[0]->cantidad  ?>'>
					<input type="hidden" id="ultima_actualizacion" name="ultima_actualizacion" value='<?= $key->ultima_actualizacion  ?>'>
					<input type="hidden" id="ajustarQuincenas" name="ajustarQuincenas" value='<?= $key->ajustar_quincena  ?>'>
					<input type="hidden" id="tasa_interes" name="tasa_interes" value='<?= $calificacion[0]->tasa_interes  ?>'>
					<input type="hidden" id="fecha_finalizacion_prestamo"   value="<?php $valor = !empty($fecha_finalizacion_prestamo[0]->fecha_vencimiento)?$fecha_finalizacion_prestamo[0]->fecha_vencimiento:null; echo set_value('fecha_finalizacion_prestamo', $valor); ?>" >
					<input type="hidden" id="fecha_efecto" name="fecha_efecto" value='<?= $key->fecha_efectivo_prestamo  ?>'>

					<input type="hidden" id="monto_total_prestamo" name="monto_total_prestamo" value='<?= $key->monto_total_prestamo ?>'>
				<?php endforeach; ?>
			</div>
		</div>
		<div class="page-content">
		</div>
	<?php endif; ?>
</div>
</div>
</div>


<!--Modal detalle de prestamo -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<h3 class="modal-title">Añadir Quincena</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                   
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Quincena</label>
                            <div class="col-md-9">
                                <input name="numero_quincena" placeholder="" class="form-control" type="text" disabled>
                            </div>
                        </div>
                     
                          <div class="form-group">
                            <label class="control-label col-md-3">Importe</label>
                            <div class="col-md-9">
                                <input name="importe" placeholder="" class="form-control" type="text" data-parsley-required data-parsley-trigger="keyup" data-parsley-type="number">
                            </div>
                         </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Status Qnc.</label>
                            <div class="col-md-9">
                               <select class="form-control" id="estado_<?= $key->id_prestamo ?>" name="estado_quincena" data-parsley-required>
											<option value="">Seleccione un estado</option>
											<?php
											if(!empty($quincenas_estados)){
												foreach ($quincenas_estados as $key1) {
													?>
													<option value="<?= $key1->id ?>"><?= $key1->nombre ?></option>
													<?php
												}
											}
											?>
							</select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Fecha de vencimiento</label>
                            <div class="col-md-9">
                                <input name="fecha_vencimiento" id="fecha_vencimiento" placeholder="" class="form-control datepicker" data-parsley-required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <input type="hidden" value="" id="id_quincena" name="id_quincena"/> 
                        <input type="hidden" id="total_prestamo" name="total_prestamo">
                        <input type="hidden" id="fecha_final_prestamo" name="fecha_final_prestamo">
                        <input type="hidden" name="id_prestamo" value="<?= $prestamo[0]->id_prestamo ?>">
                       </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="guardar_quincena();" class="btn btn-primary">Guardar</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
<!-- en modal detalle de prestamo -->

<!-- modal-Reporte de pago -->
	<div class="modal fade" id="modal_reportePago" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
           <h4 class="modal-title font-blue" id="image-gallery-title"><b>Recibo de pago</b></h4>
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
            <form action="#" id="formPago" class="form-horizontal">
              <div class="form-group">
            	 <div class="row">
                   <div class="col-md-10">
                       <img src=""  class="imagepreview" style="width: 400px; height:300px" >
                   </div>
                  
            	</div>
            </div>
               <br>
                 <div class="form-group">
                    <label class="control-label col-md-4">Importe del reporte .B/:</label>
                      <div class="col-md-8">
                        <input id="importe" name="importe" placeholder="Importe" class="form-control" type="text" data-parsley-required data-parsley-trigger="keyup" data-parsley-type="number">
                        </div>
                 </div>
                   <div class="form-group">
                            <label class="control-label col-md-4">Fecha efecto</label>
                          <div class="col-md-8">
                                <input name="fecha_efecto" id="fecha_efecto" placeholder="Fecha de efecto" class="form-control datepicker" type="text" data-parsley-required>
                                <span class="help-block"></span>
                            </div>
                        </div>
            <div class="form-group">
               <label class="control-label col-md-4">Status</label>
                  <div class="col-md-8">
                    <select class="form-control" id="selectStatusReporte" name="selectStatusReporte" data-parsley-required>
						                 <?php
											if(!empty($reporte_pagos_estados)){
												foreach ($reporte_pagos_estados as $reporte) {
													?>
													<option value="<?= $reporte->id ?>"><?= $reporte->nombre ?></option>
													<?php
												}
											}
											?>
							</select>
                       </div>
                 </div>
                  <input type="hidden" id="id_pago" name="id_pago" value="" class="form-control" type="text">
                  <input type="hidden" id="idCliente" name="idCliente" class="form-control" type="text">
                 </form>
            </div>
          <div class="modal-footer">
          	    <button type="button" id="btnSaveNotify" onclick="enviarPago();" class="btn btn-success">Guardar y notificar</button>
          	    <button type="button" class="btn btn-danger" id="eliminar_pago">Eliminar pago</button>
            	<button type="button" id="btnSave" onclick="actualizarPago();" class="btn btn-primary">Guardar</button>

            	
            	
             </div>
        </div>
      </div>
   </div><!-- end modal reporte de pago -->

   


<!-- modal ver recibo  de pago -->
<div class="modal fade" id="modal_reciboPago" tabindex="-1" role="dialog" aria-labelledby="enlargeImageModal" aria-hidden="true">
    <div class="modal-dialog" style="width:1250px;" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body">
            	<img src=""  class="imageExpand" style="width: 100%;" >
            </div>
        </div>
      </div>
  </div>


<!-- modal documentacion -->
	<div class="modal fade" id="modal_documento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
              
                  <button type="button" id="close_modal_documento" onclick="closeModalDocumento();" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    
            </div>
            <div class="modal-body">
            <form action="#" id="formDocumento" class="form-horizontal">
            	 <div class="form-group">
            	 <div class="row">
                   <div class="col-md-6">
                   
                            <div id="imagen_view"></div>
                    
                  </div>
              </div>
            </div>
                <div class="form-group">
                    <label class="control-label col-md-4">Observación</label>
                        <div class="col-md-8">
                        <textarea id="observacion" name="observacion" placeholder="" class="form-control"></textarea>
                        </div>
                 </div>

                  <div class="form-group">
                    <label class="control-label col-md-4">Descripción</label>
                        <div class="col-md-8">
                        <textarea id="descripcion" name="descripcion" placeholder="" class="form-control"></textarea>
                        </div>
                 </div>


                   <input type="hidden" id="idDocumento" name="idDocumento" class="form-control" type="text">

           
                 </form>
            </div>
          <div class="modal-footer">
          	   
            	<button type="button" id="btnSave" onclick="actualizarDocumento();" class="btn btn-primary">Guardar y salir</button>

            	
            	
             </div>
        </div>
      </div>
   </div><!-- end modal documentacion -->




<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content font-blue">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalLabel">Editar datos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			<form id="editDataForm" action="javascript:actualizarDatos();" method="post">
				<div class="modal-body">
                   <div class="form-group">
                    <label for="field-name" class="col-form-label">Nuevo dato</label>
                    <input type="text" class="form-control" id="field-name" data-parsley-required>
                  </div>
              </div>
              <input type="hidden" name="id_prestamo" value="<?= $prestamo[0]->id_prestamo ?>">
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary" id="edit">Editar</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				</div>
			</form>
    </div>
  </div>
</div>




<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"> 

<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/component-type-file.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/select2.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/lightgallery.css">

 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
<script src="<?= base_url() ?>-/js/bootstrap.min.js" type="text/javascript"></script>
<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<script type="text/javascript" src="<?= base_url() ?>js/moment.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/es.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/jquery.form.js"></script>
<script type="text/javascript" src="<?= base_url() ?>js/bootstrap-datetimepicker.min.js"></script>

<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="<?= base_url() ?>js/lightgallery-all.min.js"></script>
<script src="<?= base_url() ?>js/jquery.mousewheel.min.js"></script>






<!--<script src="<?= base_url() ?>js/bootstrap-datetimepicker.min.js"></script>
<script src="<?= base_url() ?>js/bootstrap-datepicker.min.js"></script>-->


<script src="<?= base_url() ?>js/select2.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/custom-file-input.js"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>

 <script type="text/javascript">
        $(document).ready(function(){
            $('#lightgallery').lightGallery();
        });
</script>



<script>	
//Declaración de rutas y variables

$('#edit').click(function() {
	const validate = $('#editDataForm').parsley().isValid();
		if(validate) {
			actualizarDatos();
		}
	});
	function actualizarDatos() {
		var dataString = JSON.parse( '"' + $('#editDataForm').serialize() + '"');
		$.ajax({
			type:	'POST',
			url:	'<?= base_url() ?>solicitud/actualizarDatosPrestamo',
			data: dataString,
			success: function(result){
				$('#editModal').modal('hide');
				obtenerNuevosDatos();

			}
		});
	}
	function obtenerNuevosDatos() {
		$.get({
			url:	'<?= base_url() ?>solicitud/nuevosDatosPrestamo/'+$("#id_prestamo_cliente").val(),
			dataType: 'JSON',
			success: function(result){
				const valores = result[0];
				console.log(valores)
				$('#new_cantidad').text('B./'+valores.cantidad)
				$('#new_tiempo').text(valores.tiempo)
				$('#new_fecha').text(valores.fecha_efecto)
				$('#cantidad').val(valores.cantidad)
				$('#tiempo').val(valores.tiempo)
				$('#fecha_efecto').val(valores.fecha_efectivo_prestamo)
				calcularPrestamo();
				console.log($('#new_fecha_efectiva'))
			}
		});
	}
  
	var idCliente = $("#id_cliente").val();
	var cedula_cliente = $("#cedula_cliente").val();
	
	var id_prestamo = $("#id_prestamo_cliente").val();
	var prestamo_estado_id = $("#prestamo_estado_id").val();
	var urlDetallePrestamo =  "<?php echo site_url('detalle_prestamo/obtenerDetallePrestamo')?>/"+id_prestamo;
    var urlAgregarQuincena = "<?php echo site_url('detalle_prestamo/agregarQuincena')?>/"+id_prestamo;
    var urlActualizarQuincena = "<?php echo site_url('detalle_prestamo/actualizarQuincena')?>";
    var urlEliminarQuincena = "<?php echo site_url('detalle_prestamo/eliminarQuincena')?>";
    var urlReportePago = "<?php echo site_url('reporte_pago/obtenerReportePagos')?>/"+id_prestamo;
    var urlActualizarPago = "<?php echo site_url('reporte_pago/actualizarReportePago')?>";
    var urlEnviarPago = "<?php echo site_url('reporte_pago/enviarReportePago')?>";
    var urlEliminarPago = "<?php echo site_url('reporte_pago/eliminarPago')?>";
    var urlReportarPago = "<?php echo site_url('reporte_pago/reportarPago')?>";
    var urlStatusReportePago = "<?php echo site_url('reporte_pago/getStatusReportePago')?>";
    var urlActualizarEstadoPrestamo = "<?php echo site_url('gestion-ajax/prestamos-estados-asignar')?>";
    var urlDocumentacion = "<?php echo site_url('Documentacion_asociada/obtenerDocumentacion')?>/"+id_prestamo;
    var urlUploadDocumentos =  "<?php echo site_url('Documentacion_asociada/cargarDocumentacion')?>";
    var urlActualizarDocumento = "<?php echo site_url('Documentacion_asociada/actualizarDocumento')?>";
    var urlActualizarSeleccionDocumento ="<?php echo site_url('Documentacion_asociada/actualizarSeleccionDocumento')?>";
    var urlEliminarDocumento = "<?php echo site_url('Documentacion_asociada/eliminarDocumento')?>";
    var urlActualizarAjusteQuincena = "<?php echo site_url('solicitud/actualizarAjusteQuincena')?>";
    var url = '<?= base_url() ?>';
    

</script>
<script src="<?= base_url() ?>js/functions.js" type="text/javascript"></script>
<script src="<?= base_url() ?>dist/js/template/gestion/prestamo.js" type="text/javascript"></script>


<script type="text/javascript">
	
	function guardar_prestamo(id){
		var dataString = JSON.parse( '"' + $('#datos_prestamo').serialize() + '"');
		$.ajax({
			type:	'POST',
			url:	'<?= base_url() ?>dashboard/prestamo-guardar/'+id,
			data: dataString,
			success: function(result){
				$
			}
		});
	}	
	// colocar mensaje de error en las tablas
	$(function() {
		let icons = $('.toggleVP').find('i.fa-exclamation-circle')
		icons.popover({
			template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body rounded border-blue"></div></div>',
			content: 'Los campos que presentan el mensaje error, no poseen cálculos almacenados.'
		})
	})  
</script>



<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="content cuenta">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<br>
				<hr class="hrazul">
				<h1 class="cuenta-titulo"><b>Estados</b></h1>
				<p><a class="btn btn-solicita space-bottom-20 space-top-20" href="<?= base_url() ?>gestion/prestamo-estado">Nuevo estado</a></p>
				<hr class="hrazul">
				<?php
				if(! empty($prestamos_estados)){

					foreach ($prestamos_estados as $key):
						?>
					<table class="table table-bordered table-hover">
						<tr>
							<td><p><b>Nombre:</b> <span class="pull-right"><?= $key->nombre ?></span></p></td>
						</tr>
						<tr>
							<td style="background-color:#<?= $key->color ?>"><p><b>Color:</b> <span class="pull-right"><?= $key->color ?></span></p></td>
						</tr>
					</table>
					<hr>
					<br>
					<?php
					endforeach;
				}
				?>
			</div>

		</div>
	</div>
	<div class="page-content">
		<div>

		</div>
	</div>
</div>
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
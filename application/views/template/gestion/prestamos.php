<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row mt-4 mb-5">
		<div class="col">
			<h1 class="font-blue mb-0-5">Buscador de <b>Préstamos</b></h1>
			<form role="form" class="row" method="post" action="<?= base_url() ?>dashboard/buscar-prestamos" name="prestamos_buscar" enctype="multipart/form-data">
				<div class="col-md-4">
					<div class="form-group">
						<div class="field">
							<input class="form-control" id="cliente" name="cliente" placeholder="Id, email, identificación o nombre">
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<select class="form-control" id="estado" name="estado">
						<option value="">Estado</option>
						<?php
						if(!empty($estados)){
							foreach ($estados as $key) {
								?>
								<option value="<?= $key->id ?>"><?= $key->nombre ?></option>
								<?php
							}
						}
						?>
					</select>
				</div>
				<div class="col-md-6 text-right">
					<button type="submit" class="btn px-5 blue font-white"><i class="fa fa-search"></i> Buscar</button>
					<?php if(check_rol_view($this->session->userdata('roles'),array(5))) : ?>
						<a href="<?= base_url() ?>dashboard/clientes" >
							<button type="button" class="btn font-white ml-2" style="background-color: #5EAC25">CLIENTES</button>
						</a>
					<?php endif; ?>
				</div>
			</form>
			<div class="row  mt-4 mb-2">
				<div class="col d-flex justify-content-between">
					<h2 class="font-blue mb-0">Índice de <b>Préstamos</b></h2>
					<div class="d-flex align-items-center">
						<p class="mr-3">Mostrando <?= isset($prestamos) ? sizeof($prestamos) : 0 ?> de <?php if(isset($prestamos_total)) echo $prestamos_total; ?> Préstamos</p>
						<?php if( isset( $pagination )){ echo $pagination; }?>
					</div>
				</div>
			</div>
			<?php if(! empty($prestamos)): ?>
				<table class="table table-hover text-center" id="table-prestamos">
					<thead>
						<tr class="blue font-white">
							<th>Id</th>
							<th>Indentif.</th>
							<th>Nombre</th>
							<th>Dispositivo</th>
							<th style="max-width:100px">Estado</th>
							<th>Monto</th>
							<th>Actualización</th>
							<th style="min-width: 90px">Acciones</th>
						</tr>
					</thead>
					<tbody class="font-blue">
						<?php foreach ($prestamos as $key):
								$key->datos = json_decode($key->datos);
								$prestamo_datos_array = (array) $key->datos;
						?>
							<tr>
								<td><?= $key->id_prestamo ?></td>
								<td><?= $key->n_identificacion ?></td>
								<td><?= $key->name.' '.$key->segundo_nombre.' '.$key->primer_apellido.' '.$key->segundo_apellido ?></td>
								<td><?php echo isset($key->datos->user_agent) ? $key->datos->user_agent : '-'; ?></td>
								<td><?php if(!empty($key->nombre)){ echo '<span class="badge font-white" style="background:#'.$key->color.'">'.$key->nombre.'</span>'; } ?></td>
								<td><?= $key->cantidad ?></td>
								<td><?= date('d-m-y H:i:s', strtotime($key->ultima_actualizacion)) ?></td>
								<td class="d-flex justify-content-around">
									<a href="<?= base_url() ?>dashboard/prestamo/<?= $key->id_prestamo ?>" class="btn btn-blue"><i class="fa fa-pencil"></i> </a>
									<a href="<?= base_url() ?>dashboard/cliente/<?= $key->id_cliente ?>" class="btn btn-green"><i class="fa fa-user"></i> </a>
								</td>
							</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
			<?php else : ?>
			<div class="alert alert-info mt-5"><button type="button" class="close" aria-hidden="true" data-dismiss="alert">×</button><strong><?= $info_search ?></strong></div>
			<?php endif; ?>
		</div>
	</div>
	<div class="page-content">
		<div>

		</div>
	</div>
</div>
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/datatable.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/select2.css">
<script src="<?= base_url() ?>js/datatable.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/moment.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/datetime-moment.js"></script>
<script src="<?= base_url() ?>js/select2.js" type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.select2').select2();
		$('ul.pagination li').each(function(){
			$(this).addClass('page-item');
			$(this).find('a').addClass('page-link');
		});
		$.fn.dataTable.moment('DD-MM-YY H:m:s');
		$('#table-prestamos').DataTable({
			"aoColumns": [
				/* ID */ null,
				/* identificacion */ { "bSortable": false },
				/* nombre */ { "bSortable": false },
				/* dispositivo */ { "bSortable": false },
				/* status */ null,
				/* monto */ null,
				/* peticion */ { "bSortable": true },
				/* acciones */ { "bSortable": false }
			],
			"order": [0, "desc"],
			"bPaginate": false,
			//  "bLengthChange": false,
			"bSearch": false,
			"bFilter": false,
			// "bSorter": true,
			"bInfo": false,
			"bAutoWidth": false,
			"processing": true, 
			//"lengthMenu":     "Show _MENU_ entries",
			"language": {
				"zeroRecords": "No hay usuarios",
				"search": "Buscar:"
			}
		});
		$('#table-prestamos_wrapper .row').first().remove();
		$('#table-prestamos_wrapper .row .col-sm-12').addClass('px-0');
		$('.fa-step-forward').parent().css('height', 35.19);
		$('.fa-step-backward').parent().addClass('height', 35.19);
	});
</script>
<script type="text/javascript">
	function añadir_estado(id){
		var dataString='idprestamo='+id+'&idestado='+$('#estado_'+id).val()+'&observaciones='+$('#observaciones_'+id).val();
		$.ajax({
			type:	'POST',
			url:	'<?= base_url() ?>gestion-ajax/prestamos-estados-asignar',
			data: dataString,
			success: function(result){
				alert(result);
			}
		});
		
	}
</script>
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="presentation" style="background-image: url(<?= base_url() ?>/img/<?= isset($banner_image) ? $banner_image : 'fondo4.jpg' ?>)">
	<div class="container my-auto">
		<?php if($show_image_float) : ?>
			<img src="<?= base_url() ?>/img/<?= $banner_image_float; ?>" class="banner_image_float" alt="mensaje clicredit">
		<?php endif; ?>
		<div class="row">
			<div class="col">
				<div class="cuadro-prestamos">
					<div class="cuadro-prestamos-titulo d-flex align-items-start">
						<h1 class="font-white h2 my-auto">Te prestamos <b>B./ 500,00</b> en <b>30 minutos</b>.</h1>
					</div>
					<div class="cuadro-prestamos-blanco">
						<form class="form-prestamo" id="sol" action="<?= base_url() ?>solicitud/paso-1" method="post">
							<div class="price-slider">
								<p class="mb-4 font-20px"><em>¿Cuánto necesitas?</em></p>
								<input type="range" min="20" max="500" step="5" value="<?php $valor = !empty($cantidad)?$cantidad:120; echo set_value('amount',$valor); ?>" data-orientation="horizontal" name="amount" id="amount" data-rangeslider>
								<span id="msg_maximo" class="float-right mt-3 font-blue"><em>*Tu primer préstamo hasta B./ 250,00</em></span>
							</div>
							<br>
							<div class="price-slider mt-4">
								<p class="mb-4 font-20px"><em>¿En cuánto tiempo puedes devolverlo?</em></p>
								<input type="range" min="1" max="6" step="1" value="<?php $valor = !empty($tiempo)?$tiempo:4; echo set_value('duration',$valor); ?>" data-orientation="horizontal" name="duration" id="duration" data-rangeslider>
								<div class="mt-3 d-flex justify-content-end align-items-baseline">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" id="ajustarQuincena" name="ajustarQuincena" value="<?php $valor = !empty($ajustar_quincena)?$ajustar_quincena:1; echo set_value('ajustarQuincena',$valor); ?>" checked>
										<label class="custom-control-label font-blue" for="ajustarQuincena">
											<em>Ajustar pagos a la quincena</em>
										</label>
									</div>
									<a 
										tabindex="0"
										role="button" 
										class="btn bg-transparent"
										data-toggle="popover"
										data-template='<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body rounded border-blue"></div></div>'
										data-content="Le recomendamos dejar la casilla marcada, al ajustar los días de pago con la quincena se le facilitará pagar su préstamo
										 a tiempo, evitando generar intereses adicionales. Si lo desea podrá cambiar los periodos de pago más adelante.">
										<i class="fa fa-info-circle font-blue" aria-hidden="true"></i>
									</a>
								</div>
									<input type="hidden" id="tasa" value="<?=$calificacion->tasa_interes?>">
							</div>
							<div class="row mt-3">
								<div class="col-md-6">
									<div class="cuadro row">
										<p class="col-3 col-md-12 d-flex flex-column flex-md-row justify-content-between text-center">Solicitado:<span id="solicitado">500</span></p>
										<p class="col-1 d-block d-md-none text-center" style="font-size: 1.5rem">+</p>
										<p class="col-3 col-md-12 d-flex flex-column flex-md-row justify-content-between text-center">Coste:<span id="coste">500</span></p>
										<hr class="d-none d-md-block col-10 my-1">
										<p class="col-1 d-block d-md-none text-center" style="font-size: 1.5rem">=</p>
										<p class="col-4 col-md-12 d-flex flex-column flex-md-row justify-content-between text-center"><b>Total: </b><b class="font-blue" id="total">B./ 330.00</b></p>
										<hr class="d-block d-md-none col-10 my-3">
										<p class="col-4 col-md-12 d-flex flex-column flex-md-row justify-content-between text-center"><b>Cuota: </b><b class="font-blue" id="cuota">B./ 330.00</b></p>
										<p class="col-4 col-md-12 d-flex flex-column flex-md-row justify-content-between text-center"><b>Pago inicial: </b><b class=" font-blue" id="fecha_inicializacion">Vie. 27 Junio</b></p>
										<p class="col-4 col-md-12 d-flex flex-column flex-md-row justify-content-between text-center"><b>Último pago: </b><b class=" font-blue" id="fecha_finalizacion">Vie. 27 Junio</b></p>
									</div>
									<div class="row">
										<div class="col-md-12 text-center">
											<a href="<?= base_url() ?>ayuda-preguntas-frecuentes" class="font-blue" style="font-size:12px">¿Cómo aplicar para un préstamo?</a>
										</div>
									</div>
								</div>

								<div class="col-md-6 text-center ">
									<button class="btn btn-solicita efecto mt-2 mb-3 my-md-1" type="submit" style="border-radius:5px">SOLICITA AHORA</button>								
									<div class="row">
										<div class="col-md-12">
											<span class="d-none d-md-block"style="font-weight:800;font-size:19px">¿Tienes <b class="font-blue">dudas?</b></span>
											<hr style="border:1px solid #265c96;margin: 3px 10px">
											<div class=" mt-3 mt-md-0 d-flex flex-md-column justify-content-around align-items-center">
												<div>
													<b>Chatea con nosotros</b>
													<p><b><a href="#" class="font-blue" onclick="openChat()">Haz click aquí</a></b></p>
												</div>
												<p><b>O llámanos al:<br><span class="font-24"><b>260-8951</b></span></b></p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<h4 class="font-18 w-100 mb-0 py-3 font-white d-flex flex-column flex-md-row justify-content-center bg-grad">
		<span class="my-1 my-md-0 text-center">L-V 9.00 am 8.00 pm</span>
		<span class="d-none d-md-inline">&nbsp;|&nbsp;</span>
		<a href="<?= base_url() ?>devoluciones" class="font-white my-1 my-md-0 text-center">Devolución o Prórroga</a>
		<span class="d-none d-md-inline">&nbsp;|&nbsp;</span>
		<a href="<?= base_url() ?>prestamo-vencido#devolucion" class="font-white my-1 my-md-0 text-center">Préstamo Vencido</a>
	</h4>
</div>

<div class ="container">
	<div class="row">
		<div class="col-lg-12 text-center">
			<br>
			<h1 class="font-blue"><b>¿Cómo funcionan nuestros préstamos?</b></h1>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 my-4">
			<h3 class="font-blue"><b>PASO 1.</b></h3>
			<h3 class="font-blue">Elige la cantidad de dinero y el tiempo que necesitas para devolverlo.</h3>
			<p>No necesitas papeleo, ni perder tiempo en filas que parecen interminables para pedir préstamos rápidos, ¡gracias a nuestro servicio 100% en línea! Utiliza nuestra barra dinámica para seleccionar exactamente cuánto dinero necesitas y por cuánto tiempo. Desde ese momento, sabrás cuánto tendrás que pagar.</p>
			<img src="<?= base_url() ?>img/home/Paso-1.png" class="img-fluid d-block mx-auto" alt="paso 1 solicitud">
		</div>
		<div class="col-md-6 my-4">
			<h3 class="font-blue"><b>PASO 2.</b></h3>
			<h3 class="font-blue">Rellena los formularios con tus datos.</h3>
			<p>Llena correctamente nuestros breves formularios con información personal, cuenta de banco donde deseas recibir el dinero, etc. Esto nos dará oportunidad de conocerte un poco mejor. Toda la información que nos compartas será tratada con la maxima seguridad.</p>
			<br>
			<br>
			<img src="<?= base_url() ?>img/home/Paso-2.png" class="img-fluid d-block mx-auto" alt="paso 2 solicitud">
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 my-4">
			<h3 class="font-blue"><b>PASO 3.</b></h3>
			<h3 class="font-blue">Obten el dinero en tu cuenta.</h3>
			<p>Responderemos a tu solicitud en minutos, y en caso de ser aprobado, ¡recibirás tu crédito personal en tu cuenta de banco en aproximadamente 30 minutos! (*Depende del horario bancario y la hora a que cerremos tu préstamo).</p>
			<br>
			<br>
			<img src="<?= base_url() ?>img/home/Paso-3.png" class="img-fluid d-block mx-auto" alt="paso 3 solicitud">
		</div>
		<div class="col-md-6 my-4">
			<h3 class="font-blue"><b>ULTIMO PASO.</b></h3>
			<h3 class="font-blue">Paga tu préstramos a tiempo.</h3>
			<p>Cuando tengas que cancelar, elige si quieres depositar el pago de tu préstamo en la cuenta de CliCredit o si prefieres hacerlo por transferencia electrónica. Inicialmente, puedes pedir hasta B./250.00 Si eres un excelente cliente y pagas a tiempo, podremos ofrecerte mejores tasas y condiciones en tus próximos préstamos rápidos.</p>
			<br>
			<img src="<?= base_url() ?>img/home/Paso-4.png" class="img-fluid d-block mx-auto" alt="paso 4 solicitud">
		</div>
	</div>
	<br>
	<br>
</div>
<div class="green py-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe src="https://www.youtube.com/embed/cbI-TeOwmX8" width="100%" height="43%" allowfullscreen></iframe>
				</div>
			</div>
			<div class="col-lg-6 font-white mt-4 mt-lg-0">
				<h3 class="text-center"><b>¿Qué necesito para obtener mi prestamo?</b></h3>
				<ul>
					<h4><b>NECESARIO PARA PODER APLICAR:</b></h4>
					<p>1. Correo electrónico propio</p>
					<p>2. Celular propio</p>
					<p>3. Cuenta de banco a tu nombre</p>
					<h4><b>MUY BUENO PARA APLICAR:</b></h4>
					<p>4. Tarjeta de crédito o débito</p>
					<p>5. Trabajo con más de dos años</p>
					<p>6. Buen historial de crédito</p>
				</ul>
			</div>
		</div>
	</div>
</div>

	<div class ="container">
		<br>
		<div class="row">
			<div class="col-lg-12">
				<br>
				<h2 class="font-blue text-center text-lg-left" style="font-size:36px"><b>Ayuda r&aacute;pida</b></h2>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<p class="font-blue"><strong>&iquest;C&oacute;mo funcionan los pr&eacute;stamos inmediatos con CliCredit.com?</strong></p>
				<p>Puedes pedir un préstamo desde B./50,00 hasta B./500,00 todo en línea y desde la comodidad de tu celular. Lo puedes pagar desde un 1 día hasta 90 días, (6 quincenas).</p>
				<p class="font-blue"><strong>&iquest;En cu&aacute;ntos pagos?</strong></p>
				<p>Liquida el total en un solo pago, o en los plazos que elegiste al solicitar el préstamo. Si ya no necesitas el préstamo no pagues más intereses. Cancela y paga sólo el tiempo que usaste.</p>
				<p class="font-blue"><strong>&iquest;D&oacute;nde pago mi pr&eacute;stamo?</strong></p>
				<p>El pago de tu préstamo se hace a través de un depósito en ventanilla o transferencia electrónica. Tú eliges lo que más te convenga.</p>
			</div>
			<div class="col-lg-6">
				<p class="font-blue"><strong>&iquest;Cu&aacute;ndo pago mi pr&eacute;stamo?</strong></p>
				<p>Págalo durante el día de quincena, durante los plazos que elegiste.</p>
				<p class="font-blue"><strong>¿Cómo hace Clicredit.com para depositarte el dinero?</strong></p>
				<p>Clicredit.com utiliza un sistema de pagos electrónicos, que directamente se depositan en una cuenta bancaria específica que debe estar a tu nombre.</p>
			</div>
		</div>
		<br>
	</div>
	<br>
	<script type="text/javascript">

     $('#ajustarQuincena').on( 'change',  function () {
        	var ajuste  = $('#ajustarQuincena').is(':checked');
        	console.log(ajuste)
        	if(ajuste==true){
        		$('#ajustarQuincena').val(1);
        	}else if(ajuste==false){
        		$('#ajustarQuincena').val(0);
        	}
        });



        $(document).ready(function(){

        	var ajustar_quincena = $('#ajustarQuincena').val();
        	if(ajustar_quincena=="1"){
        		$('#ajustarQuincena').attr("checked",true);

        	}else if(ajustar_quincena=="0"){
        		$('#ajustarQuincena').attr("checked",false);
        	}
        });



		$(function() {
			$('#ajustarQuincena').change(calcularPrestamo)
		})
		function calcularPrestamo(){
			$('#solicitado').html( 'B./ ' + parseFloat($('#amount').val() ).toFixed(2));
			var ajustar = $('#ajustarQuincena').is(':checked');
			if(ajustar){
				conAjuste();
			}else {
				sinAjuste();
			}
		}
		function sinAjuste() {
			var tasa = $('#tasa').val();
			var duracion = $('#duration').val();
			var cantidad = $('#amount').val();
			var hoy = new Date();
			var inicio = sumarDias(new Date(), 15);
			var fin = sumarDias(new Date(), duracion*15);
			var coste = (((duracion*15) * tasa) * cantidad).toFixed(2);
			var total = parseFloat(coste) + parseFloat(cantidad);
			$('#total').html( 'B./ ' + parseFloat(total).toFixed(2) );
			$('#cuota').html( 'B./ ' + parseFloat(total / $('#duration').val() ).toFixed(2) );
			$('#coste').html( 'B./ ' + coste);
			$('#fecha_inicializacion').html(formatDate(inicio));
			$('#fecha_finalizacion').html(formatDate(fin));
		}
		function conAjuste() {
			var tasa = $('#tasa').val();
			var hoy = new Date();
			var inicio = ajustarInicio(new Date());
			var fin = ajustarFin(new Date(), $('#duration').val());
			var duracion = Math.ceil((fin - hoy)/(1000*60*60*24));
			var cantidad = $('#amount').val();
			var coste = (duracion * tasa * cantidad).toFixed(2);
			var total =  parseFloat(coste) + parseFloat(cantidad);
			$('#total').html( 'B./ ' + parseFloat(total).toFixed(2) );
			$('#cuota').html( 'B./ ' + parseFloat(total / $('#duration').val() ).toFixed(2) );
			$('#coste').html( 'B./ ' + coste);
			$('#fecha_inicializacion').html(formatDate(inicio));
			$('#fecha_finalizacion').html(formatDate(fin));
		}
		function ajustarInicio(fecha) {
			quincenas = calcularQuincenas(fecha);
			return quincenas[0];
		}
		function ajustarFin(fecha, duracion) {
			quincenas = calcularQuincenas(fecha);
			return quincenas[duracion-1];
		}
		function calcularQuincenas(fecha) {
			var quincenas = [];
			var ultimoDia = new Date(fecha.getFullYear(), fecha.getMonth() + 1, 0);
			if(fecha.getDate() < 11) {
				quincenas.push(quincena(fecha, 0));
				quincenas.push(finDeMes(fecha, 0));
				quincenas.push(quincena(fecha, 1));
				quincenas.push(finDeMes(fecha, 1));
				quincenas.push(quincena(fecha, 2));
				quincenas.push(finDeMes(fecha, 2));
				quincenas.push(quincena(fecha, 3));
			} else if(fecha.getDate() < (ultimoDia.getDate()-4)) {
				quincenas.push(finDeMes(fecha, 0));
				quincenas.push(quincena(fecha, 1));
				quincenas.push(finDeMes(fecha, 1));
				quincenas.push(quincena(fecha, 2));
				quincenas.push(finDeMes(fecha, 2));
				quincenas.push(quincena(fecha, 3));
				quincenas.push(finDeMes(fecha, 3));
			} else {
				quincenas.push(quincena(fecha, 1));
				quincenas.push(finDeMes(fecha, 1));
				quincenas.push(quincena(fecha, 2));
				quincenas.push(finDeMes(fecha, 2));
				quincenas.push(quincena(fecha, 3));
				quincenas.push(finDeMes(fecha, 3));
				quincenas.push(quincena(fecha, 4));
			}
			return quincenas;
		}
		function quincena(fecha, numero) {
			return new Date(fecha.getFullYear(), fecha.getMonth() + numero, 15)
		}
		function finDeMes(fecha, numero) {
			return new Date(fecha.getFullYear(), fecha.getMonth() + numero + 1, 0)
		}
		function sumarDias(fecha, dias){
			fecha.setDate(fecha.getDate() + dias);
			return fecha;
		}
		function formatDate(date) {
			var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();
			var dias = new Array('domingo','lunes','martes','miercoles','jueves','viernes','sabado')
			var dias = new Array('Dom','Lun','Mar','Mié','Jue','Vie','Sáb')
			if (month.length < 2) month = '0' + month;
			switch(month){
				case '01':
				month = 'Ene';
				break;
				case '02':
				month = 'Feb';
				break;
				case '03':
				month = 'Mar';
				break;
				case '04':
				month = 'Abr';
				break;
				case '05':
				month = 'May';
				break;
				case '06':
				month = 'Jun';
				break;
				case '07':
				month = 'Jul';
				break;
				case '08':
				month = 'Ago';
				break;
				case '09':
				month = 'Sep';
				break;
				case '10':
				month = 'Oct';
				break;
				case '11':
				month = 'Nov';
				break;
				case '12':
				month = 'Dic';
				break;
			}
			if (day.length < 2) day = '0' + day;

		return dias[date.getDay()] + ' ' + day + ' ' +month;
	}
</script>
<style type="text/css">
	@media (max-width: 480px){.col-xs-1{padding:0}}
	.rangeslider__handle{line-height:29px;font-size:16px;font-style:italic;min-width: 120px !important}
</style>
<script src="<?= base_url() ?>js/rangeslider.js"></script>
<script>
	$(function() {
		var $document = $(document);
		var selector = '[data-rangeslider]';
		var $element = $(selector);
		// For ie8 support
		var textContent = ('textContent' in document) ? 'textContent' : 'innerText';

		function valueOutputDiv(element) {
			var value = element.value;
			var output = element.parentNode.getElementsByClassName('rangeslider__handle')[0];
			if(element.name == "amount"){
				output[textContent] = 'B./ ' + parseFloat( value ).toFixed(2);
			}else{
				output[textContent] = ' ' + value + ' Quincenas ';
			}
		}
		$element.rangeslider({
			polyfill: false,
			onInit: function() {
				valueOutputDiv(this.$element[0]);
				calcularPrestamo();
			},
			onSlide: function(position, value) {
				valueOutputDiv(this.$element[0]);
				calcularPrestamo();
			},
			onSlideEnd: function(position, value) {
				valueOutputDiv(this.$element[0]);
				calcularPrestamo();
			}
		});
	});
</script>
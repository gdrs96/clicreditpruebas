<?php defined('BASEPATH') OR exit('No direct script access allowed');
if(sizeof($portfolio) > 0){ ?>
<div class="portfolio" id="<?= $titulo[4]->slug ?>">
	<article>
		<div class="works container">
			<?php if($titulo[4]->image){ ?><img src="<?=$titulo[4]->image ?>" alt="<?= $titulo[4]->title ?>"><?php } ?>
			<header class="title">
				<h1><?= $titulo[4]->title ?></h1>
			</header>
			<?php
			echo $titulo[4]->content;
			$i=0;$total=0;
			$fin = sizeof($portfolio);
			foreach($portfolio as $key){
				if($i==0){echo '<ul class="filter-port row">';}
				?>
				<li class="item print col-md-4">
					<article>
						<img src="<?= $PM_portfolio[$key->id]->metavalue ?>" alt="placeholder-img">
						<a href="http://<?= $key->slug ?>" target="_blank" rel="nofollow"><?= $key->title ?><?= $key->content?></a>
					</article>
				</li>
				<?php
				$i++;$total++;
				if($i==3 || $total == $fin){$i=0;echo '</ul>';}
			} ?>
			<hr>
		</div>
	</article>
</div>
<?php } ?>
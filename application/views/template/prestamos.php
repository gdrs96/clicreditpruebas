<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row mt-4">
		<div class="col-lg-12">
			<h2 class="font-blue text-center"><b>¿Qué son los préstamos</b> rápidos en línea?</h2>
			<p class="text-center">Muchas familias en Panamá no tienen capacidad de afrontar gastos imprevistos durante la quincena, los préstamos rápidos de clicredit son ayudas financieras personalizadas destinadas a todos las personas, ya que no se necesita historial de crédito previo, te ayudaremos a crear uno o mejorar el que tienes, ya que reportamos con Asociación Panameña de Crédito (APC) . Estas ayudas estan indicadas principalmente para cubrir pequeños imprevistos económicos que te puedan surgir. No es financiación a largo plazo.</p>
			<p class="text-center">Al contrario de otros tipos de préstamos online o tradicionales, Clicredit.com no te pide aval para su solicitud. Además, aunque estés en APC con mala puntuación, en Clicredit.com consideramos otros factores para que tengas la opurtunidad de resolver tu emergencia con un préstamo rápido. </p>
		</div>
	</div>
	<div class="row my-3 justify-content-center">
		<div class="col-lg-10 versus">
			<div class="bank text-right font-blue">
				<h4>Perdir apoyo al <b>banco o financiera</b></h4>
				<span>Burocracia</span>
				<span>Depósito en unos dias</span>
				<span>Necesidad de presencia fisica</span>
				<span>Horario reducido</span>
			</div>
			<div class="vs">
				<span></span>
				<span>vs</span>
				<span>vs</span>
				<span>vs</span>
				<span>vs</span>
			</div>
			<div class="clicredit text-left font-green">
				<h4>Perdir apoyo a <b>Clicredit.com</b></h4>
				<span>Sin papeles</span>
				<span>Dinero en 30 minutos*</span>
				<span>En cualquier lugar, no importa donde estés</span>
				<span>24 horas / 7 dias a la semana</span>
			</div>
		</div>
	</div>
	<div class="row mt-1 justify-content-center prestamo-imagenes">
		<div class="col-6 d-flex">
			<img src="<?= base_url() ?>/img/bank.svg" alt="Bank" class="img-fluid ml-auto bank">
		</div>
		<div class="col-6 d-none d-md-block">
			<img src="<?= base_url() ?>/img/hombre2.svg" alt="clicredit" class="img-fluid hombre2">
		</div>
		<div class="col-6 d-md-none">
			<img src="<?= base_url() ?>/img/hombre.svg" alt="clicredit" class="img-fluid hombre">
		</div>
	</div>
</div>
<div class="green w-100 mb-3 py-3">
	<div class="container">
		<h4 class="text-white mb-0"><b>Créditos hasta  B/500.00,</b> para sus imprevistos</h4>
	</div>
</div>
<div class="container">
	<div class="row mb-5">
		<div class="col-12 col-md-6 text-center text-md-right font-blue">
			<ul class="mb-0 prestamos-lista">
				<li>Se te rompe el celular</li>
				<li>La nevera no funciona</li>
				<li>Una cuenta atrasada, que no puede esperar</li>
				<li>Una reparación de carro</li>
				<li>O cualquier otro imprevisto que surja</li>
			</ul>
		</div>
		<div class="col-12 col-md-6 d-flex align-items-end justify-content-center justify-content-md-start">
			<span class="font-green mt-4 mt-md-0">
				<b>Clicredit.com te apoya!!</b>
				<a href="<?= base_url() ?>" class="font-green">dale clic y salva el dia.</a></span>
		</div>
	</div>
</div>
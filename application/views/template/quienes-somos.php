<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<div class="row my-4 pb-5">
		<div class="col-lg-8">
		<h2 class="font-blue "><b>Clicredit.com</b> ¿Quiénes somos?</h2>
		<p>CliCredit.com es un servicio gestionado por Credit Agile, Inc. una compañía constituida en 2016 con la casa matriz en Ciudad de Panamá. Nuestra misión es ofrecer productos financieros innovadores, eficientes y transparentes contribuyendo a mejorar el bienestar de las personas usando el poder de la tecnología.</p>
		<h2 class="font-blue"><b>¿Qué </b>Hacemos?</h2>
		<p>CliCredit.com es una plataforma de préstamos en línea que está pensada para hacerte la vida un poco más fácil. Es la solución inmediata a tus problemas de efectivo.<br />&nbsp; &nbsp; - ¿Tienes gastos inesperados y necesitas dinero urgente?<br />&nbsp; &nbsp; - ¿Necesitas un poco de dinero extra para terminar la quincena?</p>
		<p>Somos un equipo de gente como tú, que conocemos lo difícil y tedioso que puede ser pedir un préstamo a una institución financiera tradicional. Por eso creamos esta plataforma, donde puedes cubrir tus necesidades de efectivo a corto plazo. ¡Todo desde la comodidad de tu casa u oficina! <span class="font-blue"><b>#Dondetuestes</b></span></p>
		</div>
		<div class="col-lg-4">
			<div class="ayuda mx-auto">
				<h4><b>CliCredit.com</b></h4>
				<hr class="bg-white my-2">
				<p class="my-1"><strong>100% transparente</strong></p>
				<p class="my-1"><strong>Rápido</strong></p>
				<p class="my-1"><strong>Sencillo</strong></p>
				<p class="my-1"><strong>Flexible</strong></p>
				<p class="my-1"><strong>Financiación responsable</strong></p>
			</div>
		</div>
	</div>
	<br>
</div>
<?php defined('BASEPATH') OR exit('No direct script access allowed');
echo '<?xml version="1.0" encoding="UTF-8" ?>'; ?>
<rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
xmlns:slash="http://purl.org/rss/1.0/modules/slash/">
	<channel>
		<title><?= $title ?></title>
		<atom:link href="<?= base_url() ?>feed" rel="self" type="application/rss+xml" />
		<link><?= base_url() ?>feed</link>
		<description><?= $description ?></description>
		<lastBuildDate><?= date('D, d M Y H:i:s O') ?></lastBuildDate>
		<language>es</language>
		<sy:updatePeriod>hourly</sy:updatePeriod>
		<sy:updateFrequency>1</sy:updateFrequency>
	<?php foreach($posts as $post): ?>
		<item>
			<title><?= $post->title ?></title>
			<link><?= base_url().'blog'.$post->slug ?></link>
			<comments><?= base_url().'blog'.$post->slug ?>#comentarios</comments>
			<pubDate>Thu, 19 Mar 2015 06:53:45 +0000</pubDate>
			<dc:creator><![CDATA[<?= $post->name ?>]]></dc:creator>
<?php if(!empty($post->nombre_categoria)){ ?>
			<category><![CDATA[<?= $post->nombre_categoria ?>]]></category>
<?php } ?>
			<guid isPermaLink="false"><?= base_url().'blog'.$post->slug ?></guid>
			<description><![CDATA[<?= $post->description ?>]]></description>
			<content:encoded><![CDATA[ <?= $post->content ?>]]></content:encoded>
		</item>
	<?php endforeach; ?>
	</channel>
</rss>
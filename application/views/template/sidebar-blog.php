<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div id="secondary" class="widget-area" role="complementary">
	<aside id="search-2" class="widget widget_search">
		<form role="search" method="post" class="search-form" action="<?= base_url() ?>blog/buscar">
			<label>
				<span class="screen-reader-text">Busca aquí:</span>
				<input type="search" class="search-field" placeholder="Busca aquí" name="s" title="Busca aquí"/>
			</label>
			<input type="submit" class="search-submit" value="Buscar" />
		</form>
	</aside>
	<aside id="recent-posts-2" class="widget widget_recent_entries">
		<h3 class="widget-title">Categorías</h3>
		<ul>
<?php foreach($categorias as $key){ ?>
<li><a href="<?= base_url() ?>blog/categoria/<?= $key->slug ?>" title="Categoría <?= $key->name ?>" > <?php echo $key->name; if($this->uri->segment(3) == $key->slug ){echo ' <i class="fa fa-chevron-right"></i>';} ?></a></li>
<?php
}
?>
		</ul>
	</aside>
	<aside class="widget widget_recent_comments"><h3 class="widget-title">Las más vistas</h3>
		<ul>
			<li><a href="http://miguelgomezsa.com/blog/seo-seguridad-rendimiento-web-htaccess" title="Mejora el SEO, rendimiento y seguridad web con .htaccess">Mejora el SEO, rendimiento y seguridad web con .htaccess</a></li>
			<li><a href="http://miguelgomezsa.com/blog/herramientas-imprescindibles-para-web-de-google">Herramientas imprescindibles de Google para tu web</a></li>
			<li><a href="http://miguelgomezsa.com/blog/hostgator-webempresa-recomendar-o-vender-hosting">Hostgator y Webempresa la realidad</a></li>
		</ul>
	</aside>
	<aside class="widget">
		<form action="//miguelgomezsa.us10.list-manage.com/subscribe/post?u=957cae9aa51e32b194c6a4d58&amp;id=c64faaafb6" method="post" id="mc-embedded-subscribe-form" class="form" target="_blank">
			<h3 class="widget-title">Suscríbete</h3>
			<p>Si quieres que te avise de interesantes novedades,<br>suscríbete aquí:</p>
				<input type="email" id="email" name="EMAIL" placeholder="email" required>
				<input type="text" id="name" name="FNAME" placeholder="nombre" required>
				<input type="submit" value="Me apunto!" name="subscribe" class="boton">
		</form>
	</aside>
	<aside class="widget-social">
		<h3 class="widget-title">Yo en Google +</h3>
		<script src="https://apis.google.com/js/platform.js" async defer></script>
		<div class="g-person" data-width="200" data-href="//plus.google.com/u/0/108162720458644835888" data-rel="author"></div>
	</aside>
	<aside class="widget-social">
		<h3 class="widget-title">Sígueme en Twitter</h3>
		<a class="twitter-timeline" href="https://twitter.com/MiguelGomezSa" data-widget-id="579699187605995521">Tweets por el @MiguelGomezSa.</a>
		<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	</aside>
	<aside class="widget-social">
		<h3 class="widget-title">Mi página de facebook</h3>
		<script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0];if (d.getElementById(id)) return;js = d.createElement(s); js.id = id;js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.0";fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
		<div class="fb-like-box" data-href="https://www.facebook.com/miguelgomezsa" data-width="200px" data-height="400px" data-colorscheme="light" data-show-faces="false" data-header="false" data-stream="true" data-show-border="false"></div>
	</aside>
	<aside class="widget-social">
		<p><a href="http://miguelgomezsa.com/ver/curso-infoproductos-agustin-grau" target="_blank">Aprende a sacar provecho de tu web con el curso de mi amigo Agustín Grau +info aquí</a></p>
	</aside>
</div>
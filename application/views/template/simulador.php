<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="content">
	<div class="container blue-font">

		<br>
		<h1 class="titulo-verde">0. Simulador</h1>
		<h2 class="titulo-verde">Escoge la cantidad y los días que lo necesitas</h2>
		<p>Fecha de hoy <?= date('D, M, d, Y') ?></p>
		<form class="form-horizontal form-asistente-solicitar" action="<?= base_url() ?>solicitar" method="post">

			<div class="form-group">
				<label for="monto" class="col-lg-6 control-label font-24" alt="El primer préstamo está limitado a 175">¿Cuánto necesitas?</label>
				<div class="col-lg-6">
					<input type="number" max="500" class="form-control" id="monto" placeholder="100">
				</div>
			</div>
			<div class="form-group">
				<label for="tiempo" class="col-lg-6 control-label font-24">¿En cúanto tiempo puedes devolverlo?</label>
				<div class="col-lg-6">
					<select class="form-control">
						<option value="1">1 quincena</option>
						<option value="2">2 quincena</option>
						<option value="3">3 quincena</option>
						<option value="4">4 quincena</option>
						<option value="5">5 quincena</option>
						<option value="6">6 quincena</option>
					</select>
				</div>
			</div>
			<p class="text-right" id="dias">90 días</p>
			<div class="col-md-6 cuadro-verde">
				<div class="form-group">
					<label for="monto" class="col-lg-6 control-label" alt="El primer préstamo está limitado a 175">Préstamo solicitado</label>
					<div class="col-lg-6">
						<input type="number" max="500" class="form-control" id="monto" placeholder="100">
					</div>
				</div>
				<div class="form-group">
					<label for="monto" class="col-lg-6 control-label">Intéres</label>
					<div class="col-lg-6">
						<input type="number" max="500" class="form-control" id="monto" placeholder="100">
					</div>
				</div>
				<div class="form-group">
					<label for="monto" class="col-lg-6 control-label">Administracion legal</label>
					<div class="col-lg-6">
						<input type="number" max="500" class="form-control" id="monto" placeholder="100">
					</div>
				</div>
				<div class="form-group">
					<label for="monto" class="col-lg-6 control-label">Manejo cuenta</label>
					<div class="col-lg-6">
						<input type="number" max="500" class="form-control" id="monto" placeholder="100">
					</div>
				</div>
				<div class="form-group">
					<label for="monto" class="col-lg-6 control-label">Total a devolver:</label>
					<div class="col-lg-6">
						<input type="number" max="500" class="form-control" id="monto" placeholder="100">
					</div>
				</div>
				<div class="form-group">
					<label for="monto" class="col-lg-6 control-label">Total aplazado en:</label>
					<div class="col-lg-6">
						<input type="number" max="500" class="form-control" id="monto" placeholder="100">
					</div>
				</div>
				<div class="form-group">
					<label class="col-lg-6 control-label">Finalizas el:</label>
					<label class="col-lg-6 control-label"><?= date('D, M, d, Y') ?></label>
				</div>
			</div>
			<div class="clearfix"></div>
			<br>
			<div class="form-group text-center">
				<button type="submit" class="btn btn-primary btn-solicitar">Solicitar ahora</button>
			</div>

		</form>
	</div>
</div>

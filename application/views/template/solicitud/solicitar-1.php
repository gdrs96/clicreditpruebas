<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container font-blue">
	<?=$wizard?>
	<div id="info"></div>
	<div class="tab-content">
		<div class="tab-pane active" role="tabpanel" id="step1">
			<div class="row">
				<div class="col-lg-6 order-2 order-lg-1 mb-5" id="login">
					<h2 class="text-center linea-azul"><b>¿Ya tienes una cuenta? <span class="font-green">Inicia sesión</span></b></h2>
					<div class="row justify-content-center mt-4">
						<div class="col-sm-10 col-md-8 form-group">
							<label for="email_login" class="control-label" >Email / Cedula (<span class="font-red">*</span>)</label>
							<input type="text" class="form-control" id="email_login" required>
						</div>
						<div class="col-sm-10 col-md-8 form-group">
							<label for="pass_login" class="control-label" >Contraseña (<span class="font-red">*</span>)</label>
							<input type="password" class="form-control" id="pass_login" minlength="4" required>
						</div>
					</div>
					<div class="text-center">
						<p><a href="<?= base_url() ?>acceso/recuperar-cuenta"><em>¿Problemas con la contraseña?</em></a></p>
						<button type="button" class="btn btn-primary btn-solicitud" onclick="login();">ENTRAR</button>
					</div>
				</div>
				<div class="col-lg-6 order-1 order-lg-2">
					<h2 class="text-center linea-azul"><b>Si no tienes una cuenta, <span class="font-green">¡Registrate!</span></b></h2>
					<form class="row justify-content-center mt-4" action="javascript:enviarForm();"  name="procesar_solicitud" method="post" id="procesar_solicitud">
						<?php if(empty($user->email)): ?>
							<div class="col-sm-10 col-md-6 form-group">
								<label for="email" class="control-label" >Correo electrónico (<span class="font-red">*</span>)</label>
								<input data-parsley-type="email" class="form-control" id="email" name="email" placeholder="email" data-parsley-required value="<?php $t = !empty($user->email)?$user->email:null; echo set_value('email',$t); ?>" >
								<div id="userExistError" class="mt-3 mt-lg-0">
									<span style="color: #FF0000">El correo electrónico que ingresaste ya está registrado</span>
									<a href="#" class="d-lg-none">Iniciar sesión</a>
								</div>
							</div>
							<div class="col-sm-10 col-md-6 form-group">
								<label for="email_check" class="control-label" >Confirma correo electrónico (<span class="font-red">*</span>)</label>
								<input data-parsley-type="email" class="form-control" id="email_check" name="email_check" placeholder="email" data-parsley-required data-parsley-equalto="#email" data-parsley-email-message="Debe ser un correo válido" data-parsley-equalto-message="El correo debe de ser el mismo" value="<?php $t = !empty($user->email_check)?$user->email_check:null; echo set_value('email_check',$t); ?>">
							</div>
						<?php endif; ?>
						<div class="col-sm-10 col-md-6 form-group">
							<label for="nombre" class="control-label" >Primer nombre (<span class="font-red">*</span>)</label>
							<input type="text" class="form-control" id="name" name="name" placeholder="Primer nombre" value="<?php $t = !empty($user->name)?$user->name:null; echo set_value('name',$t); ?>" data-parsley-required>
						</div>
						<div class="col-sm-10 col-md-6 form-group">
							<label for="nombre2" class="control-label" >Segundo nombre</label>
							<input type="text" class="form-control" id="segundo_nombre" name="segundo_nombre" placeholder="Segundo nombre" value="<?php $t = !empty($user->segundo_nombre)?$user->segundo_nombre:null; echo set_value('segundo_nombre',$t); ?>" >
						</div>
						<div class="col-sm-10 col-md-6 form-group">
							<label for="apellido" class="control-label" >Primer apellido (<span class="font-red">*</span>)</label>
							<input type="text" class="form-control" id="primer_apellido" name="primer_apellido" placeholder="Primer apellido" value="<?php $t = !empty($user->primer_apellido)?$user->primer_apellido:null; echo set_value('primer_apellido',$t); ?>" data-parsley-required>
						</div>
						<div class="col-sm-10 col-md-6 form-group">
							<label for="appelido2" class="control-label" >Segundo apellido</label>
							<input type="text" class="form-control" id="segundo_apellido" name="segundo_apellido" placeholder="Segundo apellido" value="<?php $t = !empty($user->segundo_apellido)?$user->segundo_apellido:null; echo set_value('segundo_apellido',$t); ?>">
						</div>
						<div class="col-sm-10 col-md-6 form-group">
							<label for="nombre" class="control-label" >Fecha de nacimiento (<span class="font-red">*</span>)</label>
							<div class="d-flex justify-content-between">
								<div class="width33">
									<select class="form-control" type="text" name="dia_fecha_nacimiento" id="dia_fecha_nacimiento" data-parsley-required data-parsley-errors-messages-disabled>
										<option value="">Día</option>
										<?php
										$dias_mes = 31;
										for ($i=1; $i <= $dias_mes; $i++) {
											?>
											<option <?php if (set_value('dia_fecha_nacimiento',$t) == $i): ?>selected<?php endif ?>><?= $i ?></option>
											<?php
										}
										?>
									</select>
								</div>
								<div class="width33">
									<select class="form-control" type="text" name="mes_fecha_nacimiento" id="mes_fecha_nacimiento" data-parsley-required data-parsley-errors-messages-disabled>
										<option value="">Mes</option>
										<option value="1" <?php if (set_value('mes_fecha_nacimiento',$t) == 1): ?>selected<?php endif ?>>01</option>
										<option value="2" <?php if (set_value('mes_fecha_nacimiento',$t) == 2): ?>selected<?php endif ?>>02</option>
										<option value="3" <?php if (set_value('mes_fecha_nacimiento',$t) == 3): ?>selected<?php endif ?>>03</option>
										<option value="4" <?php if (set_value('mes_fecha_nacimiento',$t) == 4): ?>selected<?php endif ?>>04</option>
										<option value="5" <?php if (set_value('mes_fecha_nacimiento',$t) == 5): ?>selected<?php endif ?>>05</option>
										<option value="6" <?php if (set_value('mes_fecha_nacimiento',$t) == 6): ?>selected<?php endif ?>>06</option>
										<option value="7" <?php if (set_value('mes_fecha_nacimiento',$t) == 7): ?>selected<?php endif ?>>07</option>
										<option value="8" <?php if (set_value('mes_fecha_nacimiento',$t) == 8): ?>selected<?php endif ?>>08</option>
										<option value="9" <?php if (set_value('mes_fecha_nacimiento',$t) == 9): ?>selected<?php endif ?>>09</option>
										<option value="10" <?php if (set_value('mes_fecha_nacimiento',$t) == 10): ?>selected<?php endif ?>>10</option>
										<option value="11" <?php if (set_value('mes_fecha_nacimiento',$t) == 11): ?>selected<?php endif ?>>11</option>
										<option value="12" <?php if (set_value('mes_fecha_nacimiento',$t) == 12): ?>selected<?php endif ?>>12</option>
									</select>									
								</div>
								<div class="width33">
									<select class="form-control" type="text" name="year_fecha_nacimiento" id="year_fecha_nacimiento"  data-parsley-required data-parsley-errors-messages-disabled>
										<option value="">Año</option>
										<?php
										$years = ( date('Y') - 19 );
										for ($i = ( $years - 60 ); $i <= $years ; $i++) {
											?>
											<option <?php if (set_value('year_fecha_nacimiento',$t) == $i): ?>selected<?php endif ?>><?= $i ?></option>
											<?php
										}
										?>
									</select>		
								</div>
							</div>
							<span id="FechaNacimientoError"></span>
						</div>
						<div class="col-sm-10 col-md-6 form-group">
							<label for="nombre" class="control-label" >Teléfono de contacto </label>
							<input type="text" class="form-control" id="telefono" name="telefono" value="<?php $t = !empty($user->telefono)?$user->telefono:null; echo set_value('telefono',$t); ?>" maxlength="9" >
						</div>
						<?php
						if(empty($user->n_identificacion)){
							?>
						<div class="col-sm-10 col-md-6 form-group">
							<label for="n_identificacion" class="control-label">
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="tipo_documento" id="cedula" value="cedula" checked>
									<label class="form-check-label" for="cedula">Cédula</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="radio" name="tipo_documento" id="otroDocumento" value="otroDocumento">
									<label class="form-check-label" for="otroDocumento">Otro documento (<span class="font-red">*</span>)</label>
								</div>
							</label>
							<input type="text" class="form-control" id="n_cedula" name="n_cedula" placeholder="E-8-12546 / 8-123-4564"  value="<?php $t = !empty($user->n_cedula)?$user->n_cedula:null; echo set_value('n_cedula',$t); ?>" data-parsley-required data-parsley-pattern="\w-\w{1,10}-\w" data-parsley-pattern-message="Debe ser un número de cédula válido" data-parsley-errors-container="#nCedulaError">
							<input type="text" class="form-control" style="display: none" id="n_otroDocumento" name="n_otroDocumento" placeholder="065223239 / PAA895132" value="<?php $t = !empty($user->n_otroDocumento)?$user->n_otroDocumento:null; echo set_value('n_otroDocumento',$t); ?>" data-parsley-errors-container="#otroDocumentoError">
							<span id="nCedulaError"></span>
							<span style="display: none" id="otroDocumentoError"></span>
							<input type="hidden" class="form-control" id="n_identificacion" name="n_identificacion">
						</div>
						<div class="col-sm-10 col-md-6 form-group">
							<label for="red_social">¿Cuál es tu red social favorita?</label>
							<select class="form-control" id="red_social" name="red_social">
								<option value="facebook">Facebook</option>
								<option value="instagram">Instagram</option>
								<option value="twitter">Twitter</option>
								<option value="linkedin">Linkedin</option>
								<option value="google">Google +</option>
								<option value="otra">Otra</option>
								<option value="ninguno">No uso ninguna</option>
							</select>
						</div>
						<?php
						}
						if(! $this->session->userdata('type') ){
							?>
						<div class="col-sm-10 col-md-6 form-group mr-auto">
							<label for="pass" class="control-label" >Contraseña (<span class="font-red">*</span>)</label>
							<input type="password" class="form-control" id="pass" name="pass" placeholder="········" data-parsley-required type="pass">
						</div>
						<input type="hidden" id="validacionPaso1" name="validacionPaso1" value="false">
						<?php
						}
						?>
						<div class="col-sm-10 col-md-12">
							<div class="checkbox">
								<label>
									<input type="checkbox" name="newsletter" id="newsletter" name="newsletter" value="1" <?php $t = !empty($user->n_identificacion)?$user->n_identificacion:null; echo set_value('n_identificacion',$t); ?> >
									<em>Deseo recibir promociones y ofertas.</em>
								</label>
							</div>
							<div class="checkbox">
								<label>
									<input type="checkbox" name="legal" id="legal" name="legal" value="1" data-parsley-required>
									<em>He leído y acepto la politica de privacidad y los terminos y condiciones. (<span class="font-red">*</span>)</em>
								</label>
							</div>
						</div>
						<div class="w-100 mx-auto my-4">
							<div class="col-md-12">
								<div class="form-group text-center">
									<button type="submit" id="next"  class="btn btn-primary btn-solicitud next-step">SIGUIENTE PASO</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>
<script src="<?= base_url() ?>dist/js/template/solicitud/paso1.js" type="text/javascript"></script>
<script type="text/javascript">
	function enviarForm(){
		$("#validacionPaso1").val(true);
		$("#procesar_solicitud").attr('action','<?= base_url() ?>solicitud/paso-1-check');
		setTimeout(1000,$("#procesar_solicitud").submit());
	}
	$('#userExistError').hide();
	$('#userExistError a').click(function() {
		var top = $('#login').offset().top - 100;
		$("html, body").animate({ scrollTop: top }, "slow");
	});
	$('#email').change(function(){
		var email = $(this).val();
		$.ajax({
			type: 'POST',
			url: '<?= base_url() ?>acceso/exist-user-by-email',
			data: 'email='+email,
			success: function(result){
				var res = JSON.parse(result)
				if(res.status) {
					$('#email').addClass('parsley-error');
					$('#userExistError').show();
					$('#next').attr('disabled', true);
				}else {
					$('#email').removeClass('parsley-error');
					$('#userExistError').hide();
					$('#next').attr('disabled', false);
				}
			}
		});
	});
	function login(){
		$.ajax({
			type:	'POST',
			url:	'<?= base_url() ?>acceso/entrar-clicredit-ajax',
			data:	'email='+$('#email_login').val()+'&pass='+$('#pass_login').val(),
			//dataType : 'json',
			success: function(result){
				//console.log(result);
				if(result == 1){
					location.href="<?= base_url() ?>solicitud/paso-1";
				}else if(result == '2'){
                    location.href="<?= base_url() ?>cuenta";
				}else{
					$('#info').css('display','none');
					$('#info').html('<div class="alert alert-danger"><strong>'+result+'</strong></div>');
					$('html, body').animate({scrollTop:"0px"});
					$('#info').slideDown(500);
				}
			}
		});
	}
	var is_login = 0;
	function comprobarForm(){
		$('#info').html();
		<?php
		if(! $this->session->userdata('type') ){
			?>
			is_login = 1;
			<?php
		}
		?>
		if(is_login == 0){
			console.log('do login');
			login();
		}else{
			console.log('already logged')
		}
	}

	//hacemos login al pulsar intro sobre email o contraseña
	//$('input[type=text]').on('keydown', function(e) {
		$('#email_login').on('keydown', function(e) {
			if (e.which == 13) {
				login();
			}
		});
		$('#pass_login').on('keydown', function(e) {
			if (e.which == 13) {
				login();
			}
		});
</script>


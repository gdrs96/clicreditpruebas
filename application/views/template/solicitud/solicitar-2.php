<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div class="container">
	<?=$wizard?>
	<div class="progress d-block d-lg-none mb-3" style="height:18px;font-size:12px;border: solid 1px #FFF;">
		<div class="progress-bar" role="progressbar" style="width: 25%; height:100%;background:#265c96" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">25%</div>
	</div>
	<div id="info"></div>
	<form class="font-blue mb-5" action="javascript:enviarForm();" name="procesar_solicitud" method="post" id="procesar_solicitud">
		<div class="tab-content">
			<div class="tab-pane active mb-4" role="tabpanel" id="step2">
				<h2 class="linea-azul">Cuéntanos sobre tu <b>situación personal</b></h2>
				<div class="row mt-4">
					<div class="col-md-6 col-xl-4">
						<div class="form-group">   
							<label for="2_1" class="control-label" ><?php echo $preguntas["2_1"] ?>   (<span class="font-red">*</span>)</label>
							<select class="form-control" id="2_1" name="2_1" data-parsley-required>
									<?php foreach($num_hijos as $num_hijo):?>
																						<option value="<?php echo $num_hijo->id_page;?>"><?php echo $num_hijo->description;?></option>
																						<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="2_2" class="control-label" ><?php echo $preguntas["2_2"] ?> (<span class="font-red">*</span>)</label>
							<select class="form-control" id="2_2" name="2_2" data-parsley-required>
									<?php foreach($lugares_nac as $lugar_nac):?>
																						<option value="<?php echo $lugar_nac->id_page;?>"><?php echo $lugar_nac->description;?></option>
																						<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="2_3" class="control-label" ><?php echo $preguntas["2_3"] ?> (<span class="font-red">*</span>)</label>
							<select class="form-control" id="2_3" name="2_3" data-parsley-required>
									<?php foreach($clases_sociales as $clase_social):?>
																						<option value="<?php echo $clase_social->id_page;?>"><?php echo $clase_social->description;?></option>
																						<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="2_4" class="control-label" ><?php echo $preguntas["2_4"] ?> (<span class="font-red">*</span>)</label>
							<select class="form-control" id="2_4" name="2_4" data-parsley-required>
									<?php foreach($generos as $genero):?>
																						<option value="<?php echo $genero->id_page;?>"><?php echo $genero->description;?></option>
																						<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="2_5" class="control-label" ><?php echo $preguntas["2_5"] ?> (<span class="font-red">*</span>)</label>
							<select class="form-control" id="2_5" name="2_5" data-parsley-required>
									<?php foreach($personas_dep as $persona_dep):?>
																						<option value="<?php echo $persona_dep->id_page;?>"><?php echo $persona_dep->description;?></option>
																						<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="2_7" class="control-label" ><?php echo $preguntas["2_7"] ?>  (<span class="font-red">*</span>)</label>
							<select class="form-control" id="2_7" name="2_7" data-parsley-required>
								<?php foreach($mant_pensiones as $mant_pension):?>
																						<option value="<?php echo $mant_pension->id_page;?>"><?php echo $mant_pension->description;?></option>
																		<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="2_10_telefono" class="control-label" ><?php 
								echo $preguntas["2_10"]; // --- jjy
							?> (<span class="font-red">*</span>)</label>
							<input type="text" class="form-control" id="2_10_telefono" name="2_10_telefono" data-parsley-length="[8, 8]" maxlength="8"  data-parsley-required data-parsley-trigger="keyup" data-parsley-type="number" onkeypress="return validarNro(event)" data-toggle="tooltip-top" title="Pon tu número, te enviaremos un código para completar el préstamo">
						</div>
						<div class="form-group">
							<label for="2_11" class="control-label" ><?php 
								echo $preguntas["2_11"]; // --- jjy
							?> (<span class="font-red">*</span>)</label>
							<select class="form-control" id="2_11" name="2_11" data-parsley-required>
								<?php // --- jjy
									foreach($pantalla_rota as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="col-md-6 col-xl-8">
						<div class="row">
							<div class="col-xl-6">
								<div class="form-group">
									<label for="2_12" class="control-label" ><?php 
										echo $preguntas["2_12"]; // --- jjy
									?> (<span class="font-red">*</span>)</label>
									<select class="form-control" id="2_12" name="2_12" onchange="otroNumero();" data-parsley-required>
										<?php // --- jjy
											foreach($cuantos_cel as $opc_rsp):?>
																						<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div id="2_12_group" style="display:none">
									<div class="form-group has-success">
										<label for="2_13_telefono2" class="control-label" ><?php 
											echo $preguntas["2_13"]; // --- jjy
										?></label>
										<input type="text" class="form-control" id="2_13_telefono2" name="2_13_telefono2" data-parsley-required data-parsley-length="[8, 8]" maxlength="8" onkeypress="return validarNro(event)" data-toggle="tooltip-top" title="Aumentan tus posibilidades proporcionando esta información">
									</div>
									<div class="form-group has-success">
										<label for="2_14" class="control-label" ><?php 
											echo $preguntas["2_14"]; // --- jjy
										?> (<span class="font-red">*</span>)</label>
										<select class="form-control" id="2_14" name="2_14" data-parsley-required>
											<?php // --- jjy
												foreach($varios_celulares as $opc_rsp):?>
																							<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="2_15" class="control-label" ><?php echo $preguntas["2_15"] ?> (<span class="font-red">*</span>)</label>
									<select class="form-control" id="2_15" name="2_15" data-parsley-required>
										<?php foreach($tip_cont_cel as $tip_cont):?>
																								<option value="<?php echo $tip_cont->id_page;?>"><?php echo $tip_cont->description;?></option>
																				<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label for="2_16" class="control-label" ><?php echo $preguntas["2_16"] ?> (<span class="font-red">*</span>)</label>
									<select class="form-control" id="2_16" name="2_16" data-parsley-required>
										<?php foreach($cot_seg_social as $cot_seg):?>
																								<option value="<?php echo $cot_seg->id_page;?>"><?php echo $cot_seg->description;?></option>
																				<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group">
									<label for="2_17_telefono_referencia" class="control-label" ><?php 
										echo $preguntas["2_17"]; // --- jjy
									?></label>
									<input type="text" class="form-control" id="2_17_telefono_referencia" name="2_17_telefono_referencia" maxlength="8" onkeypress="return validarNro(event)"  onchange="referenciaPersonal();" data-toggle="tooltip-top" title="Aumentan tus posibilidades proporcionando esta información">
								</div>
								<div style="display:none" id="2_17_1_group">
                                <div class="form-group">
                                	<label for="2_17_2" class="control-label" >Nombre (<span class="font-red">*</span>)</label>
                                 	<input type="text" class="form-control" id="2_17_2" name="2_17_2"  data-toggle="tooltip-top" data-parsley-required title="Aumentan tus posibilidades proporcionando esta información">
                                 </div>

								<div class="form-group 2.17.1 has-success sector">
									<label for="2_17_1" class="control-label" ><?php 
										echo $preguntas["2_17_1"]; // --- jjy
									?> (<span class="font-red">*</span>)</label>
									<select class="form-control select2" id="2_17_1" name="2_17_1" data-parsley-required data-parsley-errors-container="#errorSector">
										<?php // --- jjy
											foreach($sector_vives as $opc_rsp):?>
																						<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
										<?php endforeach; ?>
									</select>
									<span id="errorSector"></span>
								</div>
                             </div>
							</div>
							<div class="col-xl-6">
								<div class="form-group">
									<label for="2_6" class="control-label" ><?php echo $preguntas["2_6"] ?> (<span class="font-red">*</span>)</label>
									<select class="form-control" id="2_6" name="2_6" onchange="estadoCivil()" data-parsley-required>
											<?php foreach($estados_civil as $estado_civil):?>
																								<option value="<?php echo $estado_civil->id_page;?>"><?php echo $estado_civil->description;?></option>
																								<?php endforeach; ?>
									</select>
								</div>
								<div id="2_6_group" style="display: none">
									<div class="form-group has-success">
										<label for="2_6_1" class="control-label" ><?php echo $preguntas["2_6_1"] ?> (<span class="font-red">*</span>)</label>
										<select class="form-control" id="2_6_1" name="2_6_1" data-parsley-required>
											<?php foreach($primeros_mat as $primero_mat):?>
																								<option value="<?php echo $primero_mat->id_page;?>"><?php echo $primero_mat->description;?></option>
																								<?php endforeach; ?>
										</select>
									</div>
									<div class="form-group has-success">
										<label for="2_6_2" class="control-label" ><?php echo $preguntas["2_6_2"] ?> (<span class="font-red">*</span>)</label>
										<select class="form-control" id="2_6_2" name="2_6_2" data-parsley-required onchange="trabajoPareja();">
											<?php foreach($tienen_trab as $tiene_trab):?>
																								<option value="<?php echo $tiene_trab->id_page;?>"><?php echo $tiene_trab->description;?></option>
																								<?php endforeach; ?>
										</select>
									</div>
									<div class="form-group has-success" id="2_6_3_group" style="display: none">
										<label for="2_6_3" class="control-label" ><?php echo $preguntas["2_6_3"] ?>  (<span class="font-red">*</span>)</label>
										<select class="form-control" id="2_6_3" name="2_6_3" data-parsley-required>
											<?php foreach($en_que_trab as $trab):?>
																								<option value="<?php echo $trab->id_page;?>"><?php echo $trab->description;?></option>
																								<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="2_8" class="control-label" ><?php echo $preguntas["2_8"] ?>  (<span class="font-red">*</span>)</label>
									<select class="form-control" id="2_8" name="2_8" onchange="licencia()" data-parsley-required>
										<?php foreach($lic_manejar as $lic_manejo):?>
																								<option value="<?php echo $lic_manejo->id_page;?>"><?php echo $lic_manejo->description;?></option>
																				<?php endforeach; ?>
									</select>
								</div>
								<div style="display:none" id="2_8_group">
									<div class="form-group has-success">
										<label for="2_8_1" class="control-label" ><?php echo $preguntas["2_8_1"] ?>  (<span class="font-red">*</span>)</label>
										<div class="input-group">
											<input type="text" class="form-control" id="2_8_1" name="2_8_1_codigo_licencia" data-parsley-required data-parsley-errors-container="#codigoLicencia" >
											<div class="input-group-append">
												<button class="btn btn-success green" type="button" data-toggle="collapse" data-target="#licencia_png">
													<i class="fa fa-info" style="color:#f2f2f2"></i>
												</button>
											</div>
										</div>
										<span id="codigoLicencia"></span>
										<p class="collapse" id="licencia_png"><img class="img-fluid" src='<?= base_url() ?>img/licencia.png' alt='foto-licencia'></p>
									</div>
								</div>
								<div class="form-group">
									<label for="2_9" class="control-label" ><?php echo $preguntas["2_9"] ?>  (<span class="font-red">*</span>)</label>
									<select class="form-control" id="2_9" name="2_9" onchange="vehiculo();" data-parsley-required data-toggle="tooltip-top" title="Responde SI en caso de que el carro este a su nombre. Y responde NO en caso contrario">
										<?php foreach($tie_veh as $tie_ve):?>
																								<option value="<?php echo $tie_ve->id_page;?>"><?php echo $tie_ve->description;?></option>
																				<?php endforeach; ?>
									</select>
								</div>
								<div style="display:none" id="2_9_group">
									<div class="form-group has-success">
										<label for="2_9_1" class="control-label" ><?php echo $preguntas["2_9_1"] ?>  (<span class="font-red">*</span>)</label>
										<select class="form-control" id="2_9_1" name="2_9_1" data-parsley-required>
											<?php foreach($tipo_veh as $tipo_ve):?>
																								<option value="<?php echo $tipo_ve->id_page;?>"><?php echo $tipo_ve->description;?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="form-group has-success">
										<label for="2_9_2" class="control-label" ><?php echo $preguntas["2_9_2"] ?>  (<span class="font-red">*</span>)</label>
										<input type="text" class="form-control" id="2_9_2" name="2_9_2" data-parsley-required>
									</div>
									<div class="form-group has-success">
										<label for="2_9_3" class="control-label" ><?php echo $preguntas["2_9_3"] ?>  (<span class="font-red">*</span>)</label>
										<select class="form-control" id="2_9_3" name="2_9_3" data-parsley-required>
											<option value="">Seleccione una opción</option>
											<?php
											$year = 1940;
											for ($i = date('Y');$i >= $year;$i--) {
												?>
												<option value="<?= $i ?>"><?= $i ?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="form-group has-success">
										<label for="2_9_4" class="control-label" ><?php echo $preguntas["2_9_4"] ?>  (<span class="font-red">*</span>)</label>
										<select class="form-control" id="2_9_4" name="2_9_4" data-parsley-required>
											<?php foreach($prop_veh as $prop_ve):?>
																								<option value="<?php echo $prop_ve->id_page;?>"><?php echo $prop_ve->description;?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="2_18" class="control-label" ><?php echo $preguntas["2_18"] ?> (<span class="font-red">*</span>)</label>
									<select class="form-control" id="2_18" name="2_18" onchange="javascript:estudios()" data-parsley-required>
										<?php foreach($nivel_estudios as $nivel_estudio):?>
																								<option value="<?php echo $nivel_estudio->id_page;?>"><?php echo $nivel_estudio->description;?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div id="2_18_group" style="display:none">
									<div class="form-group has-success">
										<label for="2_18_1" class="control-label" ><?php echo $preguntas["2_18_1"] ?> (<span class="font-red">*</span>)</label>
										<select class="form-control" id="2_18_1" name="2_18_1" data-parsley-required>
											<?php foreach($fin_progr as $fin_prog):?>
																								<option value="<?php echo $fin_prog->id_page;?>"><?php echo $fin_prog->description;?></option>
											<?php endforeach; ?>
										</select>
									</div>
									<div class="form-group has-success">
										<label for="2_18_2" class="control-label" ><?php echo $preguntas["2_18_2"] ?> (<span class="font-red">*</span>)</label>
										<select class="form-control" id="2_18_2" name="2_18_2" data-parsley-required>
											<?php foreach($donde_estud as $donde_est):?>
																								<option value="<?php echo $donde_est->id_page;?>"><?php echo $donde_est->description;?></option>
											<?php endforeach; ?>
										</select>
									</div>
								</div>
								<div class="ayuda-solicitud my-2">
									<b>¿DUDAS?</b>
									<hr class="bg-white my-1">
									Chatea con nosotros <span class="pull-right"> <a href="#step2" onclick="openChat()" style="color: white;">! Haz Click !</a></span><br>
									Llámanos <span class="pull-right">260-8951</span><br>
									Escríbenos <span class="pull-right">ayuda@pa.clicredit.com</span><br>
									<br>
									<b>HAS SOLICITADO:</b> <span class="pull-right">¿Quieres cambiar?</span></br>
									<hr class="bg-white my-1">
									<div class="row">
										<div class="col-6 col-md-12 col-lg-6 text-center">
											Monto de: B./
											<br>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<button class="btn btn-primary bg-white border-blue" type="button" onclick="amountMinus();">
														<i class="fa fa-minus fa-inverse font-blue"></i>
													</button>	
												</div>
												<input type="numeric" class="form-control text-center px-0" aria-describedby="basic-addon1" name="amount" id="amount" value="<?php if( $this->session->userdata('datos_solicitud')['amount'] ){echo $this->session->userdata('datos_solicitud')['amount'];}else{ echo 120; }?>" min="20" max="<?php if( $this->session->userdata('max_amount') ){echo $this->session->userdata('max_amount');}else{ echo 500; }?>" readonly>
												<div class="input-group-append">
													<button class="btn btn-primary bg-white border-blue" type="button" onclick="amountPlus();">
														<i class="fa fa-plus fa-inverse font-blue"></i>
													</button>
												</div>
											</div>
										</div>
										<div class="col-6 col-md-12 col-lg-6 text-center">
											Quincenas:
											<br>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<button class="btn btn-primary bg-white border-blue" type="button" onclick="durationMinus();">
														<i class="fa fa-minus fa-inverse font-blue"></i>
													</button>	
												</div>
												<input type="numeric" class="form-control text-center" aria-describedby="basic-addon1" name="duration" id="duration" value="<?php if( $this->session->userdata('datos_solicitud')['duration'] ){echo $this->session->userdata('datos_solicitud')['duration'];}else{ echo 4; }?>" min="1" max="6" readonly>
												<div class="input-group-append">
													<button class="btn btn-primary bg-white border-blue" type="button" onclick="durationPlus();">
														<i class="fa fa-plus fa-inverse font-blue"></i>
													</button>													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<h2 class="linea-azul">Qué piensas <b>sobre estas cuestiones...</b></h2>
			<div class="row my-4">
				<div class="col-lg-8">
					<div class="form-group">
						<label for="2_19" class="control-label" ><?php 
							echo $preguntas["2_19"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="2_19" name="2_19" data-parsley-required>
							<?php // --- jjy
								foreach($tomar_decision as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="2_21" class="control-label" ><?php 
							echo $preguntas["2_21"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="2_21" name="2_21" data-parsley-required>
							<?php // --- jjy
								foreach($al_frente as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="2_22" class="control-label" ><?php 
							echo $preguntas["2_22"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="2_22" name="2_22" data-parsley-required>
							<?php // --- jjy
								foreach($correr_riesgo as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="2_23" class="control-label" ><?php 
							echo $preguntas["2_23"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="2_23" name="2_23" data-parsley-required>
							<?php // --- jjy
								foreach($interes_trabajo as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="2_26" class="control-label" ><?php 
							echo $preguntas["2_26"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="2_26" name="2_26" data-parsley-required>
							<?php // --- jjy
								foreach($energia as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="form-group">
						<label for="2_20" class="control-label" ><?php 
							echo $preguntas["2_20"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="2_20" name="2_20" data-parsley-required>
							<?php // --- jjy
								foreach($amigos_fallan as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="2_24" class="control-label"><?php 
							echo $preguntas["2_24"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="2_24" name="2_24" data-parsley-required>
							<?php // --- jjy
								foreach($sit_sociales as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="2_25" class="control-label" ><?php 
							echo $preguntas["2_25"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<select class="form-control" required id="2_25" name="2_25" data-parsley-required>
							<?php // --- jjy
								foreach($gusto_trabajo as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<input type="hidden" id="validacionPaso2" name="validacionPaso2" value="false">
					<div class="row">
						<div class="col-lg-12">
							<button  class="btn btn-primary btn-solicitud btn-block next-step" id="next">SIGUIENTE PASO 2 de 5</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</form>
</div>
<link rel="stylesheet" href="<?= base_url().last_version('css/gestion.css') ?>">
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/select2.css">
<script src="<?= base_url() ?>js/select2.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/functions.js" type="text/javascript"></script>
<script src="<?= base_url() ?>dist/js/template/solicitud/paso2.js" type="text/javascript"></script>
<script type="text/javascript">
	var urlValidacion = '<?= base_url() ?>solicitud/verificar_pasos';
	function enviarForm(){
		$("#validacionPaso2").val(true);
		$("#procesar_solicitud").attr('action','<?= base_url() ?>solicitud/paso-2-check');
		setTimeout(1000,$("#procesar_solicitud").submit());
	}
</script>
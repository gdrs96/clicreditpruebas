<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<?=$wizard?>
	<div class="progress d-block d-lg-none mb-3" style="height:18px;font-size:12px;border: solid 1px #FFF;">
		<div class="progress-bar" role="progressbar" style="width: 50%; height:100%;background:#265c96" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">50%</div>
	</div>
	<div id="info"></div>
	<form class="font-blue mb-5" action="javascript:enviarForm();" name="procesar_solicitud" method="post" id="procesar_solicitud">
		<div class="tab-content">
			<div class="tab-pane active" role="tabpanel" id="step3">
				<h2 class="linea-azul">Cuéntanos sobre tu <b>trabajo</b></h2>
				<div class="row my-4">
					<div class="col-md-6 col-xl-4">
						<div class="form-group">
							<label for="3_1" class="control-label"><?php echo $preguntas["3_1"] ?> (<span class="font-red">*</span>)</label>
							<select class="form-control" id="3_1" name="3_1" onchange="javascript:situacion_laboral();" data-parsley-required>
								<?php foreach($sits_laboral as $sit_laboral):?>
									<option value="<?php echo $sit_laboral->id_page;?>"><?php echo $sit_laboral->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label for="3_5_cantidad_dinero_minima" class="control-label" ><?php
								echo $preguntas["3_5"]; // --- jjy
							?> (<span class="font-red">*</span>)</label>
							<input type="text" class="form-control" id="3_5_cantidad_dinero_minima" name="3_5_cantidad_dinero_minima" data-parsley-required data-parsley-type="integer" onkeypress="return validarNro(event)">
						</div>
					</div>
					<div class="col-md-6 col-xl-8">
						<div class="row">
							<div class="col-xl-6">
								<div class="form-group periodo_pago has-success">
									<label for="3_7" class="control-label" ><?php 
										echo $preguntas["3_7"]; // --- jjy
									?> (<span class="font-red">*</span>)</label>
									<select class="form-control" id="3_7" name="3_7" data-parsley-required>
										<?php // --- jjy
											foreach($periodo_pago as $opc_rsp):?>
																						<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
										<?php endforeach; ?>
									</select>
								</div>
								
								
								<div class="form-group periodo_cobro has-success">
									<label for="3_24" class="control-label" >Período de cobro (<span class="font-red">*</span>)</label>
									<select class="form-control" id="3_24" name="3_24" data-parsley-required>
										<option value="">Seleccione una opción</option>
										<option value="1">Diario</option>
										<option value="2">Semanal</option>
										<option value="3">Quincena</option>
										<option value="4">Por trabajo</option>
									</select>
								</div>
								<div class="form-group actividad_laboral has-success" style="display:none">
									<label for="3_2" class="control-label" ><?php echo $preguntas["3_2"] ?> (<span class="font-red">*</span>)</label>
									<select class="form-control select2" id="3_2" name="3_2" data-parsley-required data-parsley-errors-container="#errorActividadLaboral">
										<?php foreach($acts_laboral as $act_laboral):?>
											<option value="<?php echo $act_laboral->id_page;?>"><?php echo $act_laboral->description;?></option>
										<?php endforeach; ?>
									</select> 
									<span id="errorActividadLaboral"></span>
								</div>
								<div class="form-group situacion_laboral has-success">
									<label for="3_8" class="control-label" ><?php echo $preguntas["3_8"] ?> (<span class="font-red">*</span>)</label>
									<select class="form-control" id="3_8" name="3_8" data-parsley-required>
										<?php foreach($tams_empresa as $tam_empresa):?>
																					<option value="<?php echo $tam_empresa->id_page;?>"><?php echo $tam_empresa->description;?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="form-group situacion_laboral has-success">
									<label for="3_9_telefono_empresa" class="control-label" ><?php 
										echo $preguntas["3_9"]; // --- jjy
									?> (<span class="font-red">*</span>)</label>
									<input type="text" class="form-control" id="3_9_telefono_empresa" name="3_9_telefono_empresa" data-parsley-required data-parsley-required data-parsley-length="[7, 7]" maxlength="7" onkeypress="return validarNro(event)">
								</div>
							</div>
							<div class="col-xl-6">
								<div class="form-group situacion_laboral has-success">
									<label for="3_6" class="control-label" ><?php echo $preguntas["3_6"] ?> (<span class="font-red">*</span>)</label>
									<div class="d-flex justify-content-around justify-content-md-between">
										<div class="w-25">
											<label for="3_6_dia" class="col-md-12 control-label" style="display:none">Día(<span class="font-red" style="display:none">*</span>)</label>
											<select class="form-control" type="text" name="3_6_dia"  id="3_6_dia" data-parsley-required>
												<option value="">Día</option>
												<?php
												$dias_mes = 31;
												for ($i=1; $i <= $dias_mes; $i++) {
													?>
													<option><?= $i ?></option>
													<?php
												}
												?>
											</select>
										</div>
										<div class="w-25">
											<label for="3_6_mes" class="col-md-12 control-label" style="display:none">Mes(<span class="font-red" style="display:none">*</span>)</label>
											<select class="form-control" type="text" name="3_6_mes" id="3_6_mes"  data-parsley-required>
												<option value="">Mes</option>
												<?php
												$meses = 12;
												for ($i=1; $i <= $meses; $i++) {
													?>
													<option><?= $i ?></option>
													<?php
												}
												?>
											</select>
										</div>
										<div class="w-25">
											<label for="3_6_year" class="col-md-12 control-label" style="display:none">Año(<span class="font-red" style="display:none">*</span>)</label>
											<select class="form-control" type="text" name="3_6_year"  id="3_6_year" data-parsley-required>
												<option value="">Año</option>
												<?php
												$years = date('Y');
												for ($i = ( $years - 60 ); $i <= $years ; $i++) {
													?>
													<option><?= $i ?></option>
													<?php
												}
												?>
											</select>
										</div>
									</div>
								</div>
								<div class="form-group situacion_laboral has-success">
									<label for="3_3_nombre_empresa" class="control-label" >Nombre de la empresa (<span class="font-red">*</span>)</label>
									<input type="text" class="form-control" id="3_3_nombre_empresa" name="3_3_nombre_empresa" data-parsley-required>
								</div>
								<div class="form-group situacion_laboral has-success">
									<label for="3_4" class="control-label" ><?php echo $preguntas["3_4"] ?> (<span class="font-red">*</span>)</label>
									<select class="form-control" id="3_4" name="3_4" data-parsley-required>
										<?php foreach($tipos_contrato as $tipo_contrato):?>
																									<option value="<?php echo $tipo_contrato->id_page;?>"><?php echo $tipo_contrato->description;?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<div class="ayuda-solicitud my-2">
									<b>¿DUDAS?</b>
									<hr class="bg-white my-1">
									Chatea con nosotros <span class="pull-right"> <a href="#step2" onclick="openChat()" style="color: white;">! Haz Click !</a></span><br>
									Llámanos <span class="pull-right">260-8951</span><br>
									Escríbenos <span class="pull-right">ayuda@pa.clicredit.com</span><br>
									<br>
									<b>HAS SOLICITADO:</b> <span class="pull-right">¿Quieres cambiar?</span></br>
									<hr class="bg-white my-1">
									<div class="row">
										<div class="col-6 col-md-12 col-lg-6 text-center">
											Monto de: B./
											<br>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<button class="btn btn-primary bg-white border-blue" type="button" onclick="amountMinus();">
														<i class="fa fa-minus fa-inverse font-blue"></i>
													</button>
												</div>
												<input type="numeric" class="form-control text-center px-0" aria-describedby="basic-addon1" name="amount" id="amount" value="<?php if( $this->session->userdata('datos_solicitud')['amount'] ){echo $this->session->userdata('datos_solicitud')['amount'];}else{ echo 120; }?>" min="20" max="<?php if( $this->session->userdata('max_amount') ){echo $this->session->userdata('max_amount');}else{ echo 500; }?>" readonly>
												<div class="input-group-append">
													<button class="btn btn-primary bg-white border-blue" type="button" onclick="amountPlus();">
														<i class="fa fa-plus fa-inverse font-blue"></i>
													</button>						
												</div>
											</div>
										</div>
										<div class="col-6 col-md-12 col-lg-6 text-center">
											Quincenas:
											<br>
											<div class="input-group mb-3">
												<div class="input-group-prepend">
													<button class="btn btn-primary bg-white border-blue" type="button" onclick="durationMinus();">
														<i class="fa fa-minus fa-inverse font-blue"></i>
													</button>
												</div>
												<input type="numeric" class="form-control text-center" aria-describedby="basic-addon1" name="duration" id="duration" value="<?php if( $this->session->userdata('datos_solicitud')['duration'] ){echo $this->session->userdata('datos_solicitud')['duration'];}else{ echo 4; }?>" min="1" max="6" readonly>
												<div class="input-group-append">
													<button class="btn btn-primary bg-white border-blue" type="button" onclick="durationPlus();">
														<i class="fa fa-plus fa-inverse font-blue"></i>
													</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
			</div>
			<h2 class="linea-azul">Cuéntanos sobre tus <b>ingresos</b></h2>
			<div class="row my-4">
				<div class="col-md-6 col-xl-8">
					<div class="row">
						<div class="col-xl-6">
							<div class="form-group">
								<label for="3_10" class="control-label"><?php
									echo $preguntas["3_10"]; // --- jjy
								?> (<span class="font-red">*</span>)</label>
								<input type="text" class="form-control" id="3_10" name="3_10" data-parsley-required data-parsley-type="integer" onkeypress="return validarNro(event)">
								<?php /* --- jjy
									<select class="form-control" id="3_10" name="3_10" data-parsley-required>
										<?php // --- jjy
											foreach($ingresos_mes as $opc_rsp):?>
												<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
										<?php endforeach; ?>
									</select> */ 
								?>
							</div>
							<div class="form-group">
								<label for="3_11_gastos_mensuales" class="control-label" ><?php
									echo $preguntas["3_11"]; // --- jjy
								?> (<span class="font-red">*</span>)</label>
								<input type="text" class="form-control" id="3_11_gastos_mensuales" name="3_11_gastos_mensuales" data-parsley-required data-parsley-type="integer" onkeypress="return validarNro(event)">
							</div>
						</div>
						<div class="col-xl-6">
							<div class="form-group">
								<label for="3_12" class="control-label" ><?php
									echo $preguntas["3_12"]; // --- jjy
								?> (<span class="font-red">*</span>)</label>
								<select class="form-control" id="3_12" name="3_12" data-parsley-required>
									<?php // --- jjy
										foreach($destino_prestamo as $opc_rsp):?>
																					<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<label for="3_13" class="control-label" ><?php
									echo $preguntas["3_13"]; // --- jjy
								?> (<span class="font-red">*</span>)</label>
								<select class="form-control" id="3_13" name="3_13" data-parsley-required>
									<?php // --- jjy
										foreach($dcto_directo as $opc_rsp):?>
																					<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-xl-4">
					<div class="form-group">
						<label for="3_14" class="control-label" ><?php echo $preguntas["3_14"] ?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="3_14" name="3_14" data-parsley-required>
							<?php foreach($has_credits_card as $has_card):?>
																		<option value="<?php echo $has_card->id_page;?>"><?php echo $has_card->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label for="3_15" class="control-label" ><?php 
							echo $preguntas["3_15"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="3_15" name="3_15" onchange="javascript:deudas()" data-parsley-required>
							<?php // --- jjy
								foreach($tienes_deudas as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div id="3_15_group" style="display:none">
						<div class="form-group has-success">
							<label for="3_15_1" class="control-label" >¿Total de deudas por mes? (<span class="font-red">*</span>)</label>
							<input type="text" class="form-control" id="3_15_1" name="3_15_1" data-parsley-required>
						</div>
						<div class="form-group has-success">
							<label for="3_15_2" class="control-label" ><?php
								echo $preguntas["3_15_2"]; // --- jjy
							?> (<span class="font-red">*</span>)</label>
							<select class="form-control" id="3_15_2" name="3_15_2" data-parsley-required>
								<?php // --- jjy
									foreach($deudas_vencidas as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group has-success">
							<label for="3_15_3" class="control-label" ><?php
								echo $preguntas["3_15_3"]; // --- jjy
							?> (<span class="font-red">*</span>)</label>
							<select class="form-control" id="3_15_3" name="3_15_3" data-parsley-required>
								<?php // --- jjy
									foreach($otros_creditos as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
			</div>
			<h2 class="linea-azul">Que piensas <b>sobre estas cuestiones...</b></h2>
			<div class="row my-4">
				<div class="form-group col-12">
					<div class="row">
						<label for="3_16" class="col-md-8 control-label" ><?php 
							echo $preguntas["3_16"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="3_16" name="3_16" data-parsley-required>
								<?php // --- jjy
									foreach($callar as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="3_17" class="col-md-8 control-label"><?php 
							echo $preguntas["3_17"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="3_17" name="3_17" data-parsley-required>
								<?php // --- jjy
									foreach($estabilidad as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="3_18" class="col-md-8 control-label" ><?php 
							echo $preguntas["3_18"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="3_18" name="3_18" data-parsley-required>
								<?php // --- jjy
									foreach($viejos_habitos as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="3_19" class="col-md-8 control-label" ><?php 
							echo $preguntas["3_19"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="3_19" name="3_19" data-parsley-required>
								<?php // --- jjy
									foreach($falta_social as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="3_20" class="col-md-8 control-label"><?php 
							echo $preguntas["3_20"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="3_20" name="3_20" data-parsley-required>
								<?php // --- jjy
									foreach($vacaciones as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="3_21" class="col-md-8 control-label" ><?php 
							echo $preguntas["3_21"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="3_21" name="3_21" data-parsley-required>
								<?php // --- jjy
									foreach($capacidad as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="3_22" class="col-md-8 control-label" ><?php 
							echo $preguntas["3_22"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="3_22" name="3_22" data-parsley-required>
								<?php // --- jjy
									foreach($temo_castigo as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="3_23" class="col-md-8 control-label" ><?php 
							echo $preguntas["3_23"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="3_23" name="3_23" data-parsley-required>
								<?php // --- jjy
									foreach($jefe_razon as $opc_rsp):?>
																				<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-4 offset-md-8">
					<button class="btn btn-primary btn-solicitud next-step btn-block" id="next">SIGUIENTE PASO 3 de 5</button>
				</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</form>
</div>
<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/select2.css">
<script src="<?= base_url() ?>js/select2.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/functions.js" type="text/javascript"></script>
<script src="<?= base_url() ?>dist/js/template/solicitud/paso3.js" type="text/javascript"></script>
<script type="text/javascript">
	var urlValidacion = '<?= base_url() ?>solicitud/verificar_pasos';
	function enviarForm(){
		$("#procesar_solicitud").attr('action','<?= base_url() ?>solicitud/paso-3-check');
			setTimeout(1000,$("#procesar_solicitud").submit());
		}
</script>





<!-- <link href="<?= base_url() ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="<?= base_url() ?>js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var ismobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
		if ( ! ismobile) {
			$('.dtpicker').datetimepicker({ pickTime: false });
		}else{
			$('#3_6').attr('type','date');
		}
	});
</script> -->
<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container">
	<?=$wizard?>
	<div class="progress d-block d-lg-none mb-3" style="height:18px;font-size:12px;border: solid 1px #FFF;">
		<div class="progress-bar" role="progressbar" style="width: 75%; height:100%;background:#265c96" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">75%</div>
	</div>
	<div id="info"></div>
	<form class="font-blue mb-5" action="javascript:enviarForm();" name="procesar_solicitud" method="post" id="procesar_solicitud">
		<div class="tab-pane active" role="tabpanel" id="step4">
			<h2 class="linea-azul">Cuéntanos <b>dónde vives actualmente</b></h2>
			<div class="row my-4">
				<div class="col-md-6 col-xl-4">
					<div class="form-group">
						<label for="4_1" class="control-label" ><?php echo $preguntas["4_1"] ?> (<span class="font-red">*</span>)</label>
						<select class="form-control select2" id="4_1" name="4_1" data-parsley-required data-parsley-errors-container="#errorProvincia">
							<option value="">Seleccione una opción</option>
							<?php foreach($provincias as $provincia):?>
								<option value="P_<?php echo $provincia->id_page;?>"><?php echo $provincia->description;?></option>
							<?php endforeach; ?>
						</select>
						<span id="errorProvincia"></span>
					</div>
					<div class="form-group has-success distrito" style="display:none">
						<label for="4_2" class="control-label" ><?php echo $preguntas["4_2"] ?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="4_2" name="4_2" data-parsley-errors-container="#errorDistrito">>
							<option value="">Seleccione una opción</option>
						</select>
						<span id="errorDistrito"></span>
					</div>
					<div class="form-group has-success corregimiento" style="display:none">
						<label for="4_3" class="control-label" ><?php echo $preguntas["4_3"] ?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="4_3" name="4_3" data-parsley-errors-container="#errorCorregimiento">>
							<option value="">Seleccione una opción</option>
						</select>
						<span id="errorCorregimiento"></span>
					</div>
					<div class="form-group has-success sector" style="display:none">
						<label for="4_4" class="control-label" ><?php echo $preguntas["4_4"] ?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="4_4" name="4_4" data-parsley-errors-container="#errorSector">>
							<option value="">Seleccione una opción</option>
						</select>
						<span id="errorSector"></span>
					</div>
					<div class="form-group has-success barrio" style="display:none">
						<label for="4_5" class="control-label" ><?php echo $preguntas["4_5"] ?> (<span class="font-red">*</span>)</label>
						<select class="form-control" id="4_5" name="4_5" data-parsley-errors-container="#errorBarrio">
							<option value="">Seleccione una opción</option>
						</select>
						<span id="errorBarrio"></span>
					</div>
					<div class="form-group">
						<label for="4_6" class="control-label" ><?php echo $preguntas["4_6"] ?> (<span class="font-red">*</span>)</label>
						<input type="text" class="form-control" id="4_6" name="4_6" data-parsley-required>
					</div>
				</div>
				<div class="col-md-6 col-xl-8">
					<div class="row">
						<div class="col-xl-6">
							<div class="form-group">
								<label for="4_7_mes" class="control-label" ><?php echo $preguntas["4_7"] ?> (<span class="font-red">*</span>)</label>
								<div class="d-flex justify-content-around justify-content-md-between">
									<div class="w-25">
										<label for="4_7_dia" class="col-md-12 control-label" style="display:none;">Día(<span class="font-red" style="">*</span>)</label>
										<select class="form-control" type="text" id="4_7_dia" name="4_7_dia" data-parsley-required>
											<option value="">Día</option>
											<?php
											$dias_mes = 31;
											for ($i=1; $i <= $dias_mes; $i++) {
												?>
												<option><?= $i ?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="w-25">
										<label for="4_7_mes" class="col-md-12 control-label" style="display:none">Mes(<span class="font-red" style="display:none">*</span>)</label>
										<select class="form-control" type="text" id="4_7_mes" name="4_7_mes" data-parsley-required>
											<option value="">Mes</option>
											<?php
											$meses = 12;
											for ($i=1; $i <= $meses; $i++) {
												?>
												<option><?= $i ?></option>
												<?php
											}
											?>
										</select>
									</div>
									<div class="w-25">
										<label for="4_7_year" class="col-md-12 control-label" style="display:none">Año(<span class="font-red" style="display:none">*</span>)</label>
										<select class="form-control" type="text" id="4_7_year" name="4_7_year" data-parsley-required>
											<option value="">Año</option>
											<?php
											$years = date('Y');
											for ($i = ( $years - 60 ); $i <= $years ; $i++) {
												?>
												<option><?= $i ?></option>
												<?php
											}
											?>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="4_8" class="control-label" >¿Tienes teléfono fijo? (<span class="font-red">*</span>)</label>
								<select class="form-control" id="4_8" name="4_8" onchange="javascript:tel_fijo()" data-parsley-required>
									<option value="">Seleccione una opción</option>
									<option value="1">Si</option>
									<option value="2">No</option>
								</select>
							</div>
							<div id="4_8_group" style="display:none">
								<div class="form-group has-success">
									<label for="4_8_1" class="control-label" >Indíquenos su teléfono: (<span class="font-red">*</span>)</label>
									<input type="text" class="form-control" id="4_8_1" name="4_8_1" data-parsley-required data-parsley-type="integer" onkeypress="return validarNro(event)" data-toggle="tooltip" title="Aumentan tus posibilidades proporcionando esta información">
								</div>
							</div>
							<div class="form-group">
								<label for="4_9" class="control-label" ><?php
										echo $preguntas["4_9"]; // --- jjy
									?> (<span class="font-red">*</span>)</label>
								<select class="form-control" id="4_9" name="4_9" data-parsley-required>
									<?php // --- jjy
										foreach($tipos_vivienda as $opc_rsp):?>
																			<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
									<?php endforeach; ?>
								</select>
							</div>
							<div class="form-group">
								<label for="4_10" class="control-label" ><?php echo $preguntas["4_10"] ?> (<span class="font-red">*</span>)</label>
								<select class="form-control" id="4_10" name="4_10" data-parsley-required>
									<?php foreach($cambios_dom as $cambio_dom):?>
											<option value="<?php echo $cambio_dom->id_page;?>"><?php echo $cambio_dom->description;?></option>
									<?php endforeach; ?>
								</select>
							</div>
						</div>
						<div class="col-xl-6">
							<div class="ayuda-solicitud my-2">
								<b>¿DUDAS?</b>
								<hr class="bg-white my-1">
								Chatea con nosotros <span class="pull-right"> <a href="#step2" onclick="openChat()" style="color: white;">! Haz Click !</a></span><br>
								Llámanos <span class="pull-right">260-8951</span><br>
								Escríbenos <span class="pull-right">ayuda@pa.clicredit.com</span><br>
								<br>
								<b>HAS SOLICITADO:</b> <span class="pull-right">¿Quieres cambiar?</span></br>
								<hr class="bg-white my-1">
								<div class="row">
									<div class="col-6 col-md-12 col-lg-6 text-center">
										Monto de: B./
										<br>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<button class="btn btn-primary bg-white border-blue" type="button" onclick="amountMinus();">
													<i class="fa fa-minus fa-inverse font-blue"></i>
												</button>
											</div>
											<input type="numeric" class="form-control text-center px-0" aria-describedby="basic-addon1" name="amount" id="amount" value="<?php if( $this->session->userdata('datos_solicitud')['amount'] ){echo $this->session->userdata('datos_solicitud')['amount'];}else{ echo 120; }?>" min="20" max="<?php if( $this->session->userdata('max_amount') ){echo $this->session->userdata('max_amount');}else{ echo 500; }?>" readonly>
											<div class="input-group-append">
												<button class="btn btn-primary bg-white border-blue" type="button" onclick="amountPlus();">
													<i class="fa fa-plus fa-inverse font-blue"></i>
												</button>
											</div>
										</div>
									</div>
									<div class="col-6 col-md-12 col-lg-6 text-center">
										Quincenas:
										<br>
										<div class="input-group mb-3">
											<div class="input-group-prepend">
												<button class="btn btn-primary bg-white border-blue" type="button" onclick="durationMinus();">
													<i class="fa fa-minus fa-inverse font-blue"></i>
												</button>
											</div>
											<input type="numeric" class="form-control text-center" aria-describedby="basic-addon1" name="duration" id="duration" value="<?php if( $this->session->userdata('datos_solicitud')['duration'] ){echo $this->session->userdata('datos_solicitud')['duration'];}else{ echo 4; }?>" min="1" max="6" readonly>
											<div class="input-group-append">
												<button class="btn btn-primary bg-white border-blue" type="button" onclick="durationPlus();">
													<i class="fa fa-plus fa-inverse font-blue"></i>
												</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<h2 class="font-blue linea-azul">Qué piensas <b>sobre estas cuestiones...</b></h2>
			<div class="row my-4">
				<div class="form-group col-12">
					<div class="row">
						<label for="4_11" class="control-label col-md-8" ><?php 
							echo $preguntas["4_11"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="4_11" name="4_11" data-parsley-required>
								<?php // --- jjy
									foreach($gozar_vida as $opc_rsp):?>
																		<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="4_12" class="control-label col-md-8" ><?php 
							echo $preguntas["4_12"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="4_12" name="4_12" data-parsley-required>
								<?php // --- jjy
									foreach($interesante_ser as $opc_rsp):?>
																		<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="4_13" class="control-label col-md-8"><?php 
							echo $preguntas["4_13"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="4_13" name="4_13" data-parsley-required>
								<?php // --- jjy
									foreach($dificultades as $opc_rsp):?>
																		<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="4_14" class="control-label col-md-8" ><?php 
							echo $preguntas["4_14"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="4_14" name="4_14" data-parsley-required>
								<?php // --- jjy
									foreach($interes_mismo_hys as $opc_rsp):?>
																		<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="4_15" class="control-label col-md-8" ><?php 
							echo $preguntas["4_15"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="4_15" name="4_15" data-parsley-required>
								<?php // --- jjy
									foreach($gente_autoritaria as $opc_rsp):?>
																	<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="4_16" class="control-label col-md-8" ><?php 
							echo $preguntas["4_16"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="4_16" name="4_16" data-parsley-required>
								<?php // --- jjy
									foreach($dormir_bien as $opc_rsp):?>
																		<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="form-group col-12">
					<div class="row">
						<label for="4_17" class="control-label col-md-8" ><?php 
							echo $preguntas["4_17"]; // --- jjy
						?> (<span class="font-red">*</span>)</label>
						<div class="col-md-4">
							<select class="form-control" id="4_17" name="4_17" data-parsley-required>
								<?php // --- jjy
									foreach($opinion_padres as $opc_rsp):?>
																		<option value="<?php echo $opc_rsp->id_page;?>"><?php echo $opc_rsp->description;?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				</div>
				<div class="col-md-4 offset-md-8">
					<button class="btn btn-primary btn-solicitud btn-block next-step" id="next">SIGUIENTE PASO 4 de 5</button>
				</div>
			</div>
		</div>
		<div class="clearfix"></div>
	</form>
</div>

<div class="collapse" distritos>
	<?php foreach($provincias as $provincia):?>
		<optgroup label="<?php echo $provincia->description;?>" class="P_<?php echo $provincia->id_page;?>">
			<?php foreach($provincia->distritos as $distrito):?>
				<option value="D_<?php echo $distrito->id_page;?>"><?php echo $distrito->description; ?></option>
			<?php endforeach; ?>
		</optgroup>
	<?php endforeach; ?>
</div>
<div class="collapse" corregimientos>
	<?php foreach($provincias as $provincia):?>
		<?php foreach($provincia->distritos as $distrito):?>
			<?php if(count($distrito->corregimientos) > 0):?>
				<optgroup label="<?php echo $distrito->description;?>" class="D_<?php echo $distrito->id_page;?>">									
					<?php foreach($distrito->corregimientos as $corregimiento):?>
						<option value="C_<?php echo $corregimiento->id_page;?>"><?php echo $corregimiento->description; ?></option>
					<?php endforeach; ?>
				</optgroup>
			<?php endif; ?>
		<?php endforeach; ?>
	<?php endforeach; ?>
</div>
<div class="collapse" sectores>
	<?php foreach($provincias as $provincia):?>
		<?php foreach($provincia->distritos as $distrito):?>
			<?php foreach($distrito->corregimientos as $corregimiento):?>
				<?php if(count($corregimiento->sectores) > 0):?>
					<optgroup label="<?php echo $corregimiento->description;?>" class="C_<?php echo $corregimiento->id_page;?>">									
						<?php foreach($corregimiento->sectores as $sector):?>
							<option value="S_<?php echo $sector->id_page;?>"><?php echo $sector->description; ?></option>
						<?php endforeach; ?>
					</optgroup>
				<?php endif; ?>
			<?php endforeach; ?>
		<?php endforeach; ?>
	<?php endforeach; ?>
</div>
<div class="collapse" barrios>
	<?php foreach($provincias as $provincia):?>
		<?php foreach($provincia->distritos as $distrito):?>
			<?php foreach($distrito->corregimientos as $corregimiento):?>
				<?php foreach($corregimiento->sectores as $sector):?>
					<?php if(count($sector->barrios) > 0):?>
						<optgroup label="<?php echo $sector->description;?>" class="S_<?php echo $sector->id_page;?>">									
							<?php foreach($sector->barrios as $barrio):?>
								<option><?php echo $barrio->description; ?></option>
							<?php endforeach; ?>
						</optgroup>
					<?php endif; ?>
				<?php endforeach; ?>
			<?php endforeach; ?>
		<?php endforeach; ?>
	<?php endforeach; ?>
</div>

<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/select2.css">
<script src="<?= base_url() ?>js/select2.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/functions.js" type="text/javascript"></script>
<script src="<?= base_url() ?>dist/js/template/solicitud/paso4.js" type="text/javascript"></script>
<script type="text/javascript">
	var urlValidacion = '<?= base_url() ?>solicitud/verificar_pasos';
	function enviarForm(){
        $("#procesar_solicitud").attr('action','<?= base_url() ?>solicitud/paso-4-check');
			setTimeout(1000,$("#procesar_solicitud").submit());
		}
</script>


<!--
<link href="<?= base_url() ?>css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<script src="<?= base_url() ?>js/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(){
		var ismobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
		if ( ! ismobile) {
			$('.dtpicker').datetimepicker({ pickTime: false });
		}else{
			$('#fecha_nacimiento').attr('type','date');
		}
	});
</script>
<script type="text/javascript">
	$(document).ready(function(){
		var ismobile = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)|(android)|(webOS)/i);
		if ( ! ismobile) {
			$('.dtpicker').datetimepicker({ pickTime: false });
		}else{
			$('#4_7').attr('type','date');
		}
	});
</script> -->



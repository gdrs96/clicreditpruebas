<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!--
	2017-04-01 - Guillermo - include new button to send new code by SMS. Only numbers can be added in the field
-->
<div class="container">
	<?=$wizard?>
	<div class="progress d-block d-lg-none mb-3" style="height:18px;font-size:12px;border: solid 1px #FFF;">
		<div class="progress-bar" role="progressbar" style="width: 100%; height:100%;background:#265c96" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%</div>
	</div>
	<div id="info"></div>
   <form class="font-blue mb-5" name="procesar_solicitud" id="procesar_solicitud" enctype="multipart/form-data" method="post">

		<div class="tab-content">
			<div class="tab-pane active" role="tabpanel" id="complete">
				<h2 class="linea-azul">¿A dónde te <b>enviamos el dinero?</b></h2>
				<div class="row my-4">
					<div class="col-md-6 col-xl-8">
						<div class="row">
							<div class="col-xl-6">
								<div class="form-group">
									<label for="5_1_numero_cuenta" class="control-label" ><?php 
										echo $preguntas["5_1"]; // --- jjy
									?> (<span class="font-red">*</span>)</label>
									<input type="text" class="form-control" id="5_1_numero_cuenta" name="5_1_numero_cuenta" data-parsley-required data-toggle="tooltip-top" title="La cuenta donde quieres que te transfiramos">
								</div>
								<div class="form-group">
									<label for="5_1_1_nombre_cuenta" class="control-label" >Nombre de la cuenta (<span class="font-red">*</span>)</label>
									<input type="text" class="form-control" id="5_1_1_nombre_cuenta" name="5_1_1_nombre_cuenta" data-parsley-required data-toggle="tooltip-top" title="Recuerda que la cuenta debe estar a tu nombre">
								</div>
								<div class="form-group">
									<label for="5_2" class="control-label" >Entidad bancaria (<span class="font-red">*</span>)</label>
									<select class="form-control" id="5_2" name="5_2" data-parsley-required>
										<?php foreach($bancos as $banco):?>
												<option value="<?php echo $banco->id_page;?>"><?php echo $banco->description;?></option>
										<?php endforeach; ?>
									</select>
								</div>
								<p class="d-none">Cierra el proceso ahora y recibe el dinero en:<br>
								<b class="font-red" id="mensaje_banco">30 minutos en tu cuenta</b>.</p>
							</div>
							<div class="col-xl-6" id="send">
								<div class="form-group">
									<label for="5_4" class="control-label" >¿Tienes tarjeta Crédito/ Débito? (<span class="font-red">*</span>)</label>
									<select class="form-control" id="5_4" name="5_4" onchange="javascript:tarjetas()" onclick="" data-parsley-required data-toggle="tooltip-top" title="Aumentan tus posibilidades proporcionando esta información">
										<option value="">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
								<div id="5_4_group" style="display:none">	
									<div class="form-group has-success">
										<label for="5_4_1" class="control-label" >¿Cuántas tienes? (<span class="font-red">*</span>)</label>
										<select class="form-control" id="5_4_1" name="5_4_1">
											<option value="0">Seleccione una opción</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8 o más</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="codigo_verificacion_telefono" class="control-label" >Introduce el código que te hemos enviado *</label>
									<input type="text" class="form-control" id="codigo_verificacion_telefono" name="codigo_verificacion_telefono"  data-parsley-required>
								</div>
								<div class="form-group">
									<div class="d-flex justify-content-between">
										<a class="btn btn-azul" onclick="sendCode();">Enviar de Nuevo</a>
										<a class="btn btn-azul" data-toggle="collapse" data-target="#nuevo_telefono_div">Cambiar Número</a>
									</div>

									<div id ="nuevo_telefono_div" class="panel-collapse collapse mt-4">
										<p class="mb-1"><b>Introduce el número de celular correcto</b></p>
										<input type="text" class="form-control" id="nuevo_telefono" name="2_10_telefono" data-parsley-length="[8, 8]" maxlength="8" onkeypress="return validarNro(event)">
										<a class="btn btn-azul mt-3" onclick="sendCodeNewPhone();">Reenviar código</a>
									</div>
								</div>
							</div>
						</div>
					</div>	
					<div class="col-md-6 col-xl-4">
						<h2 class="text-center">Hora <b>actual <?php echo date('h:i'); if(date('H') > 11 ){ echo ' pm'; }else{ echo ' am'; } ?></b></h2>
						<p class="cuadro-blue text-justify">Si se termina el proceso <b class="font-red">después de las 12.00 PM</b>, el depósito será efectivo en la mañana del día siguiente, <b class="font-red">si lo terminamos antes</b> será efectivo a lo largo del día. Dependerá de su banco, en caso de ser el mismo que el nuestro.</p>
						<div class="ayuda-solicitud my-2">
							<b>¿DUDAS?</b>
							<hr class="bg-white my-1">
							Chatea con nosotros <span class="pull-right"> <a href="#step2" onclick="openChat()" style="color: white;">! Haz Click !</a></span><br>
							Llámanos <span class="pull-right">260-8951</span><br>
							Escríbenos <span class="pull-right">ayuda@pa.clicredit.com</span><br>
							<br>
							<b>HAS SOLICITADO:</b> <span class="pull-right">¿Quieres cambiar?</span></br>
							<hr class="bg-white my-1">
							<div class="row">
								<div class="col-6 col-md-12 col-lg-6 text-center">
									Monto de: B./
									<br>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<button class="btn btn-primary bg-white border-blue" type="button" onclick="amountMinus();" id="amMinus">
												<i class="fa fa-minus fa-inverse font-blue"></i>
											</button>
										</div>
										<input type="numeric" class="form-control text-center px-0" aria-describedby="basic-addon1" name="amount" id="amount" value="<?php if( $this->session->userdata('datos_solicitud')['amount'] ){echo $this->session->userdata('datos_solicitud')['amount'];}else{ echo 120; }?>" min="20" max="<?php if( $this->session->userdata('max_amount') ){echo $this->session->userdata('max_amount');}else{ echo 500; }?>" readonly>
										<div class="input-group-append">
											<button class="btn btn-primary bg-white border-blue" type="button" onclick="amountPlus();" id="amPlus">
												<i class="fa fa-plus fa-inverse font-blue"></i>
											</button>	
										</div>
									</div>
								</div>
								<div class="col-6 col-md-12 col-lg-6 text-center">
									Quincenas:
									<br>
									<div class="input-group mb-3">
										<div class="input-group-prepend">
											<button class="btn btn-primary bg-white border-blue" type="button" onclick="durationMinus();" id="duMinus">
												<i class="fa fa-minus fa-inverse font-blue"></i>
											</button>	
										</div>
										<input type="numeric" class="form-control text-center" aria-describedby="basic-addon1" name="duration" id="duration" value="<?php if( $this->session->userdata('datos_solicitud')['duration'] ){echo $this->session->userdata('datos_solicitud')['duration'];}else{ echo 4; }?>" min="1" max="6" readonly>
										<div class="input-group-append">
											<button class="btn btn-primary bg-white border-blue" type="button" onclick="durationPlus();" id="duPlus">
												<i class="fa fa-plus fa-inverse font-blue"></i>
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row my-5">
					<div class="col-md-6">
						<h2 class="linea-azul">Envía <b>tus documentos</b></h2>
						<div class="row my-4">
							<div class="col-lg-8 d-flex align-items-center justify-content-between mb-2">
								<label for="imagen_cedula">Confirma tu identidad: *, con una foto de su cedula (que se pueda leer bien):</label>
								<span class="btn btn-outline-success rounded-circle" data-toggle="tooltip-hover" title="Tome una imagen de su cedula con buena calidad" data-trigger="click">
									<i class="fa fa-info"></i>
								</span>
							</div>
							<div class="col-lg-4 mb-2">
								<input type="file" name="imagen_cedula" id="imagen_cedula" class="inputfile inputfile-1-orange" style="display: none" data-parsley-required   data-parsley-errors-container="#errorName"  />
								<label for="imagen_cedula"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Subir imagen</span></label>
								<span id="errorName"></span>
							</div>
							<div class="col-lg-8 d-flex align-items-center justify-content-between mb-2">
								<label for="imagen_seguro_social">Ficha del seguro social:</label>
								<span class="btn btn-outline-success rounded-circle" data-toggle="tooltip-hover" title="Tome una imagen con buena calidad">
									<i class="fa fa-info"></i>
								</span>
							</div>
							<div class="col-lg-4 mb-2">
								<input type="file" name="imagen_seguro_social" id="imagen_seguro_social" class="inputfile inputfile-1"  style="display: none"/>
								<label for="imagen_seguro_social"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Subir imagen</span></label>
							</div>
							<div class="col-lg-8 d-flex align-items-center justify-content-between mb-2">
								<label for="imagen_talonarios">Ultimos talonarios:</label>
								<span class="btn btn-outline-success rounded-circle" data-toggle="tooltip-hover" title="Tome una imagen con buena calidad">
									<i class="fa fa-info"></i>
								</span>							
							</div>
							<div class="col-lg-4 mb-2">
								<input type="file" name="imagen_talonarios[]" id="imagen_talonarios" class="inputfile inputfile-1" data-multiple-caption="{count} archivos seleccionados" multiple style="display: none"/>
								<label for="imagen_talonarios"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Subir imagen</span></label>
							</div>
							<div class="col-lg-8 d-flex align-items-center justify-content-between mb-2">
								<label for="imagen_recibos">Recibo de luz, agua o telefono:</label>
								<span class="btn btn-outline-success rounded-circle" data-toggle="tooltip-hover" title="Tome una imagen con buena calidad">
									<i class="fa fa-info"></i>
								</span>							
							</div>
							<div class="col-lg-4">
								<input type="file" name="imagen_recibos[]" id="imagen_recibos" class="inputfile inputfile-1" data-multiple-caption="{count} archivos seleccionados" multiple style="display: none"/>
								<label for="imagen_recibos"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Subir imagen</span></label>
							</div>
							<div class="col-12 mt-4">
								<p>Sólo es obligatorio confirmar la identidad, el resto de documentos lo puedes enviar por <a target="_blank" href="https://api.whatsapp.com/send?phone=50763777870" class="font-red d-inline d-sm-none">WhatsApp</a><b class="font-red d-none d-sm-inline">WhatsApp al número 6377-7870</b></p>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<h2 class="linea-azul">Resumen de <b>tu préstamo</b></h2>
							<div class="row my-4">
								<div class="col-5 text-right">
									<p><b>Solicitado:</b></p>
									<p><b>Coste</b></p>
									<p><b>Total aplazado en:</b></p>
									<p><b>Total de días:</b></p>
									<p><b>Pagas en:</b></p>
									<p><b>Finalizas el:</b></p>
								</div>
								<div class="col-7 text-left">
									<p><b class="font-green" id="solicitado">B./ <?= $this->session->userdata('datos_solicitud')['amount'] ?></b></p>
									<p><b class="font-green" id="interes"></b></p>
									<p><b class="font-green" id="cuota"></b></p>
									<p><b class="font-green" id="total_dias"></b></p>
									<p><b class="font-green" id="pagas_en"></b></p>
									<p><b class="font-green" id="fecha_finalizacion"></b></p>
								</div>
							</div>
							<input type="hidden" name="telefono_verificado" id="telefono_verificado" value="0">
							<input type="hidden" name="ajustarQuincenas" id="ajustarQuincenas" value="<?=$this->session->userdata('ajustar_quincenas')?>">
							<input type="hidden" id="tasa" value="<?=$calificacion->tasa_interes?>">
							<input type="hidden" id="total_aplazado" name="total_aplazado">
							<input type="hidden" id="monto_total" name="monto_total">
							<div class="row mb-2">
								<div class="col-12">
									<button id="enviar" class="btn btn-block orange font-white">! SOLICÍTALO YA !</button>
								</div>
							</div>
							<p class="text-center">Solicítalo sólo si estas seguro que puedes devolverlo.</p>
						</div>
					</div>
				</div>
				<h5 class="text-center">Recuerda que Clicredit.com <b>no es financiación a largo plazo</b>, sólo es para ayudarte puntualmente con un imprevisto.</h5>
				<div class="clearfix"></div>
			</div>
		</div>	
	</form>
</div>

<link rel="stylesheet" href="<?= base_url() ?>css/gestion.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/select2.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/component-type-file.css">
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">


<script type="text/javascript">
var url = '<?= base_url() ?>';
var urlPaso5 ='<?= base_url() ?>solicitud/paso-5-check';
var urlValidacion = '<?= base_url() ?>solicitud/verificar_pasos';
</script>
<script type="text/javascript" src="<?= base_url() ?>js/jquery.form.js"></script>
<script src="<?= base_url() ?>js/custom-file-input.js"></script>
<script src="<?= base_url() ?>js/functions.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/select2.js" type="text/javascript"></script>
<script src="<?= base_url() ?>-/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>
<script src="<?= base_url() ?>dist/js/template/solicitud/paso5.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		var coste = (((($('#duration').val() *15) * 0.008332) * $('#amount').val()) ).toFixed(2);
	    
        function sendCode() {
		var params = {'usuario' : '<?= $this->session->userdata('user_id') ?>'};
		$.ajax({
          url: '<?= base_url("sendSms") ?>',
          type: 'POST',               
          data: params,
          success: function(msg)
          {
            alert(msg);
          }
        });
	}

	function sendCodeNewPhone() {
		if ($("#nuevo_telefono").val() == '') {
			alert("Debe Ingresar un nuevo Número para enviar el Código");
		} else {
			var params = {'usuario' : '<?= $this->session->userdata('user_id') ?>', 'newPhone' : $("#nuevo_telefono").val()};
			$.ajax({
	          url: '<?= base_url("sendSms/newPhone") ?>',
	          type: 'POST',               
	          data: params,
	          success: function(msg)
	          {
	            alert(msg);
	            //$("#nuevo_telefono_div").removeClass("in");
	          }
	        });
		}	
	}
</script>
	

<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="content">
	<div class="container blue-font">
		<div class="wizard">
			<div class="wizard-inner">
				<div class="connecting-line"></div>
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active">
						<a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="1">
							<div class="round-tab">
								<h2>1</h2>
								<p class="font-12">PERSONAL</p>
							</div>
						</a>
					</li>
					<li role="presentation" class="disabled">
						<a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="2">
							<div class="round-tab">
								<h2>2</h2>
								<p class="font-12">SITUACIÓN</p>
							</div>
						</a>
					</li>
					<li role="presentation" class="disabled">
						<a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="3">
							<div class="round-tab">
								<h2>3</h2>
								<p class="font-12">TRABAJO</p>
							</div>
						</a>
					</li>
					<li role="presentation" class="disabled">
						<a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="4">
							<div class="round-tab">
								<h2>4</h2>
								<p class="font-12">CASA</p>

							</div>
						</a>
					</li>
					<li role="presentation" class="disabled">
						<a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Complete">
							<div class="round-tab">
								<h2>5</h2>
								<p class="font-12">HISTORIAL</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="text-center">
			<h1>Solicitud de préstamo Clicredit.com</h1>
			<p>Los campos con el (*) son obligatorios</p>
		</div>
		<div id="info"></div>
		<form class="form-horizontal form-asistente-solicitar" action="javascript:enviarForm();" name="procesar_solicitud" method="post" id="solicitar">
			<div class="tab-content">
				<div class="tab-pane active" role="tabpanel" id="step1">
					<h2 class="titulo-verde">1.	DATOS BASICOS</h2>
					<?php
					if( ! $this->session->userdata('type')){
						?>
						<h3 class="titulo-verde">Registro de cuenta</h3>
						<h4 class="text-center">Si está ya registrado por favor inicie sesión</h4>
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form-group">
									<label for="email_login" class="control-label" >Email / Cedula</label>
									<div>
										<input type="text" class="form-control" id="email_login" required>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-8 col-md-offset-2">
								<div class="form-group">
									<label for="pass_login" class="control-label" >Contraseña</label>
									<div>
										<input type="password" class="form-control" id="pass_login" required>
									</div>
								</div>
							</div>
						</div>
						<div class="text-center">
							<p><a href="<?= base_url() ?>acceso/recuperar-cuenta">¿Problemas con la contraseña?</a></p>
							<button type="button" class="btn btn-primary btn-solicitar" onclick="login();">Entrar</button>
						</div>

						<h3>Si no está registrado rellene los datos</h3>

						<?php
					}
					?>
					<h3>Por favor compruebe los datos</h3>

					<br>
					<div class="form-group">
						<label for="nombre" class="col-lg-6 control-label" >Primer nombre</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="name" placeholder="Alberto" required value="<?php $t = !empty($user->name)?$user->name:null; echo set_value('name',$t); ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="nombre2" class="col-lg-6 control-label" >Segundo nombre</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="segundo_nombre" placeholder="Jose" value="<?php $t = !empty($user->segundo_nombre)?$user->segundo_nombre:null; echo set_value('segundo_nombre',$t); ?>" >
						</div>
					</div>
					<div class="form-group">
						<label for="apellido" class="col-lg-6 control-label" >Primer apellido</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="primer_apellido" placeholder="Rios" value="<?php $t = !empty($user->primer_apellido)?$user->primer_apellido:null; echo set_value('primer_apellido',$t); ?>" >
						</div>
					</div>
					<div class="form-group">
						<label for="appelido2" class="col-lg-6 control-label" >Segundo apellido</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="segundo_appelido" placeholder="Laez" value="<?php $t = !empty($user->segundo_appelido)?$user->segundo_appelido:null; echo set_value('segundo_appelido',$t); ?>" >
						</div>
					</div>
					<div class="form-group">
						<label for="nombre" class="col-lg-6 control-label" >Fecha de nacimiento</label>
						<div class="col-lg-6">
							<input type="date" class="form-control" id="fecha_nacimiento" value="<?php $t = !empty($user->fecha_nacimiento)?$user->fecha_nacimiento:null; echo set_value('fecha_nacimiento',$t); ?>" >
						</div>
					</div>
					<div class="form-group">
						<label for="email" class="col-lg-6 control-label" >Email</label>
						<div class="col-lg-6">
							<input type="email" class="form-control" id="email" placeholder="prueba@gmail.com" required value="<?php $t = !empty($user->email)?$user->email:null; echo set_value('email',$t); ?>" >
						</div>
					</div>
					<div class="form-group">
						<label for="email_check" class="col-lg-6 control-label" >Confirma email</label>
						<div class="col-lg-6">
							<input type="email" class="form-control" id="email_check" placeholder="prueba@gmail.com" required value="<?php $t = !empty($user->email)?$user->email:null; echo set_value('email',$t); ?>">
						</div>
					</div>
					<div class="form-group">
						<label for="n_identificacion" class="col-lg-6 control-label" >Cedula</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="n_identificacion" placeholder="E-8-12546 / 8-123-4564" name="n_identificacion" required value="<?php $t = !empty($user->n_identificacion)?$user->n_identificacion:null; echo set_value('n_identificacion',$t); ?>" >
						</div>
					</div>
					<div class="form-group">
						<label for="pass" class="col-lg-6 control-label" >Contraseña</label>
						<div class="col-lg-6">
							<input type="password" class="form-control" id="pass" placeholder="········" required type="pass">
						</div>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="newsletter" id="newsletter" value="1" <?php $t = !empty($user->n_identificacion)?$user->n_identificacion:null; echo set_value('n_identificacion',$t); ?> >
							Deseo recibir promociones y ofertas.
						</label>
					</div>
					<div class="checkbox">
						<label>
							<input type="checkbox" name="legal" id="legal" value="1" required>
							He leído y acepto la politica de privacidad y los terminos y condiciones.
						</label>
					</div>
					<br>
					<div class="form-group text-center">
						<button type="button" class="btn btn-primary btn-solicitar next-step">Registrarme y continuar</button>
					</div>
				</div>
				<div class="tab-pane" role="tabpanel" id="step2">
					<h2 class="titulo-verde">2. PERSONAL</h2>
					<h3 class="titulo-verde">Cuentanos sobre tu situacion personal</h3>
					<br>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="2_1" class="col-lg-8 control-label" >2.1 ¿Cuántos hijos tienes?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_1" name="2_1_">
										<option value="0">Seleccione una opción</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10 o más</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_2" class="col-lg-8 control-label" >2.2 Dónde naciste</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_2" name="2_2">
										<option value="0">Seleccione una opción</option>
										<option value="1">Panama</option>
										<option value="2">Chiriquí</option>
										<option value="3">Coclé</option>
										<option value="4">Veraguas</option>
										<option value="5">Herrera</option>
										<option value="6">Los Santos</option>
										<option value="7">Boc. del Toro</option>
										<option value="8">Ngabe Bugle</option>
										<option value="9">Guna Yala</option>
										<option value="10">Darién</option>
										<option value="11">Emberá</option>
										<option value="12">Extranjero</option>
										<option value="13">Colon</option>
									</select>
								</div>
							</div>

							<div class="form-group">
								<label for="2_3" class="col-lg-8 control-label" >2.3 ¿Con que clase social te identificas?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_3" name="2_3">
										<option value="0">Seleccione una opción</option>
										<option value="1">Baja</option>
										<option value="2">Media-baja</option>
										<option value="3">Media-media</option>
										<option value="4">Media-alta</option>
										<option value="5">Alta</option>
										<option value="6">Muy alta</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_4" class="col-lg-8 control-label" >2.4 ¿Cuál es tu genero?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_4" name="2_4">
										<option value="0">Seleccione una opción</option>
										<option value="1">Hombre</option>
										<option value="2">Mujer</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_5" class="col-lg-8 control-label" >2.5 Personas que dependen de mi</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_5" name="2_5">
										<option value="0">Seleccione una opción</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10 o más</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_7" class="col-lg-8 control-label" >2.7 ¿Tienes pensiones de manutención?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_7" name="2_7">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_10_telefono" class="col-lg-8 control-label" >2.10 ¿Cuál es tu número de celular?</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" id="2_10_telefono" name="2_10_telefono">
								</div>
							</div>
							<div class="form-group">
								<label for="2_11" class="col-lg-8 control-label" >2.11 ¿Tienes la pantalla rota del celular?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_11" name="2_11">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
										<option value="3">Un poco</option>
										<option value="4">Mucho</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_12" class="col-lg-8 control-label" >2.12 ¿Cuántos numeros de celular tienes?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_12" name="2_12">
										<option value="0">Seleccione una opción</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5 o más</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_13_telefono2" class="col-lg-8 control-label" >2.13 Indicanos otro numero</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" id="2_13_telefono2" name="2_13_telefono2">
								</div>
							</div>
							<div class="form-group">
								<label for="2_14" class="col-lg-8 control-label" >2.14 ¿Por qué tienes varios celulares?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_14" name="2_14">
										<option value="0">Seleccione una opción</option>
										<option value="1">No sé</option>
										<option value="2">Trabajo</option>
										<option value="3">bulling</option>
										<option value="4">Regalo</option>
										<option value="5">Otro</option>
										<option value="6">No tengo</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_15" class="col-lg-8 control-label" >2.15 Tipo de contrato de celular</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_15" name="2_15">
										<option value="0">Seleccione una opción</option>
										<option value="1">PrePago</option>
										<option value="2">PostPago</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_16" class="col-lg-8 control-label" >2.16 ¿Cotizas a la seguridad social?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_16" name="2_16">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_17_telefono_referencia" class="col-lg-8 control-label" >2.17 Celular de una referencia personal</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" id="2_17_telefono_referencia" name="2_17_telefono_referencia">
								</div>
							</div>
							<div class="form-group 2.17.1">
								<label for="2.17.1" class="col-lg-8 control-label" >2.17.1 Sector donde vive</label>
								<div class="col-lg-4">
									<select class="form-control" id="2.17.1" name="2.17.1">
										<option value="0">Seleccione una opción</option>
										<option value="1">S-24D</option>
										<option value="1">Santa Maria de los Angeles</option>
										<option value="1">24 de Diciembre</option>
										<option value="1">Vista Hermosa</option>
										<option value="1">Monte Rico </option>
										<option value="1">Cabuyita</option>
										<option value="1">Rancho Café</option>
										<option value="1">Nueva Esperanza</option>
										<option value="1">Felipillo</option>
										<option value="1">Nuevo Tocumen</option>
										<option value="1">San Pedro</option>
										<option value="1">Buena Vista</option>
										<option value="1">El Lago</option>
										<option value="1">Cerro Azul</option>
										<option value="1">Villa Belen</option>
										<option value="1">El Valle de Cerro Azul</option>
										<option value="1">Los Cantaros</option>
										<option value="1">S-ANCON</option>
										<option value="1">Amador</option>
										<option value="1">Albrook</option>
										<option value="1">Amador Cause Way</option>
										<option value="1">Balboa</option>
										<option value="1">Bosques de Cibeles</option>
										<option value="1">Cardenas</option>
										<option value="1">Clayton</option>
										<option value="1">Dorado Lake</option>
										<option value="1">Diablo</option>
										<option value="1">Gamboa</option>
										<option value="1">Los rios</option>
										<option value="1">Paraiso</option>
										<option value="1">San Francisco</option>
										<option value="1">Kuna Nega</option>
										<option value="1">Pedro Miguel</option>
										<option value="1">Ancon</option>
										<option value="1">Llanos de Curundu</option>
										<option value="1">La Boca</option>
										<option value="1">Altos de Curundu</option>
										<option value="1">S-CHORRILLO</option>
										<option value="1">Ave de los Poetas</option>
										<option value="1">Avenida Sur</option>
										<option value="1">Avenida A</option>
										<option value="1">Calle B</option>
										<option value="1">Calle "1"2</option>
										<option value="1">Calle 22 Oeste</option>
										<option value="1">Calle 25 Oeste</option>
										<option value="1">Calle 26 Oeste</option>
										<option value="1">Calle "1"9 Oeste</option>
										<option value="1">Calle 23 Oeste</option>
										<option value="1">Calle 27</option>
										<option value="1">Barraza</option>
										<option value="1">Pedro Obarrio</option>
										<option value="1">S-CURUNDU</option>
										<option value="1">Brooklin</option>
										<option value="1">Viejo Veranillo</option>
										<option value="1">Cabo Verde</option>
										<option value="1">Llano Bonito</option>
										<option value="1">Metropol</option>
										<option value="1">Transporte</option>
										<option value="1">Talleres A y B</option>
										<option value="1">Sector Aguja</option>
										<option value="1">Sector Tivoli</option>
										<option value="1">Sector Aguila</option>
										<option value="1">Santa Cruz</option>
										<option value="1">Sector Relleno</option>
										<option value="1">LA Primavera</option>
										<option value="1">Hollywood</option>
										<option value="1">S-TABOGA</option>
										<option value="1">San Antonio</option>
										<option value="1">Dorasol</option>
										<option value="1">Camino real</option>
										<option value="1">Colinas del Golf</option>
										<option value="1">Club de Golf</option>
										<option value="1">S-CHILIBRE</option>
										<option value="1">Quebrada Ancha</option>
										<option value="1">El Roble</option>
										<option value="1">Hogar Crea</option>
										<option value="1">El Sitio</option>
										<option value="1">San Pablo</option>
										<option value="1">San Vicente</option>
										<option value="1">Agua Buena </option>
										<option value="1">LA Union</option>
										<option value="1">Alto Lindo</option>
										<option value="1">LA Fe</option>
										<option value="1">El Pedernal </option>
										<option value="1">La Loma</option>
										<option value="1">Don Bosco</option>
										<option value="1">Puente Roque</option>
										<option value="1">San Jose</option>
										<option value="1">Maria Eugenia</option>
										<option value="1">Chilibre Centro</option>
										<option value="1">Agua Bendita</option>
										<option value="1">La Union Veraguense</option>
										<option value="1">San Antonio</option>
										<option value="1">Los Pinos</option>
										<option value="1">Caimitillo Centro</option>
										<option value="1">Guamural</option>
										<option value="1">Nuevo Mexico</option>
										<option value="1">Nuevo Caimitillo</option>
										<option value="1">LA Reforma</option>
										<option value="1">Llano Verde</option>
										<option value="1">Victoriano Lorenzo</option>
										<option value="1">San Juan de Pequeni</option>
										<option value="1">Quebrada Ancha</option>
										<option value="1">Quebrada Fea</option>
										<option value="1">Mono Congo</option>
										<option value="1">Embera Drua</option>
										<option value="1">Monte Ore</option>
										<option value="1">S-BELLAVISTA</option>
										<option value="1">Avenida Balboa</option>
										<option value="1">Campo Alegre</option>
										<option value="1">Nuevo Campo Alegre</option>
										<option value="1">Pasadena</option>
										<option value="1">San Gabriel</option>
										<option value="1">Marbella</option>
										<option value="1">Calle 50</option>
										<option value="1">El_cangrejo</option>
										<option value="1">El Carmen</option>
										<option value="1">LA Cresta</option>
										<option value="1">Marbella</option>
										<option value="1">Obarrio</option>
										<option value="1">Via España</option>
										<option value="1">Vista Hermosa</option>
										<option value="1">Via Brazil</option>
										<option value="1">S-BETANIA</option>
										<option value="1">Edison Park</option>
										<option value="1">El Dorado</option>
										<option value="1">El ingenio</option>
										<option value="1">La Alameda</option>
										<option value="1">La loceria</option>
										<option value="1">Las mercedes</option>
										<option value="1">Los Angeles</option>
										<option value="1">Miraflores</option>
										<option value="1">Tumba Muerto</option>
										<option value="1">Villa Caceres</option>
										<option value="1">Villa de las Fuentes</option>
										<option value="1">Altos de Shase</option>
										<option value="1">Dos Mares </option>
										<option value="1">LA Gloria</option>
										<option value="1">Los Libertadores</option>
										<option value="1">Sara Sotillo</option>
										<option value="1">San Antonio</option>
										<option value="1">Linda Vista</option>
										<option value="1">Tuira</option>
										<option value="1">Chucunaque</option>
										<option value="1">Santa Maria</option>
										<option value="1">Residencial Colonial</option>
										<option value="1">Altos de Betania</option>
										<option value="1">El Avance</option>
										<option value="1">Club X</option>
										<option value="1">Soberania</option>
										<option value="1">S-CALIDONIA</option>
										<option value="1">Avenida justo Arosemena</option>
										<option value="1">Perejil</option>
										<option value="1">Marañon</option>
										<option value="1">San Miguel</option>
										<option value="1">La Exposicion</option>
										<option value="1">S-JUANDIAZ</option>
										<option value="1">Costa Sur</option>
										<option value="1">Ciudad Radial</option>
										<option value="1">Costa del este</option>
										<option value="1">Las Acacias</option>
										<option value="1">Urbanizacion Don Bosco</option>
										<option value="1">Llano Bonito</option>
										<option value="1">Santa Maria</option>
										<option value="1">Campo limberg</option>
										<option value="1">Santa Clara</option>
										<option value="1">Area Industrial</option>
										<option value="1">El Sitio </option>
										<option value="1">Residencial Bernal</option>
										<option value="1">Villa Ines</option>
										<option value="1">San Cristobal</option>
										<option value="1">Jardin Olimpico</option>
										<option value="1">Las Estaciones</option>
										<option value="1">Residencial Olimpico</option>
										<option value="1">Villas del Sol</option>
										<option value="1">La Cantera</option>
										<option value="1">Altos del Complejo</option>
										<option value="1">San Pedro</option>
										<option value="1">Altos del Hipodromo</option>
										<option value="1">El Nance</option>
										<option value="1">Urb. El Guayacan</option>
										<option value="1">Altos de San Pedro</option>
										<option value="1">Urb. Nuevo San Pedro</option>
										<option value="1">Urb. Nuevo Hipodromo</option>
										<option value="1">Villa Venus</option>
										<option value="1">Urb. El Laurel</option>
										<option value="1">Urb. Camino Real</option>
										<option value="1">Villa olga</option>
										<option value="1">Los Almendros</option>
										<option value="1">Juan Diaz Centro</option>
										<option value="1">Residencial Santa Ines</option>
										<option value="1">Residencial Carolina</option>
										<option value="1">Urb. Anayansi</option>
										<option value="1">El Porvenir Nuevo</option>
										<option value="1">Urb. Villa Esperanza</option>
										<option value="1">Urb. El Trebol</option>
										<option value="1">San Fernando</option>
										<option value="1">Urb. Belen</option>
										<option value="1">Urb. Santarela</option>
										<option value="1">LA Toscana</option>
										<option value="1">Residencial Juan Diaz</option>
										<option value="1">Urb. Colonias del Carmen</option>
										<option value="1">Urb. Rosa Mistica</option>
										<option value="1">El Milagro</option>
										<option value="1">El Palmar- El Pailon</option>
										<option value="1">Urb. LA poderosa</option>
										<option value="1">Villa Serena</option>
										<option value="1">Guayabito</option>
										<option value="1">Nueva California</option>
										<option value="1">La Terraza</option>
										<option value="1">Los Pueblos</option>
										<option value="1">Urb LA Arboleda</option>
										<option value="1">Villa Bonita</option>
										<option value="1">Villa Alegre</option>
										<option value="1">Francisco Arias Paredes</option>
										<option value="1">El Bosque Altamira</option>
										<option value="1">Barriada del Guardia</option>
										<option value="1">Jose Ramon Cantera</option>
										<option value="1">Residencial Alfa</option>
										<option value="1">Residencial El Carmen</option>
										<option value="1">Barriada el Encanto</option>
										<option value="1">Pedregalito</option>
										<option value="1">Concepcion LA nueva</option>
										<option value="1">Villa Las Acacias</option>
										<option value="1">Altos de las acacias</option>
										<option value="1">Los Robles</option>
										<option value="1">Don Bosco</option>
										<option value="1">Villa Licia</option>
										<option value="1">Villa Catalina</option>
										<option value="1">Colonias del Prado</option>
										<option value="1">Los Caobos</option>
										<option value="1">Bello Horizonte</option>
										<option value="1">Teremar</option>
										<option value="1">Rio Tapia</option>
										<option value="1">S-LAFEVRE</option>
										<option value="1">Chanis</option>
										<option value="1">Carrasquilla</option>
										<option value="1">Panamá Viejo</option>
										<option value="1">Reparto Nuevo Panama</option>
										<option value="1">Via Cincuentenario</option>
										<option value="1">S-PUEBLONUEVO</option>
										<option value="1">"1"2 de Octubre</option>
										<option value="1">Hato Pintado</option>
										<option value="1">S-RIOABAJO</option>
										<option value="1">Urb. Altos del Rio</option>
										<option value="1">Santa Clara</option>
										<option value="1">S-SANFRANCISCO</option>
										<option value="1">Altos de Golf</option>
										<option value="1">Coco del Mar</option>
										<option value="1">Club de Golf</option>
										<option value="1">Punta Pacifica</option>
										<option value="1">Punta Paitilla</option>
										<option value="1">Via Porras</option>
										<option value="1">S-AMELIADENIS</option>
										<option value="1">Altos de Panamá</option>
										<option value="1">Altos de Santa Maria</option>
										<option value="1">Condado del Rey</option>
										<option value="1">El Bosque</option>
										<option value="1">El Doral</option>
										<option value="1">Fuente del Fresno</option>
										<option value="1">S-BELISARIOPORRAS</option>
										<option value="1">Samaria</option>
										<option value="1">Cerro Batea</option>
										<option value="1">S-JOSEDOMINGO</option>
										<option value="1">Bariloche</option>
										<option value="1">La Castellana</option>
										<option value="1">Villas de Cerro Lindo</option>
										<option value="1">Paseo del Valle</option>
										<option value="1">El Crisol</option>
										<option value="1">Villa Lucre</option>
										<option value="1">Boulevard Los Laureles</option>
										<option value="1">S-OMARTORRIJOS</option>
										<option value="1">Milla 8</option>
										<option value="1">San Isidro</option>
										<option value="1">Santa Librada</option>
										<option value="1">S-TOCUMEN</option>
										<option value="1">Las_Mañanitas</option>
										<option value="1">Buena vista</option>
										<option value="1">Santa Eduviges</option>
										<option value="1">Jorge Illueca</option>
										<option value="1">Nuevo Belén</option>
										<option value="1">Belén</option>
										<option value="1">Sector Sur</option>
										<option value="1">La Colina</option>
										<option value="1">La Alborada</option>
										<option value="1">San Antonio</option>
										<option value="1">Bajo Cordero</option>
										<option value="1">"1"6 de Diciembre</option>
										<option value="1">Nueva Barriada</option>
										<option value="1">Victoriano Lorenzo</option>
										<option value="1">Altos de Tocumen</option>
										<option value="1">Villa Marta</option>
										<option value="1">Los Pilones</option>
										<option value="1">Cabuya</option>
										<option value="1">La Siesta</option>
										<option value="1">Altos del lago</option>
										<option value="1">La Primavera</option>
										<option value="1">El Ceremi</option>
										<option value="1">Punta del Este</option>
										<option value="1">Torremolinos</option>
										<option value="1">Puerta del Este</option>
										<option value="1">S-RUFINA</option>
										<option value="1">Brisas del Golf</option>
										<option value="1">San Antonio</option>
										<option value="1">Cerro Viento</option>
										<option value="1">Dorasol</option>
										<option value="1">Camino Real</option>
										<option value="1">Colinas del Golf</option>
										<option value="1">Club de Golf</option>
										<option value="1">S-LASLAJAS</option>
										<option value="1">Coronado</option>
										<option value="1">Playa Coronado</option>
										<option value="1">S-NUEVAGORGONA</option>
										<option value="1">Playa Gorgona</option>
										<option value="1">Playa Malibu</option>
										<option value="1">S-ARRAIJAN</option>
										<option value="1">Cocoli</option>
										<option value="1">Pedro Miguel</option>
										<option value="1">Bajo Las  Palmas</option>
										<option value="1">Barriada el Chorro</option>
										<option value="1">S-VERACRUZ</option>
										<option value="1">Howard</option>
										<option value="1">Rodman</option>
										<option value="1">Panamá Pacifico</option>
										<option value="1">S-VISTAALEGRE</option>
										<option value="1">Vacamonte</option>
										<option value="1">El Mirador</option>
										<option value="1">S-CONCEPTCION</option>
										<option value="1">Belén</option>
										<option value="1">Solano</option>
										<option value="1">El Porvenir</option>
										<option value="1">Vista Hermosa</option>
										<option value="1">Dulce Hogar</option>
										<option value="1">Cuesta de Piedra</option>
										<option value="1">Bello Horizonte </option>
										<option value="1">Bugabita</option>
										<option value="1">S-DAVID</option>
										<option value="1">Los Andes 2</option>
										<option value="1">Barriada Monte fresco</option>
										<option value="1">Paraiso</option>
										<option value="1">Urb. Santa Maria</option>
										<option value="1">Barriada 20 de diciembre</option>
										<option value="1">Portal Las Margaritas</option>
										<option value="1">Villa Laura</option>
										<option value="1">Villa San Carlitos</option>
										<option value="1">Residencial Casa Hacienda</option>
										<option value="1">Urb. La Fontana</option>
										<option value="1">Villa Venice</option>
										<option value="1">Las Margaritas</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="2_6" class="col-lg-8 control-label" >2.6 Estado Civil</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_6" name="2_6">
										<option value="0">Seleccione una opción</option>
										<option value="1">Soltero/a</option>
										<option value="2">Casado/a</option>
										<option value="3">Divorciado/a</option>
										<option value="4">Viudo/a</option>
										<option value="5">Unido</option>
									</select>
								</div>
							</div>
							<div class="cuadro-verde 2.6">
								<div class="form-group has-success">
									<label for="2_6_1" class="col-lg-8 control-label" >2.6.1 ¿Es tu primer matrimonio?</label>
									<div class="col-lg-4">
										<select class="form-control" id="2_6_1" name="2_6_1">
											<option value="0">Seleccione una opción</option>
											<option value="1">Si</option>
											<option value="2">No</option>
										</select>
									</div>
								</div>
								<div class="form-group has-success">
									<label for="2_6_2" class="col-lg-8 control-label" >2.6.2 ¿Tu esposa tiene trabajo?</label>
									<div class="col-lg-4">
										<select class="form-control" id="2_6_2" name="2_6_2">
											<option value="0">Seleccione una opción</option>
											<option value="1">Si</option>
											<option value="2">No</option>
										</select>
									</div>
								</div>
								<div class="form-group has-success">
									<label for="2_6_3" class="col-lg-8 control-label" >2.6.3 ¿En que trabaja? </label>
									<div class="col-lg-4">
										<select class="form-control" id="2_6_3" name="2_6_3">
											<option value="0">Seleccione una opción</option>
											<option value="1">Empresa Privada</option>
											<option value="2">Empresa Publica</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="2_8" class="col-lg-8 control-label" >2.8 ¿Tines licencia de manejar?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_8" name="2_8">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div>
								<div class="form-group has-success">
									<label for="2_8_1" class="col-lg-8 control-label" >2.8.1 Indicanos código de licencia</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" id="2_8_1" name="2_8_1_codigo_licencia">
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="2_9" class="col-lg-8 control-label" >2.9 ¿Tienes vehiculo?</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_9" name="2_9">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div>
								<div class="form-group">
									<label for="2_9_1" class="col-lg-8 control-label" >2.9.1 ¿De que tipo es?</label>
									<div class="col-lg-4">
										<select class="form-control" id="2_9_1" name="2_9_1">
											<option value="0">Seleccione una opción</option>
											<option value="1">Si</option>
											<option value="2">No</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="2_9_2" class="col-lg-8 control-label" >2.9.2 Dinos la placa</label>
									<div class="col-lg-4">
										<select class="form-control" id="2_9_2" name="2_9_2">
											<option value="0">Seleccione una opción</option>
											<option value="1">Si</option>
											<option value="2">No</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="2_9_3" class="col-lg-8 control-label" >2.9.3 De que año es tu vehiculo</label>
									<div class="col-lg-4">
										<select class="form-control" id="2_9_3" name="2_9_3">
											<option value="0">Seleccione una opción</option>
											<option value="1">Si</option>
											<option value="2">No</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="2_9_4" class="col-lg-8 control-label" >2.9.4 ¿Eres propietario del vehiculo?</label>
									<div class="col-lg-4">
										<select class="form-control" id="2_9_4" name="2_9_4">
											<option value="0">Seleccione una opción</option>
											<option value="1">Si</option>
											<option value="2">No</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="2_18" class="col-lg-8 control-label" >2.18 Nivel de estudios</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_18" name="2_18">
										<option value="0">Seleccione una opción</option>
										<option value="1">Ninguno</option>
										<option value="2">Básico</option>
										<option value="3">Bachiller</option>
										<option value="4">Tecnico</option>
										<option value="5">Tecnologico</option>
										<option value="6">Universitario</option>
									</select>
								</div>
							</div>
							<div>
								<div class="form-group">
									<label for="2.18.1" class="col-lg-8 control-label" >2.18.1 ¿Finalizaste el programa?</label>
									<div class="col-lg-4">
										<select class="form-control" id="2.18.1" name="2.18.1">
											<option value="0">Seleccione una opción</option>
											<option value="1">Si</option>
											<option value="2">No</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="2.18.2" class="col-lg-8 control-label" >2.18.2 ¿Dónde estudiaste?</label>
									<div class="col-lg-4">
										<select class="form-control" id="2.18.2" name="2.18.2">
											<option value="0">Seleccione una opción</option>
											<option value="1">Panamá</option>
											<option value="2">Extranjero</option>
										</select>
									</div>
								</div>

							</div>
						</div>
					</div>
					<h3 class="titulo-verde">Que piensas sobre estas cuestiones...</h3>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="2_19" class="col-lg-8 control-label" >2.19 Cuando tengo que tomar una decision suelo inclinarme más en</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_19" name="2_19">
										<option value="0">Seleccione una opción</option>
										<option value="1">El corazón</option>
										<option value="2">Los sentimientos y la razón</option>
										<option value="3">La cabeza</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_20" class="col-lg-8 control-label" >2.20 Mis amigos me han fallado</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_20" name="2_20">
										<option value="0">Seleccione una opción</option>
										<option value="1">Muy rara vez</option>
										<option value="2">Ocasionalmente</option>
										<option value="3">Muchas veces</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_21" class="col-lg-8 control-label" >2.21 Si estoy al frente, se hacen las cosas como yo digo, sino renuncio</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_21" name="2_21">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">Termino Medio</option>
										<option value="3">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_22" class="col-lg-8 control-label" >2.22 Aún con pocas posibilidades de éxito, merece la pena correr el riesgo</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_22" name="2_22">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">Alguna vez</option>
										<option value="3">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_23" class="col-lg-8 control-label" >2.23 Te resultaria más interesante trabajar en una empresa</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_23" name="2_23">
										<option value="0">Seleccione una opción</option>
										<option value="1">Atendiendo clientes</option>
										<option value="2">Termino medio</option>
										<option value="3">Llevando las cuentas</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_24" class="col-lg-8 control-label" >2.24 En situaciones sociales</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_24" name="2_24">
										<option value="0">Seleccione una opción</option>
										<option value="1">Facilmente tomo la iniciativa</option>
										<option value="2">Intervengo algunas veces</option>
										<option value="3">Me quedo tranquilo a distancia</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_25" class="col-lg-8 control-label" >2.25 Me gustaria más tener un trabajo con</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_25" name="2_25">
										<option value="0">Seleccione una opción</option>
										<option value="1">Sueldo determinado fijo</option>
										<option value="2">Termino medio</option>
										<option value="3">Sueldo más alto, pero demostrando que lo merezco</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="2_26" class="col-lg-8 control-label" >2.26 Tengo tanta energia que siempre estoy ocupado/a</label>
								<div class="col-lg-4">
									<select class="form-control" id="2_26" name="2_26">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No estoy seguro</option>
										<option value="3">No</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group text-center">
						<button type="button" class="btn btn-default prev-step">Atrás</button>
						<button type="button" class="btn btn-primary btn-solicitar next-step">Guardar y continuar</button>
					</div>
				</div>
				<div class="tab-pane" role="tabpanel" id="step3">
					<h2 class="titulo-verde">3. SITUACIÓN LABORAL</h2>
					<h3 class="titulo-verde">Cuentanos sobre tu trabajo...</h3>
					<br>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="3_1" class="col-lg-8 control-label" >3.1 Situación laboral</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_1" name="3_1">
										<option value="0">Seleccione una opción</option>
										<option value="1">Empleado</option>
										<option value="2">Independiente</option>
										<option value="3">Desemplead</option>
										<option value="4">Pensionado</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_2" class="col-lg-8 control-label" >3.2 Actividadad laboral</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_2" name="3_2">
										<option value="0">Seleccione una opción</option>
										<option value="1">Administrativa</option>
										<option value="2">Gerencia</option>
										<option value="3">Operativas o de servicio</option>
										<option value="4">Ventas y mercadeo</option>
										<option value="5">Construccion</option>
										<option value="6">Tecnicas (ingenieria y similares)</option>
										<option value="7">Vigilancia y seguridad</option>
										<option value="8">Conductor (taxita, Metrobus…)</option>
										<option value="9">Funcionario del estado</option>
										<option value="10">Banca y finanzas</option>
										<option value="11">Agricultura</option>
										<option value="12">Inmobiliaria</option>
										<option value="13">Hosteleria</option>
										<option value="14">Energia</option>
										<option value="15">Educacion</option>
										<option value="16">Motor</option>
										<option value="17">Residuos</option>
										<option value="18">Otra</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_3_nombre_empresa" class="col-lg-8 control-label" >3.3 Nombre de la empresa</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" id="3_3_nombre_empresa" name="3_3_nombre_empresa">
								</div>
							</div>
							<div class="form-group">
								<label for="3_4" class="col-lg-8 control-label" >3.4 Tipo de contrato</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_4" name="3_4">
										<option value="0">Seleccione una opción</option>
										<option value="1">Permanente</option>
										<option value="2">Temporal</option>
										<option value="3">Independiente</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_5_cantidad_dinero_minima" class="col-lg-8 control-label" >3.5 ¿Cuál es la cantidad minima de dinero que te seria útil hoy?</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" id="3_5_cantidad_dinero_minima" name="3_5_cantidad_dinero_minima">
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="3_6_mes" class="col-lg-8 control-label" >3.6 Antigüedad en la empresa</label>
								<div class="col-lg-2">
									<select class="form-control" id="3_6_mes" name="3_6_mes">
										<option value="0">Seleccione una opción</option>
										<option value="1">1</option>
										<option value="1">2</option>
										<option value="1">3</option>
										<option value="1">4</option>
										<option value="1">5</option>
										<option value="1">6</option>
										<option value="1">7</option>
										<option value="1">8</option>
										<option value="1">9</option>
										<option value="1">10</option>
										<option value="1">11</option>
										<option value="1">12</option>
									</select>
								</div>
								<div class="col-lg-2">
									<select class="form-control" id="3_6_year" name="3_6_year">
										<option value="0">Seleccione una opción</option>
										<?php
										$year = date('Y');
										for ($i=$year; $i < $year ; $i--) {
											?>
											<option value="<?= $i ?>"><?= $i ?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_7" class="col-lg-8 control-label" >3.7 Periodo de pago</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_7" name="3_7">
										<option value="0">Seleccione una opción</option>
										<option value="1">Semanal</option>
										<option value="2">Quincenal</option>
										<option value="3">Mensual</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_8" class="col-lg-8 control-label" >3.8 Tamaño de la empresa</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_8" name="3_8">
										<option value="0">Seleccione una opción</option>
										<option value="1">1 a 5 Empleados</option>
										<option value="2">5 a 10 Empleados</option>
										<option value="3">10 a 15 Empleados</option>
										<option value="4">15 a 20 Empleados</option>
										<option value="5">20 a 25 Empleados</option>
										<option value="6">25 a 100 Empleados</option>
										<option value="7">Mas 100 Empleados</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_9_telefono_empresa" class="col-lg-8 control-label" >3.9 Telefono de la empresa</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" id="3_9_telefono_empresa" name="3_9_telefono_empresa">
								</div>
							</div>
						</div>
					</div>
					<h3 class="titulo-verde">Como son tus ingresos (Recuerda que no debes pedirlo si no puedes devolverlo)</h3>
					<br>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="3_10" class="col-lg-8 control-label" >3.10 Total de ingresos mensuales</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_10" name="3_10">
										<option value="0">Seleccione una opción</option>
										<option value="1">muy bajo	0 a 550</option>
										<option value="2">bajo	550 a 650</option>
										<option value="3">medio bajo	650 a 850</option>
										<option value="4">medio medio	850 a 1050</option>
										<option value="5">medio alto	1050 a 1500</option>
										<option value="6">alto	1500 a 2000</option>
										<option value="7">muy alto	>2000</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_11_gastos_mensuales" class="col-lg-8 control-label" >3.11 Total de gastos mensuales</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" id="3_11_gastos_mensuales" name="3_11_gastos_mensuales">
								</div>
							</div>
							<div class="form-group">
								<label for="3_12" class="col-lg-8 control-label" >3.12 ¿Para que vas a usar el préstamo?</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_12" name="3_12">
										<option value="0">Seleccione una opción</option>
										<option value="1">Prefiero no contestar</option>
										<option value="2">Lo quiero para pasarlo bien (rumba)</option>
										<option value="3">Necesito arreglar mi vehiculo</option>
										<option value="4">Necesito para gastos de salud</option>
										<option value="5">Cuentas del veterinario</option>
										<option value="6">Lo necesito para comprar regalos</option>
										<option value="7">Necesito ayudar a alguien cercano</option>
										<option value="8">Gastos funerarios</option>
										<option value="9">Lo necesito por un cambio de empleo</option>
										<option value="10">Me quiero ir de vacaciones</option>
										<option value="11">Necesito pagar otro préstamo/s</option>
										<option value="12">Necesito pagar colegios/metriculas/estudios</option>
										<option value="13">Imprevistos de mis hijos (ropa, utiles..)</option>
										<option value="14">Pagar deudas vencidas</option>
										<option value="15">Lo necesito porque tengo lleno el cupo de mi tarjeta de credito</option>
										<option value="16">Pagar servicios, luz, agua, cable, etc..</option>
										<option value="17">Gasto inesperado</option>
										<option value="18">Compra semanal, super</option>
										<option value="19">Comprar ropa</option>
										<option value="20">Pagar alquiler</option>
										<option value="21">Otro, no mencionado</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_13" class="col-lg-8 control-label" >3.13 ¿Tienes disponible descuento directo?</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_13" name="3_13">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="3_14" class="col-lg-8 control-label" >3.14 ¿Tienes tarjeta de credito?</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_14" name="3_14">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div>
								<div class="form-group has-success">
									<label for="3_14_1" class="col-lg-8 control-label" >3.14.1 ¿Cuántas tienes?</label>
									<div class="col-lg-4">
										<select class="form-control" id="3_14_1" name="3_14_1">
											<option value="0">Seleccione una opción</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8 o más</option>
										</select>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label for="3_15" class="col-lg-8 control-label" >3.15 ¿Tienes deudas?</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_15" name="3_15">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div>
								<div class="form-group has-success">
									<label for="3_15_1_deudas_al_mes" class="col-lg-8 control-label" >3.15.1 ¿Total de deudas por mes?</label>
									<div class="col-lg-4">
										<input type="text" class="form-control" id="3_15_1_deudas_al_mes" name="3_15_1_deudas_al_mes">
									</div>
								</div>
								<div class="form-group">
									<label for="3_15_2" class="col-lg-8 control-label" >3.15.2 ¿Tienes deudas vencidas?</label>
									<div class="col-lg-4">
										<select class="form-control" id="3_15_2" name="3_15_2">
											<option value="0">Seleccione una opción</option>
											<option value="1">Sin deudas vencidas</option>
											<option value="2">Servicios publicos</option>
											<option value="3">Banco</option>
											<option value="4">Celular</option>
											<option value="5">Carro</option>
											<option value="6">Otras</option>
											<option value="7">Estoy en APC</option>
										</select>
									</div>
								</div>
								<div class="form-group">
									<label for="3_15_3" class="col-lg-8 control-label" >3.15.3 ¿Qué otros créditos tienes actuamente?</label>
									<div class="col-lg-4">
										<select class="form-control" id="3_15_3" name="3_15_3">
											<option value="0">Seleccione una opción</option>
											<option value="1">Sin deudas</option>
											<option value="2">Hipoteca</option>
											<option value="3">Hipoteca y vehiculo</option>
											<option value="4">Hipoteca, vehiculo y otra</option>
											<option value="5">Credito personal</option>
											<option value="6">No tengo historial credito</option>
										</select>
									</div>
								</div>
							</div>
						</div>
					</div>

					<h3 class="titulo-verde">Que piensas sobre estas cuestiones...</h3>
					<br>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">
								<label for="3_16" class="col-lg-8 control-label" >3.16 Solo me callo ante personas mayores, con mas experiencia, edad o jerarquia</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_16" name="3_16">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">Termino medio</option>
										<option value="3">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_17" class="col-lg-8 control-label" >3.17 Mejor un trabajo inestable con salario alto que uno estable con salario bajo</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_17" name="3_17">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No estoy seguro</option>
										<option value="3">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_18" class="col-lg-8 control-label" >3.18 Puedo cambiar viejos habitos sin dificultad y sin volver a ellos</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_18" name="3_18">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">Termino medio</option>
										<option value="3">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_19" class="col-lg-8 control-label" >3.19 Si cometo una falta social desagradable, puedo olvidarme pronto</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_19" name="3_19">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_20" class="col-lg-8 control-label" >3.20 Seria mejor que las vaciones fuesen largas y obligatorias para todas las personas</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_20" name="3_20">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No estoy seguro</option>
										<option value="3">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_21" class="col-lg-8 control-label" >3.21 Me gusta un trabajo que requiera mas capacidad de atención y exactitud</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_21" name="3_21">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">Termino medio</option>
										<option value="3">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_22" class="col-lg-8 control-label" >3.22 Temo algún castigo incluso cuando no he hecho nada malo</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_22" name="3_22">
										<option value="0">Seleccione una opción</option>
										<option value="1">A menudo</option>
										<option value="2">Ocasionalmete</option>
										<option value="3">Nunca</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="3_23" class="col-lg-8 control-label" >3.23 Creo firmemente que tal vez el jefe no tenga razón siempre, pero siempre tiene</label>
								<div class="col-lg-4">
									<select class="form-control" id="3_23" name="3_23">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No estoy seguro</option>
										<option value="3">No</option>
									</select>
								</div>
							</div>
							<div class="form-group text-center">
								<button type="button" class="btn btn-default prev-step">Atrás</button>
								<button type="button" class="btn btn-primary btn-solicitar btn-info-full next-step">Guardar y continuar</button>
							</div>
						</div>
					</div>
				</div>
				<div class="tab-pane" role="tabpanel" id="step4">
					<h2 class="titulo-verde">4. RESIDENCIA</h2>
					<h3 class="titulo-verde">¿Cúentanos donde vives actualmente?...</h3>
					<br>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="4_1" class="col-lg-8 control-label" >4.1 Povincia</label>
								<div class="col-lg-4">
									<select class="form-control" id="4_1" name="4_1">
										<option value="0">Seleccione una opción</option>
										<option value="Panama">Panama</option>
										<option value="Panama_Oeste">Panama_Oeste</option>
										<option value="Chiriqui">Chiriqui</option>
										<option value="Coclé">Coclé</option>
										<option value="Veraguas">Veraguas</option>
										<option value="Herrera">Herrera</option>
										<option value="Los_Santos">Los_Santos</option>
										<option value="Colon">Colon</option>
										<option value="Bocas_del_Toro">Bocas_del_Toro</option>
										<option value="Darién">Darién</option>
										<option value="Comarca_San_Blas">Comarca_San_Blas</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="4_2" class="col-lg-8 control-label" >4.2 Distrito</label>
								<div class="col-lg-4">
									<select class="form-control" id="4_2" name="4_2">
										<option value="0">Seleccione una opción</option>
										<option value="Ciudad_de_Panama">Ciudad_de_Panama</option>
										<option value="Balboa">Balboa</option>
										<option value="Chepo">Chepo</option>
										<option value="Chiman">Chiman</option>
										<option value="San_Miguelito">San_Miguelito</option>
										<option value="Toboga">Toboga</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="4_3" class="col-lg-8 control-label" >4.3 Corregimiento</label>
								<div class="col-lg-4">
									<select class="form-control" id="4_3" name="4_3">
										<option value="0">Seleccione una opción</option>
										<option value="0">C-PANAMA</option>
										<option value="_24_de_Diciembre">_24_de_Diciembre</option>
										<option value="Alcalde_Diaz">Alcalde_Diaz</option>
										<option value="Ancón">Ancón</option>
										<option value="Bella_vista">Bella_vista</option>
										<option value="Betania">Betania</option>
										<option value="Calidonia">Calidonia</option>
										<option value="Casco_Viejo">Casco_Viejo</option>_San_Felipe<
										<option value="Curundú">Curundú</option>
										<option value="El_Chorrillo">El_Chorrillo</option>
										<option value="Ernesto_Cordoba">Ernesto_Cordoba</option>
										<option value="Juan_Diaz">Juan_Diaz</option>
										<option value="Las_Cumbres">Las_Cumbres</option>
										<option value="Pacora">Pacora</option>
										<option value="Parque_Lefevre">Parque_Lefevre</option>
										<option value="Pueblo_Nuevo">Pueblo_Nuevo</option>
										<option value="Rio_Abajo">Rio_Abajo</option>
										<option value="San_Francisco">San_Francisco</option>
										<option value="San_Martin">San_Martin</option>
										<option value="Santa_Ana">Santa_Ana</option>
										<option value="Tocumen">Tocumen</option>
										<option value="Via_Tocumen">Via_Tocumen</option>
										<option value="Via_Transismica">Via_Transismica</option>
										<option value="0">C-BALBOA</option>
										<option value="Isla_Contadora">Isla_Contadora</option>
										<option value="Isla_Pedro_Gonzalez">Isla_Pedro_Gonzalez</option>
										<option value="Isla_Saboga">Isla_Saboga</option>
										<option value="Isla_Viveros">Isla_Viveros</option>
										<option value="La_Guinea">La_Guinea/Isal_Rey</option>
										<option value="La_Esmeralda">La_Esmeralda</option>
										<option value="San_Miguel">San_Miguel</option>
										<option value="0">C-CHEPO</option>
										<option value="Cañita">Cañita</option>
										<option value="Chepito">Chepito</option>
										<option value="Chepo">Chepo</option>
										<option value="El_llano">El_llano</option>
										<option value="Las_Margaritas">Las_Margaritas</option>
										<option value="Madungandi">Madungandi</option>
										<option value="Santa_Cruz_de_Chinina">Santa_Cruz_de_Chinina</option>
										<option value="Torti">Torti</option>
										<option value="0">C_CHIMAN</option>
										<option value="Brujas">Brujas</option>
										<option value="Chimán">Chimán</option>
										<option value="Gonzalo_Vásquez">Gonzalo_Vásquez</option>
										<option value="Pásiga">Pásiga</option>
										<option value="Union_Santeña">Union_Santeña</option>
										<option value="C-SANMIGUELITO">C-SANMIGUELITO</option>
										<option value="Amelia_Denis_de_Icaza">Amelia_Denis_de_Icaza</option>
										<option value="Arnulfo_Arias">Arnulfo_Arias</option>
										<option value="Belisario_Frias">Belisario_Frias</option>
										<option value="Belisario_Porras">Belisario_Porras</option>
										<option value="José_Domingo_Espinar">José_Domingo_Espinar</option>
										<option value="Mateo_Iturralde">Mateo_Iturralde</option>
										<option value="Omar_Torrijos">Omar_Torrijos</option>
										<option value="Rufina_Alfaro">Rufina_Alfaro</option>
										<option value="Victoriano_Lorenzo">Victoriano_Lorenzo</option>
										<option value="0">C-TOBOGA</option>
										<option value="Otoque_Oriente">Otoque_Oriente</option>
										<option value="Otoque_Occidente">Otoque_Occidente</option>
										<option value="Taboga">Taboga</option>
										<option value="Chilibre">Chilibre</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="4_4" class="col-lg-8 control-label" >4.4 Sector</label>
								<div class="col-lg-4">
									<select class="form-control" id="4_4" name="4_4">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="4_5" class="col-lg-8 control-label" >4.5 Barrio</label>
								<div class="col-lg-4">
									<select class="form-control" id="4_5" name="4_5">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="4_6" class="col-lg-8 control-label" >4.6 Indicanos la refencias que tengas para ubicarla casa</label>
								<div class="col-lg-4">
									<select class="form-control" id="4_6" name="4_6">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="4_7_mes" class="col-lg-8 control-label" >4.7 ¿Desde cuándo vives ahí?</label>
								<div class="col-lg-2">
									<select class="form-control" id="4_7_mes" name="4_7_mes">
										<option value="0">Seleccione una opción</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6">6</option>
										<option value="7">7</option>
										<option value="8">8</option>
										<option value="9">9</option>
										<option value="10">10</option>
										<option value="11">11</option>
										<option value="12">12</option>
									</select>
								</div>
								<div class="col-lg-2">
									<select class="form-control" id="4_7_year" name="4_7_year">
										<option value="0">Seleccione una opción</option>
										<?php
										$year = date('Y')+1;
										for ($i=1940; $i < $year ; $i++) {
											?>
											<option value="<?= $i ?>"><?= $i ?></option>
											<?php
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="4_8" class="col-lg-8 control-label" >4.8 ¿Tienes teléfono fijo?</label>
								<div class="col-lg-4">
									<select class="form-control" id="4_8" name="4_8">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="4_9" class="col-lg-8 control-label" >4.9 Tipo de vivienda</label>
								<div class="col-lg-4">
									<select class="form-control" id="4_9" name="4_9">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="4_10" class="col-lg-8 control-label" >4.10 ¿Tienes intención de cambiar de vivienda en los próximos meses?</label>
								<div class="col-lg-4">
									<select class="form-control" id="4_10" name="4_10">
										<option value="0">Seleccione una opción</option>
										<option value="1">Rentada (vives solo)</option>
										<option value="1">Rentada (compartes)</option>
										<option value="1">Propietario con hipoteca</option>
										<option value="1">Propietario sin hipoteca</option>
										<option value="1">Alojado/a por los padres</option>
										<option value="1">Otro</option>
									</select>
								</div>
							</div>
						</div>
					</div>

					<h3 class="titulo-verde">Que piensas sobre estas cuestiones...</h3>
					<br>
					<div class="form-group">
						<label for="4_11" class="col-lg-8 control-label" >4.11 Me gustaria más gozar de la vida tranquilamente que ser admirado por mis logros</label>
						<div class="col-lg-4">
							<select class="form-control" id="4_11" name="4_11">
								<option value="0">Seleccione una opción</option>
								<option value="1">Si, me gustaria</option>
								<option value="2">No estoy seguro</option>
								<option value="3">No</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="4_12" class="col-lg-8 control-label" >4.12 Me parece más interesante ser</label>
						<div class="col-lg-4">
							<select class="form-control" id="4_12" name="4_12">
								<option value="0">Seleccione una opción</option>
								<option value="1">Artista</option>
								<option value="2">No estoy seguro</option>
								<option value="3">Funcionario</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="4_13" class="col-lg-8 control-label" >4.13 Pensando en las dificultades de mi trabajo</label>
						<div class="col-lg-4">
							<select class="form-control" id="4_13" name="4_13">
								<option value="0">Seleccione una opción</option>
								<option value="1">Intento organizarme antes de que aparezcan</option>
								<option value="2">Termino medio</option>
								<option value="3">Doy por supuesto que lo puedo dominar</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="4_14" class="col-lg-8 control-label" >4.14 Con el mismo horario y sueldo, seria más interesante ser</label>
						<div class="col-lg-4">
							<select class="form-control" id="4_14" name="4_14">
								<option value="0">Seleccione una opción</option>
								<option value="1">Cocinero de un buen restaurante</option>
								<option value="2">No estoy seguro, entre ambos</option>
								<option value="3">El que sirve las mesas</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="4_15" class="col-lg-8 control-label" >4.15 Cuando la gente autoritaria trata de domimarme, hago lo contrario de lo que quiere</label>
						<div class="col-lg-4">
							<select class="form-control" id="4_15" name="4_15">
								<option value="0">Seleccione una opción</option>
								<option value="1">Si, normalmente</option>
								<option value="2">Termino medio</option>
								<option value="3">No</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="4_16" class="col-lg-8 control-label" >4.16 Has tenido sueños repetitivos que no te han dejado dormir bien</label>
						<div class="col-lg-4">
							<select class="form-control" id="4_16" name="4_16">
								<option value="0">Seleccione una opción</option>
								<option value="1">A menudo</option>
								<option value="2">Ocasinalmente</option>
								<option value="3">Practicamente nunca</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="4_17" class="col-lg-8 control-label" >4.17 Siendo adolescente, cuando mi opinion era disitinta de la de mis padres, normalmente</label>
						<div class="col-lg-4">
							<select class="form-control" id="4_17" name="4_17">
								<option value="0">Seleccione una opción</option>
								<option value="1">Mantenia mi opinion</option>
								<option value="2">Termino medio</option>
								<option value="3">Aceptaba la autoridad</option>
							</select>
						</div>
					</div>

					<!--<h3>Introduce el codigo que te hemos enviado al celular</h3>-->
					<div class="form-group text-center">
						<button type="button" class="btn btn-default prev-step">Atrás</button>
						<button type="button" class="btn btn-primary btn-solicitar btn-info-full next-step">Guardar y continuar</button>
					</div>
				</div>
				<div class="tab-pane" role="tabpanel" id="complete">
					<h3 class="titulo-verde">5. ENVIO DE DINERO</h3>
					<h2 class="titulo-verde">¿A dónde te enviamos el dinero?</h2>
					<h2 class="titulo-verde text-right">Hora actual <?= date('H:i:s') ?></h2>
					<br>
					<div class="row">
						<div class="col-lg-6">
							<div class="form-group">
								<label for="5_1_numero_cuenta" class="col-lg-8 control-label" >5.1 Nº Cuenta</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" name="5_1_numero_cuenta" id="5_1_numero_cuenta" >
								</div>
							</div>
							<div class="form-group">
								<label for="5_1_1_nombre_cuenta" class="col-lg-8 control-label" >5.1.1 Nombre de la cuenta</label>
								<div class="col-lg-4">
									<input type="text" class="form-control" name="5_1_1_nombre_cuenta" id="5_1_1_nombre_cuenta" >
								</div>
							</div>
							<div class="form-group">
								<label for="5_2" class="col-lg-8 control-label" >5.2 Entidad bancaria</label>
								<div class="col-lg-4">
									<select class="form-control" id="5_2" name="5_2">
										<option value="0">Seleccione una opción</option>
										<option value="1">Allbank</option>
										<option value="2">Bac Internacional Bank</option>
										<option value="3">BBP Bank SA</option>
										<option value="4">BCT Bank</option>
										<option value="5">BI-Bank</option>
										<option value="6">Banco Aliado</option>
										<option value="7">Banco Azteca</option>
										<option value="8">Banco Delta</option>
										<option value="9">Banco Ficohsa</option>
										<option value="10">Banco General SA</option>
										<option value="11">Bicsa</option>
										<option value="12">Banco Lafise Panamá</option>
										<option value="13">Banco Nacional de Panamá</option>
										<option value="14">Banco Pichincha</option>
										<option value="15">Bancolombia sucursal Panamá</option>
										<option value="16">Banesco SA</option>
										<option value="17">Banisi SA</option>
										<option value="18">Banitmo SA</option>
										<option value="19">Banvivienda</option>
										<option value="20">CACECHI</option>
										<option value="21">COEDUCO</option>
										<option value="22">COOESAN</option>
										<option value="23">COOPEDUC</option>
										<option value="24">COOPEVE</option>
										<option value="25">Caja de Ahorros</option>
										<option value="26">Canal Bank</option>
										<option value="27">Citibank</option>
										<option value="28">Cooperativa Cristobal</option>
										<option value="29">Cooperativa Profesionales </option>
										<option value="30">Cridicorp Bank</option>
										<option value="31">Davivienda</option>
										<option value="32">FPB Bank</option>
										<option value="33">Global Bank</option>
										<option value="34">MetroBank</option>
										<option value="35">MMG Bank</option>
										<option value="36">Mercantil Bank</option>
										<option value="37">Multibank,Inc.</option>
										<option value="38">Prival Bank</option>
										<option value="39">Produbank</option>
										<option value="40">Scotia Bank</option>
										<option value="41">ST George Bank</option>
										<option value="42">TowerBank</option>
										<option value="43">UniBank and Trust</option>
										<option value="100">Otro</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label for="5_3" class="col-lg-8 control-label" >5.3 Tipo de cuenta</label>
								<div class="col-lg-4">
									<select class="form-control" id="5_3" name="5_3">
										<option value="0">Seleccione una opción</option>
										<option value="1">Cuenta corriente</option>
										<option value="2">Cuenta ahorros</option>
										<option value="3">Tarjeta de credito</option>
									</select>
								</div>
							</div>
							<p>Si tu cuenta coincide con la de uno de nuestros bancos será más rápido el traspaso de fondos</p>
							<div class="form-group">
								<label for="bancos_propios" class="col-lg-8 control-label" ><b>COMPRUEBA EN LA LISTA:</b></label>
								<div class="col-lg-4">
									<select class="form-control" id="bancos_propios" name="bancos_propios">
										<option value="0">Ninguno</option>
										<option value="1">Multibank, Inc.</option>
										<option value="1">Banco General SA</option>
										<option value="1">Banitmo SA</option>
										<option value="1">Global Bank</option>
										<option value="1">Banco Nacional de Panama</option>
										<option value="1">Bac Internacional Bank</option>
										<option value="1">Caja de Ahorros</option>
										<option value="1">Banesco SA</option>
									</select>
								</div>
							</div>
							<p>Cierra el proceso ahora y recibe el dinero en: <span class="font-red">en 30 minutos en tu cuenta</span></p>

							<p>Si se termina el proceso <span class="font-red">después de las 12.00 PM</span>, el depósito será efectivo en la mañana del dia siguiente, <span class="font-red">si lo terminamos antes</span> será efectivo a lo largo del dia. Dependerá de su banco. En caso de no sea el mismo que el nuestro.</p>
						</div>
						<div class="col-lg-6">
							<div class="form-group">
								<label for="5_4" class="col-lg-8 control-label" >5.4 ¿Tienes tarjeta Crédito/débito?</label>
								<div class="col-lg-4">
									<select class="form-control" id="5_4" name="5_4">
										<option value="0">Seleccione una opción</option>
										<option value="1">Si</option>
										<option value="2">No</option>
									</select>
								</div>
							</div>
							<div>
								<div class="form-group has-success">
									<label for="5_4_1" class="col-lg-8 control-label" >5.4.1 ¿Cuántas tienes?</label>
									<div class="col-lg-4">
										<select class="form-control" id="5_4_1" name="5_4_1">
											<option value="0">Seleccione una opción</option>
											<option value="1">1</option>
											<option value="2">2</option>
											<option value="3">3</option>
											<option value="4">4</option>
											<option value="5">5</option>
											<option value="6">6</option>
											<option value="7">7</option>
											<option value="8">8</option>
											<option value="9">9</option>
											<option value="10">10 o más</option>
										</select>
									</div>
								</div>
								<div class="form-group has-success">
									<label for="5_4_2_numero_tarjeta" class="col-lg-12 control-label" >5.4.2 Indicanos  la tarjeta asociada a tu cuenta para pasarte el pago cuando corresponda que tengas que cancelar.</label>
									<div class="col-lg-8">
										<input type="text" class="form-control" name="5_4_2_numero_tarjeta" id="5_4_2_numero_tarjeta" placeholder="NUMEROS">
									</div>
									<div class="col-lg-4">
										<input type="text" class="form-control" name="ccv" id="ccv" placeholder="CCV">
									</div>
									<div class="col-lg-12">
										<input type="text" class="form-control" name="nombre_tarjeta" id="nombre_tarjeta" placeholder="Nombre que figura en la tarjeta">
									</div>
								</div>
							</div>
						</div>
					</div>
					<?php
					/*
					Falta hacer foto y enviarla
					Sube a la plataforma tus documentos (desde celular o computador)

					5.5	Para confirmar tu identidad:
					Tomate una foto con la camara del celular o computador  para confirmar que eres tú. Es por tu seguridad												Iniciar camara

					5.6	Foto de la cedula (que se pueda leer bien)												Iniciar camara/subir imagen

					5.7	Ficha del seguro social												Iniciar camara/subir imagen

					5.8	Ultimos (2) dos talonarios: 												Iniciar camara/subir imagen

					Iniciar camara/subir imagen

					5.9	Recibo de luz, agua o telefono												Iniciar camara/subir imagen

					* Si lo prefieres puedes enviar los documentos por Whassapp
					*/
					?>

					<h2 class="titulo-verde">Resumen de tu préstamo... Confirmalo y solicita el dinero!!</h2>
					<br>
					<div class="form-group">
						<label for="prestamo_solicitado" class="col-lg-6 control-label" >Préstamo solicitado</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="prestamo_solicitado" required>
						</div>
					</div>
					<div class="form-group">
						<label for="interes" class="col-lg-6 control-label" >Interés</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="interes" placeholder="Jose" >
						</div>
					</div>
					<div class="form-group">
						<label for="administracion_legal" class="col-lg-6 control-label">Administracion legal</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="administracion_legal" placeholder="Rios">
						</div>
					</div>
					<div class="form-group">
						<label for="manejo_cuenta" class="col-lg-6 control-label" >Manejo cuenta</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="manejo_cuenta" placeholder="Laez">
						</div>
					</div>
					<div class="form-group">
						<label for="total_devolver" class="col-lg-6 control-label" >Total a devolver</label>
						<div class="col-lg-6">
							<input type="text" class="form-control" id="total_devolver" placeholder="Laez">
						</div>
					</div>
					<p>Asegúrate que tienes bien el correo electronico, porque te enviáremos la respuesta al correo</p>
					<div>
						<div class="form-group has-success">
							<label for="total_aplazado_quincenas" class="col-lg-6 control-label" >Total aplazado en</label>
							<div class="col-lg-6">
								<input type="text" class="form-control" id="total_aplazado_quincenas" placeholder="Laez">
							</div>
						</div>
						<div class="form-group has-success">
							<label for="total_aplazado_dias" class="col-lg-6 control-label" >Total de días</label>
							<div class="col-lg-6">
								<input type="text" class="form-control" id="total_aplazado_dias" placeholder="Laez">
							</div>
						</div>
						<div class="form-group has-success">
							<label for="fecha_finalizacion" class="col-lg-6 control-label" >Finalizas el</label>
							<div class="col-lg-6">
								<input type="text" class="form-control" id="fecha_finalizacion" placeholder="Laez">
							</div>
						</div>
						<div class="form-group has-success">
							<label for="pagas_en_quincenas" class="col-lg-6 control-label" >Pagas en</label>
							<div class="col-lg-6">
								<input type="text" class="form-control" id="pagas_en_quincenas" placeholder="Laez">
							</div>
						</div>

					</div>

					<div class="form-group text-center">
						<input type="submit" class="btn btn-primary btn-solicitar btn-info-full" value="SOLICITAR">
					</div>

					<h4 class="titulo-verde">***********Recuerda que Clicredit.com no es financiacion a largo plazo, solo es para ayudarte puntualmente con un imprevisto************</h4>
				</div>
				<div class="clearfix"></div>
			</div>
		</form>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		//Initialize tooltips
		//$('.nav-tabs > li a[title]').tooltip();

		//Wizard
		$('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
			var $target = $(e.target);
			if ($target.parent().hasClass('disabled')) {
				return false;
			}
		});

		$(".next-step").click(function (e) {
			$("html, body").animate({ scrollTop:0 }, 200);
			var $active = $('.wizard .nav-tabs li.active');
			$active.next().removeClass('disabled');
			nextTab($active);

		});
		$(".prev-step").click(function (e) {
			var $active = $('.wizard .nav-tabs li.active');
			prevTab($active);
		});
	});

	function nextTab(elem) {
		$(elem).next().find('a[data-toggle="tab"]').click();
	}
	function prevTab(elem) {
		$(elem).prev().find('a[data-toggle="tab"]').click();
	}

	function login(){
		$.ajax({
			type:	'POST',
			url:	'<?= base_url() ?>acceso/entrar-clicredit-ajax',
			data:	'email='+$('#email_login').val()+'&pass='+$('#pass_login').val(),
			success: function(result){
				if(result == '1'){
					location.reload();
				}else{
					$('#info').html('<div class="alert alert-danger"><strong>'+result+'</strong></div>');
				}
			}
		});
	}

	var is_login = 0;
	var validate = 1;
	function comprobarForm(){
		$('#info').html();
		<?php
		if(! $this->session->userdata('type') ){
			?>
			is_login = 1;
			<?php
		}
		?>
		if(is_login == 0){
			console.log('haciendo_login');
			login();
		}else{
			console.log('ya estamos logueados')
		}


		$(document).ready(function(){

			if($('#email').val() != $('#email_check').val() ){
				$('#info').html('<div class="alert alert-danger"><strong>El email no coincide</strong></div>');
				validate = 0;
			}

			$("#solicitar").find(':input').each(function() {
				var elemento = this;
				if(elemento.value == 0){
					console.log($('#'+elemento.id).val());
				}
			});
		});

	}
</script>
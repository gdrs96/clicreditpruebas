<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container font-blue">
	<h1 class="text-center mt-5 mb-5">El proceso de solicitud del préstamo<br> se ha completado con <b>ÉXITO</b></h1>
	<div class="row justify-content-center mb-2">
		<div class="col-lg-4 borderrgt-blue d-flex align-items-center">
			<p class="text-right">En breve recibirá un email a la dirección <b><?= $this->session->userdata('email') ?></b> que nos has indicado con la respuesta y las intrucciones</p>
		</div>
		<div class="col-lg-4">
			<p>Si el email está mal escrito, puede cambiarlo <b class="pointer" data-toggle="modal" data-target="#nuevoEmailModal">AQUÍ</b>, o a través de su panel de usuario para recibir su respuesta.</p>
			<p>Si no lo recibe en un máximo de <b>5 minutos</b>, contáctanos <b>en 260- 8951</b> o escríbenos a <b><a href="mailto:ayuda@pa.clicredit.com" class="font-blue">ayuda@pa.clicredit.com</a></b></p>
		</div>
	</div>
  <div class="row justify-content-center mb-5">
    <a href="<?= base_url(); ?>" class="btn btn-mi-cuenta mx-3" style="width: 140px">Ir a Inicio</a>
    <a href="<?= base_url(); ?>cuenta" class="btn btn-mi-cuenta mx-3" style="width: 140px; background-color: #5EAC25">Ir a Mi Cuenta</a>
  </div>
</div>
<div class="modal fade font-blue" tabindex="-1" role="dialog" id="nuevoEmailModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Corrección de Correo Electrónico</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
			<form id="nuevoEmailForm">
				<div class="modal-body">
					<label for="nuevoEmail">Ingrese su nuevo correo electrónico</label>
					<input data-parsley-type="email" class="form-control" id="nuevo_email" name="nuevo_email" placeholder="email" data-parsley-required>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="send">Reenviar mensaje</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				</div>
			</form>
    </div>
	</div>
</div>
<link rel="stylesheet" type="text/css" href="<?= base_url() ?>css/sweetalert.css">
<script src="<?= base_url() ?>js/sweetalert.min.js" type="text/javascript"></script>
<script src="<?= base_url() ?>js/parsley/parsley.js" type="text/javascript"></script>

<script>
	$('#send').click(function() {
		const validate = $('#nuevoEmailForm').parsley().isValid();
		if(validate) {
			enviarForm();
		}
	});
	function enviarForm(){
	/*	$("#nuevoEmailForm").attr('action','<?= base_url() ?>solicitud/cambiarEmail');
		setTimeout(500,$("#nuevoEmailForm").submit());*/
      $.ajax({
            url: '<?= base_url() ?>solicitud/cambiarEmail',
            type: "POST",
            dataType : 'json',
            data: $('#nuevoEmailForm').serialize(),
 
            success: function (data) {
            //	console.log(data.status);
             if(data.status==true){
               swal({
                title: "Se ha actualizado su correo con éxito",
                text:  "",
                type: "success",
                timer: 3000
            }, function () {
                location.reload(true);
              });
               
             }
               else{

                 swal({
                  title: "Ya existe un usuario registrado con este correo",
                  text:  "",
                  type: "warning"
            });

               }
            },
             error: function (data)
             {
                 swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
             }
           
        });
	}
</script>

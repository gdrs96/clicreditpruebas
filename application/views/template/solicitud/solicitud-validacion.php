<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="content">
	<div class="container font-blue py-5 text-center">
		<div><img src="<?= base_url() ?>img/alerta-icono.svg"></div>
		<h1>Hola <b><?= $this->session->userdata('nicename') ?></b></h1>
		<h2>Actualmente mantiene un préstamo <b style="color:#<?= $estado_prestamo->color ?>"><?= $estado_prestamo->nombre ?></b></h2>
		<h2 class="pb-5"><b>SÓLO SE PUEDE DISPONER DE UN PRÉSTAMO AL MISMO TIEMPO</b></h2>
		
        <div style="margin-left: 0.9rem"><a href="<?= base_url() ?>" class="btn btn-azul">Ir a inicio</a>     <a href="<?= base_url() ?>cuenta" class="btn btn btn-verde">Ver estado</a></div>

		<h5 style="margin-top: 6rem">Si su situación es diferente, favor contáctenos a <a href="mailto:ayuda@pa.clicredit.com"><b class="font-blue">ayuda@pa.clicredit.com</b></a></h5>
		<h5 class="text-center">o llamándonos al <a href="tel:2608951"><b class="font-blue">teléfono 260-8951</b></a></h5>
	</div>
</div>
<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<link href="<?= base_url().last_version('css/view/template/solicitud/wizard.css') ?>" rel="stylesheet">
<div class="wizard d-none d-lg-block mt-2 mb-5 mx-auto">
  <div class="wizard-inner">
    <div class="connecting-line"></div>
    <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="<?php if($nivel=='paso-1'): ?>active <?php else: ?> disabled<?php endif ?>">
        <a href="#" aria-controls="step1" title="1">
          <div class="round-tab">
            <h2>1</h2>
            <p class="font-12"><b>PERSONAL</b></p>
          </div>
        </a>
      </li>
      <li role="presentation" class="<?php if($nivel=='paso-2'): ?>active <?php else: ?> disabled<?php endif ?>">
        <a href="#" aria-controls="step2" title="2">
          <div class="round-tab">
            <h2>2</h2>
            <p class="font-12"><b>SITUACIÓN</b></p>
          </div>
        </a>
      </li>
      <li role="presentation" class="<?php if($nivel=='paso-3'): ?>active <?php else: ?> disabled<?php endif ?>">
        <a href="#" aria-controls="step3" title="3">
          <div class="round-tab">
            <h2>3</h2>
            <p class="font-12"><b>TRABAJO</b></p>
          </div>
        </a>
      </li>
      <li role="presentation" class="<?php if($nivel=='paso-4'): ?>active <?php else: ?> disabled<?php endif ?>">
        <a href="#" aria-controls="step4" title="4">
          <div class="round-tab">
            <h2>4</h2>
            <p class="font-12"><b>CASA</b></p>

          </div>
        </a>
      </li>
      <li role="presentation" class="<?php if($nivel=='paso-5'): ?>active <?php else: ?> disabled<?php endif ?>">
        <a href="#" aria-controls="complete" title="Complete">
          <div class="round-tab">
            <h2>5</h2>
            <p class="font-12"><b>HISTORIAL</b></p>
          </div>
        </a>
      </li>
    </ul>
  </div>
</div>
<div class="text-center font-blue mb-5">
  <h1 class="mb-0">Solicitud de préstamo <b>Clicredit.com</b></h1>
  <p class="mb-0">Los campos con el asterisco (<span class="font-red">*</span>) son obligatorios</p>
  <?php if($this->session->userdata('msg_val_max')) : ?>
		<div class="text-center msg_validacion">* <?= $this->session->userdata('msg_val_max') ?></div>
	<?php endif; ?>
</div>
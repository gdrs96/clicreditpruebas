console.log("paso");
var tableCoeficientes;
 tableCoeficientes = $('#table-coeficientes-inferencia').DataTable({
               processing: true,
               serverSide: false,
               responsive:true,
               lengthChange: false,
               bInfo: false,
               order:[[0,"desc"]],
              // ordering: true,
               iDisplayLength: 10,
              
                 language: {
                  zeroRecords: "No hay datos para mostrar",
                    search: "Buscar:",
                        paginate: {
                        next:       "Siguiente",
                        previous:   "Anterior"
                       },
                 },

       ajax:{
        url: urlCoeficientesInferencia,
        type:'post',
        data: function ( d ) {
           //return  $.extend(d, myData);
        }
    } ,
   
    columns: [
       
        {data:'id'},
        {data:'ultima_actualizacion'},
        {data:'description'},
        {data:'coeficiente'},
        {data:'observacion'},
        {data: 'status'}
        ],

     "columnDefs": [
        {
            "targets": [6],
            //"data": 'status',
            "defaultContent":'<a class="btn btn-green" id="editar_coeficiente"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar"></i></a>'
           /*render : function (data, type, row) {
            switch(data) {
               case '0' : return '<a class="btn btn-green" id="editar_perfil"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar"></i></a><a class="btn btn-red mx-1" id="eliminar_perfil"><i class="fa fa-times-circle"aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Eliminar perfil"></i></a>'; break;
               case '1' : return '<a class="btn btn-green disabled" id="editar_perfil"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar"></i></a><a class="btn btn-red mx-1 disabled" id="eliminar_perfil"><i class="fa fa-times-circle"aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Eliminar perfil"></i></a>'; break;
               default  : return data;
            }
          }*/
          
        },
        {
            "targets": [5],
            "data": 'status',
            render : function (data, type, row) {
            switch(data) {
               case '0' : return '<span class="badge badge-success">Activo</span>'; break;
               case '1' : return '<span class="badge badge-danger">Inactivo</span>'; break;
               default  : return data;
            }
          }
          
        }
    ],

      "order": [[ 0, "asc" ]]

});


function agregarCoeficienteInf()
{
    save_method = 'add';
    $('#form_coeficiente')[0].reset(); // resetea el formulario modal

    $('#modal_form').modal('show'); // muestra el formulario modal

    $('.modal-title').text('Código de inferencia'); // Agrega un titulo al formulario modal
    var cantidad_coeficiente = $('[name="cantidad_coeficiente"]').val();
    $('#indicador').text(parseInt(cantidad_coeficiente)+1);
   
   
   
}

function guardar_coeficiente(){
   
   var validate = $('#form_coeficiente').parsley().isValid();
   console.log(validate);
   if(validate){
   if(validarCoeficiente()){
   $('#btnSave').text('Guardando...'); //cambia el texto del boton
   $('#btnSave').attr('disabled',true); 
    var urlData;
 
    if(save_method == 'add') {
        urlData = urlCrearCoeficienteInf;
    } else {
        urlData = urlActualizarCoeficienteInf;
    }

    $.ajax({
        url :urlData,
        type: "POST",
        data: $('#form_coeficiente').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          
           if(data.status==true) //si la peticion retorna true ocultamos el modal y refrescamos el datatable
            {
                $('#modal_form').modal('hide');
                reload_table();
             
            }else{
                swal({
                  title: "Debe rellenar uno o mas campos",
                  text:  "",
                  type: "warning"
            });
            }
          
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
        }
    });
      }
    }
}


function validacionCoeficienteInput(){
  var coeficiente =  $('#coeficiente').val();
  console.log(coeficiente);
  if(coeficiente>1){
     $('#coeficiente').addClass('parsley-error');
     $('#message').text("Debe seleccionar un valor entre 0 y 1");
  }else{
    $('#coeficiente').removeClass('parsley-error');
    $('#message').text("");
  }
}

 function validarCoeficiente(){
  var coeficiente =  $('#coeficiente').val();
  if(coeficiente>1){
      swal({
        title: "El campo coeficiente debe contener valores entre 0 y 1",
        text:  "",
        type: "error"
    });
       return false;
     }
  else{
    return true;
  }
 }

function reload_table()
{
     tableCoeficientes.ajax.reload(null,false); 
}


$('#table-coeficientes-inferencia').on( 'click', '#editar_coeficiente', function () {
        var data = tableCoeficientes.row( $(this).parents('tr') ).data();
            $('#id_coeficiente').val(data.id);
            $('#indicador').text(data.id);
            $('#etiqueta').val(data.description); //descripcion 
            $('#coeficiente').val(data.coeficiente); //valor
            $('#observacion').val(data.observacion); //observacion
            $('#modal_form').modal('show'); // desplegamos el formulario modal
            $('.modal-title').text('Código de inferencia'); // Agrega un titulo al formulario modal
            save_method='edit';
    });



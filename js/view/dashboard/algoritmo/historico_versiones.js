console.log("paso");
var tableVersiones;
 tableVersiones = $('#table-versiones').DataTable({
               processing: true,
               serverSide: false,
               responsive:true,
               lengthChange: false,
               bInfo: false,
               ordering: true,
               iDisplayLength: 10,
               bFilter:false,
               bSearch:false,
              
                 language: {
                  zeroRecords: "No hay datos para mostrar",
                    search: "Buscar:",
                        paginate: {
                        next:       "Siguiente",
                        previous:   "Anterior"
                       },
                 },
       ajax:{
        url: urlVersionesAlgoritmo,
        dataType:'json',
        type:'post',
        //cache: false,
        data: function ( d ) {
           //return  $.extend(d, myData);
        }
    },
   
    columns: [
        {data:'codigo'},
        {data:'fecha_inicio'},
        {data:'fecha_inactividad'},
        {data:'version_variables'},
        {data:'nombre_version'},
        {data:'actions'},
        {data:'id'}
        ],

     "columnDefs": [
        {
                targets: [5],
                defaultContent: '<input type="radio" id="version_seleccionada" name="version_seleccionada" value="">'
            
          
        },
        {
            targets: [6],
            visible: false
        }
    ],

    

});

 $('#table-versiones tbody').on( 'click', '#version_seleccionada', function () {
        var data = tableVersiones.row( $(this).parents('tr') ).data();
      
        console.log(data.id);
        
        
    });
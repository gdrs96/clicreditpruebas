console.log("paso");
 
 var tablePerfilCaptacion;
 tablePerfilCaptacion = $('#table-perfil-captacion').DataTable({
              processing: true,
               serverSide: false,
               responsive:true,
               lengthChange: false,
               bInfo: false,
               ordering: true,
               iDisplayLength: 10,
              
                 language: {
                  zeroRecords: "No hay datos para mostrar",
                    search: "Buscar:",
                        paginate: {
                        next:       "Siguiente",
                        previous:   "Anterior"
                       },
                 },
       

       ajax:{
        url: urlPerfilesCaptacion,
        type:'post',
        data: function ( d ) {
           //return  $.extend(d, myData);
        }
    } ,
   
    columns: [
       
        {data:'id'},
        {data:'ultima_actualizacion'},
        {data:'descripcion'},
        {data:'valor'},
        {data:'observacion'},
        {data:'name'},
        ],

     "columnDefs": [
        {
            "targets": [6],
            "data": 'status',
           // "defaultContent":'<a class="btn btn-green" id="editar_perfil"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar"></i></a><a class="btn btn-red mx-1" id="eliminar_documento"><i class="fa fa-times-circle"aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Eliminar perfil"></i></a>'
           render : function (data, type, row) {
            switch(data) {
               case '0' : return '<a class="btn btn-green" id="editar_perfil"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar"></i></a><a class="btn btn-red mx-1" id="eliminar_perfil"><i class="fa fa-times-circle"aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Eliminar perfil"></i></a>'; break;
               case '1' : return '<a class="btn btn-green disabled" id="editar_perfil"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar"></i></a><a class="btn btn-red mx-1 disabled" id="eliminar_perfil"><i class="fa fa-times-circle"aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Eliminar perfil"></i></a>'; break;
               default  : return data;
            }
          }
          
        }
    ],

      "order": [[ 0, "asc" ]]

});


 function agregarPerfil()
{
    save_method = 'add';
    $('#form_perfil')[0].reset(); // resetea el formulario modal

    $('#modal_form').modal('show'); // muestra el formulario modal

    $('.modal-title').text('Perfil de captación'); // Agrega un titulo al formulario modal
   
}

function guardar_perfil(){
    $('#form_perfil').parsley();
   var validate = $('#form_perfil').parsley().isValid();
   if(validate){
   $('#btnSave').text('Guardando...'); //cambia el texto del boton
   $('#btnSave').attr('disabled',true); 
    var urlData;
 
    if(save_method == 'add') {
        urlData = urlAgregarPerfilCaptacion;
    } else {
        urlData = urlActualizarPerfilCaptacion;
    }

    $.ajax({
        url :urlData,
        type: "POST",
        data: $('#form_perfil').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          
           if(data.status==true) //si la peticion retorna true ocultamos el modal y refrescamos el datatable
            {
                $('#modal_form').modal('hide');
                reload_table();
             
            }
            else if(data.status=="PERFIL EN USO"){
                
            swal({
                  title: "No se puede realizar esta operación",
                  text:  "El perfil seleccionado ya se encuentra en uso",
                  type: "warning"
            });
            }else{
                swal({
                  title: "Debe rellenar uno o mas campos",
                  text:  "",
                  type: "warning"
            });
            }
          
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
        }
    });

    }
}

function reload_table()
{
     tablePerfilCaptacion.ajax.reload(null,false); 
}


$('#table-perfil-captacion tbody').on( 'click', '#editar_perfil', function () {
        var data = tablePerfilCaptacion.row( $(this).parents('tr') ).data();
            $('#id_perfil').val(data.id);
            $('#descripcion').val(data.descripcion); //descripcion 
            $('#valor').val(data.valor); //valor
            $('#observacion').val(data.observacion); //observacion
            $('#modal_form').modal('show'); // desplegamos el formulario modal
            $('.modal-title').text('Perfil de captación'); // Agrega un titulo al formulario modal
            save_method='edit';
    });


$('#table-perfil-captacion tbody').on( 'click', '#eliminar_perfil', function () {
        var data = tablePerfilCaptacion.row( $(this).parents('tr') ).data();
         swal({
                title: "¿Desea eliminar este perfil de captación?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

             
                if (isConfirm) {
        $.ajax({
        url :urlEliminarPerfilCaptacion+'/'+data.id,
        dataType: "JSON",
        type: "POST",
        success: function(data)
        {
          
           if(data.status==true) //si la peticion retorna true ocultamos el modal y refrescamos el datatable
            {
               swal({
                  title: "Perfil eliminado",
                  text:  "",
                  type: "success"
            });
                $('#modal_form').modal('hide');
                reload_table();
             
            }
            else if(data.status=="PERFIL EN USO"){
                
            swal({
                  title: "No se puede realizar esta operación",
                  text:  "El perfil seleccionado ya se encuentra en uso",
                  type: "warning"
            });
            }
          
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
        }

    });
      }
      else {
             swal("Acción cancelada", "", "warning");
            }});
          
    });
var tableLogs;
$.fn.dataTable.moment('DD-MM-YY H:m:s');
 tableLogs = $('#table-logs').DataTable({

               processing: true,
               serverSide: false,
               responsive:true,
               lengthChange: false,
               bInfo: false,
               ordering: true,
               iDisplayLength: 30,
              
                 language: {
                  zeroRecords: "No hay datos para mostrar",
                    search: "Buscar:",
                        paginate: {
                        next:       "Siguiente",
                        previous:   "Anterior"
                       },
                 },
       ajax:{
        url: urlLogsSms,
        dataType:'json',
        type:'post',
        //cache: false,
        data: function ( d ) {
           //return  $.extend(d, myData);
        }
    },
   
    columns: [
        {data:'fecha'},
        {data:'terminal'},
        {data:'codigo_verificacion'},
        {data: null,
                   "render": function ( data, type, full, meta) {
                    return '<a href='+urlPrestamo+data.id_prestamo+' target="_blank" title="ir a préstamo">'+data.id_prestamo+'</a>';}},
        {data:'sms_id'},
        {data:'status'},
        {data:'actions'},
        {data:'id'},
        {data:'error_id'},
        {data:'error_msg'},
        ],

     "columnDefs": [
        {
            "targets": [5,8],
            "data": 'status',
          
           render : function (data, type, full, meta) {
            switch(data) {
               case 'ok' : return '<span data-toggle="tooltip"><img src='+routeImageOk+'  data-placement="bottom" title="Status: el mensaje ha sido entregado con éxito al destinatario"></img></span>'; break;
               case 'error' : return '<img src='+routeImageAlert+' data-toggle="tooltip" data-placement="bottom" title="error_id: '+full.error_id+' error_msg: '+full.error_msg+'"></img>'; break;
               case 'send_manual' : return '<img src='+routeImageAlert+' data-toggle="tooltip" data-placement="bottom" title="error_id: '+full.error_id+' error_msg: '+full.error_msg+'"></img> <img src='+routeImageHand+' data-toggle="tooltip" data-placement="bottom" title="Mensaje enviado manualmente"></img>'; break;
               default  : return data;
            }
          }
          
        },
         {
            "targets": [7,8,9],
            "visible": false
         },
        {
            "targets": [6],
            "defaultContent":'<a class="btn btn-green" id="editar_log"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar"></i></a>'
         
          
        }
    ],

      "order": [[ 7, "desc" ]]

});




 $('#table-logs tbody').on( 'click', '#editar_log', function () {
        var data = tableLogs.row( $(this).parents('tr') ).data();
        var $tr = $(this).parents('tr');
         $('#id_log').val(data.id);
         $('#terminal').text(data.terminal);
         $('#id_prestamo').text(data.id_prestamo);
         $('#codigo_verificacion').text(data.codigo_verificacion);
         if(data.status=="send_manual"){
           $('#sms_manual').prop('checked', true);
         }else{
           $('#sms_manual').prop('checked', false);
         }
            $('#modal_form').modal('show'); // desplegamos el formulario modal
            $('.modal-title').text('Obtener Código manualmente'); // Agrega un titulo al formulario modal
        
    });


 function activar_envio_manual(){
    $.ajax({
        url :urlActivarEnvioManual,
        type: "POST",
        data: $('#form_logs_sms').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          
           if(data.status==true) //si la peticion retorna true ocultamos el modal y refrescamos el datatable
            {
                $('#modal_form').modal('hide');
                reload_table();
            }else{
                $('#modal_form').modal('hide');
                reload_table();
            }
           
          
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
        }
    });
 }


function reload_table()
{
    tableLogs.ajax.reload(null,false); 
}
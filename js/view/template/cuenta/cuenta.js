//  Muestra la vista modal de la descripcion de los status del reporte de pago
$("#infoStatus").click(function() { 
$('#modal_infoEstatus').modal('show'); 
});
//Modal para editar
$('#editModal').on('show.bs.modal', function (event) {
  var button = $(event.relatedTarget) // Button that triggered the modal
  var field = button.data('field') // Extract info from data-* attributes
  var modal = $(this)
	modal.find('.modal-title').text('Editar tu ' + field);
	modal.find('.modal-body input').val('');
	if (field == 'contraseña') {
		modal.find('.modal-body label').text('Introduce tu nueva contraseña')
		modal.find('.modal-body input').attr('type', 'password')
		modal.find('.modal-body input').attr(`name`, 'nuevo_pass')
		modal.find('.modal-body input').removeAttr(`data-parsley-type`)
	} else if (field == 'email'){
		modal.find('.modal-body label').text('Introduce tu nuevo email')
		modal.find('.modal-body input').attr('type', 'text')
		modal.find('.modal-body input').attr(`data-parsley-type`, 'email')
		modal.find('.modal-body input').attr(`name`, 'nuevo_email')
	} else if(field == 'telefono'){
		modal.find('.modal-body label').text('Introduce tu nuevo teléfono')
		modal.find('.modal-body input').attr('type', 'text')
		modal.find('.modal-body input').attr(`name`, 'nuevo_telefono')
		modal.find('.modal-body input').removeAttr(`data-parsley-type`)
	}

})


$('#btnInfoDevolucion').on( 'click',  function () {
	$('#modalDevolucionPago').modal('hide');
	$('#infoDevolucion').modal('show');
});

$('#btn_devolver').on( 'click',  function () {
	$('#modalDevolucionPago').modal('show');
});
$('#modalRegresar').on ('click', function () {
	$('#infoDevolucion').modal('hide');
	$('#modalDevolucionPago').modal('show');
});

//  Muestra las quincenas asociadas a un prestamo a traves del datatable 
tableQuincenas = $('#table_quincenas').DataTable({
	"bPaginate": false,
	"bLengthChange": false,
	"bSearch": false,
	"bFilter": false,
	"bInfo": false,
	"bAutoWidth": false,
	"processing": true, 
	"serverSide": true,
	"language": {
		"zeroRecords": "No hay quincenas creadas"
	},
	"footerCallback": function ( row, data, start, end, display ) {
                     
                        var api = this.api();
                        var intVal = function ( i ) {
                           return typeof i === 'string' ?
                           i.replace(/[\$,]/g, '')*1 :
                           typeof i === 'number' ?
                           i : 0;
                         };

                        var total = api.column(2).data().reduce( function (a, b) {
                        
                         return intVal(a) + intVal(b);
                      }, 0 );
                     
                    
                      
                       $( api.column(2).footer() ).html('<span style="color:green;">'+ currencyFormat (total)+'<span>')
                      
                      


                    },			  
	ajax: {
		url: urlDetallePrestamo,
		type:'post'
	},
	columns: [
		{ 
			"data": null,
			"render": function ( data, type, full, meta) {
				return '<span class="badge badge-primary mr-2" style="font-size:1rem; background:#'+data.color+';">'+data.numero_quincena+'</span>'+'<span class="d-none d-lg-inline">Qnc.</span>';
			}
		},
		{
			"data": 'fecha_vencimiento',
			"render": function ( data, type, full, meta) {
				return '<span style="font-size: 1rem">'+data+'</span>'
			}
		},
		{
			"data": 'cuota',
			"render": function ( data, type, full, meta) {
				return '<span style="font-size: 1rem">'+data+'</span>'
			}
		},
		{
			"data": null,
			"render": function ( data, type, full, meta) {
				return '<b class="badge" style="color:#'+data.color+';  font-size:1rem;">'+data.nombre+'</b>';
			}
		},
	],
	"order": [[ 1, "desc" ]]
});
//Mejorar estilos de tabla
$('#table_quincenas_wrapper .row').first().remove();
$('#table_quincenas_wrapper .row .col-sm-12').addClass('px-0');

//Muestra los reporte de pagos asociados al prestamo
 
table = $('#tableReportePago').DataTable({
	"bPaginate": false,
	"bLengthChange": false,
	"bSearch": false,
	"bFilter": false,
	"bInfo": false,
	"bAutoWidth": false,
	"processing": true, 
	"serverSide": true,
	"language": {
		"zeroRecords": "No hay pagos reportados"
	},
	ajax: {
		url: urlReportePago,
		type: 'post'
	} ,   
	columns: [
		{"data":'action'},
		{
			"data":'fecha_subida_recibo',
			"render": function ( data, type, full, meta) {
				return '<span style="font-size: 1rem">'+data+'</span>'
			}
		},
		{
			"data":'fecha_efecto',
			"render": function ( data, type, full, meta) {
				if(data!=null){
				return '<span style="font-size: 1rem">'+data+'</span>'
			}else{
				return '<span style="font-size: 1rem">-</span>'
			}
			}
		},
		{
			"data":'importe',
			"render": function ( data, type, full, meta) {
				return '<span style="font-size: 1rem">'+data+'</span>'
			}
		},
		{"data": null,
		  "render": function ( data, type, full, meta) {
		   return '<b class="badge" style="color:#'+data.color+'; font-size: 1rem; white-space: normal">'+data.nombre+'</b>';}},
				  
		],

	 "columnDefs": [
		{
			"targets": [0],
			"data": null,
			"defaultContent": "<span class='enlace' style='font-size:1rem'><i class='fa fa-eye'></i></span>"
		}
	],
	  "order": [[ 1, "desc" ]]
});
//Mejorar estilos
$('#tableReportePago_wrapper .row').first().remove();
$('#tableReportePago_wrapper .row .col-sm-12').addClass('px-0');
//Desaparece columna F.subida en mobiles
function tableResponsive () {
	if ($(window).width() <= 438 ) {  
		$('table#tableReportePago thead tr th#no-mobile').hide();
		$('table#tableReportePago tbody tr td.sorting_1').hide();
	}else {
		$('table#tableReportePago thead tr th#no-mobile').show();
		$('table#tableReportePago tbody tr td.sorting_1').show();
	}
}
$(function(){
	tableResponsive();
	$(window).resize(tableResponsive);
})

 

/******************************************************************************
 función para visualizar el recibo de pago
******************************************************************************/
 $('#tableReportePago tbody').on( 'click', 'span', function () {
		var data = table.row( $(this).parents('tr') ).data();
		   var ruta = url+data.imagen_recibo;
		  $('#imagen').attr('src',ruta);

		  $('#modal_reportePago').modal('show');
	});


/************************************************************************************
 Muestra la tabla de la descripción de cada uno de los estados del reporte de pago
**************************************************************************************/
 var tableInfoStatus = $('#tableInfoStatus').DataTable({
				"bPaginate": false,
				"bLengthChange": false,
				"bSearch": false,
				"bFilter": false,
				"bInfo": false,
				"bAutoWidth": false,
				"processing": true, 
				"serverSide": true,


	   ajax:{
		url: urlStatusReportePago,
	   // headers:{'X-CSRF-TOKEN': token},
		type:'post',
	} ,
   
	columns: [
		 {"data": null,
		  "render": function ( data, type, full, meta) {
				return '<b class="badge" style="color:#'+data.color+'; font-size: 1rem; white-space: normal">'+data.nombre+'</b>';
			},
			sWidth: "15%"},
		  {"data": null,
		   "render": function ( data, type, full, meta) {
			return '<p class="text-status">'+data.descripcion+'<p>';}
		  	},
	  
		],

	 "columnDefs": [
		{
			  "width": "20%", "targets": 0 ,
		}
	],

	  "order": [[ 1, "desc" ]]

});
	   

/******************************************************************************
Refresca el datatable de los reportes de pagos
******************************************************************************/
function reload_table()
{
	table.ajax.reload(null,false); //reload datatable ajax 
}
function allowUploadImage(element) {
	element.change(function () {
		var allow = true;
		var input = $(this)[0];
		var span = $(this).next().find('span');
		var files = input.files;
		Object.keys(files).forEach(function(index) {
			var size = files[index].size;
			if(size > 4000000) allow = false;
		});
		if(!allow) {
			swal({
				title: "Esta imagen excede el peso máximo permitido.",
				text:  "Peso máximo de cada imagen: 4MB. \n\n Le recomendamos reducir el tamaño de las imágenes o enviarlas por el WhatsApp.",
				type: "warning"
			});
			$(this).val(null);
			span.text('Reporta tu recibo de pago');
		} else {
			reportarPago();
		}
	});
}
allowUploadImage($('#imagen_recibo'));
/******************************************************************************
 Función para reportar un pago
******************************************************************************/
	 function reportarPago(){
	 	jQuery.support.cors = true;

	 	 $.ajax({
			url: urlReportarPago,
			type: "POST",
			dataType : 'json',
			crossDomain: true,
			data: new FormData($("#upload_form")[0]),
			processData: false,
			contentType: false,
			cache: false,
			beforeSend:function() { 
				swal({
					title: 'Estamos procesando su información',
					imageUrl: url+'img/loading.gif',
					showConfirmButton: false
				});
			},
 
			success: function (data) {
			 if(data.status==true){
			   swal({
				title: "Su pago fue registrado con éxito",
				text: "En las próximas horas sera procesado!",
				confirmButtonColor: '#8CD4F5',
				type: "success"
			});
			   reload_table();
			   $('#imagen_recibo').val(null);
			   $('#imagen_recibo').next().find('span').text('Reporta tu recibo de pago');
			   }
			},
			 error: function (jqXHR, textStatus, errorThrown)
			 {
				console.log(jqXHR);
			 }
		   
		});
	 }

	 
$(document).ready(function(){

     calcularPrestamo();
    $('.dtpicker').datetimepicker();
    $('.select2').select2();
    $('.datepicker').datetimepicker({
       format: 'DD-MM-YYYY'
    });


  });

$('#ajustar_quincena').on( 'change',  function () {
  var ajuste  = $('#ajustar_quincena').is(':checked');
  if(ajuste==true){
      $('#ajustarQuincenas').val(1);
    }else if(ajuste==false){
        $('#ajustarQuincenas').val(0);
    }
        var data={'ajustar_quincena': $('#ajustarQuincenas').val(),'id_prestamo': id_prestamo};
        $.ajax({
        url : urlActualizarAjusteQuincena,
        type: "POST",
        data: data,
        dataType: "JSON",
        success: function(data)
        {
          
           if(data.status) 
            {
                calcularPrestamo();
                location.reload();
                //reload_tableQuincenas();
            }
          
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
           
 
        }
    });




        });



        $(document).ready(function(){

          var ajustar_quincena = $('#ajustar_quincena').val();
          if(ajustar_quincena=="1"){
            $('#ajustar_quincena').attr("checked",true);

          }else if(ajustar_quincena=="0"){
            $('#ajustar_quincena').attr("checked",false);
          }
        });


$('#editModal').on('show.bs.modal', function (event) {
  $('.input-group.date').datetimepicker({
       format: 'DD-MM-YYYY'
    });
  var button = $(event.relatedTarget); // Button that triggered the modal
  var field = button.data('whatever'); // Extract info from data-* attributes
  var nameField = button.data('field');
  var modal = $(this);
  modal.find('.modal-title').text('Editar ' + nameField);
  modal.find('.modal-body input').val('');
  if (field == 'cantidad') {
    modal.find('.modal-body label').text('Introduce la cantidad solicitada')
     modal.find('.modal-body input').datetimepicker({
       format: 'DD-MM-YYYY'})
    modal.find('.modal-body input').datetimepicker("destroy")
    modal.find('.modal-body input').attr('type', 'text')
    modal.find('.modal-body input').attr(`name`, 'cantidad')
    modal.find('.modal-body input').val($('#cantidad').val())
    modal.find('.modal-body input').removeAttr(`data-parsley-type`)
  } else if (field == 'fecha'){
     modal.find('.modal-body label').text('Introduce la fecha')
     modal.find('.modal-body input').datetimepicker({
       format: 'DD-MM-YYYY ',
       useCurrent: false,
       'minDate': new Date() 
     })
     modal.find('.modal-body input').attr('type', 'text')
     modal.find('.modal-body input').attr(`name`, 'fecha_deposito')
     modal.find('.modal-body input').val(convertDate($('#fecha_efecto').val()))
     modal.find('.modal-body input').removeAttr(`data-parsley-type`)
  } else if (field == 'cantidad_quincenas') {
    modal.find('.modal-body label').text('Introduce la cantidad de quincenas')
     modal.find('.modal-body input').datetimepicker({
       format: 'DD-MM-YYYY'
     })
    modal.find('.modal-body input').datetimepicker("destroy")
    modal.find('.modal-body input').attr('type', 'text')
    modal.find('.modal-body input').attr(`name`, 'tiempo')
     modal.find('.modal-body input').val($('#tiempo').val())
    modal.find('.modal-body input').removeAttr(`data-parsley-type`)
  }

});




function calcularPrestamo(){
      $('#solicitado').html( 'B./ ' + parseFloat($('#amount').val() ).toFixed(2));
      $('#pagas_en').html($('#duration').val() + ' Quincenas');
      var estado_prestamo = $('#prestamo_estado_id').val();
      var ajuste = $('#ajustarQuincenas').val();
      var tasa = parseFloat($('#tasa_interes').val());
      var ultima_actualizacion = new Date($('#ultima_actualizacion').val());
      var fecha_ultima_quincena = $('#fecha_finalizacion_prestamo').val();
      var fecha_efecto = new Date($('#fecha_efecto').val());
     
      if(ajuste==0){
        var coste = ((((parseInt($('#tiempo').val()) *15) * tasa) * parseFloat($('#cantidad').val()))).toFixed(2);
        var fFinal = sumarDias(fecha_efecto,$('#tiempo').val()*15);
        $('#total_dias').html($('#tiempo').val()*15 + ' Días');
        $('#fecha_final_prestamo').val(formatYYYYMMDD(fFinal));
        $('#fecha_finalizacion').html(formatDate2(fFinal));
        $('#interes').html('B./ ' + coste );
       
      }else if(ajuste==1){
        
        var quincenas = calcularQuincenas(fecha_efecto);
        var fFinal = new Date(quincenas[$('#tiempo').val() - 1 ]);
        var lapso = Math.ceil((fFinal - fecha_efecto)/(1000*60*60*24));
        var coste = (((parseInt(lapso) * parseFloat(tasa)) * parseFloat($('#cantidad').val()))).toFixed(2);
        $('#total_dias').html(lapso + ' Días');
        $('#fecha_finalizacion').html(formatDate2(fFinal));
        $('#interes').html('B./ ' + coste );
        $('#fecha_final_prestamo').val(formatYYYYMMDD(fFinal));
       
      }
       var total =  (parseFloat(coste) + parseFloat( $('#cantidad').val()) );
      if(prestamo_estado_id==8 || prestamo_estado_id==9 || prestamo_estado_id==10 || prestamo_estado_id==11 || prestamo_estado_id==12 || prestamo_estado_id==13){
        $('#total_prestamo').val(parseFloat(total).toFixed(2));
      }else{
        $('#total_prestamo').val($('#monto_total_prestamo').val());
       
      }
     
      $('#total').html('B./' + parseFloat(total).toFixed(2));
      $('#cuota').html( parseFloat(total / $('#duration').val() ).toFixed(2) + ' B./  Quincena' );
    }

  function calcularQuincenas(fecha) {
      var quincenas = [];
      var ultimoDia = new Date(fecha.getFullYear(), fecha.getMonth() + 1, 0);
      if(fecha.getDate() < 11) {
        quincenas.push(quincena(fecha, 0));
        quincenas.push(finDeMes(fecha, 0));
        quincenas.push(quincena(fecha, 1));
        quincenas.push(finDeMes(fecha, 1));
        quincenas.push(quincena(fecha, 2));
        quincenas.push(finDeMes(fecha, 2));
        quincenas.push(quincena(fecha, 3));
      } else if(fecha.getDate() < (ultimoDia.getDate()-4)) {
        quincenas.push(finDeMes(fecha, 0));
        quincenas.push(quincena(fecha, 1));
        quincenas.push(finDeMes(fecha, 1));
        quincenas.push(quincena(fecha, 2));
        quincenas.push(finDeMes(fecha, 2));
        quincenas.push(quincena(fecha, 3));
        quincenas.push(finDeMes(fecha, 3));
      } else {
        quincenas.push(quincena(fecha, 1));
        quincenas.push(finDeMes(fecha, 1));
        quincenas.push(quincena(fecha, 2));
        quincenas.push(finDeMes(fecha, 2));
        quincenas.push(quincena(fecha, 3));
        quincenas.push(finDeMes(fecha, 3));
        quincenas.push(quincena(fecha, 4));
      }
      return quincenas;
    }
    function quincena(fecha, numero) {
      return new Date(fecha.getFullYear(), fecha.getMonth() + numero, 15)
    }
    function finDeMes(fecha, numero) {
      return new Date(fecha.getFullYear(), fecha.getMonth() + numero + 1, 0)
    }

    function sumarDias(fecha, dias){
      fecha.setDate(fecha.getDate() + dias);
      return fecha;
    }


    function formatDate2(date) {
      var d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();
      var dias = new Array('Dom','Lun','Mar','Mié','Jue','Vie','Sáb')
      var dias = new Array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado')
      if (month.length < 2) month = '0' + month;
      switch(month){
        case '01':
        month = 'Enero';
        break;
        case '02':
        month = 'Febrero';
        break;
        case '03':
        month = 'Marzo';
        break;
        case '04':
        month = 'Abril';
        break;
        case '05':
        month = 'Mayo';
        break;
        case '06':
        month = 'Junio';
        break;
        case '07':
        month = 'Julio';
        break;
        case '08':
        month = 'Agosto';
        break;
        case '09':
        month = 'Septiembre';
        break;
        case '10':
        month = 'Octubre';
        break;
        case '11':
        month = 'Noviembre';
        break;
        case '12':
        month = 'Diciembre';
        break;
      }
      if (day.length < 2) day = '0' + day;

      //return [year, month, dias];
      return dias[date.getDay()] + ', ' +day+ ' de ' +month + ' de ' +year;
    }





$(document).ready(function(){
    $('#imagen_recibo').on('change',function(){
        var allow = true;
		var input = $(this)[0];
		var span = $(this).next().find('span');
		var files = input.files;
		Object.keys(files).forEach(function(index) {
			var size = files[index].size;
			if(size > 4000000) allow = false;
		});
		if(!allow) {
			swal({
				title: "Esta imagen excede el peso máximo permitido.",
				text:  "Peso máximo de cada imagen: 4MB. \n\n Le recomendamos reducir el tamaño de las imágenes o enviarlas por el WhatsApp.",
				type: "warning"
			});
			$(this).val(null);
			span.text('Reportar recibo de pago');
		} else {
            $('#upload_form').ajaxForm({
                //display the uploaded images
                //target:'#images_preview',

                /*beforeSubmit:function(e){
                    //$('#content').html('<div><img src=url/></div>');

                },*/
                beforeSend:function() { 
                    swal({
                        title: 'Estamos procesando su información',
                        imageUrl: url+'img/loading.gif',
                        showConfirmButton: false
                    });
                },
                success:function(data){
                data = JSON.parse(data);
                if(data.status==true){
                swal({
                    title: "Pago registrado con éxito",
                    text:  "",
                    type: "success"
                });
                reload_table();
                $('#imagen_recibo').val(null);
			   	$('#imagen_recibo').next().find('span').text('Reportar recibo de pago');
                }
                else{

                    swal({
                    title: "Formato de imagen invalido",
                    text:  "",
                    type: "warning"
                });

                }
                    
                },
                error:function(data){
                data = JSON.parse(data);
                alert(data);
                }
            }).submit();
        }
    });
});



 $(document).ready(function(){     
            
        var prestamo_estado_id =  $('#prestamo_estado_id').val();
        var id_prestamo_cliente = $('#id_prestamo_cliente').val();
        var estado_prestamo =  $('#estado_'+id_prestamo_cliente).val();
      
   /* if(estado_prestamo==0){
      $('#estado_'+id_prestamo_cliente).val(0);
      }else{*/
           $('#estado_'+id_prestamo_cliente).val(prestamo_estado_id);
        
     // }
    

});

/******************************************************************************
 Función encargada de desplegar el formulario modal para actualizar un pago
******************************************************************************/
function actualizarPago(){

 $('#formPago').parsley().validate();
 var validate = $('#formPago').parsley().isValid();
 
  if(validate){

    $('#btnSave').text('Guardando..'); //cambia el texto del botón guardar
    $('#btnSave').attr('disabled',true); //deshabilita el botón guardar
    $.ajax({
        url : urlActualizarPago,
        type: "POST",
        data: $('#formPago').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          
           if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_reportePago').modal('hide');
                 reload_table();
            }
          
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
    }
}

// definicion de variable para llamar a funcion editar o guardar quincena en el ajax
 var  save_method = null;

/******************************************************************************
 Función encargada de desplegar el formulario modal al seleccionar agregar quincena
******************************************************************************/
function agregarQuincena()
{


    save_method = 'add';
    $('#form')[0].reset(); // resetea el formulario modal
    $('[name="numero_quincena"]').attr('disabled',true);
    //var cantidad_quincena = $('[name="cantidad_quincenas"]').val();
    $('[name="numero_quincena"]').val(tableQuincenas.data().count()+1);
    calcularPrestamo();
    $('#modal_form').modal('show'); // muestra el formulario modal

    $('.modal-title').text('Agregar quincena'); // Agrega un titulo al formulario modal
    
}




/******************************************************************************
 Función encargada de agregar una quincena
******************************************************************************/
function guardar_quincena()
{

  $('#form').parsley().validate();
  var numero_quincena = $('#tiempo').val();
  var validate = $('#form').parsley().isValid();
  if(validate) {
      
    
  calcularPrestamo();
  $('#btnSave').text('Guardando...'); //cambia el texto del boton
    $('#btnSave').attr('disabled',true); 
   $('[name="numero_quincena"]').removeAttr('disabled');
    var urlData;
    if(save_method == 'add') {
        urlData = urlAgregarQuincena;
    } else {
        urlData = urlActualizarQuincena;
    }
    $.ajax({
        url :urlData,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
           if(data.status) //si la peticion retorna true ocultamos el modal y refrescamos el datatable
            {

                $('#modal_form').modal('hide');
                $('[name="numero_quincena"]').val("");
                if(data.numero_quincena==numero_quincena){
                  location.reload();
                }else{
                reload_tableQuincenas();
               }
             
            }
           if(data.status=="ERROR_OPERACION_ESTADO"){

               swal({
                  title: "No se puede realizar esta operación",
                  text:  "La fecha de vencimiento de la quincena aún no ha transcurrido",
                  type: "warning"
              });

            }
           if(data.status=="ERROR_OPERACION_QUINCENA"){

               swal({
                  title: "No se puede realizar esta operación",
                  text:  "El número de quincenas o el monto total del préstamo no coincide con lo solicitado",
                  type: "error"
              });

            }

            if(data.status=="ERROR_AJUSTE_FECHA"){
               swal({
                  title: "No se puede realizar esta operación",
                  text:  "La fecha de vencimiento ingresada no se encuentra dentro de las fechas establecidas en el préstamo",
                  type: "error"
              });
            }

               if(data.status=="ERROR_AJUSTE_QUINCENA"){
                swal({
                  title: "No se puede realizar esta operación",
                  text:  "La fecha de vencimiento ingresada no puede ser inferior a la quincena anterior",
                  type: "error"
              });
            }
          
          
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
             swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
 
        }
    });

 }
}

/******************************************************************************
 Muestra las quincenas asociadas a un prestamo a traves del datatable 
******************************************************************************/
calcularPrestamo();
var total_prestamo = $('#total_prestamo').val();
var cantidad_quincenas = parseInt($('#tiempo').val());
var cantidad_quincenas_acumuladas = parseInt($('#cantidad_quincenas').val());


var id_prestamo = $("#id_prestamo_cliente").val();
var tableQuincenas;
tableQuincenas = $('#table_quincenas').DataTable({
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bSearch": false,
                    "bFilter": false,
                    "bInfo": false,
                    "bAutoWidth": false,
                    "processing": true, 
                    "serverSide": true,
                    "language": {
                        "zeroRecords": "No hay quincenas creadas"
                     },
                    "footerCallback": function ( row, data, start, end, display ) {
                     
                        var api = this.api();
                        var intVal = function ( i ) {
                           return typeof i === 'string' ?
                           i.replace(/[\$,]/g, '')*1 :
                           typeof i === 'number' ?
                           i : 0;
                         };

                        var total = api.column(2) .data().reduce( function (a, b) {
                        
                         return intVal(a) + intVal(b);
                      }, 0 );
                     
                     if(parseFloat(total).toFixed(2)==parseFloat(total_prestamo) && cantidad_quincenas_acumuladas==cantidad_quincenas){
                      
                       $( api.column(4).footer() ).html('<span style="color:green;">'+ currencyFormat (total)+'<span>')
                      
                      }else{
                        $( api.column(4).footer() ).html('<span style="color:red;">'+ currencyFormat (total)+'<span>')
                      }


                    },
                  
                   ajax:{
                        url: urlDetallePrestamo,
                        type:'post',
                        data: function ( d ) {
                          //return  $.extend(d, myData);
                        }
                      } ,
                  columns: [
                  { data: null,
                   "render": function ( data, type, full, meta) {
                    return '<span class="badge font-white " style="font-size: 1.2rem; background:#'+data.color+';">'+data.numero_quincena+'</span>';}},
                  {data:'fecha_vencimiento'},
                  {data:'cuota'},
                  {data: null,
                  "render": function ( data, type, full, meta) {
                   return '<span class="badge font-white" style="background:#'+data.color+';  width:100px;white-space:normal">'+data.nombre+'</span>';}},
                   //return '<FONT COLOR="#'+data.color+';">'+data.nombre+'</FONT>';}},
                  {data:'action'}
                 ],

                 "columnDefs": [
                 {
                   "targets": [4],
                   "data": null,
                   "defaultContent": '<a class="btn btn-green" id="editar_quincena"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar quincena"></i></a> <a class="btn btn-red" id="eliminar_quincena"><i class="fa fa-times-circle"aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Eliminar quincena"></i></a>'
                  //"visible": false
                 }
                ],
               "order": [[ 1, "desc" ]]
             });

$('#table_quincenas_wrapper .row').first().remove();
$('#table_quincenas_wrapper .row .col-sm-12').addClass('px-0');

/******************************************************************************
 Muestra el detalle de la quincena a traves del formulario modal
******************************************************************************/
$('#table_quincenas tbody').on( 'click', '#editar_quincena', function () {
        var data = tableQuincenas.row( $(this).parents('tr') ).data();
        
            $('[name="id_quincena"]').val(data.id); //id del detalle de prestamo
            $('[name="numero_quincena"]').val(data.numero_quincena); //numero de quincena
            $('[name="fecha_vencimiento"]').val(data.fecha_vencimiento); //fecha de vencimiento
            $('[name="estado_quincena"]').val(data.quincena_estado); // estado de la quincena
            $('[name="importe"]').val(data.cuota); //importe
            $('[name="id_prestamo"]').val(data.id_prestamo); // id del prestamo asociado
            $('[name="fecha_final_prestamo"]').val(data.fecha_final_prestamo);
            $('[name="numero_quincena"]').attr('disabled','disabled');
            if(prestamo_estado_id!=13){
            $('[name="importe"]').attr('readonly','readonly');
            }
            save_method='edit';
            $('#modal_form').modal('show'); // desplegamos el formulario modal
            $('.modal-title').text('Editar quincena'); // asignamos el titulo 
    });



/*********************************************************************************
                    Ejecuta la accion eliminar quincena
**********************************************************************************/
$('#table_quincenas tbody').on( 'click', '#eliminar_quincena', function () {
        var data = tableQuincenas.row( $(this).parents('tr') ).data();
  
        swal({
                title: "¿Desea eliminar esta quincena?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

             
                if (isConfirm) {
                   $.ajax({
                    url : urlEliminarQuincena,
                    type: "POST",
                    data: data,
                   dataType: "JSON",
                   success: function(data)
                   {
                    
                      if(data.status) //if success close modal and reload ajax table
                       {


                        swal({
                        title: 'quincena eliminada!',
                        text: '',
                        type: 'success'
                          }, function() {
                              reload_tableQuincenas();
                          });
                       }else{


                           swal({
                              title: "No se puede realizar esta operación",
                              text:  "La quincena no puede ser eliminada debido a que el préstamo pasó por el estado activo",
                              type: "error"
                            });


                       }
           },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
         
 
        }
    });
                    
                    
                } else {
                    swal("Acción cancelada", "", "warning");
                }
            });
         
  });




/******************************************************************************
 Muestra los pagos asociados a un prestamo a traves del datatable 
******************************************************************************/
var table;
 table = $('#tableReportePago').DataTable({
              "bPaginate": false,
              "bLengthChange": false,
              "bSearch": false,
              "bFilter": false,
              "bInfo": false,
                "bAutoWidth": false,
                "processing": true, 
                "serverSide": true,
                 "language": {
                  "zeroRecords": "No hay pagos reportados"
                 },

     "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api();

             var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                        //return  formatMoney(i);
            };

            var total = api
                .column(2)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

          $(api.column(4).footer() ).html( currencyFormat (total))
            


        },
       

       ajax:{
        url: urlReportePago,
        type:'post',
        data: function ( d ) {
           //return  $.extend(d, myData);
        }
    } ,
   
    columns: [
        {data:'fecha_subida_recibo'},
        {data:'fecha_efecto'},
        {data:'importe'},
        {data: null,
          "render": function ( data, type, full, meta) {
           return '<span class="badge font-white" style="background:#'+data.color+'; width:100px;white-space:normal">'+data.nombre+'</span>';}},
        {data:'action'},
        ],

     "columnDefs": [
        {
            "targets": [4],
            "data": null,
            "defaultContent":'<a class="btn btn-green" id="editar_pago"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar pago"></i></a> <a class="btn btn-blue ver_pago"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Ver documento"></i></a> '
          
        }
    ],

      "order": [[ 1, "desc" ]]

});

$('#tableReportePago_wrapper .row').first().remove();
$('#tableReportePago_wrapper .row .col-sm-12').addClass('px-0');

/******************************************************************************
 Muestra la vista modal para editar un pago (accion ver/editar pago)
******************************************************************************/
 $('#tableReportePago tbody').on( 'click', '#editar_pago', function () {
        var data = table.row( $(this).parents('tr') ).data();
           var ruta = url+data.imagen_recibo;
           $('.imagepreview').attr('src',ruta);
           $('#importe').val(data.importe);
           $('#id_pago').val(data.id);
           $('#fecha_efecto').val(data.fecha_efecto);
           $('#idCliente').val(idCliente);
           $('select[name=selectStatusReporte]').val(data.id_reporte_pago_status);
           $('.selectpicker').selectpicker('refresh');
           $("#expandir").click(function(event) {
                $('#urlImagen').attr('href',ruta);
                $('.imageExpand').attr('src',ruta);
                  $('#modal_reciboPago').modal('show');
           });
            $("#descargar").click(function(event) {
                $('#imagenDownload').attr('href',ruta);
              
           });

             $("#eliminar_pago").click(function(event) {
               eliminarPago(data);
              
           });
             $('#modal_reportePago').modal('show');
          
    });



/*************************************************************************************
 Muestra la vista modal para visualizar un recibo de pago (accion ver recibo de pago)
**************************************************************************************/
 $('#tableReportePago tbody').on( 'click', '.ver_pago', function () {
        var data = table.row( $(this).parents('tr') ).data();
        var ruta = url+data.imagen_recibo;
        var extension = ruta.split('.').pop();
        console.log(ruta);
        if(extension=="pdf"){
           $(this).lightGallery({
               dynamic:true,
               html:true,
               thumbnail:false,
               selector:'#iframe',
               dynamicEl:[
                {"src":ruta, iframe:"true" }]
          });
       }else{
          $(this).lightGallery({
              dynamic:true,
              html:true,
              mobileSrc:true,
              dynamicEl:[
                {"src":ruta}]
           });

       }
  });

 

/******************************************************************************
 Acción para eliminar un pago
******************************************************************************/

 // $('#tableReportePago tbody').on( 'click', '#eliminar_pago', function () {
  function eliminarPago(data){
      
        
         swal({
                title: "¿Deseas eliminar este pago?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

             
                if (isConfirm) {
                   $.ajax({
                    url : urlEliminarPago,
                    type: "POST",
                    data: {'id_pago': data.id},
                   dataType: "JSON",
                   success: function(data)
                   {
                    
                      if(data.status) //if success close modal and reload ajax table
                       {


                        swal({
                        title: 'Pago eliminado!',
                        text: '',
                        type: 'success'
                          }, function() {
                              reload_table();
                              $('#modal_reportePago').modal('hide');
                          });
                       }else{


                       swal("El pago no pudo ser eliminado intente más tarde", "", "error");

                       }
           },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
         
 
        }
    });
                    
                    
                } else {
                    swal("Acción cancelada", "", "warning");
                }
            });
       }
    //});






       

/******************************************************************************
 función para refrescar el datatable  del reporte de pagos
******************************************************************************/
function reload_table()
{
    table.ajax.reload(null,false); 
}

/******************************************************************************
 función para refrescar el datatable  de las quincenas
******************************************************************************/
function reload_tableQuincenas()
{
    tableQuincenas.ajax.reload(null,false); 
}


/******************************************************************************
 función para reportar un pago de lado del administrador
******************************************************************************/
   function reportarPago(){
    
     jQuery.support.cors = true;
    var imagen_recibo = $('#imagen_recibo')[0].files[0];
   // data= {'imagen_recibo': imagen_recibo, 'id_prestamo':};
   
    var formData = new FormData($("#upload_form")[0]);

       
    
     $.ajax({
            url: urlReportarPago,
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            mimeType:"multipart/form-data",
 
            success: function (data) {
            
             if(data.status==true){
               swal({
                title: "Pago registrado con éxito",
                text:  "",
                type: "success"
            });
               reload_table();

               }
               else{

                 swal({
                  title: "Formato de imagen invalido",
                  text:  "",
                  type: "warning"
            });

               }
            },
             error: function (jqXHR, textStatus, errorThrown)
             {
                 //alert('Error get data from ajax');
                 //  alert(jqXHR.status);
                 swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
             }
           
        });
   }



   function uploadPago(){
     var formData = new FormData($("#upload_form")[0]);
      var xhr = new XMLHttpRequest();
       xhr.upload.addEventListener(null, null, false);
       xhr.open("POST", "reporte_pago/reportarPago");
       xhr.send(formData);
   }

/*****************************************************************************************
 función para actualizar un reporte de pago haciendo uso del envio de emails en el servidor
********************************************************************************************/

function enviarPago(){

 $('#formPago').parsley().validate();
 var validate = $('#formPago').parsley().isValid();
 
  if(validate){
     $('#btnSave').text('Guardando..'); //cambia el texto del botón guardar
     $('#btnSave').attr('disabled',true); //deshabilita el botón guardar
    $.ajax({
        url : urlEnviarPago,
        type: "POST",
        data: $('#formPago').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          
          
         if(data.status) //si la respuesta es True refresca la tabla
            {
                $('#modal_reportePago').modal('hide');
                 reload_table();
            }
          
            $('#btnSave').text('Guardar'); //cambia el texto del boton
            $('#btnSave').attr('disabled',false); 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
 
        }
    });
   }
}

/***********************************************************************************************************
 función para actualizar el estado y ademas de actualizar el table de los históricos de estados del préstamo
*************************************************************************************************************/

   function añadir_estado(id){
    var dataString='idprestamo='+id+'&idestado='+$('#estado_'+id).val()+'&observaciones='+$('#observaciones_'+id).val()+'&idCliente='+idCliente+'&total_prestamo='+$('#total_prestamo').val()+'&fecha_final_prestamo='+$('#fecha_final_prestamo').val();
    $.ajax({
      type:'POST',
      url: urlActualizarEstadoPrestamo,
      data: dataString,
      dataType: "JSON",
      success: function(data){
            if(data.status==true){
                swal({
                       title: "Se ha actualizado el estado del préstamo",
                       text:  "",
                       type: "success"
                     },  function() {
                              location.reload();
                          });
              }else if(data.status=="ERROR_OPERACION_PRESTAMO"){
                     swal({
                            title: "No se puede realizar esta operación",
                            text:  "El préstamo no puede pasar a estado activo debido a que el total del préstamo o cantidad de quincenas no coincide con la información reflejada en la solicitud",
                            type: "error"
                          });

              }else if(data.status=="ERROR_AJUSTE_FECHA"){
                     swal({
                            title: "No se puede realizar esta operación",
                            text:  "La fecha de la última quincena debe coincidir con la fecha de finalización del préstamo",
                            type: "error"
                          });

              }else if(data.status=="ERROR_ESTADO_ACTUAL"){
                 swal({
                            title: "No se puede realizar esta operación",
                            text:  "El estado seleccionado ya se encuentra en la posición actual",
                            type: "error"
                          });
              }

               
              },
          error: function (jqXHR, textStatus, errorThrown)
          {
             swal("No se ha logrado actualizar el estado del préstamo, intente más tarde", "", "warning");
          }
        
      
    });
  }



  /******************************************************************************
 Muestra los pagos asociados a un prestamo a traves del datatable 
******************************************************************************/
var tableDocumentos;
 tableDocumentos = $('#table_documentos').DataTable({
              "bPaginate": false,
              "bLengthChange": false,
              "bSearch": false,
              "bFilter": false,
              "bInfo": false,
              "ordering": true,
                "bAutoWidth": false,
                "processing": true, 
                "serverSide": true,
                 "language": {
                  "zeroRecords": "No se han enviado documentos"
                 },
       

       ajax:{
        url: urlDocumentacion,
        type:'post',
        data: function ( d ) {
           //return  $.extend(d, myData);
        }
    } ,
   
    columns: [
        {data:'tipo_documento_id'},
        {data:'fecha_subida'},
        {data:'descripcion'},
        {data:'observacion'},
        {data:'action'},
        {data: 'id'},
        {data:'prestamo_id'}
        ],

     "columnDefs": [
        {
            "targets": [4],
            "data": null,
            "defaultContent":'<a class="btn btn-green" id="editar_documento"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar"></i></a><a class="btn btn-red mx-1" id="eliminar_documento"><i class="fa fa-times-circle"aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Eliminar documento"></i></a><a class="btn btn-blue ver_documento"><i class="fa fa-eye" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Ver documento"></i></a> '
          
        },
        {
           "targets": [0],
           render : function (data, type, row) {
           switch(data) {
               case '0' : return '<input type="radio" id="seleccionar_documento" name="seleccionar_documento" value="">'; break;
               case '1' : return '<input type="radio" checked="checked" id="seleccionar_documento" name="seleccionar_documento" value="">'; break;
               default  : return data;
            }
          }
           
        },
         {
            "targets": [5,6],
            "visible": false
         }
        
    ],

    
      "order": [[ 5, "desc" ]]

});
//selecciona el documento que tiene la cédula para actualizar el boton ver cédula en la parte superior de la vista
$('#table_documentos tbody').on( 'click', '#seleccionar_documento', function () {
   var data = tableDocumentos.row( $(this).parents('tr') ).data();
   var dataDocumentos = {'id_prestamo':data.prestamo_id, 'id_documento':data.id};

   $.ajax({
        url : urlActualizarSeleccionDocumento,
        type: "POST",
        data: dataDocumentos,
        dataType: "JSON",
        success: function(data)
        {
          
           if(data.status) 
            {
              
               location.reload();
            }
          
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
           
 
        }
    });

  
  
});








 $('#table_documentos tbody').on( 'click', '#editar_documento', function () {
        var data = tableDocumentos.row( $(this).parents('tr') ).data();
           var ruta = url+data.fichero;
           $('#imagen_documento_prestamo').attr('src',ruta);
           $('#observacion').val(data.observacion);
           $('#descripcion').val(data.descripcion);
           $('#idDocumento').val(data.id);
            var extension = ruta.split('.').pop();
           if(extension=="pdf"){
              $("#imagen_view").empty();
              $("#imagen_documento_prestamo").empty();
              $('#imagen_view').append('<iframe  src="'+ruta+'"  id="imagen_documento_prestamo" style="width:400px; height:300px;" frameborder="0" scrolling></iframe>');
            }
            else{
               $("#imagen_view").empty();
               $("#imagen_documento_prestamo").empty();
               $('#imagen_view').append('<img  src="'+ruta+'"  id="imagen_documento_prestamo" style="width:400px; height:300px;" frameborder="0" scrolling></img>');
            }


             $("#ampliarDocumento").click(function(event) {
               
                $('#imagenAmpliarDocumento').attr('href',ruta);
                 
           });
            $("#descarDocumento").click(function(event) {
                $('#imagenDescargarDocumento').attr('href',ruta);
              
           });
           $('#modal_documento').modal('show');
          



  });

function closeModalDocumento(){
    $('#imagen_view').html('');
    $('#modal_documento').modal('hide');
}



 

 $('#table_documentos tbody').on( 'click', '#eliminar_documento', function () {
        var data = tableDocumentos.row( $(this).parents('tr') ).data();
        console.log(data);
        swal({
                title: "¿Deseas eliminar este documento?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },

           function(isConfirm) {

             
                if (isConfirm) {
                  if(data.tipo_documento_id==1){
                    eliminarCedula(data);
                  }else{
                    eliminarDocumento(data);
                  }
                   
                    
                    
                } else {
                    swal("Acción cancelada", "", "warning");
                }
            });
         
  });




 function eliminarDocumento(data){

          $.ajax({
                    url : urlEliminarDocumento,
                    type: "POST",
                    data: data,
                   dataType: "JSON",
                   success: function(data)
                   {
                    
                      if(data.status) //if success close modal and reload ajax table
                       {


                        swal({
                        title: 'Documento eliminado!',
                        text: '',
                        type: 'success'
                          }, function() {
                              $("#imagen_view").empty();
                              reload_tableDocumentos();
                              
                              
                          });
                       }else{


                       swal("El documento no pudo ser eliminado intente más tarde", "", "error");

                       }
           },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
         
 
        }
    });

 }
//elimina la cédula y actualiza la url de la imagen del botón ver cédula
 function eliminarCedula(data){

          $.ajax({
                    url : urlEliminarDocumento,
                    type: "POST",
                    data: data,
                   dataType: "JSON",
                   success: function(data)
                   {
                    
                      if(data.status) //if success close modal and reload ajax table
                       {


                        swal({
                        title: 'Documento eliminado!',
                        text: '',
                        type: 'success'
                          }, function() {
                              $("#imagen_view").empty();
                              location.reload();
                              
                              
                          });
                       }else{


                       swal("El documento no pudo ser eliminado intente más tarde", "", "error");

                       }
           },
        error: function (jqXHR, textStatus, errorThrown)
        {
            swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
         
 
        }
    });

 }





 $('#table_documentos tbody').on( 'click', '.ver_documento', function () {
        var data = tableDocumentos.row( $(this).parents('tr') ).data();
           var ruta = url+data.fichero;
            var extension = ruta.split('.').pop();
          if(extension=="pdf"){
           $(this).lightGallery({
               dynamic:true,
               html:true,
               thumbnail:false,
               selector:'#iframe',
               dynamicEl:[
                {"src":ruta, iframe:"true" }]
          });
       }else{

          $(this).lightGallery({
              dynamic:true,
              html:true,
              mobileSrc:true,
              dynamicEl:[
                {"src":ruta}]
           });

       }
  });





 

 $('#ver_cedula').on('click', function(e){

    var ruta = url+$("#cedula_cliente").val();
    var extension = ruta.split('.').pop();
    console.log(ruta);
     if(extension=="pdf"){
          $("#ver_cedula").lightGallery({
             dynamic:true,
            html:true,
            thumbnail:false,
            selector:'#iframe',
            dynamicEl:[
                {"src":ruta, iframe:"true" }]

           });
       }else{

          $("#ver_cedula").lightGallery({
            dynamic:true,
            html:true,
            mobileSrc:true,
            dynamicEl:[
                {"src":ruta}]

           });

       }
 });


function reload_tableDocumentos()
{
    tableDocumentos.ajax.reload(null,false); 
}


$('#imagen_documento').on('change', function(){
    var allow = true;
    var input = $(this)[0];
    var span = $(this).next().find('span');
    var files = input.files;
    Object.keys(files).forEach(function(index) {
        var size = files[index].size;
        if(size > 4000000) allow = false;
    });
    if(!allow) {
        swal({
            title: "Esta imagen excede el peso máximo permitido.",
            text:  "Peso máximo de cada imagen: 4MB. \n\n Le recomendamos reducir el tamaño de las imágenes o enviarlas por el WhatsApp.",
            type: "warning"
        });
        $(this).val(null);
        span.text('Reportar recibo de pago');
    } else {
        jQuery.support.cors = true;
    
        $.ajax({
            url: urlUploadDocumentos,
            type: "POST",
            dataType : 'json',
            data: new FormData($("#cargar_recaudos")[0]),
            processData: false,
            contentType: false,
            beforeSend:function() { 
                swal({
                    title: 'Estamos procesando su información',
                    imageUrl: url+'img/loading.gif',
                    showConfirmButton: false
                });
            },
            success: function (data) {
             if(data.status==true){
               swal({
                title: "Información guardada con éxito",
                text:  "",
                type: "success"
            });
              reload_tableDocumentos();

               }
               else{

                 swal({
                  title: "Formato de imagen invalido",
                  text:  "",
                  type: "warning"
            });

               }
            },
             error: function (data)
             {
                 swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
             }
           
        });
    }
});

   function actualizarDocumento(){
    $('#btnSave').text('Guardando..'); //cambia el texto del botón guardar
    $('#btnSave').attr('disabled',true); //deshabilita el botón guardar
    $.ajax({
        url : urlActualizarDocumento,
        type: "POST",
        data: $('#formDocumento').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          
           if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_documento').modal('hide');
                 reload_tableDocumentos();
            }
          
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });
 
        }
    });
}


function formatYYYYMMDD(date) {
    /**
        *date parametro en formato date
        return fecha dd/mm/YYYY
    **/
      return  date.getFullYear() +
      "-" +  (date.getMonth() + 1) +
      "-" +  date.getDate();
}

function formatDDMMYYYY(date) {
    /**
        *date parametro en formato date
        return fecha dd/mm/YYYY
    **/ 
     return  date.getDate() +
      "/" +  (date.getMonth() + 1) +
      "/" +  date.getFullYear();
}

function convertDate(inputFormat) {
  function pad(s) { return (s < 10) ? '0' + s : s; }
  var d = new Date(inputFormat);
  return [pad(d.getDate()), pad(d.getMonth()+1), d.getFullYear()].join('/');
}



//Esconder y Mostrar tablas VP
$('.tablesVP').find('tbody').slideUp();

$('.toggleVP').each(function() {
    const element = $(this).find('b.enlace');
    element.click(toggleContent);
});
function toggleContent() {
    const icono = $(this).find('i');
    icono.toggleClass('reverse');
    const padre = $(this).parents()[0];
    const hermano = $(padre).next();
    hermano.find('tbody').slideToggle();
}
//Quitar margin top de primer tabla de variables
$('.toggleVP').first().removeClass('mt-3')
//Esconder y Mostrar Historial Préstamos
const registros = $('#historialPrestamoTable').find('tbody tr');
if(registros.length > 3) {
    const subregistros = registros.slice(3);
    subregistros.slideUp('fast');
    //esconder a partir de la 4
    $('#historialPrestamoButton').click(function() {
        $(this).toggleClass('reverse');
        subregistros.slideToggle('fast');
    });    
} else {
    $('#historialPrestamoButton').css('display', 'none');
}


 



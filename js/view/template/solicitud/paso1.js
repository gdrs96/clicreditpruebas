$('#cedula').on('change', function() {
  $('#n_otroDocumento').hide();
  $('#n_otroDocumento').attr('data-parsley-required', 'false');
  $('#n_otroDocumento').val('');
  $('#otroDocumentoError').hide();
  $('#n_cedula').show();
  $('#n_cedula').attr('data-parsley-required', 'true');
  $('#nCedulaError').show();
});
$('#otroDocumento').on('change', function() {
  $('#n_cedula').hide();
  $('#n_cedula').attr('data-parsley-required', 'false');
  $('#n_cedula').val('');
  $('#nCedulaError').hide();
  $('#n_otroDocumento').show();
  $('#n_otroDocumento').attr('data-parsley-required', 'true');
  $('#otroDocumentoError').show();
});
const dia = $('#dia_fecha_nacimiento');
const mes = $('#mes_fecha_nacimiento');
const anio = $('#year_fecha_nacimiento');
const listErrors = $("<ul class='parsley-errors-list'></ul>");
const errorMessage = $("<li class='parsley-required'>Este campo es requerido</li>");
listErrors.append(errorMessage);
function fNacValidation() {
  const diaVal = dia.parsley().isValid();
  const mesVal = mes.parsley().isValid();
  const anioVal = anio.parsley().isValid();
  if(diaVal && mesVal && anioVal) {
    listErrors.removeClass('filled')
    $('#FechaNacimientoError').html('');
  } else{
    $('#FechaNacimientoError').append(listErrors);
    listErrors.addClass('filled')
  }
}
$('#next').click(function(event){
  fNacValidation();
  dia.on('change', fNacValidation);
  mes.on('change', fNacValidation);
  anio.on('change', fNacValidation); 
  var $tipoDocumento = $('input[name=tipo_documento]:checked', '#procesar_solicitud').val();
  if($tipoDocumento == 'cedula'){
    $('#n_identificacion').val($('#n_cedula').val());
  }else{
    $('#n_identificacion').val($('#n_otroDocumento').val());
  }
  var validate = $('#procesar_solicitud').parsley().isValid();
  if(validate==true) {
    enviarForm();
  } else {
    let wasFocused = false;
    let failedInputs = [];
    const inputs = $('#procesar_solicitud').parsley().fields;
    inputs.forEach(function(input) {
      if((input.validationResult[0]) && input.$element[0].tagName=='INPUT') {
        input.$element.focus();
        wasFocused = true;
      }
    });
    if(!wasFocused) $('html, body').animate({ scrollTop: 0 }, 'slow');
  }
});
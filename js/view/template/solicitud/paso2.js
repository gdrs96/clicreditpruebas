$(document).ready(function(){
	$('#2_8_1').on('focus', function() {$('#licencia_png').collapse('show')})
	$('#2_8_1').on('focusout', function() {$('#licencia_png').collapse('hide')})
	$('.select2').select2();
	$('.select2#2_17_1').on('select2:open', function(){
		$('.select2-container .select2-search__field').tooltip({
			placement: 'top',
			title: 'Si no encuentras el sector de tu referencia elige el más cercano',
			trigger: 'focus'
		});
	});
});	


    
	function estudios(){
		if($('#2_18').val() != 1 && $('#2_18').val() != 0 ){
			$('#2_18_group').slideDown('fast');
		}else{
			$('#2_18_group').slideUp('fast');
		}
	}
	function vehiculo(){
		if($('#2_9').val() == 1 ){
			$('#2_9_group').slideDown('fast');
		}else{
			$('#2_9_group').slideUp('fast');
		}
	}
	function licencia(){
		if($('#2_8').val() == 1 ){
			$('#2_8_group').slideDown('fast');
		}else{
			$('#2_8_group').slideUp('fast');
		}
	}
	function estadoCivil(){
		if($('#2_6').val() == 2 || $('#2_6').val() == 5 ){
			$('#2_6_group').slideDown('fast');
		}else{
			$('#2_6_1').val(0);
			$('#2_6_2').val(0);
			$('#2_6_3').val(0);
			$('#2_6_group').slideUp('fast');
		}
	}

  function trabajoPareja(){
  	if($('#2_6_2').val() == 1){
  		$('#2_6_3_group').slideDown('fast');
  	}else{
  		$('#2_6_3').val(0);
  		$('#2_6_3_group').slideUp('fast');
  	}
  }
	function otroNumero(){
		if($('#2_12').val() > 1 ){
			$('#2_12_group').slideDown('fast');
		}else{
			$('#2_12_group').slideUp('fast');
		}
	}

	function select2Handler() {
		if($('#2_17_1').val() == '') {
			$('#select2-2_17_1-container').parent().removeClass('parsley-success');
			$('#select2-2_17_1-container').parent().addClass('parsley-error');
			$('#select2-2_17_1-container').css('color', 'red');
		} else {
			$('#select2-2_17_1-container').parent().removeClass('parsley-error');
			$('#select2-2_17_1-container').parent().addClass('parsley-success');
			$('#select2-2_17_1-container').css('color', '#088A08');
		}
	}

	/*$('#2_17_1').on( 'change',  function () {

           select2Handler();
	});*/

	function referenciaPersonal(){
	//	console.log($('#2_17_telefono_referencia').val().length);
		if($('#2_17_telefono_referencia').val().length > 0 ){
            $('#2_17_1_group').slideDown('fast');
           
           // select2Handler();
           }else{
			$('#2_17_1_group').slideUp('fast');
		}
	}

	

	$("#next").click(function(event) { 
		$("#procesar_solicitud").find(':input').each(function(){
			if(($('#2_6').val() == 1 || $('#2_6').val() == 3 || $('#2_6').val() == 4) && this.id == '2_6_1' && this.value == ""){
				$('#2_6_1').attr('data-parsley-required', 'false');
			}else if(($('#2_6').val() == 1 || $('#2_6').val() == 3 || $('#2_6').val() == 4) && this.id == '2_6_2' && this.value == ''){					
				$('#2_6_2').attr('data-parsley-required', 'false');
			}else if(($('#2_6').val() == 1 || $('#2_6').val() == 3 || $('#2_6').val() == 4 || $('#2_6_2').val() == 2) && this.id == '2_6_3' && this.value == ''){
				$('#2_6_3').attr('data-parsley-required', 'false');
				$('#2_6_3').val(0);
			}else if(($('#2_8').val() == 2) && this.id == '2_8_1' && this.value == ''){
						$('#2_8_1').attr('data-parsley-required', 'false');
			}else if(($('#2_9').val() == 2) && this.id == '2_9_1' && this.value == ''){
					$('#2_9_1').attr('data-parsley-required', 'false');
			}else if(($('#2_9').val() == 2) && this.id == '2_9_2' && this.value == ''){
					$('#2_9_2').attr('data-parsley-required', 'false');
			}else if(($('#2_9').val() == 2) && this.id == '2_9_3' && this.value == ''){
					$('#2_9_3').attr('data-parsley-required', 'false');
			}else if(($('#2_9').val() == 2) && this.id == '2_9_4' && this.value == ''){
					$('#2_9_4').attr('data-parsley-required', 'false');
			}else if(($('#2_12').val() == 1) && this.id == '2_13_telefono2' && this.value == ''){
					$('#2_13_telefono2').attr('data-parsley-required', 'false');
			}else if(($('#2_12').val() == 1) && this.id == '2_14' && this.value == ''){
					$('#2_14').attr('data-parsley-required', 'false');
			}else if($('#2_17_telefono_referencia').val().length==0 && this.value == ''){
                    $('#2_17_1').attr('data-parsley-required', 'false');
                    $('#2_17_telefono_referencia').attr('maxlength', '15');
                    $('#2_17_1').removeAttr('data-parsley-type');
                    $('#2_17_1').removeAttr('data-parsley-trigger');
                    $('#2_17_telefono_referencia').attr('data-parsley-required', 'false');
                    $('#2_17_2').attr('data-parsley-required', 'false');
                    $('#2_17_telefono_referencia').val("Sin respuesta");
			}else if($('#2_17_telefono_referencia').val()!=='Sin respuesta' && !isNumber($('#2_17_telefono_referencia').val()) && this.id=='2_17_1' && this.value == ''){
				    $('#2_17_telefono_referencia').attr('data-parsley-trigger','keyup change');
				    $('#2_17_telefono_referencia').attr('data-parsley-type','number');
				    $('#2_17_telefono_referencia').attr('maxlength', '8');
				    $('#2_17_1').attr('data-parsley-required', 'true');
                    select2Handler();
				    $('#2_17_1').change(select2Handler);
			}else if($('#2_17_telefono_referencia').val()!=='Sin respuesta' && this.id=='2_17_2' && this.value == ''){
				$('#2_17_2').attr('data-parsley-required', 'true');			
			}else if(($('#2_18').val() == 1) && this.id == '2_18_1' && this.value == ''){
					$('#2_18_1').attr('data-parsley-required', 'false');
			}else if(($('#2_18').val() == 1) && this.id == '2_18_2' && this.value == ''){
					$('#2_18_2').attr('data-parsley-required', 'false');
			}
			else if(this.id == 'next'){
				return;
			}/*else if($('#'+this.id).parsley().isValid()==false){
				console.log(this.id);
			}*/
		
			//console.log(this.id +" "+ $('#'+this.id).parsley().isValid());
		   
		});
		//console.log(isNumber($('#2_17_telefono_referencia').val()));
		//console.log($('#2_17_telefono_referencia').val().length);
		//console.log($("#procesar_solicitud").parsley());
		var validate = $("#procesar_solicitud").parsley().isValid();
		//console.log(validate);
		if(validate==true) {
			enviarForm();
		} else {
			let wasFocused = false;
			let failedInputs = [];
			const inputs = $("#procesar_solicitud").parsley().fields;
			inputs.forEach(function(input) {
				if((input.validationResult[0]) && input.$element[0].tagName=="INPUT") {
					input.$element.focus();
					wasFocused = true;
				}
			});
			if(!wasFocused) $("html, body").animate({ scrollTop: 0 }, "slow");
		}
	});

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

	function situacion_laboral(){
		if($('#3_1').val() == 1){
			$('.situacion_laboral').slideDown('fast');
			$('.periodo_cobro').slideUp('fast');
			$('.periodo_pago').slideDown('fast');
			$('.actividad_laboral').slideDown('fast');
		}else if($('#3_1').val() == 2){
			$('.periodo_cobro').slideDown('fast');
			$('.situacion_laboral').slideUp('fast');
			$('.periodo_pago').slideUp('fast');
			$('.actividad_laboral').slideDown('fast');
		}else if($('#3_1').val() == 4){
			$('.periodo_pago').slideDown('fast');
			$('.situacion_laboral').slideUp('fast');
			$('.periodo_cobro').slideUp('fast');
			$('.actividad_laboral').slideUp('fast');
		}else if($('#3_1').val() == 3){
			$('.periodo_pago').slideUp('fast');
			$('.situacion_laboral').slideUp('fast');
			$('.periodo_cobro').slideUp('fast');
			$('.actividad_laboral').slideUp('fast');
		}
	}
	function select2Handler() {
		if(($('#3_1').val() == "1") && this.value == '' ){
			$('.select2-selection').removeClass('parsley-success');
			$('.select2-selection').addClass('parsley-error');
			$('.select2-selection__rendered').css('color', 'red');
		} else {
			$('.select2-selection').removeClass('parsley-error');
			$('.select2-selection').addClass('parsley-success');
			$('.select2-selection__rendered').css('color', '#088A08');
		}
	}
	function enviarForm(){
		
			$("#procesar_solicitud").attr('action','<?= base_url() ?>solicitud/paso-3-check');
			setTimeout(1000,$("#procesar_solicitud").submit());
		
	}
	function deudas(){
		if($('#3_15').val() == 1 ){
			$('#3_15_group').slideDown('fast');
		}else{
			$('#3_15_group').slideUp('fast');
		}
	}

	



	$("#next").click(function(event) { 
      $("#procesar_solicitud").find(':input').each(function(){
      	if(($('#3_15').val() == 2) && this.id == '3_15_1' && this.value == '' ){
					$('#3_15_1').attr('data-parsley-required', 'false');
				}

				else if(($('#3_15').val() == 2) && this.id == '3_15_2' && this.value == '' ){
					$('#3_15_2').attr('data-parsley-required', 'false');
				}

				else if(($('#3_15').val() == 2) && this.id == '3_15_3' && this.value == '' ){
					$('#3_15_3').attr('data-parsley-required', 'false');
				}
				else if(($('#3_1').val() == '') && this.id == '3_2'){
					$('.select2-selection').removeClass('parsley-success');
					$('.select2-selection').addClass('parsley-error');
					$('.select2-selection__rendered').css('color', 'red');
					$('#3_2').on('select2:select', select2Handler);
				}
				else if(($('#3_1').val() == 1) && this.id == '3_2'){
					const handler3_1 = select2Handler.bind(this);
					handler3_1();
					$('#3_2').on('select2:select', select2Handler);
				}
				else if(($('#3_1').val() == 3 || $('#3_1').val() == 2 || $('#3_1').val() == 4) && this.id == '3_2' && this.value == '' ){
					$('#3_2').attr('data-parsley-required', 'false');
				}
				else if(($('#3_1').val() == 3 || $('#3_1').val() == 2 || $('#3_1').val() == 4) && this.id == '3_3_nombre_empresa' && this.value == '' ){
					$('#3_3_nombre_empresa').attr('data-parsley-required', 'false');
				}

				else if(($('#3_1').val() == 2 || $('#3_1').val() == 3 || $('#3_1').val() == 4) && this.id == '3_4' && this.value == '' ){
					$('#3_4').attr('data-parsley-required', 'false');
				}

				else if(($('#3_1').val() == 3 || $('#3_1').val() == 2 || $('#3_1').val() == 4) && this.id=='3_6_dia' && this.value== ''){
					$('#3_6_dia').attr('data-parsley-required', 'false');
				}
				
				else if(($('#3_1').val() == 3 || $('#3_1').val() == 2 || $('#3_1').val() == 4) && this.id=='3_6_mes' && this.value== ''){
					$('#3_6_mes').attr('data-parsley-required', 'false');
				}
				else if(($('#3_1').val() == 3 || $('#3_1').val() == 2 || $('#3_1').val() == 4) && this.id=='3_6_year' && this.value== ''){
					$('#3_6_year').attr('data-parsley-required', 'false');
				}

				else if(($('#3_1').val() == 3 || $('#3_1').val() == 2) && this.id == '3_7' && this.value == 0 ){
					$('#3_7').attr('data-parsley-required', 'false');
				}

				else if(($('#3_1').val() == 3 || $('#3_1').val() == 2 || $('#3_1').val() == 4) && this.id == '3_8' && this.value == '' ){
					$('#3_8').attr('data-parsley-required', 'false');
				}

				else if(($('#3_1').val() == 3 || $('#3_1').val() == 2 || $('#3_1').val() == 4) && this.id == '3_9_telefono_empresa' && this.value == '' ){
					$('#3_9_telefono_empresa').attr('data-parsley-required', 'false');
				}

				else if(($('#3_1').val() == 3 || $('#3_1').val() == 4 || $('#3_1').val() == 1) && this.id == '3_24' && this.value == '' ){
					$('#3_24').attr('data-parsley-required', 'false');

			        }

      			else if(this.id == 'next'){
					return;
				}
		});
      var validate = $("#procesar_solicitud").parsley().isValid();
			if(validate==true) {
				enviarForm();
			} else {
			let wasFocused = false;
			let failedInputs = [];
			const inputs = $("#procesar_solicitud").parsley().fields;
			inputs.forEach(function(input) {
				if((input.validationResult[0]) && input.$element[0].tagName=="INPUT") {
					input.$element.focus();
					wasFocused = true;
				}
			});
			if(!wasFocused) $("html, body").animate({ scrollTop: 0 }, "slow");
		}
		}); 


		

 $(document).ready(function(){
		$('.select2').select2();
		$('.select2#3_2').on('select2:open', function(){
			$('.select2-container .select2-search__field').tooltip({
				placement: 'top',
				title: 'Si no encuentras el tuyo selecciona el más parecido',
				trigger: 'focus'
			});
		});
  });


	
$(document).ready(function(){
	$('.select2').select2();		
});

	$('#4_1').change(function(){
		var clase = $(this).val();
		if(clase=="")
		   clase=0;
		if( $('.' + clase ).length > 0 ){
			$('.distrito').css('display','block');
			$('#4_2').attr('data-parsley-required', 'true');
			$('#4_2').html('<option value="">Seleccione una opción</option>');
			$('.'+clase).clone().appendTo( $('#4_2') );
			$('#4_2').select2();
		}else{
			$('#4_2').attr('data-parsley-required', 'false');
			$('.distrito').css('display','none');
		}
		$('.corregimiento').css('display','none');
		$('.sector').css('display','none');
		$('.barrio').css('display','none');
	});
	$('#4_2').change(function(){
		var clase = $(this).val();
		if(clase=="")
		   clase=0;
		if( $('.' + clase ).length > 0 ){
			$('.corregimiento').css('display','block');
			$('#4_3').attr('data-parsley-required', 'true');
			$('#4_3').html('<option value="">Seleccione una opción</option>');
			$('.'+clase).clone().appendTo( $('#4_3') );
			$('#4_3').select2();
		}else{
			$('#4_3').attr('data-parsley-required', 'false');
			$('.corregimiento').css('display','none');
		}
		$('.sector').css('display','none');
		$('.barrio').css('display','none');
	});
	$('#4_3').change(function(){
		var clase = $(this).val();
		if(clase=="")
		   clase=0;
		if( $('.' + clase ).length > 0 ){
			$('.sector').css('display','block');
			$('#4_4').attr('data-parsley-required', 'true');
			$('#4_4').html('<option value="">Seleccione una opción</option>');
			$('.'+clase).clone().appendTo( $('#4_4') );
			$('#4_4').select2();
		}else{
			$('#4_4').attr('data-parsley-required', 'false');
      $('.sector').css('display','none');
		}
		$('.barrio').css('display','none');
	});
	$('#4_4').change(function(){
		var clase = $(this).val();
		if(clase=="")
		   clase=0;
		if( $('.' + clase ).length > 0 ){
			$('.barrio').css('display','block');
			$('#4_5').attr('data-parsley-required', 'true');
			$('#4_5').html('<option value="">Seleccione una opción</option>');
			$('.'+clase).clone().appendTo( $('#4_5') );
			$('#4_5').select2();
		}else{
			$('#4_5').attr('data-parsley-required', 'false');
			$('.barrio').css('display','none');
		}
	});

	function tel_fijo(){
		if($('#4_8').val() == 1 ){
			$('#4_8_group').slideDown('fast');
		}else{
			$('#4_8_group').slideUp('fast');
		}
	}
	function select2Handler(event) {
		if(this.value == '') {
			$('#select2-4_' + event.data.number + '-container').parent().removeClass('parsley-success');
			$('#select2-4_' + event.data.number + '-container').parent().addClass('parsley-error');
			$('#select2-4_' + event.data.number + '-container').css('color', 'red');
		} else {
			$('#select2-4_' + event.data.number + '-container').parent().removeClass('parsley-error');
			$('#select2-4_' + event.data.number + '-container').parent().addClass('parsley-success');
			$('#select2-4_' + event.data.number + '-container').css('color', '#088A08');
		}
	}

	$("#next").click(function(event) { 
       $("#procesar_solicitud").find(':input').each(function(){
      	 if(($('#4_8').val() == 2) && this.id == '4_8_1' && this.value == '' ){
				   $('#4_8_1').attr('data-parsley-required', 'false');
				}
				else if(this.id == '4_1') {
					const handler4_1 = select2Handler.bind(this, {data: {number: 1}});
					handler4_1();
					$('#4_1').on('select2:select', {number: 1},select2Handler);
				}
				else if(this.id == '4_2') {
					const handler4_2 = select2Handler.bind(this, {data: {number: 2}});
					handler4_2();
					$('#4_2').on('select2:select', {number: 2}, select2Handler);
				}
				else if(this.id == '4_3') {
					const handler4_3 = select2Handler.bind(this, {data: {number: 3}});
					handler4_3();
					$('#4_3').on('select2:select', {number: 3}, select2Handler);
				}
				else if(this.id == '4_4') {
					const handler4_4 = select2Handler.bind(this, {data: {number: 4}});
					handler4_4();
					$('#4_4').on('select2:select', {number: 4}, select2Handler);
				}
				else if(this.id == '4_5') {
					const handler4_5 = select2Handler.bind(this, {data: {number: 5}});
					handler4_5();
					$('#4_5').on('select2:select', {number: 5}, select2Handler);
				}
				else if(this.id == 'next'){
					return;
				}
			});
			
      var validate = $("#procesar_solicitud").parsley().isValid();
    //  console.log(validate);
			if(validate==true) {
				enviarForm();
			} else {
				let wasFocused = false;
				let failedInputs = [];
				const inputs = $("#procesar_solicitud").parsley().fields;
				inputs.forEach(function(input) {
					if((input.validationResult[0]) && input.$element[0].tagName=="INPUT") {
						input.$element.focus();
						wasFocused = true;
					}
				});
				if(!wasFocused) $("html, body").animate({ scrollTop: 0 }, "slow");
			}
		});       

 
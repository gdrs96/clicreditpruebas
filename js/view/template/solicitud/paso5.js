function tarjetas() {
	if($('#5_4').val() == 1){
		$('#5_4_group').slideDown('fast');
	}else{
		$('#5_4_group').slideUp('fast');
	}
}
$("#enviar").click(function(event) { 
    $("#procesar_solicitud").find(':input').each(function(){
		if(($('#5_4').val() != 1) && this.id == '5_4_1' && this.value == ''){
			$('#5_4_1').attr('data-parsley-required', 'false');
		}
		else if(this.id == 'enviar'){
			return;
		}
	});

	var validate = $("#procesar_solicitud").parsley().isValid();
	//console.log(validate)
    if(validate==true) {
		enviarForm();
    }else{
		let wasFocused = false;
		let failedInputs = [];
		const inputs = $("#procesar_solicitud").parsley().fields;
		inputs.forEach(function(input) {
			if((input.validationResult[0]) && input.$element[0].tagName=="INPUT") {
				input.$element.focus();
				wasFocused = true;
			}
		});
		if(!wasFocused) $("html, body").animate({ scrollTop: 0 }, "slow");
	}
});
//Función para restringir tamaño de imágenes 
function allowUploadImage(element) {
	element.change(function () {
		var allow = true;
		var input = $(this)[0];
		var span = $(this).next().find('span');
		var files = input.files;
		Object.keys(files).forEach(function(index) {
			var size = files[index].size;
			if(size > 1000000) allow = false;
		});
		if(!allow) {
			swal({
				title: "Esta imagen excede el peso máximo permitido.",
				text:  "Peso máximo de cada imagen: 1MB. \n\n Le recomendamos reducir el tamaño de las imágenes o enviarlas por el WhatsApp.",
				type: "warning"
			});
			$(this).val(null);
			span.text('Subir imagen');
		}
	});
}
allowUploadImage($('#imagen_seguro_social'));
allowUploadImage($('#imagen_talonarios'));
allowUploadImage($('#imagen_recibos'));
//Función ajax para enviar peticion al controlador solicitud paso 5
	function enviarForm(){
        var fileImagenTalonarios = $("#imagen_talonarios");
        var fileImagenRecibos = $("#imagen_recibos");
	  
		if(parseInt(fileImagenTalonarios.get(0).files.length)<=3 && parseInt(fileImagenRecibos.get(0).files.length)<=3){	
        $('#procesar_solicitud').ajaxForm({
          	url: urlPaso5, 
          	type: 'post',
           	beforeSend:function(data,xhr) { 
				swal({
					title: 'Estamos procesando su información',
					imageUrl: url+'img/loading.gif',
					text: '0%',
					showConfirmButton: false
				});
			
				var container = $('.sweet-alert');
	            var progress = $('<div class="progress"></div>');
				var bar = $('<div></div>');
				bar.attr({
					class: 'progress-bar blue',
					role: 'progressbar',	
				});

				progress.prepend(bar);
				container.append(progress);

			},
			uploadProgress: function(event, position, total, percentComplete) {
				var text = $('.sweet-alert p').first();
				var bar = $('.sweet-alert .progress-bar').first();
				bar.css('width', percentComplete+'%');
				text.html(percentComplete+'%');
			},
         	success:function(data){
              data = JSON.parse(data);
              if(data.status==true){
               
               window.location.href = url+'solicitud/finalizado';
              }else if(data.status==false){
              	 swal({
                  title: "En estos momentos no podemos procesar su información",
                  text:  "intente más tarde",
                  type: "warning"
              });
              }
              else if(data.status=="ERROR_IMAGEN"){
              	swal({
                  title: "Error al guardar imagen",
                  text:  "Formato de imagen inválido",
                  type: "warning"
              });

           }else if(data.status=="ERROR_CODIGO_VERIFICACION"){
           	   
           	   swal({
                  title: "El código de verificación ingresado no es correcto.",
                  text:  "",
                  type: "error",
                  confirmButtonColor: '#DD6B55',
              });
             
			   $('.progress').hide();
             
           }
                
            },
            error:function(data){
              data = JSON.parse(data);
              alert(data);
            }
        });
    
     }else{
	 	   swal({
                  title: "Sólo puede adjuntar hasta tres(3) imagenes",
                  text:  "",
                  type: "warning",
                  confirmButtonColor: '#DD6B55',
                });
	 	}
	 
	}

		$(document).ready(function(){
			calcularPrestamoPaso5();
			$('#5_1_numero_cuenta').focus(function(){
				$('#5_1_info').collapse()
			});
			$('#5_1_1_nombre_cuenta').focus(function(){
				$('#5_1_1_info').collapse()
			});
			$('#5_4').click(function(){
				$('#5_4_info').collapse()
			});
			//Mostrar mensaje según el Banco
			$('#5_2').change(function() {
				if($(this).val()) {
					$('#mensaje_banco').parent().removeClass('d-none')
					$('#mensaje_banco').parent().addClass('d-block')
					var momento = new Date();
					var hora = momento.getHours();
					var minuto = momento.getMinutes();
					if($(this).val() == 2 || $(this).val() == 16 || $(this).val() == 37){
						$('#mensaje_banco').text('los próximos 30 minutos a su cuenta')
					} else {
						if(hora < 11 ){
							$('#mensaje_banco').text('el día de hoy, dependerá de su banco')
						} else if(hora == 11 && minuto < 30) {
							$('#mensaje_banco').text('el día de hoy, dependerá de su banco')
						} else {
							$('#mensaje_banco').text('la mañana del siguiente dia hábil')
						}
					}
				} else {
					$('#mensaje_banco').parent().removeClass('d-block');
					$('#mensaje_banco').parent().addClass('d-none');
				}
			});
			//Actualizar valor de inputs amount y duration
			$('#amPlus').click(calcularPrestamoPaso5);		
			$('#amMinus').click(calcularPrestamoPaso5);			
			$('#duPlus').click(calcularPrestamoPaso5);		
			$('#duMinus').click(calcularPrestamoPaso5);
		})


		function calcularPrestamoPaso5(){
			$('#solicitado').html( 'B./ ' + parseFloat($('#amount').val() ).toFixed(2));
			$('#pagas_en').html($('#duration').val() + ' Quincenas');
			var ajuste = $('#ajustarQuincenas').val();
			var tasa = $('#tasa').val();
			if(ajuste==1){
				var hoy = new Date();
				var quincenas = calcularQuincenas(new Date());
				var fFinal = new Date(quincenas[$('#duration').val() - 1 ]);
				var lapso = Math.ceil((fFinal - hoy)/(1000*60*60*24));
				var coste = (((lapso * tasa) * $('#amount').val()) ).toFixed(2);
				$('#total_dias').html(lapso + ' Días');
				$('#fecha_finalizacion').html(formatDate2(new Date(fFinal)));
				$('#interes').html('B./ ' + coste );
			}else{
				var coste = (((($('#duration').val() *15) * tasa) * $('#amount').val()) ).toFixed(2);
				$('#total_dias').html($('#duration').val()*15 + ' Días');
				$('#fecha_finalizacion').html(formatDate2(sumarDias(new Date(),$('#duration').val()*15) ));
				$('#interes').html('B./ ' + coste );
			}
			var total =  (parseFloat(coste) + parseFloat( $('#amount').val()) );
			var cuota = parseFloat(total / $('#duration').val() ).toFixed(2);
			$('#monto_total').val(total);
			$('#total_aplazado').val(cuota);
			$('#cuota').html( cuota + ' B./  Quincena' );
		}

         function calcularQuincenas(fecha) {
			var quincenas = [];
			var ultimoDia = new Date(fecha.getFullYear(), fecha.getMonth() + 1, 0);
			if(fecha.getDate() < 11) {
				quincenas.push(quincena(fecha, 0));
				quincenas.push(finDeMes(fecha, 0));
				quincenas.push(quincena(fecha, 1));
				quincenas.push(finDeMes(fecha, 1));
				quincenas.push(quincena(fecha, 2));
				quincenas.push(finDeMes(fecha, 2));
				quincenas.push(quincena(fecha, 3));
			} else if(fecha.getDate() < (ultimoDia.getDate()-4)) {
				quincenas.push(finDeMes(fecha, 0));
				quincenas.push(quincena(fecha, 1));
				quincenas.push(finDeMes(fecha, 1));
				quincenas.push(quincena(fecha, 2));
				quincenas.push(finDeMes(fecha, 2));
				quincenas.push(quincena(fecha, 3));
				quincenas.push(finDeMes(fecha, 3));
			} else {
				quincenas.push(quincena(fecha, 1));
				quincenas.push(finDeMes(fecha, 1));
				quincenas.push(quincena(fecha, 2));
				quincenas.push(finDeMes(fecha, 2));
				quincenas.push(quincena(fecha, 3));
				quincenas.push(finDeMes(fecha, 3));
				quincenas.push(quincena(fecha, 4));
			}
			return quincenas;
		}
		function quincena(fecha, numero) {
			return new Date(fecha.getFullYear(), fecha.getMonth() + numero, 15)
		}
		function finDeMes(fecha, numero) {
			return new Date(fecha.getFullYear(), fecha.getMonth() + numero + 1, 0)
		}
		function sumarDias(fecha, dias){
			fecha.setDate(fecha.getDate() + dias);
			return fecha;
		}
		function formatDate2(date) {
			var d = new Date(date),
			month = '' + (d.getMonth() + 1),
			day = '' + d.getDate(),
			year = d.getFullYear();
			var dias = new Array('Dom','Lun','Mar','Mié','Jue','Vie','Sáb')
			var dias = new Array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado')
			if (month.length < 2) month = '0' + month;
			switch(month){
				case '01':
				month = 'Enero';
				break;
				case '02':
				month = 'Febrero';
				break;
				case '03':
				month = 'Marzo';
				break;
				case '04':
				month = 'Abril';
				break;
				case '05':
				month = 'Mayo';
				break;
				case '06':
				month = 'Junio';
				break;
				case '07':
				month = 'Julio';
				break;
				case '08':
				month = 'Agosto';
				break;
				case '09':
				month = 'Septiembre';
				break;
				case '10':
				month = 'Octubre';
				break;
				case '11':
				month = 'Noviembre';
				break;
				case '12':
				month = 'Diciembre';
				break;
			}
			if (day.length < 2) day = '0' + day;

			//return [year, month, dias];
			return dias[date.getDay()] + ', ' +day+ ' de ' +month + ' de ' +year;
		}

			var quincenas = calcularQuincenas(new Date());
			var date = new Date();


	function mensajeErrorImagen(){
		/*swal({
                  title: "No se puede realizar esta operación en este momento intente más tarde",
                  text:  "",
                  type: "warning"
              });*/

              	swal({
             	title: 'Estamos procesando su información',
             	imageUrl: url+'img/loading.gif'
             });
	}

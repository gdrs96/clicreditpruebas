$(document).ready(function(){
    
    $('.dtpicker').datetimepicker({ pickTime: false });
    $('.select2').select2();
    $('.datepicker').datepicker({
        autoclose: true,
        format: "dd-mm-yyyy",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true, 
     
    });


  });

/******************************************************************************
 Función encargada de desplegar el formulario modal al seleccionar agregar quincena
******************************************************************************/
function actualizarPago(){
	  $('#btnSave').text('Guardando..'); //cambia el texto del botón guardar
    $('#btnSave').attr('disabled',true); //deshabilita el botón guardar
    $.ajax({
        url : urlActualizarPago,
        type: "POST",
        data: $('#formPago').serialize(),
        dataType: "JSON",
        success: function(data)
        {
        	console.log(data);
          
           if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_reportePago').modal('hide');
                 reload_table();
            }
          
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}




/******************************************************************************
 Función encargada de desplegar el formulario modal al seleccionar agregar quincena
******************************************************************************/
function agregarQuincena()
{
    save_method = 'add';
    $('#form')[0].reset(); // resetea el formulario modal

    $('#modal_form').modal('show'); // muestra el formulario modal

    $('.modal-title').text('Agregar quincena'); // Agrega un titulo al formulario modal
    var cantidad_quincena = $('[name="cantidad_quincenas"]').val();
    $('[name="numero_quincena"]').val(parseInt(cantidad_quincena)+1);
}




/******************************************************************************
 Función encargada de agregar una quincena
******************************************************************************/
var save_method;
function guardar_quincena()
{
	$('#btnSave').text('Guardando...'); //cambia el texto del boton
    $('#btnSave').attr('disabled',true); 
   $('[name="numero_quincena"]').removeAttr('disabled');
    var urlData;
 
    if(save_method == 'add') {
        urlData = urlAgregarQuincena;
    } else {
        urlData = urlActualizarQuincena;
    }

    $.ajax({
        url :urlData,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          
           if(data.status) //si la peticion retorna true ocultamos el modal y refrescamos el datatable
            {
                $('#modal_form').modal('hide');
                reload_tableQuincenas();
             
            }
          
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Guardar'); 
            $('#btnSave').attr('disabled',false); 
 
        }
    });

}

/******************************************************************************
 Muestra las quincenas asociadas a un prestamo a traves del datatable 
******************************************************************************/
var id_prestamo = $("#id_prestamo_cliente").val();
var tableQuincenas;
tableQuincenas = $('#table_quincenas').DataTable({
            	      "bPaginate": false,
            	      "bLengthChange": false,
            	      "bSearch": false,
            	      "bFilter": false,
            	      "bInfo": false,
                    "bAutoWidth": false,
                    "processing": true, 
                    "serverSide": true,
                    "footerCallback": function ( row, data, start, end, display ) {
                     
                        var api = this.api(), data;
                        var intVal = function ( i ) {
                           return typeof i === 'string' ?
                           i.replace(/[\$,]/g, '')*1 :
                           typeof i === 'number' ?
                           i : 0;
                         };

                        total = api.column(2) .data().reduce( function (a, b) {
                        
                         return intVal(a) + intVal(b);
                      }, 0 ),
                      
                      $( api.column(3).footer() ).html( currencyFormat (total))
                    },
                  
                   ajax:{
                        url: urlDetallePrestamo,
                        type:'post',
                        data: function ( d ) {
                          //return  $.extend(d, myData);
                        }
                      } ,
                  columns: [
                  { data: null,
                   "render": function ( data, type, full, meta) {
                    return '<span class="label" style="background:#'+data.color+';">'+data.numero_quincena+'</span>';}},
                  {data:'fecha_vencimiento'},
                  {data:'cuota'},
                  {data: null,
                  "render": function ( data, type, full, meta) {
                   return '<span class="label spanStatus" style="background:#'+data.color+';  width:100%;">'+data.nombre+'</span>';}},
                   //return '<FONT COLOR="#'+data.color+';">'+data.nombre+'</FONT>';}},
                  {data:'action'}
                 ],

                 "columnDefs": [
                 {
                   "targets": [4],
                   "data": null,
                   "defaultContent": '<a class="btn btn-primary" id="editar_quincena"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar quincena"></i></a> '
                  //"visible": false
                 }
                ],
               "order": [[ 1, "desc" ]]
             });

/******************************************************************************
 Muestra el detalle de la quincena a traves del formulario modal
******************************************************************************/
$('#table_quincenas tbody').on( 'click', '#editar_quincena', function () {
        var data = tableQuincenas.row( $(this).parents('tr') ).data();
            $('[name="id"]').val(data.id); //id del detalle de prestamo
            $('[name="numero_quincena"]').val(data.numero_quincena); //numero de quincena
            $('[name="fecha_vencimiento"]').val(data.fecha_vencimiento); //fecha de vencimiento
            $('[name="estado_quincena"]').val(data.quincena_estado); // estado de la quincena
            $('[name="importe"]').val(data.cuota); //importe
            $('[name="id_prestamo"]').val(data.id_prestamo); // id del prestamo asociado
            $('#modal_form').modal('show'); // desplegamos el formulario modal
            $('.modal-title').text('Editar quincena'); // asignamos el titulo 
    });




/******************************************************************************
 Muestra los pagos asociados a un prestamo a traves del datatable 
******************************************************************************/
var table;
 table = $('#tableReportePago').DataTable({
            	"bPaginate": false,
            	"bLengthChange": false,
            	"bSearch": false,
            	"bFilter": false,
            	"bInfo": false,
                "bAutoWidth": false,
                "processing": true, 
                "serverSide": true,

     "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;

             var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
                        //return  formatMoney(i);
            };

             total = api
                .column(2)
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 ),

          $( api.column(2).footer() ).html( currencyFormat (total))
            


        },
       

       ajax:{
        url: urlReportePago,
        type:'post',
        data: function ( d ) {
           //return  $.extend(d, myData);
        }
    } ,
   
    columns: [
       
        {data:'fecha_subida_recibo'},
        {data:'fecha_efecto'},
        {data:'importe'},
        {data: null,
          "render": function ( data, type, full, meta) {
           return '<span class="label spanStatus" style="background:#'+data.color+'; width:100%;">'+data.nombre+'</span>';}},
        {data:'action'},
        ],

     "columnDefs": [
        {
            "targets": [4],
            "data": null,
            "defaultContent":'<a class="btn btn-primary" id="editar_pago"><i class="fa fa-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Editar pago"></i></a> '
          
        }
    ],

      "order": [[ 1, "desc" ]]

});

/******************************************************************************
 Muestra la vista modal para editar un pago (accion ver/editar pago)
******************************************************************************/
 $('#tableReportePago tbody').on( 'click', '#editar_pago', function () {
        var data = table.row( $(this).parents('tr') ).data();
           var ruta = url+data.imagen_recibo;
           $('.imagepreview').attr('src',ruta);
           $('#importe').val(data.importe);
           $('#id_pago').val(data.id);
           $('#fecha_efecto').val(data.fecha_efecto);
           $('#idCliente').val(idCliente);
           $('select[name=selectStatusReporte]').val(data.id_reporte_pago_status);
           $('.selectpicker').selectpicker('refresh');
           $("#expandir").click(function(event) {
                $('#urlImagen').attr('href',ruta);
                $('.imageExpand').attr('src',ruta);
                  $('#modal_reciboPago').modal('show');
           });
            $("#descargar").click(function(event) {
                $('#imagenDownload').attr('href',ruta);
              
           });

             $("#eliminar_pago").click(function(event) {
               eliminarPago(data);
              
           });

             $('#modal_reportePago').modal('show');
          
    });


/******************************************************************************
 Muestra la vista modal para visualizar un recibo de pago (accion ver recibo)
******************************************************************************/
 $('#tableReportePago tbody').on( 'click', '#ver_recibo', function () {
        var data = table.row( $(this).parents('tr') ).data();
           var ruta = url+data.imagen_recibo;
           $('.imagepreview').attr('src',ruta);
           $('#urlImagen').attr('href',ruta);
       
           
          $('#modal_reciboPago').modal('show');

          
    });

/******************************************************************************
 Acción para eliminar un pago
******************************************************************************/

 // $('#tableReportePago tbody').on( 'click', '#eliminar_pago', function () {
  function eliminarPago(data){
      
        
         swal({
                title: "¿Deseas eliminar este pago?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Si',
                cancelButtonText: "No",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm) {

             
                if (isConfirm) {
                   $.ajax({
                    url : urlEliminarPago,
                    type: "POST",
                    data: {'id_pago': data.id},
                   dataType: "JSON",
                   success: function(data)
                   {
                   	
                      if(data.status) //if success close modal and reload ajax table
                       {


                       	swal({
                        title: 'Pago eliminado!',
                        text: '',
                        type: 'success'
                          }, function() {
                              reload_table();
                              $('#modal_reportePago').modal('hide');
                          });
                       }else{


                       swal("El pago no pudo ser eliminado intente más tarde", "", "error");

                       }
           },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
         
 
        }
    });
                    
                    
                } else {
                    swal("Acción cancelada", "", "warning");
                }
            });
       }
    //});






       

/******************************************************************************
 función para refrescar el datatable  del reporte de pagos
******************************************************************************/
function reload_table()
{
    table.ajax.reload(null,false); 
}

/******************************************************************************
 función para refrescar el datatable  de las quincenas
******************************************************************************/
function reload_tableQuincenas()
{
    tableQuincenas.ajax.reload(null,false); 
}


/******************************************************************************
 función para reportar un pago de lado del administrador
******************************************************************************/
	 function reportarPago(){

	 	var imagen_recibo = $('#imagen_recibo')[0].files[0];
	 	data= {'imagen_recibo': imagen_recibo};
       
	 	
	 	 $.ajax({
            url: urlReportarPago,
            type: "POST",
            dataType : 'json',
            data: new FormData($("#upload_form")[0]),
            processData: false,
            contentType: false,
 
            success: function (data) {
            
             if(data.status==true){
               swal({
                title: "Pago registrado con éxito",
                text:  "",
                type: "success"
            });
               reload_table();

               }
               else{

               	 swal({
                  title: "Formato de imagen invalido",
                  text:  "",
                  type: "warning"
            });

               }
            },
             error: function (jqXHR, textStatus, errorThrown)
             {
            alert('Error get data from ajax');
             }
           
        });
	 }


   function enviarPago(){
     $('#btnSave').text('Guardando..'); //cambia el texto del botón guardar
     $('#btnSave').attr('disabled',true); //deshabilita el botón guardar
    $.ajax({
        url : urlEnviarPago,
        type: "POST",
        data: $('#formPago').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          console.log(data);
          
          if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_reportePago').modal('hide');
                 reload_table();
            }
          
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
   }


   /******************************************************************************
 Función encargada de desplegar el formulario modal al seleccionar agregar quincena
******************************************************************************/
function actualizarDocumentacion(){
    $('#btnSave').text('Guardando..'); //cambia el texto del botón guardar
    $('#btnSave').attr('disabled',true); //deshabilita el botón guardar
    $.ajax({
        url : urlActualizarPago,
        type: "POST",
        data: $('#formPago').serialize(),
        dataType: "JSON",
        success: function(data)
        {
          console.log(data);
          
           if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_reportePago').modal('hide');
                 reload_table();
            }
          
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave').text('Guardar'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}






 /*$.ajax({
        url: urlStatusReportePago,
        //data:data,
        type: 'post',
        dataType:'json'
    })

    .done(function(data) {
       //data = $.parseJSON(data);
       console.log(data);
       $.each(data.data, function(k,v){
         
            $("#selectStatusReporte").append("<option value=\""+v.id +"\">"+v.nombre+"</option>");
        });

   })

   .fail(function(data) {
        console.log('error'+data);
    });*/




 


